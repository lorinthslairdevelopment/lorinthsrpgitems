package com.gmail.nossr50.skills.repair;

import com.gmail.nossr50.config.AdvancedConfig;
import com.gmail.nossr50.config.Config;
import com.gmail.nossr50.config.experience.ExperienceConfig;
import com.gmail.nossr50.datatypes.experience.XPGainReason;
import com.gmail.nossr50.datatypes.interactions.NotificationType;
import com.gmail.nossr50.datatypes.player.McMMOPlayer;
import com.gmail.nossr50.datatypes.skills.PrimarySkillType;
import com.gmail.nossr50.datatypes.skills.SubSkillType;
import com.gmail.nossr50.locale.LocaleLoader;
import com.gmail.nossr50.mcMMO;
import com.gmail.nossr50.skills.SkillManager;
import com.gmail.nossr50.skills.repair.repairables.Repairable;
import com.gmail.nossr50.util.EventUtils;
import com.gmail.nossr50.util.Permissions;
import com.gmail.nossr50.util.StringUtils;
import com.gmail.nossr50.util.player.NotificationManager;
import com.gmail.nossr50.util.random.RandomChanceSkillStatic;
import com.gmail.nossr50.util.random.RandomChanceUtil;
import com.gmail.nossr50.util.skills.RankUtils;
import com.gmail.nossr50.util.skills.SkillActivationType;
import com.gmail.nossr50.util.skills.SkillUtils;
import com.gmail.nossr50.util.sounds.SoundManager;
import com.gmail.nossr50.util.sounds.SoundType;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.permissions.Permissible;

import java.util.Map;

public class RpgItemsRepairManager
        extends SkillManager
{
    private boolean placedAnvil;
    private int lastClick;

    public RpgItemsRepairManager(McMMOPlayer mcMMOPlayer)
    {
        super(mcMMOPlayer, PrimarySkillType.REPAIR);
    }

    public void placedAnvilCheck() {
        Player player = getPlayer();
        if (getPlacedAnvil())
            return;
        if (Config.getInstance().getRepairAnvilMessagesEnabled())
            NotificationManager.sendPlayerInformation(player, NotificationType.SUBSKILL_MESSAGE, "Repair.Listener.Anvil");
        if (Config.getInstance().getRepairAnvilPlaceSoundsEnabled())
            SoundManager.sendSound(player, player.getLocation(), SoundType.ANVIL);
        togglePlacedAnvil();
    }

    public void handleRepair(ItemStack item) {
        Player player = getPlayer();
        Repairable repairable = mcMMO.getRepairableManager().getRepairable(item.getType());
        if (item.getItemMeta().isUnbreakable()) {
            NotificationManager.sendPlayerInformation(player, NotificationType.SUBSKILL_MESSAGE_FAILED, "Anvil.Unbreakable");
            return;
        }
        if (!Permissions.repairMaterialType(player, repairable.getRepairMaterialType())) {
            NotificationManager.sendPlayerInformation(player, NotificationType.NO_PERMISSION, "mcMMO.NoPermission");
            return;
        }
        if (!Permissions.repairItemType(player, repairable.getRepairItemType())) {
            NotificationManager.sendPlayerInformation(player, NotificationType.NO_PERMISSION, "mcMMO.NoPermission");
            return;
        }
        int skillLevel = getSkillLevel();
        int minimumRepairableLevel = repairable.getMinimumLevel();
        if (skillLevel < minimumRepairableLevel) {
            NotificationManager.sendPlayerInformation(player, NotificationType.SUBSKILL_MESSAGE_FAILED, "Repair.Skills.Adept", String.valueOf(minimumRepairableLevel), StringUtils.getPrettyItemString(item.getType()));
            return;
        }
        PlayerInventory inventory = player.getInventory();
        Material repairMaterial = repairable.getRepairMaterial();
        ItemStack toRemove = new ItemStack(repairMaterial);
        short startDurability = item.getDurability();
        if (startDurability <= 0) {
            NotificationManager.sendPlayerInformation(player, NotificationType.SUBSKILL_MESSAGE_FAILED, "Repair.Skills.FullDurability");
            return;
        }
        if (!inventory.contains(repairMaterial)) {
            String prettyName = (repairable.getRepairMaterialPrettyName() == null) ? StringUtils.getPrettyItemString(repairMaterial) : repairable.getRepairMaterialPrettyName();
            String materialsNeeded = "";
            NotificationManager.sendPlayerInformation(player, NotificationType.SUBSKILL_MESSAGE_FAILED, "Skills.NeedMore.Extra", prettyName, materialsNeeded);
            return;
        }
        if (item.getAmount() != 1) {
            NotificationManager.sendPlayerInformation(player, NotificationType.SUBSKILL_MESSAGE_FAILED, "Repair.Skills.StackedItems");
            return;
        }
        SkillUtils.removeAbilityBuff(item);
        int baseRepairAmount = repairable.getBaseRepairDurability(item);
        short newDurability = repairCalculate(startDurability, baseRepairAmount);
        if (EventUtils.callRepairCheckEvent(player, (short)(startDurability - newDurability), toRemove, item).isCancelled())
            return;
        if (ArcaneForging.arcaneForgingEnchantLoss && !Permissions.hasRepairEnchantBypassPerk(player))
            addEnchants(item);
        toRemove = inventory.getItem(inventory.first(repairMaterial)).clone();
        toRemove.setAmount(1);
        inventory.removeItem(toRemove);
        applyXpGain(

                (float)(getPercentageRepaired(newDurability-startDurability, repairable.getMaximumDurability()) * repairable.getXpMultiplier() * ExperienceConfig.getInstance().getRepairXPBase() * ExperienceConfig.getInstance().getRepairXP(repairable.getRepairMaterialType())), XPGainReason.PVE);
        if (Config.getInstance().getRepairAnvilUseSoundsEnabled()) {
            SoundManager.sendSound(player, player.getLocation(), SoundType.ANVIL);
            SoundManager.sendSound(player, player.getLocation(), SoundType.ITEM_BREAK);
        }
        item.setDurability(newDurability);
    }


    private double getPercentageRepaired(double durabilityAdded, double totalDurability)
    {
        return durabilityAdded / totalDurability;
    }

    public boolean checkConfirmation(boolean actualize)
    {
        Player player = getPlayer();
        long lastUse = getLastAnvilUse();
        if ((!SkillUtils.cooldownExpired(lastUse, 3)) || (!Config.getInstance().getRepairConfirmRequired())) {
            return true;
        }
        if (!actualize) {
            return false;
        }
        actualizeLastAnvilUse();
        player.sendMessage(LocaleLoader.getString("Skills.ConfirmOrCancel", LocaleLoader.getString("Repair.Pretty.Name")));

        return false;
    }

    public int getArcaneForgingRank() {
        return RankUtils.getRank(getPlayer(), SubSkillType.REPAIR_ARCANE_FORGING);
    }

    public double getKeepEnchantChance() {
        return AdvancedConfig.getInstance().getArcaneForgingKeepEnchantsChance(getArcaneForgingRank());
    }

    public double getDowngradeEnchantChance() {
        return AdvancedConfig.getInstance().getArcaneForgingDowngradeChance(getArcaneForgingRank());
    }

    private short repairCalculate(short durability, int repairAmount) {
        Player player = getPlayer();
        if (Permissions.isSubSkillEnabled(player, SubSkillType.REPAIR_REPAIR_MASTERY) &&
                RankUtils.hasUnlockedSubskill(getPlayer(), SubSkillType.REPAIR_REPAIR_MASTERY)) {
            double maxBonusCalc = Repair.repairMasteryMaxBonus / 100.0D;
            double skillLevelBonusCalc = Repair.repairMasteryMaxBonus / Repair.repairMasteryMaxBonusLevel * getSkillLevel() / 100.0D;
            double bonus = repairAmount * Math.min(skillLevelBonusCalc, maxBonusCalc);
            repairAmount = (int)(repairAmount + bonus);
        }
        if (Permissions.isSubSkillEnabled(player, SubSkillType.REPAIR_SUPER_REPAIR) && checkPlayerProcRepair())
            repairAmount = (int)(repairAmount * 2.0D);
        if (repairAmount <= 0 || repairAmount > 32767)
            repairAmount = 32767;
        return (short)Math.max(durability - repairAmount, 0);
    }

    private boolean checkPlayerProcRepair() {
        if (!RankUtils.hasUnlockedSubskill(getPlayer(), SubSkillType.REPAIR_SUPER_REPAIR))
            return false;
        if (RandomChanceUtil.isActivationSuccessful(SkillActivationType.RANDOM_LINEAR_100_SCALE_WITH_CAP, SubSkillType.REPAIR_SUPER_REPAIR, getPlayer())) {
            NotificationManager.sendPlayerInformation(getPlayer(), NotificationType.SUBSKILL_MESSAGE, "Repair.Skills.FeltEasy");
            return true;
        }
        return false;
    }

    private void addEnchants(ItemStack item) {
        Player player = getPlayer();
        Map<Enchantment, Integer> enchants = item.getEnchantments();
        if (enchants.isEmpty())
            return;
        if (Permissions.arcaneBypass(player)) {
            NotificationManager.sendPlayerInformation(getPlayer(), NotificationType.SUBSKILL_MESSAGE, "Repair.Arcane.Perfect");
            return;
        }
        if (getArcaneForgingRank() == 0 || !Permissions.isSubSkillEnabled(player, SubSkillType.REPAIR_ARCANE_FORGING)) {
            for (Enchantment enchant : enchants.keySet())
                item.removeEnchantment(enchant);
            NotificationManager.sendPlayerInformation(getPlayer(), NotificationType.SUBSKILL_MESSAGE_FAILED, "Repair.Arcane.Lost");
            return;
        }
        boolean downgraded = false;
        for (Map.Entry<Enchantment, Integer> enchant : enchants.entrySet()) {
            int enchantLevel = enchant.getValue().intValue();
            if (!ExperienceConfig.getInstance().allowUnsafeEnchantments() &&
                    enchantLevel > enchant.getKey().getMaxLevel()) {
                enchantLevel = enchant.getKey().getMaxLevel();
                item.addEnchantment(enchant.getKey(), enchantLevel);
            }
            Enchantment enchantment = enchant.getKey();
            if (RandomChanceUtil.checkRandomChanceExecutionSuccess(new RandomChanceSkillStatic(getKeepEnchantChance(), getPlayer(), SubSkillType.REPAIR_ARCANE_FORGING))) {
                if (ArcaneForging.arcaneForgingDowngrades && enchantLevel > 1 &&
                        !RandomChanceUtil.checkRandomChanceExecutionSuccess(new RandomChanceSkillStatic(100.0D - getDowngradeEnchantChance(), getPlayer(), SubSkillType.REPAIR_ARCANE_FORGING))) {
                    item.addUnsafeEnchantment(enchantment, enchantLevel - 1);
                    downgraded = true;
                }
                continue;
            }
            item.removeEnchantment(enchantment);
        }
        Map<Enchantment, Integer> newEnchants = item.getEnchantments();
        if (newEnchants.isEmpty()) {
            NotificationManager.sendPlayerInformationChatOnly(getPlayer(), "Repair.Arcane.Fail");
        } else if (downgraded || newEnchants.size() < enchants.size()) {
            NotificationManager.sendPlayerInformationChatOnly(getPlayer(), "Repair.Arcane.Downgrade");
        } else {
            NotificationManager.sendPlayerInformationChatOnly(getPlayer(), "Repair.Arcane.Perfect");
        }
    }

    public boolean getPlacedAnvil() {
        return this.placedAnvil;
    }

    public void togglePlacedAnvil() {
        this.placedAnvil = !this.placedAnvil;
    }

    public int getLastAnvilUse() {
        return this.lastClick;
    }

    public void setLastAnvilUse(int value) {
        this.lastClick = value;
    }

    public void actualizeLastAnvilUse() {
        this.lastClick = (int)(System.currentTimeMillis() / 1000L);
    }
}
