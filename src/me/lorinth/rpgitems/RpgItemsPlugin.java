package me.lorinth.rpgitems;

import me.lorinth.rpgitems.commands.LorinthsRpgItemsCommand;
import me.lorinth.rpgitems.enums.ItemStatsView;
import me.lorinth.rpgitems.enums.LevelHook;
import me.lorinth.rpgitems.enums.MonsterLevelHook;
import me.lorinth.rpgitems.enums.ShowDroppedItemNames;
import me.lorinth.rpgitems.listener.*;
import me.lorinth.rpgitems.managers.*;
import me.lorinth.rpgitems.objects.placeholder.RpgItemsPlaceholder;
import me.lorinth.rpgitems.tasks.AutoSaveTask;
import me.lorinth.rpgitems.util.*;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.FormulaMethod;
import me.lorinth.utils.ResourceHelper;
import me.lorinth.utils.TryParse;
import me.lorinth.utils.javascript.JavascriptEngine;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class RpgItemsPlugin extends JavaPlugin {

    public static RpgItemsPlugin instance;
    private static int autoSaveInterval;
    private static BukkitTask autoSaveTask;

    @Override
    public void onEnable(){
        instance = this;
        RpgItemsOutputHandler rpgItemsOutputHandler = new RpgItemsOutputHandler();
        firstLoad();
        loadConfig();
        loadCommands();
        loadManagers();
        setupListeners();
        runAutoSave();
    }

    @Override
    public void onDisable(){
        if(autoSaveTask != null) {
            autoSaveTask.cancel();
        }
        PlayerCharacterManager.saveSynchronously();
        CombatManager.instance.save();
        EnhancementManager.onDisable();
        RepairManager.onDisable();

        HandlerList.unregisterAll(this);
    }

    public static void Reload(){
        instance.onDisable();
        instance.onEnable();
    }

    private void firstLoad(){
        try{
            ResourceHelper.copy(getResource("resources/attributes.yml"), new File(getDataFolder(), "attributes.yml"));
            ResourceHelper.copy(getResource("resources/classes.yml"), new File(getDataFolder(), "classes.yml"));
            ResourceHelper.copy(getResource("resources/combat.yml"), new File(getDataFolder(), "combat.yml"));
            ResourceHelper.copy(getResource("resources/config.yml"), new File(getDataFolder(), "config.yml"));
            ResourceHelper.copy(getResource("resources/consumables.yml"), new File(getDataFolder(), "consumables.yml"));
            ResourceHelper.copy(getResource("resources/display.yml"), new File(getDataFolder(), "display.yml"));
            ResourceHelper.copy(getResource("resources/durability.yml"), new File(getDataFolder(), "durability.yml"));
            ResourceHelper.copy(getResource("resources/effects.yml"), new File(getDataFolder(), "effects.yml"));
            ResourceHelper.copy(getResource("resources/enhancement.yml"), new File(getDataFolder(), "enhancement.yml"));
            ResourceHelper.copy(getResource("resources/experience.yml"), new File(getDataFolder(), "experience.yml"));
            ResourceHelper.copy(getResource("resources/formulas.yml"), new File(getDataFolder(), "formulas.yml"));
            ResourceHelper.copy(getResource("resources/gearsets.yml"), new File(getDataFolder(), "gearsets.yml"));
            ResourceHelper.copy(getResource("resources/hooks.yml"), new File(getDataFolder(), "hooks.yml"));
            ResourceHelper.copy(getResource("resources/itemgeneration.yml"), new File(getDataFolder(), "itemgeneration.yml"));
            ResourceHelper.copy(getResource("resources/leather.yml"), new File(getDataFolder(), "leather.yml"));
            ResourceHelper.copy(getResource("resources/loot.yml"), new File(getDataFolder(), "loot.yml"));
            ResourceHelper.copy(getResource("resources/names.yml"), new File(getDataFolder(), "names.yml"));
            ResourceHelper.copy(getResource("resources/materials.yml"), new File(getDataFolder(), "materials.yml"));
            ResourceHelper.copy(getResource("resources/messages.yml"), new File(getDataFolder(), "messages.yml"));
            ResourceHelper.copy(getResource("resources/modifications.yml"), new File(getDataFolder(), "modifications.yml"));
            ResourceHelper.copy(getResource("resources/rarity.yml"), new File(getDataFolder(), "rarity.yml"));
            ResourceHelper.copy(getResource("resources/repair.yml"), new File(getDataFolder(), "repair.yml"));
            ResourceHelper.copy(getResource("resources/sell.yml"), new File(getDataFolder(), "sell.yml"));
            ResourceHelper.copy(getResource("resources/sockets.yml"), new File(getDataFolder(), "sockets.yml"));
            ResourceHelper.copy(getResource("resources/base.js"), new File(getDataFolder() + File.separator + "Javascript", "base.js"));
        }
        catch(Exception exc){
            exc.printStackTrace();
        }
    }

    private void setupListeners(){
        PluginManager manager = Bukkit.getPluginManager();

        manager.registerEvents(new ArmorListener(), this);
        manager.registerEvents(new CombatListener(), this);
        manager.registerEvents(new ConsumableListener(), this);
        manager.registerEvents(new ItemEquipListener(), this);
        manager.registerEvents(new ItemGenerationListener(), this);
        manager.registerEvents(new ItemGenerationTagListener(), this);
        manager.registerEvents(new ItemPickupListener(), this);
        manager.registerEvents(new PlayerListener(), this);
        manager.registerEvents(new PlayerRpgListener(), this);
        manager.registerEvents(new LorinthsRpgMobsListener(), this);

        PluginHelper.DungeonsXLEnabled = manager.getPlugin("DungeonsXL") != null;
        PluginHelper.HeroesEnabled = manager.getPlugin("Heroes") != null;
        PluginHelper.McMMoEnabled = manager.getPlugin("mcMMO") != null;
        PluginHelper.MMOCoreEnabled = manager.getPlugin("MMOCore") != null;
        PluginHelper.MythicDropsEnabled = manager.getPlugin("MythicDrops") != null;
        PluginHelper.MythicMobsEnabled = manager.getPlugin("MythicMobs") != null;
        PluginHelper.RPGInventoryEnabled = manager.getPlugin("RPGInventory") != null;
        PluginHelper.PlaceholderAPIEnabled = manager.getPlugin("PlaceholderAPI") != null;
        PluginHelper.SkillAPIEnabled = manager.getPlugin("SkillAPI") != null;

        YamlConfiguration config = new YamlConfiguration();
        try{
            config.load(new File(getDataFolder(), "hooks.yml"));

            if(LevelHelper.levelHook == LevelHook.BuiltIn)
                manager.registerEvents(new ExperienceListener(), this);
            tryLoadDungeonsXL(config);
            if(PluginHelper.HeroesEnabled && config.getBoolean("Heroes"))
                manager.registerEvents(new HeroesListener(), this);
            if(PluginHelper.McMMoEnabled && config.getBoolean("McMMo"))
                manager.registerEvents(new McmmoRepairListener(), this);
            if(PluginHelper.MythicDropsEnabled && config.getBoolean("MythicDrops"))
                manager.registerEvents(new MythicDropsListener(), this);
            if(PluginHelper.MythicMobsEnabled && config.getBoolean("MythicMobs"))
                manager.registerEvents(new MythicMobsListener(), this);
            if(PluginHelper.RPGInventoryEnabled && config.getBoolean("RpgInventory"))
                manager.registerEvents(new RpgInventoryListener(), this);
            if(PluginHelper.PlaceholderAPIEnabled && config.getBoolean("PlaceholderAPI"))
                new RpgItemsPlaceholder().register();

            PluginHelper.MMOCoreEnabled = PluginHelper.MMOCoreEnabled && config.getBoolean("MMOCore");
            PluginHelper.SkillAPIEnabled = PluginHelper.SkillAPIEnabled && config.getBoolean("SkillAPI");

        } catch(Exception e){
            RpgItemsOutputHandler.PrintException("Error handling hooks.yml", e);
        }

        loadItemDropDisplay();
    }

    private void tryLoadDungeonsXL(FileConfiguration config){
        DungeonsXLManager.isEnabled = PluginHelper.DungeonsXLEnabled && config.getBoolean("DungeonsXL");
    }

    private void loadItemDropDisplay(){
        FileConfiguration config = getFileConfig("display.yml");
        String showDroppedItemNames = config.getString("ShowDroppedItemNames");
        ShowDroppedItemNames result = ShowDroppedItemNames.Named;
        if(TryParse.parseEnum(ShowDroppedItemNames.class, showDroppedItemNames))
            result = ShowDroppedItemNames.valueOf(showDroppedItemNames);

        if(result != ShowDroppedItemNames.None)
            Bukkit.getPluginManager().registerEvents(new ItemDropDisplayListener(result, config.getString("ItemDroppedNameLocale")), this);

        SettingsHelper.itemGlow = config.getBoolean("ItemGlow");
    }

    private void loadCommands(){
        getCommand("lri").setExecutor(new LorinthsRpgItemsCommand());
    }

    private void loadConfig(){
        new ItemGenerationManager(getFileConfig("itemgeneration.yml"));
        loadHooks();
        new AttributeManager(getFileConfig("attributes.yml"));
        new ClassManager(getFileConfig("classes.yml"));
        new CombatManager(getFileConfig("combat.yml"));
        new ConsumableManager(getFileConfig("consumables.yml"));
        new DurabilityManager(getFileConfig("durability.yml"));
        new EnhancementManager(getFileConfig("enhancement.yml"));
        new GearSetsManager(getFileConfig("gearsets.yml"));
        new LeatherManager(getFileConfig("leather.yml"));
        new LootManager(getFileConfig("loot.yml"));
        new RarityManager(getFileConfig("rarity.yml"));
        new MaterialTierManager(getFileConfig("materials.yml"));
        new MessageHelper(this);
        new RepairManager(getFileConfig("repair.yml"));
        new SellManager(getFileConfig("sell.yml"));
        new SocketManager(getFileConfig("sockets.yml"));
        loadStaticHelper(DisplayHelper.class, getFileConfig("display.yml"), true);
        loadStaticHelper(FormulaHelper.class, getFileConfig("formulas.yml"), false);
        loadStaticHelper(NameHelper.class, getFileConfig("names.yml"), false);

        if(SettingsHelper.formulaMethod == FormulaMethod.Javascript) {
            new JavascriptEngine(RpgItemsPlugin.instance.getClass(), getDataFolder() + File.separator + "Javascript", RpgItemsOutputHandler.getInstance());
        }
    }

    private void loadHooks(){
        FileConfiguration config = getFileConfig("config.yml");
        String levelHook = config.getString("LevelHook");
        if(TryParse.parseEnum(LevelHook.class, levelHook))
            LevelHelper.levelHook = LevelHook.valueOf(levelHook);
        String monsterLevelHook = config.getString("MonsterLevelHook");
        if(TryParse.parseEnum(MonsterLevelHook.class, monsterLevelHook))
            LevelHelper.monsterLevelHook = MonsterLevelHook.valueOf(monsterLevelHook);

        String itemStatsView = config.getString("ItemStatsView");
        if(TryParse.parseEnum(ItemStatsView.class, itemStatsView))
            SettingsHelper.itemStatsView = ItemStatsView.valueOf(itemStatsView);

        String formulaMethod = config.getString("FormulaMethod");
        if(TryParse.parseEnum(FormulaMethod.class, formulaMethod))
            SettingsHelper.formulaMethod = FormulaMethod.valueOf(formulaMethod);

        if(ConfigHelper.ConfigContainsPath(config, "AutoSaveInterval")) {
            autoSaveInterval = config.getInt("AutoSaveInterval");
        }
        else{
            autoSaveInterval = 5;
        }
    }

    private FileConfiguration getFileConfig(String fileName){
        YamlConfiguration yml = new YamlConfiguration();
        try {
            yml.load(new File(getDataFolder(), fileName));
        }
        catch(Exception e){
            RpgItemsOutputHandler.PrintException("Error loading, " + fileName, e);
        }
        return yml;
    }

    private void loadManagers(){
        new PlayerCharacterManager();
        new EffectManager(getFileConfig("effects.yml"));
        new ExperienceManager(getFileConfig("experience.yml"));
        new ModificationManager(getFileConfig("modifications.yml"));
    }

    private void loadStaticHelper(Class clazz, FileConfiguration config, boolean allowColorCodes){
        for(Field field : clazz.getFields()){
            field.setAccessible(true);
            try{
                String name = field.getName();
                Object value = config.get(name.replace("_", "."));
                if(value instanceof String && allowColorCodes)
                    value = ChatColor.translateAlternateColorCodes('&', (String) value);
                if(value != null)
                    field.set(clazz, value);
            }
            catch(Exception excep){
                excep.printStackTrace();
            }
        }
        try{
            Method loadMethod = clazz.getMethod("load", FileConfiguration.class);
            if(loadMethod != null){
                loadMethod.invoke(null, config);
            }
        } catch(NoSuchMethodException e){

        } catch(Exception e){
            e.printStackTrace();
        }
    }

    private void runAutoSave(){
        if(autoSaveInterval > 0){
            RpgItemsOutputHandler.PrintInfo("Running an autosave every " + autoSaveInterval + " minutes! (config.yml (AutoSaveInterval))");
            autoSaveTask = new AutoSaveTask().runTaskTimer(RpgItemsPlugin.instance, 20 * 60 * autoSaveInterval, 20 * 60 * autoSaveInterval);
        }
    }

}
