package me.lorinth.rpgitems.commands;

import me.lorinth.rpgitems.commands.classes.ClassGuiCommand;
import me.lorinth.rpgitems.commands.classes.ClassListCommand;
import me.lorinth.rpgitems.commands.classes.ClassSetCommand;
import me.lorinth.rpgitems.managers.ClassManager;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Arrays;

public class ClassCommand {

    public static void executeCommand(CommandSender commandSender, String[] args){
        if(args.length < 1){
            RpgItemsOutputHandler.PrintCommand(commandSender, ChatColor.AQUA + "/lri class set <player> <classId> [level] " + RpgItemsOutputHandler.HIGHLIGHT + "- sets the target players class/level");
            return;
        }

        if(!ClassManager.isEnabled()){
            RpgItemsOutputHandler.PrintError(commandSender, "Class System is disabled... Enable it to use this command");
            return;
        }

        String action = args[0];
        if(action.equalsIgnoreCase("set")){
            ClassSetCommand.executeCommand(commandSender, Arrays.copyOfRange(args, 1, args.length));
            return;
        }
        else if(action.equalsIgnoreCase("list")){
            ClassListCommand.executeCommand(commandSender, Arrays.copyOfRange(args, 1, args.length));
            return;
        }
        else if(action.equalsIgnoreCase("gui")){
            ClassGuiCommand.executeCommand(commandSender, Arrays.copyOfRange(args, 1, args.length));
            return;
        }
    }

}
