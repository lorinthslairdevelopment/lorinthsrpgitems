package me.lorinth.rpgitems.commands;

import me.lorinth.rpgitems.commands.consumable.ConsumableGiveCommand;
import me.lorinth.rpgitems.commands.consumable.ConsumableListCommand;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Arrays;

public class ConsumableCommand {

    public static void executeCommand(CommandSender commandSender, String[] args){
        if(args.length < 1){
            RpgItemsOutputHandler.PrintRawInfo(commandSender, ChatColor.AQUA + "/lri consumable give <player> <id> [amount] " + RpgItemsOutputHandler.HIGHLIGHT + "- gives a player a number of the consumable with a given id");
            RpgItemsOutputHandler.PrintRawInfo(commandSender, ChatColor.AQUA + "/lri consumable list " + RpgItemsOutputHandler.HIGHLIGHT + "- lists all consumable ids");
            return;
        }

        String action = args[0];
        if(action.equalsIgnoreCase("list")){
            ConsumableListCommand.executeCommand(commandSender, Arrays.copyOfRange(args, 1, args.length));
        }
        else if(action.equalsIgnoreCase("give")){
            ConsumableGiveCommand.executeCommand(commandSender, Arrays.copyOfRange(args, 1, args.length));
        }
    }


}
