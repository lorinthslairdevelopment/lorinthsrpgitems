package me.lorinth.rpgitems.commands;

import me.lorinth.rpgitems.listener.DeathEventDebugger;
import me.lorinth.rpgitems.managers.LootManager;
import me.lorinth.rpgitems.objects.DeathEventLootInfo;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class DebugCommand {

    public static void executeCommand(CommandSender commandSender, String[] args){
        if(args.length < 1) {
            RpgItemsOutputHandler.PrintRawInfo(commandSender, ChatColor.AQUA + "/lri debug <loot> " + RpgItemsOutputHandler.HIGHLIGHT + "- prints out the event history of loot changes for the last entity you killed");
            return;
        }

        if(args[0].equalsIgnoreCase("loot")){
            if(!LootManager.isDebugging()){
                RpgItemsOutputHandler.PrintRawInfo(commandSender, RpgItemsOutputHandler.INFO + "You need to enabled " + RpgItemsOutputHandler.HIGHLIGHT + "Debug: true" + RpgItemsOutputHandler.INFO + " in loot.yml to use this command");
                return;
            }

            DeathEventLootInfo lastEvent = DeathEventDebugger.getLatestEventData((Player) commandSender);
            if(lastEvent == null) {
                RpgItemsOutputHandler.PrintRawInfo(commandSender, ChatColor.YELLOW + "You need to kill something first!");
                return;
            }

            RpgItemsOutputHandler.PrintRawInfo(commandSender, ChatColor.AQUA + "================ " + ChatColor.YELLOW + "Last Death Event Loot" + ChatColor.AQUA + " ================");
            LivingEntity entity = lastEvent.getEvent().getEntity();
            RpgItemsOutputHandler.PrintRawInfo(commandSender, ChatColor.AQUA + "Entity: " + entity.getName() + " (" + entity.getType() + ")");
            for(EventPriority eventPriority : EventPriority.values()) {
                List<ItemStack> items = lastEvent.getItems().get(eventPriority);
                RpgItemsOutputHandler.PrintRawInfo(commandSender, ChatColor.AQUA + "" + eventPriority.name());
                for (ItemStack item : items) {
                    if (item != null)
                        RpgItemsOutputHandler.PrintRawInfo(commandSender, ChatColor.YELLOW + " - " + (item.hasItemMeta() ? item.getItemMeta().getDisplayName() : item.getType().name()));
                    else
                        RpgItemsOutputHandler.PrintRawInfo(commandSender, ChatColor.RED + " - NULL");
                }
            }

            RpgItemsOutputHandler.PrintRawInfo(commandSender, ChatColor.AQUA + "=====================================================");
        }
    }
}
