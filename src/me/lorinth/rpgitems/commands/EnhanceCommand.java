package me.lorinth.rpgitems.commands;

import me.lorinth.rpgitems.gui.EnhancementMenu;
import me.lorinth.rpgitems.managers.EnhancementManager;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EnhanceCommand {

    public static void executeCommand(CommandSender commandSender, String[] args){
        String playerName = "";
        if(args.length > 0){
            playerName = args[0];
        }

        Player targetPlayer;
        if(playerName.length() > 0){
            targetPlayer = Bukkit.getServer().getPlayer(playerName);
        }
        else if(commandSender instanceof Player){
            targetPlayer = (Player) commandSender;
        }
        else{
            RpgItemsOutputHandler.PrintError(commandSender, "You need to be a player to run this command without the [player] argument");
            return;
        }

        if(!EnhancementManager.enabled) {
            RpgItemsOutputHandler.PrintError(commandSender, "You need to enabled the Enhancement System to use this command");
            return;
        }

        new EnhancementMenu(targetPlayer);
    }


}
