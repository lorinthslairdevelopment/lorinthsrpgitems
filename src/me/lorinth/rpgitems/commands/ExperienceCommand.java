package me.lorinth.rpgitems.commands;

import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ExperienceCommand {

    public static void executeCommand(CommandSender commandSender, String[] args){
        if(args.length < 3){
            RpgItemsOutputHandler.PrintError(commandSender, "/lri experience <add/remove> <player> <amount>");
            return;
        }
        String type = args[0];
        String playerName = args[1];
        String amount = args[2];

        if(!type.equalsIgnoreCase("add") && !type.equalsIgnoreCase("remove")){
            RpgItemsOutputHandler.PrintError(commandSender, "Valid options are " + RpgItemsOutputHandler.HIGHLIGHT + "add / remove");
            return;
        }

        Player target = Bukkit.getPlayer(playerName);
        if(target == null){
            RpgItemsOutputHandler.PrintError(commandSender, "Could not find a player based on search string, " + RpgItemsOutputHandler.HIGHLIGHT + args[1]);
            return;
        }

        Integer experience;
        if(!TryParse.parseInt(amount)){
            RpgItemsOutputHandler.PrintError(commandSender, "Invalid <amount> argument, " + RpgItemsOutputHandler.HIGHLIGHT + amount);
            return;
        }

        experience = Integer.parseInt(amount);

        if(type.equalsIgnoreCase("add")){
            PlayerCharacterManager.getCharacter(target).addExp(experience);
            RpgItemsOutputHandler.PrintInfo(commandSender, "You have given " + RpgItemsOutputHandler.HIGHLIGHT + experience + RpgItemsOutputHandler.INFO + " exp to " + RpgItemsOutputHandler.HIGHLIGHT + target.getDisplayName());
        }
        else if(type.equalsIgnoreCase("remove")){
            PlayerCharacterManager.getCharacter(target).addExp(experience);
            RpgItemsOutputHandler.PrintInfo(commandSender, "You have removed " + RpgItemsOutputHandler.HIGHLIGHT + experience + RpgItemsOutputHandler.INFO + " exp from " + RpgItemsOutputHandler.HIGHLIGHT + target.getDisplayName());
        }

    }

}