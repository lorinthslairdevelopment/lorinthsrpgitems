package me.lorinth.rpgitems.commands;

import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.managers.GearSetsManager;
import me.lorinth.rpgitems.managers.ItemGenerationManager;
import me.lorinth.rpgitems.objects.GearSet;
import me.lorinth.utils.TryParse;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.regex.Pattern;

public class ItemGenerateCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;

            if(args.length >= 3 && args[2].equalsIgnoreCase("hand"))
                player.getInventory().setItemInMainHand(makeItem(args, player.getInventory().getItemInMainHand()));
            else
                player.getInventory().addItem(makeItem(args, null));
        }

        return false;
    }

    private static ItemStack makeItem(String[] args, ItemStack item){
        RarityTier tier = null;
        int level = 1;
        boolean orHigher = false;
        boolean overrideHand = false;
        GearSet gearSet = null;

        if(args.length >= 1)
            if(TryParse.parseInt(args[0]))
                level = Integer.parseInt(args[0]);
        if(args.length >= 2) {
            String rarity = args[1];
            if(rarity.contains("+")){
                orHigher = true;
                rarity = rarity.replaceAll(Pattern.quote("+"), "");
            }
            for (RarityTier thisTier : RarityTier.values()) {
                if (thisTier.name().equalsIgnoreCase(rarity.toLowerCase())) {
                    tier = thisTier;
                }
            }
        }
        if(args.length >= 3){
            for(String misc : Arrays.copyOfRange(args, 2, args.length)){
                if(misc.equalsIgnoreCase("hand") && item != null){
                    overrideHand = true;
                }
                else{
                    GearSet gs = GearSetsManager.getGearSetById(misc);
                    if(gs != null){
                        gearSet = gs;
                    }
                }
            }
        }

        if(tier != null) {
            if (orHigher)
                return (overrideHand ? ItemGenerationManager.generateItemWithGivenItemStackAtRarityOrAbove(level, tier, item, gearSet) : ItemGenerationManager.generateItemAtRarityOrAbove(level, tier, gearSet)).createItemStack();
            else
                return (overrideHand ? ItemGenerationManager.generateItemWithGivenItemStack(level, tier, item, gearSet) : ItemGenerationManager.generateItem(level, tier, gearSet)).createItemStack();
        }
        else
            return ItemGenerationManager.generateItem(level).createItemStack();
    }

    public static ItemStack executeCommand(String[] args, ItemStack item){
        return makeItem(args, item);
    }
}
