package me.lorinth.rpgitems.commands;

import me.lorinth.rpgitems.enums.ItemStatsView;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.gui.RpgStatsMenu;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.objects.ValueRange;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.rpgitems.util.SettingsHelper;
import me.lorinth.utils.MathHelper;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class ItemStatsCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        executeCommand(commandSender, Arrays.copyOfRange(args, 1, args.length));
        return false;
    }

    public static void executeCommand(CommandSender commandSender, String[] args){
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;

            if(SettingsHelper.itemStatsView == ItemStatsView.Text || (args.length > 0 && args[0].equalsIgnoreCase("text"))){
                RpgCharacter character = PlayerCharacterManager.getCharacter(player);
                AttributeMap attributeMap = character.getStats();
                RpgItemsOutputHandler.PrintCommand(player, ChatColor.UNDERLINE + "RpgItem Stats");
                for(RpgAttribute attribute : RpgAttribute.values()){
                    if(!attribute.isEnabled())
                        continue;

                    Object attributeValue = attributeMap.get(attribute);

                    if(!MathHelper.isZero(attributeMap.get(attribute)))
                        RpgItemsOutputHandler.PrintCommand(player, attribute.getDisplayName() + " : " + attribute.getValueAsString(attributeMap));
                }
            } else if(SettingsHelper.itemStatsView == ItemStatsView.UiMenu || SettingsHelper.itemStatsView == ItemStatsView.UiMenuNonZero){
                new RpgStatsMenu(player, (SettingsHelper.itemStatsView == ItemStatsView.UiMenu));
            }
        }
    }

    private boolean isZero(Object value){
        if(value instanceof ValueRange){
            return ((ValueRange)value).isZero();
        }
        return MathHelper.isZero(value);
    }
}
