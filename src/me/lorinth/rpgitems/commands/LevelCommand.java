package me.lorinth.rpgitems.commands;

import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LevelCommand {

    public static void executeCommand(CommandSender commandSender, String[] args){
        if(args.length < 3){
            RpgItemsOutputHandler.PrintError(commandSender, "/lri level set <player> <level>");
            return;
        }
        String command = args[0];
        String playerName = args[1];
        String levelArg = args[2];
        if(command.equalsIgnoreCase("set")){
            Player target = Bukkit.getPlayer(playerName);
            if(target == null){
                RpgItemsOutputHandler.PrintError(commandSender, "Could not find a player based on search string, " + RpgItemsOutputHandler.HIGHLIGHT + args[1]);
                return;
            }

            Integer level;
            if(!TryParse.parseInt(levelArg)){
                RpgItemsOutputHandler.PrintError(commandSender, "Invalid <level> argument, " + RpgItemsOutputHandler.HIGHLIGHT + args[2]);
                return;
            }

            level = Integer.parseInt(levelArg);

            PlayerCharacterManager.getCharacter(target).setLevel(level);
            RpgItemsOutputHandler.PrintInfo(target, "You are now level, " + RpgItemsOutputHandler.HIGHLIGHT + level);
            RpgItemsOutputHandler.PrintInfo(commandSender, "You have set " + target.getDisplayName() + RpgItemsOutputHandler.INFO + " to " + RpgItemsOutputHandler.HIGHLIGHT + level);
        }

    }

}
