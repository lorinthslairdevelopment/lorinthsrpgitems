package me.lorinth.rpgitems.commands;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.gui.RepairMenu;
import me.lorinth.rpgitems.gui.SellMenu;
import me.lorinth.rpgitems.managers.CombatManager;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.managers.RepairManager;
import me.lorinth.rpgitems.managers.SellManager;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class LorinthsRpgItemsCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command cmd, String s, String[] args) {
        if(args.length > 0) {
            String command = args[0];
            String[] nextArgs = Arrays.copyOfRange(args, 1, args.length);
            if (command.equalsIgnoreCase("reload") && commandSender.hasPermission("lri.admin")) {
                RpgItemsPlugin.Reload();
                RpgItemsOutputHandler.PrintInfo(commandSender, "Plugin Reloaded");
            }
            else if((command.equalsIgnoreCase("class") && commandSender.hasPermission("lri.admin"))){
                ClassCommand.executeCommand(commandSender, nextArgs);
            }
            else if((command.equalsIgnoreCase("enhance") || command.equalsIgnoreCase("enh") && commandSender.hasPermission("lri.admin"))){
                EnhanceCommand.executeCommand(commandSender, nextArgs);
            }
            else if((command.equalsIgnoreCase("experience") || command.equalsIgnoreCase("exp") && commandSender.hasPermission("lri.admin"))) {
                ExperienceCommand.executeCommand(commandSender, nextArgs);
            }
            else if((command.equalsIgnoreCase("generate") || command.equalsIgnoreCase("gen")) && commandSender.hasPermission("lri.admin")){
                if(commandSender instanceof Player)
                    ((Player) commandSender).getInventory().addItem(ItemGenerateCommand.executeCommand(nextArgs, ((Player) commandSender).getInventory().getItemInMainHand()));
            }
            else if((command.equalsIgnoreCase("consumable") || command.equalsIgnoreCase("consume"))){
                ConsumableCommand.executeCommand(commandSender, nextArgs);
            }
            else if(command.equalsIgnoreCase("stats")){
                ItemStatsCommand.executeCommand(commandSender, nextArgs);
            }
            else if(command.equalsIgnoreCase("level") && commandSender.hasPermission("lri.admin")){
                LevelCommand.executeCommand(commandSender, nextArgs);
            }
            else if(command.equalsIgnoreCase("pvp")){
                PvpCommand.executeCommand(commandSender, nextArgs);
            }
            else if(command.equalsIgnoreCase("repair") && commandSender.hasPermission("lri.repair")){
                if(nextArgs.length == 0 && commandSender instanceof Player){
                    new RepairMenu((Player) commandSender);
                } else if(nextArgs.length == 1){
                    Player player = Bukkit.getPlayer(nextArgs[0]);
                    new RepairMenu(player);
                }
            }
            else if(command.equalsIgnoreCase("reset")){
                if(!commandSender.hasPermission("lri.reset")){
                    RpgItemsOutputHandler.PrintError("You don't have permission to do that!");
                }
                if(commandSender instanceof Player) {
                    RpgCharacter rpgCharacter = PlayerCharacterManager.getCharacter((Player) commandSender);
                    if(rpgCharacter != null){
                        rpgCharacter.reset();

                        RpgItemsOutputHandler.PrintInfo("Player, " + ((Player) commandSender).getDisplayName() + ", has reset their level!");
                        RpgItemsOutputHandler.PrintInfo(commandSender, "Your level has been reset!");
                    }
                }
            }
            else if(command.equalsIgnoreCase("sell") && SellManager.isEnabled() && commandSender.hasPermission("lri.sell")){
                if(args.length > 1){
                    String playerName = args[1];
                    new SellMenu(Bukkit.getPlayer(playerName));
                }
                else if(commandSender instanceof Player ){
                    new SellMenu((Player) commandSender);
                }
            }
            else if((command.equalsIgnoreCase("mb") || command.equalsIgnoreCase("multiblock")) && commandSender.hasPermission("lri.mb")) {
                if(commandSender instanceof Player)
                    MultiblockCommand.executeCommand((Player) commandSender, nextArgs);
            }
            else if(command.equalsIgnoreCase("debug")){
                DebugCommand.executeCommand(commandSender, nextArgs);
            }
            else {
                sendHelp(commandSender);
            }
        }
        else{
            sendHelp(commandSender);
        }
        return false;
    }

    private void sendHelp(CommandSender commandSender){
        RpgItemsOutputHandler.PrintCommand(commandSender, "================== " + ChatColor.YELLOW + "Lorinths Rpg Items" + ChatColor.AQUA + " ==================");
        if(commandSender.hasPermission("lri.admin")) {
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri reload " + RpgItemsOutputHandler.HIGHLIGHT + "- reloads the plugin");
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri class set <player> <classId> [level] " + RpgItemsOutputHandler.HIGHLIGHT + "- sets the target players class/level");
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri enhance [player] " + RpgItemsOutputHandler.HIGHLIGHT + "- opens the enhancement GUI for target player");
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri experience <add/remove> <player> <amount> " + RpgItemsOutputHandler.HIGHLIGHT + "- gives or removes an amount of experience from the specified player");
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri gen <level> <rarity> <hand> " + RpgItemsOutputHandler.HIGHLIGHT + "- generates and item with the given level/rarity");
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri level set <player> <level> " + RpgItemsOutputHandler.HIGHLIGHT + "- sets a players level to a given value");
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri consumable give <player> <id> [amount] " + RpgItemsOutputHandler.HIGHLIGHT + "- gives a player a number of the consumable with a given id");
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri consumable list " + RpgItemsOutputHandler.HIGHLIGHT + "- lists all consumable ids");
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri debug <loot>" + RpgItemsOutputHandler.HIGHLIGHT + "- debugs the last loot event");
        }
        if(CombatManager.isTogglingEnabled() && commandSender.hasPermission("lri.pvp") && commandSender instanceof Player) {
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri pvp toggle " + RpgItemsOutputHandler.HIGHLIGHT + "- enables / disables your pvp status");
        }
        if(RepairManager.enabled && commandSender.hasPermission("lri.repair")) {
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri repair <player>" + RpgItemsOutputHandler.HIGHLIGHT + "- opens the repair ui");
        }
        if(commandSender.hasPermission("lri.mb")){
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri mb set [repair] " + RpgItemsOutputHandler.HIGHLIGHT + "- updates a given multiblock structure with your current World Edit selection");
        }
        if(commandSender instanceof Player && commandSender.hasPermission("lri.reset")) {
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri reset " + RpgItemsOutputHandler.HIGHLIGHT + "- resets you back to level 1");
        }
        if(commandSender instanceof Player) {
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri stats " + RpgItemsOutputHandler.HIGHLIGHT + "- displays item stats");
        }

        RpgItemsOutputHandler.PrintCommand(commandSender, "=====================================================");
    }

}
