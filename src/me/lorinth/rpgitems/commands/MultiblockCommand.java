package me.lorinth.rpgitems.commands;

import me.lorinth.rpgitems.managers.EnhancementManager;
import me.lorinth.rpgitems.managers.RepairManager;
import me.lorinth.rpgitems.objects.structure.Layer;
import me.lorinth.rpgitems.objects.structure.MultiBlockStructure;
import me.lorinth.rpgitems.objects.structure.WorldEditHelper;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class MultiblockCommand {

    public static void executeCommand(Player player, String[] args){
        if(args.length >= 2){
            if(args[0].equalsIgnoreCase("set")){
                WorldEditHelper helper = new WorldEditHelper(player);

                for(String arg : args){
                    if(arg.equalsIgnoreCase("-usedata"))
                        helper.setNoData(false);
                }

                if(args[1].equalsIgnoreCase("repair")){
                    Layer[] layers = helper.getSelectionLayers();
                    if(layers == null){
                        RpgItemsOutputHandler.PrintInfo(player, "No layers processed, not updating Repair Structure");
                    } else{
                        MultiBlockStructure structure = RepairManager.getRepairStructure();
                        structure.updateLayers(layers);
                        RpgItemsOutputHandler.PrintInfo(player, "Updated Repair Structure");
                    }
                }
                else if(args[1].equalsIgnoreCase("enhance")){
                    Layer[] layers = helper.getSelectionLayers();
                    if(layers == null){
                        RpgItemsOutputHandler.PrintInfo(player, "No layers processed, not updating Enhancement Structure");
                    } else{
                        MultiBlockStructure structure = EnhancementManager.getEnhancementStructure();
                        structure.updateLayers(layers);
                        RpgItemsOutputHandler.PrintInfo(player, "Updated Enhancement Structure");
                    }
                }
            }
            else
                sendHelp(player);
        }
        else
            sendHelp(player);
    }

    private static void sendHelp(Player player){
        RpgItemsOutputHandler.PrintRawInfo(player, ChatColor.AQUA + "================== " + ChatColor.YELLOW + "Multiblock Command" + ChatColor.AQUA + " ==================");
        RpgItemsOutputHandler.PrintRawInfo(player, ChatColor.AQUA + "/lri mb set <repair/enhance> (tags) " + RpgItemsOutputHandler.HIGHLIGHT + "- updates a given multiblock structure with your current World Edit selection");
        RpgItemsOutputHandler.PrintRawInfo(player, ChatColor.AQUA + " -usedata " + RpgItemsOutputHandler.HIGHLIGHT + "- will take data values and block directions into account");
        RpgItemsOutputHandler.PrintRawInfo(player, ChatColor.AQUA + "=====================================================");
    }
}
