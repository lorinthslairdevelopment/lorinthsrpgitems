package me.lorinth.rpgitems.commands;

import me.lorinth.rpgitems.managers.CombatManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PvpCommand implements CommandExecutor{

    private static CombatManager combatManager = CombatManager.instance;

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        return false;
    }

    public static void executeCommand(CommandSender commandSender, String[] args){
        if(commandSender instanceof Player){
            if(args.length > 0){
                String command = args[0];
                if(command.equalsIgnoreCase("toggle")){
                    String result = CombatManager.instance.changePlayerStatus((Player) commandSender);
                    if(!result.trim().equalsIgnoreCase("") && !result.trim().equalsIgnoreCase("N/A"))
                        commandSender.sendMessage(result);
                }
            }
        }
    }
}
