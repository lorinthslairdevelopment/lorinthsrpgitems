package me.lorinth.rpgitems.commands.classes;

import me.lorinth.rpgitems.gui.ClassMenu;
import me.lorinth.rpgitems.managers.ClassManager;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.character.CharacterClass;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClassGuiCommand {

    public static void executeCommand(CommandSender commandSender, String[] args){
        Player target = null;
        CharacterClass characterClass = null;

        for(String arg : args){
            if(characterClass == null){
                characterClass = ClassManager.getClassById(arg);
                if(characterClass != null){
                    continue;
                }
            }
            if(target == null){
                target = Bukkit.getPlayer(arg);
            }
        }


        if(target == null && commandSender instanceof Player){
            target = (Player) commandSender;
        }

        if(target == null){
            return;
        }
        if(characterClass != null && !characterClass.hasAccessToSubClass(PlayerCharacterManager.getCharacter(target))){
            return;
        }

        new ClassMenu(target, characterClass);
    }
}
