package me.lorinth.rpgitems.commands.classes;

import me.lorinth.rpgitems.managers.ClassManager;
import me.lorinth.rpgitems.objects.character.CharacterClass;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.command.CommandSender;

public class ClassListCommand {

    public static void executeCommand(CommandSender commandSender, String[] args){
        RpgItemsOutputHandler.PrintCommand(commandSender, "Class List");
        for(CharacterClass characterClass : ClassManager.getAllClasses()){
            RpgItemsOutputHandler.PrintRawInfo(commandSender, "- " + characterClass.getId() + " : " + characterClass.getDisplayName());
        }
    }

}
