package me.lorinth.rpgitems.commands.classes;

import me.lorinth.rpgitems.managers.ClassManager;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.objects.character.CharacterClass;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClassSetCommand {


    public static void executeCommand(CommandSender commandSender, String[] args){
        if(args.length < 2){
            RpgItemsOutputHandler.PrintCommand(commandSender, ChatColor.AQUA + "/lri class set <player> <classId> [level] " + RpgItemsOutputHandler.HIGHLIGHT + "- sets the target players class/level");
            return;
        }

        String playerName = args[0];
        String classId = args[1];
        Integer level = null;
        if(args.length == 3){
            String levelValue = args[2];
            if(!TryParse.parseInt(levelValue)){
                RpgItemsOutputHandler.PrintError(commandSender, "Invalid integer, " + RpgItemsOutputHandler.HIGHLIGHT + levelValue);
                return;
            }
            level = Integer.parseInt(levelValue);
        }

        Player player = Bukkit.getPlayer(playerName);
        if(player == null){
            RpgItemsOutputHandler.PrintError(commandSender, "No player found by string, " + RpgItemsOutputHandler.HIGHLIGHT + playerName);
            return;
        }

        CharacterClass characterClass = ClassManager.getClassById(classId);
        if(characterClass == null){
            RpgItemsOutputHandler.PrintError(commandSender, "No class found with the id, " + RpgItemsOutputHandler.HIGHLIGHT + classId);
            return;
        }

        if(!characterClass.hasPermission(player)){
            RpgItemsOutputHandler.PrintError(commandSender, "That player doesn't have permission for the provided class");
            return;
        }

        RpgCharacter character = PlayerCharacterManager.getCharacter(player);
        character.setCurrentCharacterClass(characterClass, level);

        RpgItemsOutputHandler.PrintCommand(commandSender, "Player, " +
                RpgItemsOutputHandler.HIGHLIGHT + player.getDisplayName() +
                RpgItemsOutputHandler.COMMAND + " was successfully set to class, " +
                RpgItemsOutputHandler.HIGHLIGHT + characterClass.getDisplayName());
    }

}
