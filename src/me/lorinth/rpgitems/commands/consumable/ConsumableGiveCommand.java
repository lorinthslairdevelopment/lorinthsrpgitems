package me.lorinth.rpgitems.commands.consumable;

import me.lorinth.rpgitems.managers.ConsumableManager;
import me.lorinth.rpgitems.objects.consumable.RpgConsumable;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ConsumableGiveCommand {

    public static void executeCommand(CommandSender commandSender, String[] args){
        if(args.length < 2){
            RpgItemsOutputHandler.PrintCommand(commandSender, "/lri consumable give <player> <id> [amount] " + RpgItemsOutputHandler.HIGHLIGHT + "- gives a player a number of the consumable with a given id");
        }

        String playerName = args[0];
        String consumableId = args[1];
        Integer count = 1;

        if(args.length == 3 && TryParse.parseInt(args[2])){
            count = Integer.parseInt(args[2]);
        }

        Player player = Bukkit.getPlayer(playerName);
        if(player == null){
            RpgItemsOutputHandler.PrintError(commandSender, "Unable to find player");
            return;
        }

        RpgConsumable consumable = ConsumableManager.getInstance().getConsumableById(consumableId);
        if(consumable == null){
            RpgItemsOutputHandler.PrintError(commandSender, "No Consumable by that id, use " + RpgItemsOutputHandler.HIGHLIGHT + "/lri consumable list");
            return;
        }

        ItemStack item = consumable.getItemStack().clone();
        item.setAmount(count);

        player.getInventory().addItem(item);
    }

}
