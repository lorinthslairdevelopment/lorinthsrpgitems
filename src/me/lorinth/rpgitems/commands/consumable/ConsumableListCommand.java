package me.lorinth.rpgitems.commands.consumable;

import me.lorinth.rpgitems.managers.ConsumableManager;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class ConsumableListCommand {

    public static void executeCommand(CommandSender commandSender, String[] args){
        RpgItemsOutputHandler.PrintInfo(commandSender, ChatColor.UNDERLINE + "Consumable Ids");
        ConsumableManager cm = ConsumableManager.getInstance();
        if(cm != null){
            for(String id : ConsumableManager.getInstance().getConsumableIds()){
                RpgItemsOutputHandler.PrintRawInfo(commandSender, id);
            }
        }
    }

}
