package me.lorinth.rpgitems.enums;

public enum ArmorTier {
    Leather, Chain, Iron, Gold, Diamond
}
