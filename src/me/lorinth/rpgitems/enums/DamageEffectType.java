package me.lorinth.rpgitems.enums;

public enum DamageEffectType{
    POISON, BURNING
}