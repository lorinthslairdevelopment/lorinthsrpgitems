package me.lorinth.rpgitems.enums;

public enum DurabilityDamageCost {

        Weapons_Melee_MainHand, Weapons_Melee_OffHand, Weapons_Bow_Melee, Weapons_Bow_Shoot, Weapons_Shield_Passive, Weapons_Shield_Block,
        Armor_DamageChance_Head, Armor_DamageChance_Chest, Armor_DamageChance_Legs, Armor_DamageChance_Feet,
        Armor_DamageAmount_Head, Armor_DamageAmount_Chest, Armor_DamageAmount_Legs, Armor_DamageAmount_Feet

}
