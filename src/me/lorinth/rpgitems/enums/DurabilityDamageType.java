package me.lorinth.rpgitems.enums;

public enum DurabilityDamageType {

    Melee_Attack, Shield_Block, Ranged_Shoot, Armor_Damage

}
