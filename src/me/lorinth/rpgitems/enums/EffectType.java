package me.lorinth.rpgitems.enums;

public enum EffectType {
    BLOCK, BURN, CRITICAL, DAMAGE, DODGE, HEAL, POISON
}
