package me.lorinth.rpgitems.enums;

public enum LevelHook {
    Vanilla, BuiltIn, Heroes, BattleStats, SkillAPI, MMOCore
}
