package me.lorinth.rpgitems.enums;

public enum MonsterExperienceHook {
    None, Vanilla, BuiltIn, LorinthsRpgMobs
}
