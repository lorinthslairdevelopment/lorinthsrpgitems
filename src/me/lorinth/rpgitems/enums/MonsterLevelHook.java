package me.lorinth.rpgitems.enums;

public enum MonsterLevelHook {
    LorinthsRpgMobs, MythicMobs, None
}
