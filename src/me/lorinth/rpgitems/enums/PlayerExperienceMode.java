package me.lorinth.rpgitems.enums;

public enum PlayerExperienceMode {
    FORMULA, LEVEL_DEFINITION
}
