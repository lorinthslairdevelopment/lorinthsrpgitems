package me.lorinth.rpgitems.enums;

import me.lorinth.rpgitems.objects.MaterialDurability;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Random;

public enum RarityTier {

    Common("Common", true),
    Uncommon("Uncommon", true),
    Rare("Rare", true),
    Epic("Epic", true),
    Legendary("Legendary", true);

    private String displayName;
    private boolean enabled;
    private String colorCode = "&7";
    private int modificationCount = 0;
    private double attributeMultiplier = 1.0;
    private ArrayList<MaterialDurability> weapons;
    private ArrayList<MaterialDurability> armor;
    private Random random = new Random();

    RarityTier(String displayName, boolean enabled){
        this.displayName = displayName;
        this.enabled = enabled;

        weapons = new ArrayList<>();
        armor = new ArrayList<>();
    }

    public static RarityTier getTierByDisplayName(String displayName){
        for(RarityTier tier : RarityTier.values()){
            if(ChatColor.stripColor(displayName).equalsIgnoreCase(ChatColor.stripColor(tier.getDisplayName())))
                return tier;
        }
        return RarityTier.Common;
    }

    public String getFullDisplayName(){ return colorCode + displayName; }

    public String getDisplayName(){
        return displayName;
    }

    public void setDisplayName(String displayName){
        this.displayName = displayName;
    }

    public String getColorCode(){
        return colorCode;
    }

    public void setColorCode(String colorCode){
        this.colorCode = colorCode;
    }

    public int getModificationCount(){
        return modificationCount;
    }

    public void setModificationCount(int modificationCount){
        this.modificationCount = modificationCount;
    }

    public double getAttributeMultiplier(){
        return this.attributeMultiplier;
    }

    public void setAttributeMultiplier(double attributeMultiplier){
        this.attributeMultiplier = attributeMultiplier;
    }

    public boolean isEnabled(){
        return enabled;
    }

    public MaterialDurability getRandomWeapon(){
        return weapons.get(random.nextInt(weapons.size()));
    }

    public MaterialDurability getRandomArmor(){
        return armor.get(random.nextInt(armor.size()));
    }

    public void setWeapons(ArrayList<MaterialDurability> weapons){
        this.weapons = weapons;
    }

    public void setArmor(ArrayList<MaterialDurability> armor){
        this.armor = armor;
    }

    public boolean hasItem(MaterialDurability materialDurability){
        return weapons.contains(materialDurability) || armor.contains(materialDurability);
    }

}
