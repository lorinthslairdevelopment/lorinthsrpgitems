package me.lorinth.rpgitems.enums;

import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.objects.ValueRange;
import me.lorinth.rpgitems.util.LoreHelper;
import org.bukkit.Material;

import java.text.DecimalFormat;

public enum RpgAttribute {

    Health("Health", true, false, true, Material.COOKED_BEEF, false),
    Mana("Mana", true, false, true, Material.POTION, false),
    Stamina("Stamina", true, false, true, Material.SALMON, false),
    Damage("Damage", false, false, true, Material.DIAMOND_AXE, true),
    PvEDamage("PvE Damage", false, true, true, Material.IRON_SWORD, false),
    PvPDamage("PvP Damage", false, true, true, Material.GOLDEN_SWORD, false),
    Defense("Defense", false, true, true, Material.DIAMOND_CHESTPLATE, false),
    PvEDefense("PvE Defense", false, true, true, Material.IRON_CHESTPLATE, false),
    PvPDefense("PvP Defense", false, true, true, Material.GOLDEN_CHESTPLATE, false),
    CriticalChance("Critical", false, true, true, Material.GUNPOWDER, false),
    CriticalDamage("Critical Dmg", false, true, true, Material.TNT, false),
    DodgeChance("Dodge", false, true, true, Material.STRING, false),
    BlockChance("Block", false, true, true, Material.SHIELD, false),
    BlockDamage("Block Damage", true, false, true, Material.SHIELD, false),
    MovementSpeed("Move Speed", false, false, true, Material.LEATHER_BOOTS, false),
    Jump("Jump", false, false, true, Material.RABBIT_FOOT, false),
    Penetration("Penetration", false, true, true, Material.ARROW, false),
    AttackSpeed("Attack Speed", false, false, true, Material.FEATHER, false),
    Vampirism("Vampirism", true, true, true, Material.REDSTONE_BLOCK, false),
    Accuracy("Accuracy", false, true, true, Material.BOW, false),
    BurnChance("Burn Chance", false, true, true, Material.FLINT_AND_STEEL, false),
    BurnDamage("Burn Dmg", false, true, true, Material.LAVA_BUCKET, false),
    PoisonChance("Poison Chance", false, true, true, Material.RED_MUSHROOM, false),
    PoisonDamage("Poison Dmg", false, true, true, Material.BROWN_MUSHROOM, false),
    ExpGain("Exp Gain", false, true, true, Material.EXPERIENCE_BOTTLE, false),
    GoldGain("Gold Gain", false, true, true, Material.GOLD_INGOT, false),

    //Heroes
    Strength("Strength", true, false, false, Material.DIAMOND_SWORD, false),
    Constitution("Constitution", true, false, false, Material.COOKED_CHICKEN, false),
    Endurance("Endurance", true, false, false, Material.RABBIT_HIDE, false),
    Dexterity("Dexterity", true, false, false, Material.BOW, false),
    Intellect("Intellect", true, false, false, Material.NETHER_WART, false),
    Wisdom("Wisdom", true, false, false, Material.END_ROD, false),
    Charisma("Charisma", true, false, false, Material.ROSE_BUSH, false),
    ManaRegeneration("Mana Regeneration", true, false, false, Material.POTION, false),
    StaminaRegeneration("Stamina Regeneration", true, false, false, Material.APPLE, false);

    private String displayName;
    private Material displayMaterial;
    private Object defaultValue;
    private Double maxValue = Double.MAX_VALUE;
    private boolean isDecimal;
    private boolean isPercent;
    private boolean isRange;
    private boolean enabled;

    RpgAttribute(String displayName, boolean isInteger, boolean isPercent, boolean enabled, Material displayMaterial, boolean isRange){
        this.displayName = displayName;
        this.displayMaterial = displayMaterial;
        this.isDecimal = !isInteger;
        this.isPercent = isPercent;
        this.enabled = enabled;
        this.isRange = isRange;

        if(isRange) {
            defaultValue = new ValueRange();
            ((ValueRange) defaultValue).setIsDecimal(isDecimal);
        }
        else {
            defaultValue = 0.0d;
        }
    }

    public String getDisplayName(){
        return displayName;
    }

    public void setDisplayName(String displayName){
        if(displayName != null)
            this.displayName = displayName;
    }

    public Material getDisplayMaterial(){
        return displayMaterial;
    }

    public void setDisplayMaterial(Material displayMaterial){
        if(displayMaterial != null)
            this.displayMaterial = displayMaterial;
    }

    public boolean isEnabled(){
        return enabled;
    }

    public void setEnabled(boolean enabled){
        this.enabled = enabled;
    }

    public boolean isDecimal() { return isDecimal; }

    public void setIsDecimal(boolean isDecimal){
        this.isDecimal = isDecimal;
    }

    public boolean isPercent(){ return isPercent; }

    public void setIsPercent(boolean isPercent){
        this.isPercent = isPercent;
    }

    public Object getDefaultValue(){ return defaultValue; }

    public void setDefaultValue(Object value){
        this.defaultValue = value;
    }

    public double getMaxValue(){ return maxValue; }

    public void setMaxValue(double value) {this.maxValue = value; }

    public boolean isRange(){
        return isRange;
    }

    public String getValueAsString(AttributeMap attributeMap){
        Object value = attributeMap.get(this);
        DecimalFormat df = LoreHelper.DecimalFormat;
        double min,max;

        String valueString;
        if(value instanceof ValueRange){
            ValueRange range = (ValueRange) value;
            min = range.getMinimum();
            max = range.getMaximum();
        }
        else {
            min = (double) value;
            max = (double) value;
        }

        min = Math.min(min, maxValue);
        max = Math.min(max, maxValue);

        if(value instanceof ValueRange){
            if(isDecimal())
                valueString = "" + (df.format(min)) + "-" + (df.format(max));
            else
                valueString = "" + ((int) min) + "-" + ((int) max);
        }
        else{
            if(isDecimal())
                valueString = "" + (df.format(min));
            else
                valueString = "" + ((int) min);
        }

        if(isPercent())
            valueString += "%";

        return valueString;
    }

}
