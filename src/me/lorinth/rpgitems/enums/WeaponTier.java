package me.lorinth.rpgitems.enums;

public enum WeaponTier {
    Wood, Stone, Iron, Gold, Diamond
}
