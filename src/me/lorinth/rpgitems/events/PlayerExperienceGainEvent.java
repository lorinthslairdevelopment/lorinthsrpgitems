package me.lorinth.rpgitems.events;

import me.lorinth.rpgitems.objects.RpgCharacter;
import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerExperienceGainEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;

    private RpgCharacter character;
    private Entity entity;
    private Integer amount;

    public PlayerExperienceGainEvent(RpgCharacter character, Entity entity, Integer amount) {
        this.character = character;
        this.amount = amount;
        this.entity = entity;
        this.cancelled = false;
    }

    public RpgCharacter getCharacter(){
        return character;
    }

    public Integer getExp() {
        return amount;
    }

    public Entity getEntity(){
        return entity;
    }

    public void setExp(Integer amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Experience cannot be negative");
        } else {
            this.amount = amount;
        }
    }

    public boolean isCancelled() {
        return this.cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
