package me.lorinth.rpgitems.events;

import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.RpgCharacter;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerLevelUpEvent extends PlayerEvent {

    private static final HandlerList handlers = new HandlerList();
    private Integer originalLevel;
    private Integer newLevel;
    private RpgCharacter rpgCharacter;

    public PlayerLevelUpEvent(Player player, Integer originalLevel, Integer newLevel) {
        super(player);
        this.originalLevel = originalLevel;
        this.newLevel = newLevel;
        rpgCharacter = PlayerCharacterManager.getCharacter(player);
    }

    public Integer getOriginalLevel(){
        return originalLevel;
    }

    public Integer getNewLevel(){
        return newLevel;
    }

    public RpgCharacter getCharacter(){
        return rpgCharacter;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
