package me.lorinth.rpgitems.gui;

import javafx.util.Pair;
import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.managers.ClassManager;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.objects.character.CharacterClass;
import me.lorinth.rpgitems.objects.character.CharacterClassInstance;
import me.lorinth.rpgitems.objects.gui.ClassGuiOptions;
import me.lorinth.utils.objects.gui.ClickableGUI;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ClassMenu {

    private Player player;
    private RpgCharacter rpgCharacter;
    private ClassGuiOptions options;
    private ClickableGUI iconMenu;

    private final ArrayList<Integer> positions = new ArrayList<Integer>(){{
        add(10);
        add(12);
        add(14);
        add(16);
        add(20);
        add(22);
        add(24);
        add(28);
        add(30);
        add(32);
        add(34);
        add(38);
        add(40);
        add(42);
        add(44);
    }};

    public ClassMenu(Player player, CharacterClass characterClass) {
        this.player = player;
        rpgCharacter = PlayerCharacterManager.getCharacter(player);
        options = ClassManager.getGuiOptions();

        String title = characterClass != null ? options.ClassTitle.replace("{className}", characterClass.getDisplayName()) :
                options.Title;
        iconMenu = new ClickableGUI(RpgItemsPlugin.instance, 6, title);
        loadDisplay(characterClass);
        iconMenu.open(player);
    }

    private void loadDisplay(CharacterClass characterClass){
        Collection<CharacterClass> classes;
        if(characterClass == null){
            classes = ClassManager.getAllBaseClasses();
        }
        else{
            List<CharacterClass> subClasses = characterClass.getSubClasses();
            classes = subClasses.subList(0, subClasses.size());
        }

        if(options.AutoPlace){
            int index = 0;
            for(CharacterClass characterClassTarget : classes){
                if(!characterClassTarget.isUnlocked(rpgCharacter)){
                    continue;
                }

                int displaySlot = positions.get(index);
                place(displaySlot, characterClassTarget);

                index++;
                if(index == positions.size()){
                    break;
                }
            }
        }
        else{
            for(final CharacterClass characterClassTarget : classes){
                if(!characterClassTarget.isUnlocked(rpgCharacter)){
                    continue;
                }

                int displaySlot = characterClassTarget.getMetadata().GuiSlot;
                place(displaySlot, characterClassTarget);
            }
        }
    }

    private void place(int slot, CharacterClass characterClass){
        boolean hasAccessToSubclass = characterClass.hasAccessToSubClass(rpgCharacter);
        CharacterClassInstance classInstance = rpgCharacter.getInfoForClass(characterClass);
        if(classInstance == null){
            classInstance = new CharacterClassInstance(player, characterClass);
        }

        //Add open/select lore
        ItemStack displayItem = characterClass.getDisplayItem().clone();
        ItemMeta itemMeta = displayItem.getItemMeta();
        itemMeta.setDisplayName(itemMeta.getDisplayName());
        List<String> lore = itemMeta.getLore();
        lore.add(0,  ChatColor.YELLOW + "Level: " + classInstance.getLevel());
        lore.add("");
        lore.add(options.SelectText);

        if(hasAccessToSubclass){
            lore.add(options.OpenText);
        }
        itemMeta.setLore(lore);
        displayItem.setItemMeta(itemMeta);

        ArrayList<Pair<ClickType, Runnable>> actions = new ArrayList<>();
        actions.add(new Pair<>(options.SelectClickType, () -> onSelected(characterClass)));
        if(hasAccessToSubclass){
            actions.add(new Pair<>(options.OpenSubClassMenu, () -> onOpen(characterClass)));
        }

        iconMenu.addIcon(displayItem, slot, actions.toArray(new Pair[actions.size()]));
    }

    private void onSelected(CharacterClass characterClass){
        rpgCharacter.setCurrentCharacterClass(characterClass, null);
        iconMenu.close(player);
    }

    private void onOpen(CharacterClass characterClass){
        iconMenu.close(player);
        new ClassMenu(player, characterClass);
    }

}