package me.lorinth.rpgitems.gui;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.managers.EnhancementManager;
import me.lorinth.rpgitems.managers.RepairManager;
import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.objects.AttributeRange;
import me.lorinth.rpgitems.objects.RpgItemStack;
import me.lorinth.rpgitems.objects.ValueRange;
import me.lorinth.rpgitems.objects.enhancement.EnhancementData;
import me.lorinth.rpgitems.objects.enhancement.EnhancementTierInfo;
import me.lorinth.rpgitems.objects.gui.EnhancementGuiOptions;
import me.lorinth.rpgitems.util.LoreHelper;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.MathHelper;
import me.lorinth.utils.NmsUtils;
import me.lorinth.utils.OutputHandler;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.List;

public class EnhancementMenu {

    private EnhancementGuiOptions options;
    private InventoryInteractiveMenu iconMenu;

    private static HashMap<Player, ItemStack> playerTargetEnhancementItem = new HashMap<>();
    private static HashMap<Player, ItemStack> playerEnhancementItem = new HashMap<>();
    private static HashMap<Player, RpgItemStack> playerResultItem = new HashMap<>();
    private static HashMap<Player, EnhancementData> playerEnhancementData = new HashMap<>();
    private static HashMap<Player, Integer> playerTargetEnhancementTier = new HashMap<>();
    private static HashMap<Player, EnhancementTierInfo> playerEnhancementTierInfo = new HashMap<>();

    public EnhancementMenu(Player player){
        options = EnhancementManager.getEnhancementGuiOptions();
        iconMenu = new InventoryInteractiveMenu(options.Title, options.Rows, this::click, this::playerInventoryClick, this::playerInventoryClose);

        for(int i=0;i<options.Rows;i++){
            for(int x=0; x<9; x++)
                iconMenu.addButton(iconMenu.getRow(i), x, options.LockedSlot);
        }

        setSlot(options.Slots.TargetItem, new ItemStack(Material.AIR));
        setSlot(options.Slots.EnhancementMaterial, new ItemStack(Material.AIR));
        setSlot(options.Slots.Result, new ItemStack(Material.AIR));

        iconMenu.addButton(iconMenu.getRow(options.Slots.Instruction / 9), options.Slots.Instruction % 9, options.Instructions);

        playerTargetEnhancementItem.put(player, null);
        playerEnhancementItem.put(player, null);
        playerResultItem.put(player, null);
        playerEnhancementData.put(player, null);
        playerEnhancementTierInfo.put(player, null);
        playerTargetEnhancementTier.put(player, null);

        iconMenu.open(player);
    }

    private void setSlot(int slot, ItemStack item){
        iconMenu.addButton(iconMenu.getRow(slot / 9), slot % 9, item, item.hasItemMeta() ? item.getItemMeta().getDisplayName() : null);
    }

    private void setInventorySlot(Player player, int slot, ItemStack item){
        iconMenu.getInventory(player).setItem(slot, item);
    }

    public boolean click(Player clicker, InventoryInteractiveMenu menu, InventoryInteractiveMenu.Row row, int slot, ItemStack item, ClickType click){
        int clickedSlot = (row.row * 9 + slot);
        if(options.Slots.Result == clickedSlot && item != null){
            enhanceItem(clicker);
        }
        else if(options.Slots.TargetItem == clickedSlot && item != null){
            setTargetItem(clicker, null, new ItemStack(Material.AIR), true);
        }
        else if(options.Slots.EnhancementMaterial == clickedSlot && item != null){
            setEnhancementMaterial(clicker, null, new ItemStack(Material.AIR), true);
        }

        return true;
    }

    public boolean playerInventoryClick(Player clicker, Inventory inventory, int slot, ItemStack item, ClickType click){
        if(item == null)
            return true;

        RpgItemStack rpgItem = new RpgItemStack(item);
        if(rpgItem.isRpgItem()){
            setTargetItem(clicker, inventory, item, true);
            return true;
        }

        EnhancementData data = EnhancementManager.getEnhancementData(item);
        if(data != null){
            playerEnhancementData.put(clicker, data);
            setEnhancementMaterial(clicker, inventory, item, true);
            return true;
        }

        return true;
    }

    public void playerInventoryClose(Player player, Inventory inventory){
        givePlayerItem(player, inventory.getItem(options.Slots.TargetItem));
        givePlayerItem(player, inventory.getItem(options.Slots.EnhancementMaterial));

        player.updateInventory();
    }

    private void givePlayerItem(Player player, ItemStack item){
        if(player != null && item != null && item.getType() != Material.AIR){
            if(player.getInventory().firstEmpty() != -1) {
                player.getInventory().addItem(item);
            }
            else{
                World world = player.getWorld();
                Item itemEntity = world.spawn(player.getLocation(), Item.class);
                itemEntity.setItemStack(item);
                NmsUtils.addOwner(RpgItemsPlugin.instance, itemEntity, player);

                RpgItemsOutputHandler.PrintInfo(player, "Your inventory is full, dropping at your feet");
            }
        }
    }

    private void setEnhancementMaterial(Player player, Inventory inv, ItemStack item, boolean returnOldItem){
        ItemStack oldItem = playerEnhancementItem.put(player, item);
        if(returnOldItem && oldItem != null && oldItem.getType() != Material.AIR) {
            player.getInventory().addItem(oldItem);
        }

        setInventorySlot(player, options.Slots.EnhancementMaterial, item);
        if(inv != null) {
            inv.remove(item);
        }

        updateResult(player);
    }

    private void setTargetItem(Player player, Inventory inv, ItemStack item, boolean returnOldItem){
        ItemStack oldItem = playerTargetEnhancementItem.put(player, item);
        if(returnOldItem && oldItem != null && oldItem.getType() != Material.AIR) {
            player.getInventory().addItem(oldItem);
        }

        setInventorySlot(player, options.Slots.TargetItem, item);
        if(inv != null) {
            inv.remove(item);
        }

        updateResult(player);
    }

    private void updateResult(Player player){
        ItemStack targetItem = playerTargetEnhancementItem.get(player);
        ItemStack enhancementItem = playerEnhancementItem.get(player);
        EnhancementData enhancementData = playerEnhancementData.get(player);

        if(targetItem == null || enhancementItem == null || enhancementData == null ||
                targetItem.getType() == Material.AIR || enhancementItem.getType() == Material.AIR) {
            ItemStack empty = new ItemStack(Material.AIR);
            setInventorySlot(player, options.Slots.Result, empty);
            return;
        }

        RpgItemStack rpgItem = new RpgItemStack(targetItem.clone());
        int targetTier = rpgItem.getEnhancementLevel() + 1;
        EnhancementTierInfo tierInfo = enhancementData.getTierChances().get(targetTier);
        if(tierInfo == null){
            setInventorySlot(player, options.Slots.Result, options.NotValidEnhancementItem);
            setInventorySlot(player, options.Slots.ChanceInfo, options.LockedSlot);
            return;
        }
        else{
            playerEnhancementTierInfo.put(player, tierInfo);
            updateChances(player, tierInfo, rpgItem.getRarityTier());
        }

        AttributeMap overallBonuses = new AttributeMap(false);
        for(int i=0; i<enhancementData.getNumberOfUpgrades(); i++){
            AttributeMap bonuses = calculateBonuses(enhancementData, rpgItem.getAttributes().clone());
            AttributeMap upgradedStats = rpgItem.getAttributes();
            upgradedStats.applyAttributeMap(bonuses);
            overallBonuses.applyAttributeMap(bonuses);

            rpgItem.setAttributes(upgradedStats);
            rpgItem.setEnhancementLevel(rpgItem.getEnhancementLevel() + 1); //+3
        }

        playerResultItem.put(player, rpgItem);

        //Add visual for output display
        rpgItem = rpgItem.clone();
        rpgItem.setVisualBonus(overallBonuses);
        playerTargetEnhancementTier.put(player, rpgItem.getEnhancementLevel());
        setInventorySlot(player, options.Slots.Result, rpgItem.getBukkitItem());
    }

    private AttributeMap calculateBonuses(EnhancementData enhancementData, AttributeMap current){
        AttributeMap allIncreaseBonus = current.clone();
        allIncreaseBonus.multiplyAll(enhancementData.getAllAttributePercentIncrease());

        //Handle specific increase
        AttributeMap specificIncreaseBonus = current.clone();
        for(RpgAttribute rpgAttribute : specificIncreaseBonus.getNonZeroAttributes().keySet()){
            Object specificIncrease = enhancementData.getAttributeSpecificPercentIncrease().get(rpgAttribute);
            if(specificIncrease != null){
                specificIncreaseBonus.multiplyObject(rpgAttribute, specificIncrease);
            }
        }

        allIncreaseBonus.applyAttributeMap(specificIncreaseBonus);

        for(RpgAttribute rpgAttribute : enhancementData.getAttributeMaximumValueIncrease().getNonZeroAttributes().keySet()){
            Double maxValue = (Double) enhancementData.getAttributeMaximumValueIncrease().get(rpgAttribute);
            Double currentValue = 0d;
            Object currentBonus = allIncreaseBonus.get(rpgAttribute);
            if(currentBonus instanceof Double){
                currentValue = (Double) currentBonus;
            }
            else if(currentBonus instanceof ValueRange){
                currentValue = ((ValueRange) currentBonus).getMaximum();
            }

            if(currentValue > maxValue){
                if(currentBonus instanceof Double){
                    allIncreaseBonus.put(rpgAttribute, maxValue);
                }
                else if(currentBonus instanceof ValueRange){
                    double min = Math.min(((ValueRange) currentBonus).getMinimum(), currentValue);
                    double max = Math.min(((ValueRange) currentBonus).getMaximum(), currentValue);
                    allIncreaseBonus.put(rpgAttribute, new ValueRange(min, max));
                }
            }
        }

        for(RpgAttribute rpgAttribute : enhancementData.getAttributeMinimumValueIncrease().getNonZeroAttributes().keySet()){
            if(MathHelper.isZero(current.get(rpgAttribute))) {
                continue;
            }

            Double minValue = (Double) enhancementData.getAttributeMinimumValueIncrease().get(rpgAttribute);
            double currentValue = 0;
            Object currentBonus = allIncreaseBonus.get(rpgAttribute);
            if(currentBonus instanceof Double){
                currentValue = (Double) currentBonus;
            }
            else if(currentBonus instanceof ValueRange){
                currentValue = ((ValueRange) currentBonus).getMaximum();
            }

            if(currentValue < minValue){
                if(currentBonus instanceof Double){
                    allIncreaseBonus.put(rpgAttribute, minValue);
                }
                else if(currentBonus instanceof ValueRange){
                    double min = Math.max(((ValueRange) currentBonus).getMinimum(), currentValue);
                    double max = Math.max(((ValueRange) currentBonus).getMaximum(), currentValue);
                    allIncreaseBonus.put(rpgAttribute, new ValueRange(min, max));
                }
            }
        }

        return allIncreaseBonus;
    }

    private void updateChances(Player player, EnhancementTierInfo tierInfo, RarityTier rarityTier){
        ItemStack item = options.ChanceInfo.clone();
        ItemMeta meta = item.getItemMeta();
        double failureChance = (100.0 - tierInfo.getSuccessChance(rarityTier)) - tierInfo.getDestoryChance(rarityTier);
        List<String> lore = meta.getLore();
        for(int i = 0; i<lore.size(); i++){
            String line = lore.get(i);
            if(line.contains("{successChance}")){
                lore.set(i, line.replace("{successChance}", LoreHelper.DecimalFormat.format(tierInfo.getSuccessChance(rarityTier))));
            }
            if(line.contains("{failureChance}")){
                lore.set(i, line.replace("{failureChance}", LoreHelper.DecimalFormat.format(failureChance)));
            }
            if(line.contains("{destroyChance}")){
                lore.set(i, line.replace("{destroyChance}", LoreHelper.DecimalFormat.format(tierInfo.getDestoryChance(rarityTier))));
            }
        }
        meta.setLore(lore);
        item.setItemMeta(meta);
        setInventorySlot(player, options.Slots.ChanceInfo, item);
    }

    private void enhanceItem(Player player){
        ItemStack enhancementItem = playerEnhancementItem.get(player);
        EnhancementTierInfo enhancementTierInfo = playerEnhancementTierInfo.get(player);
        RpgItemStack resultItem = playerResultItem.get(player);
        if(enhancementItem == null || enhancementTierInfo == null || resultItem == null){
            return;
        }

        //Reduce the enhancement material count by 1
        takeOneEnhancementMaterial(player, enhancementItem);

        Double roll = Math.random() * 100.0;
        if(roll <= enhancementTierInfo.getSuccessChance(resultItem.getRarityTier())){
            //Success
            setTargetItem(player, null, new ItemStack(Material.AIR), false);
            player.sendMessage(EnhancementManager.enhancementSuccessMessage.replace("{tier}", playerTargetEnhancementTier.get(player).toString()));
            player.getInventory().addItem(resultItem.getBukkitItem());
        }
        else if(roll <= enhancementTierInfo.getSuccessChance(resultItem.getRarityTier()) + enhancementTierInfo.getDestoryChance(resultItem.getRarityTier())){
            // Critical Failure
            setTargetItem(player, null, new ItemStack(Material.AIR), false);
            player.sendMessage(EnhancementManager.enhancementDestroyMessage);
        }
        else{
            // Normal Failure
            player.sendMessage(EnhancementManager.enhancementFailureMessage);
        }

        updateResult(player);

    }

    private void takeOneEnhancementMaterial(Player player, ItemStack enhancementItem){
        //Remove enhancementMaterial
        enhancementItem.setAmount(enhancementItem.getAmount()-1);
        if(enhancementItem.getAmount() < 1){
            enhancementItem = new ItemStack(Material.AIR);
        }

        setEnhancementMaterial(player, null, enhancementItem, false);
    }

}
