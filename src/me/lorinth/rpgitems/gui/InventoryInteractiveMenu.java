package me.lorinth.rpgitems.gui;

import me.lorinth.rpgitems.RpgItemsPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class InventoryInteractiveMenu implements Listener {

    private String name;
    private int size;
    private onClick click;
    private onPlayerInventoryClick playerClick;
    private onInventoryClose inventoryClose;
    private Player viewer;

    private HashMap<Player, Inventory> playerInventories = new HashMap<>();
    private List<Player> ignoreCloseInventory = new ArrayList<>();

    private ItemStack[] items;

    public InventoryInteractiveMenu(String name, int size, onClick click, onPlayerInventoryClick playerClick, onInventoryClose inventoryClose) {
        this.name = name;
        this.size = size * 9;
        items = new ItemStack[this.size];
        this.click = click;
        this.playerClick = playerClick;
        this.inventoryClose = inventoryClose;
        Bukkit.getPluginManager().registerEvents(this, RpgItemsPlugin.instance);
    }

    @EventHandler
    public void onPluginDisable(PluginDisableEvent event) {
        close();
    }

    public InventoryInteractiveMenu open(Player p) {
        p.openInventory(getInventory(p));
        viewer = p;
        return this;
    }

    public Inventory getInventory(Player p) {
        Inventory inv = playerInventories.get(p);
        if(inv == null){
            inv = Bukkit.createInventory(null, size, name);
            for (int i = 0; i < items.length; i++)
                if (items[i] != null)
                    inv.setItem(i, items[i]);

            playerInventories.put(p, inv);
        }
        return inv;
    }

    protected InventoryInteractiveMenu close() {
        if (viewer.getOpenInventory().getTitle().equals(name))
            viewer.closeInventory();

        return this;
    }

    private Player getViewer() {
        return viewer;
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if(event.getWhoClicked().getName().equalsIgnoreCase(viewer.getName())){
            event.setCancelled(true);
            Player p = (Player) event.getWhoClicked();
            Row row = getRowFromSlot(event.getSlot());
            Inventory clickedInventory = event.getClickedInventory();
            if(clickedInventory == null)
                return;

            InventoryHolder holder = clickedInventory.getHolder();
            boolean isPlayerInventory = holder instanceof Player;
            if(isPlayerInventory){
                if (!playerClick.click(p, event.getClickedInventory(), event.getSlot(), event.getCurrentItem(), event.getClick()))
                    close();
            }
            else{
                if (!click.click(p, this, row, event.getSlot() - row.getRow() * 9, event.getCurrentItem(), event.getClick()))
                    close();
            }
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if(ignoreCloseInventory.contains(event.getPlayer())){
            return;
        }
        if(event.getView().getTitle().equalsIgnoreCase(this.name)){
            this.inventoryClose.close((Player) event.getPlayer(), event.getInventory());
            HandlerList.unregisterAll(this);
        }
    }

    public void setTitle(String title, Player player){
        //TODO: Close isn't working
        ignoreCloseInventory.add(player);
        this.close();

        Bukkit.getScheduler().runTaskLater(RpgItemsPlugin.instance, () -> {
            this.name = title;
            this.open(player);
            ignoreCloseInventory.remove(player);
        }, 1);
    }

    public void setSlot(int slot, ItemStack item){
        addButton(getRow(slot / 9), slot % 9, item, item.hasItemMeta() ? item.getItemMeta().getDisplayName() : null);
    }

    public InventoryInteractiveMenu addButton(Row row, int position, ItemStack item) {
        items[row.getRow() * 9 + position] = item;
        return this;
    }

    public InventoryInteractiveMenu addButton(Row row, int position, ItemStack item, String name, String... lore) {
        items[row.getRow() * 9 + position] = getItem(item, name, lore);
        return this;
    }

    private Row getRowFromSlot(int slot) {
        return new Row(slot / 9, items);
    }

    public Row getRow(int row) {
        return new Row(row, items);
    }

    public interface onClick {
        boolean click(Player clicker, InventoryInteractiveMenu menu, Row row, int slot, ItemStack item, ClickType click);
    }

    public interface onPlayerInventoryClick{
        boolean click(Player clicker, Inventory inventory, int slot, ItemStack item, ClickType click);
    }

    public interface onInventoryClose{
        void close(Player player, Inventory inventory);
    }

    public class Row {
        private ItemStack[] rowItems = new ItemStack[9];
        int row;

        private Row(int row, ItemStack[] items) {
            this.row = row;
            int j = 0;
            try{
                for (int i = (row * 9); i < (row * 9) + 9; i++) {
                    rowItems[j] = items[i];
                    j++;
                }
            }
            catch(ArrayIndexOutOfBoundsException exception){
                //Swallows clicking outside of inventory
            }
        }

        public ItemStack[] getRowItems() {
            return rowItems;
        }

        public ItemStack getRowItem(int item) {
            return rowItems[item] == null ? new ItemStack(Material.AIR) : rowItems[item];
        }

        private int getRow() {
            return row;
        }
    }

    private ItemStack getItem(ItemStack item, String name, String... lore) {
        if(name != null || (lore != null && lore.length > 0)) {
            ItemMeta im = item.getItemMeta();
            im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            if (name != null)
                im.setDisplayName(name);
            if (lore.length > 0)
                im.setLore(Arrays.asList(lore));
            item.setItemMeta(im);
        }
        return item;
    }


}