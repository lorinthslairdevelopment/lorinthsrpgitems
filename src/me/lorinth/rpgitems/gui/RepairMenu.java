package me.lorinth.rpgitems.gui;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.managers.RepairManager;
import me.lorinth.rpgitems.objects.gui.RepairGuiOptions;
import me.lorinth.rpgitems.objects.RpgItemStack;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.NmsUtils;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class RepairMenu {

    private RepairGuiOptions options;
    private InventoryInteractiveMenu iconMenu;

    private int targetItemSlot;
    private int repairMaterialSlot;
    private int resultSlot;

    private static HashMap<Player, ItemStack> playerTargetRepairItem = new HashMap<>();
    private static HashMap<Player, ItemStack> playerRepairMaterialItem = new HashMap<>();

    public RepairMenu(Player player){
        options = RepairManager.getRepairGuiOptions();
        iconMenu = new InventoryInteractiveMenu(options.Title, options.Rows, this::click, this::playerInventoryClick, this::playerInventoryClose);

        for(int i=0;i<options.Rows;i++){
            for(int x=0; x<9; x++)
                iconMenu.addButton(iconMenu.getRow(i), x, options.LockedSlot);
        }

        targetItemSlot = options.Slots.TargetRepairItem;
        setSlot(targetItemSlot, new ItemStack(Material.AIR));

        repairMaterialSlot = options.Slots.RepairMaterial;
        setSlot(repairMaterialSlot, new ItemStack(Material.AIR));

        resultSlot = options.Slots.Result;
        setSlot(resultSlot, new ItemStack(Material.AIR));

        iconMenu.addButton(iconMenu.getRow(options.Slots.Instruction / 9), options.Slots.Instruction % 9, options.Instructions);
        iconMenu.addButton(iconMenu.getRow(options.Slots.RepairValues / 9), options.Slots.RepairValues % 9, options.RepairValues);

        playerTargetRepairItem.put(player, null);
        playerRepairMaterialItem.put(player, null);

        iconMenu.open(player);
    }

    private void setSlot(int slot, ItemStack item){
        iconMenu.addButton(iconMenu.getRow(slot / 9), slot % 9, item, item.hasItemMeta() ? item.getItemMeta().getDisplayName() : null);
    }

    private void setInventorySlot(Player player, int slot, ItemStack item){
        iconMenu.getInventory(player).setItem(slot, item);
    }

    public boolean click(Player clicker, InventoryInteractiveMenu menu, InventoryInteractiveMenu.Row row, int slot, ItemStack item, ClickType click){
        int clickedSlot = (row.row * 9 + slot);
        if(resultSlot == clickedSlot){
            repairItem(clicker, click);
        }
        else if(targetItemSlot == clickedSlot && item != null){
            setTargetItem(clicker, null, new ItemStack(Material.AIR), true);
        }
        else if(repairMaterialSlot == clickedSlot && item != null){
            setRepairMaterialItem(clicker, null, new ItemStack(Material.AIR), true);
        }

        return true;
    }

    public boolean playerInventoryClick(Player clicker, Inventory inventory, int slot, ItemStack item, ClickType click){
        if(item == null)
            return true;

        RpgItemStack rpgItem = new RpgItemStack(item);

        if(!rpgItem.hasMaxDurability()){
            setTargetItem(clicker, inventory, item, true);
        }
        else {
            Double repairValue = RepairManager.getRepairValue(item);
            if(repairValue != null && repairValue > 0.0d){
                setRepairMaterialItem(clicker, inventory, item, true);
            }
        }

        return true;
    }

    public void playerInventoryClose(Player player, Inventory inventory){
        givePlayerItem(player, inventory.getItem(targetItemSlot));
        givePlayerItem(player, inventory.getItem(repairMaterialSlot));

        player.updateInventory();
    }

    private void givePlayerItem(Player player, ItemStack item){
        if(player != null && item != null && item.getType() != Material.AIR){
            if(player.getInventory().firstEmpty() != -1) {
                player.getInventory().addItem(item);
            }
            else{
                World world = player.getWorld();
                Item itemEntity = world.spawn(player.getLocation(), Item.class);
                itemEntity.setItemStack(item);
                NmsUtils.addOwner(RpgItemsPlugin.instance, itemEntity, player);

                RpgItemsOutputHandler.PrintInfo(player, "Your inventory is full, dropping at your feet");
            }
        }
    }

    private void setRepairMaterialItem(Player player, Inventory inv, ItemStack item, boolean returnOldItem){
        ItemStack oldItem = playerRepairMaterialItem.put(player, item);
        if(returnOldItem && oldItem != null && oldItem.getType() != Material.AIR)
            player.getInventory().addItem(oldItem);

        setInventorySlot(player, repairMaterialSlot, item);
        if(inv != null)
            inv.remove(item);

        updateResult(player);
    }

    private void setTargetItem(Player player, Inventory inv, ItemStack item, boolean returnOldItem){
        ItemStack oldItem = playerTargetRepairItem.put(player, item);
        if(returnOldItem && oldItem != null && oldItem.getType() != Material.AIR)
            player.getInventory().addItem(oldItem);

        setInventorySlot(player, targetItemSlot, item);
        if(inv != null)
            inv.remove(item);

        updateResult(player);
    }

    private void updateResult(Player player){
        ItemStack targetItem = playerTargetRepairItem.get(player);
        ItemStack repairMaterial = playerRepairMaterialItem.get(player);

        if(targetItem != null && repairMaterial != null &&
            targetItem.getType() != Material.AIR && repairMaterial.getType() != Material.AIR){
            RpgItemStack rpgItem = new RpgItemStack(targetItem.clone());
            rpgItem.addDurability(RepairManager.getRepairValue(repairMaterial));
            ItemStack result = rpgItem.getBukkitItem();

            setInventorySlot(player, resultSlot, result);
        }
        else{
            ItemStack empty = new ItemStack(Material.AIR);
            setInventorySlot(player, resultSlot, empty);
        }
    }

    private void repairItem(Player player, ClickType click){
        ItemStack targetRepairItem = playerTargetRepairItem.get(player);
        ItemStack repairMaterialItem = playerRepairMaterialItem.get(player);

        if(targetRepairItem != null && repairMaterialItem != null){
            Double durability = RepairManager.getRepairValue(repairMaterialItem);
            RpgItemStack rpgItem = new RpgItemStack(targetRepairItem);

            if(click == ClickType.LEFT){
                if(repairMaterialItem.getAmount() > 0 && !rpgItem.hasMaxDurability()){
                    rpgItem.addDurability(durability);
                    repairMaterialItem.setAmount(repairMaterialItem.getAmount() - 1);
                }
            }
            else if(click == ClickType.SHIFT_LEFT){
                while(repairMaterialItem.getAmount() > 0 && !rpgItem.hasMaxDurability()){
                    rpgItem.addDurability(durability);
                    repairMaterialItem.setAmount(repairMaterialItem.getAmount() - 1);
                }
            }

            ItemStack result = rpgItem.getBukkitItem();

            if(rpgItem.hasMaxDurability()){
                player.getInventory().addItem(result);
                setTargetItem(player, null, new ItemStack(Material.AIR), false);
            }
            else{
                setTargetItem(player, null, result, false);
            }

            setRepairMaterialItem(player, null, repairMaterialItem, false);
        }
    }

}
