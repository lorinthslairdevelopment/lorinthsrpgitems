package me.lorinth.rpgitems.gui;

import me.lorinth.rpgitems.enums.LevelHook;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.managers.ClassManager;
import me.lorinth.rpgitems.managers.ExperienceManager;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.Experienced;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.objects.character.CharacterClassInstance;
import me.lorinth.rpgitems.util.LevelHelper;
import me.lorinth.utils.MathHelper;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class RpgStatsMenu {

    private TemporaryIconMenu iconMenu;

    public RpgStatsMenu(Player player, boolean showZero){
        iconMenu = new TemporaryIconMenu(ChatColor.GREEN + player.getDisplayName() + "'s Item Stats", 4, this::click);
        RpgCharacter playerCharacter = PlayerCharacterManager.getCharacter(player);

        int row = 0; int slot = 0;

        if(LevelHelper.levelHook == LevelHook.BuiltIn){
            if(ClassManager.isEnabled()){
                CharacterClassInstance classInstance = playerCharacter.getCurrentCharacterClass();
                iconMenu.addButton(iconMenu.getRow(3), 8, new ItemStack(Material.NETHER_STAR, 1),
                        classInstance.getCharacterClass().getDisplayName() + ChatColor.GOLD + ": " + ChatColor.YELLOW + classInstance.getLevel(),
                        ChatColor.GOLD + "Experience: " + ChatColor.YELLOW +  classInstance.getExperience(),
                        ChatColor.GOLD + "Next Level: " + ChatColor.YELLOW +  ExperienceManager.getExpToNextLevel(classInstance.getLevel(), classInstance.getCharacterClass().getMetadata().ExperienceRequiredModifier));
            }
            else{
                Experienced info = playerCharacter.getInfo();
                iconMenu.addButton(iconMenu.getRow(3), 8, new ItemStack(Material.NETHER_STAR, 1),
                        ChatColor.GOLD + "Level: " + ChatColor.YELLOW + info.getLevel(),
                        ChatColor.GOLD + "Experience: " + ChatColor.YELLOW +  info.getExperience(),
                        ChatColor.GOLD + "Next Level: " + ChatColor.YELLOW +  ExperienceManager.getExpToNextLevel(info.getLevel()));
            }
        }
        for(RpgAttribute attribute : RpgAttribute.values()){
            if(attribute.isEnabled()){
                Object value = playerCharacter.getStats().get(attribute);
                String stringValue = attribute.getValueAsString(playerCharacter.getStats());

                if(attribute == RpgAttribute.Health)
                    stringValue = "" + player.getMaxHealth();

                if(!showZero && MathHelper.isZero(value)) {
                    continue;
                }
                else{
                    iconMenu.addButton(iconMenu.getRow(row), slot, new ItemStack(attribute.getDisplayMaterial(), 1), attribute.getDisplayName() + " : " + stringValue);

                    slot++;
                    if(slot >= 9) {
                        row++;
                        slot = 0;
                    }
                }
            }
        }

        iconMenu.open(player);
    }

    public boolean click(Player clicker, IconMenu menu, IconMenu.Row row, int slot, ItemStack item){
        return true;
    }

}
