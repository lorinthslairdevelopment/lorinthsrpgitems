package me.lorinth.rpgitems.gui;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.managers.SellManager;
import me.lorinth.rpgitems.objects.gui.SellGuiOptions;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.NmsUtils;
import me.lorinth.utils.MathHelper;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class SellMenu {

    private Player player;
    private SellGuiOptions options;
    private InventoryInteractiveMenu iconMenu;
    private boolean isDoubleConfirm = false;
    private boolean hasSold = false;
    private Double totalValue = 0.0;

    public SellMenu(Player player){
        this.player = player;
        options = SellManager.getSellGuiOptions();
        iconMenu = new InventoryInteractiveMenu(options.Title, options.Rows, this::click, this::playerInventoryClick, this::playerInventoryClose);
        updateTotal();
        iconMenu.getInventory(player).setItem(options.Slots.Confirm, options.Items.Confirm);
        iconMenu.getInventory(player).setItem(options.Slots.Cancel, options.Items.Cancel);
        iconMenu.open(player);
    }


    public boolean click(Player clicker, InventoryInteractiveMenu menu, InventoryInteractiveMenu.Row row, int slot, ItemStack item, ClickType click) {
        int clickedSlot = (row.row * 9 + slot);

        handleConfirm(clicker, clickedSlot);
        handleCancel(clickedSlot);
        handleRemoveItemFromSale(clicker, clickedSlot, item, click);

        return true;
    }

    private void handleConfirm(Player player, int clickedSlot){
        if(clickedSlot == options.Slots.Confirm) {
            if(options.UseDoubleConfirm){
                if(isDoubleConfirm){
                    handleFinalConfirm(player, getSoldItems(player));
                }
                else{
                    isDoubleConfirm = true;
                    iconMenu.getInventory(player).setItem(options.Slots.Confirm, options.Items.DoubleConfirm);
                    player.updateInventory();
                }
            }
            else{
                handleFinalConfirm(player, getSoldItems(player));
            }
        }
        else{
            if(isDoubleConfirm){
                isDoubleConfirm = false;
                iconMenu.getInventory(player).setItem(options.Slots.Confirm, options.Items.Confirm);
            }
        }
    }

    private void handleFinalConfirm(Player player, List<ItemStack> items){
        hasSold = true;
        Double value = SellManager.sellItems(player, items);
        if(!MathHelper.isZero(value)){
            player.sendMessage(SellManager.getSellMessages().Sold.replaceAll(Pattern.quote("{amount}"), String.format("%.2f", value)));
        }
        else{
            RpgItemsOutputHandler.PrintError("Error selling items");
        }
        iconMenu.close();
    }

    private void handleCancel(int clickedSlot){
        if(clickedSlot == options.Slots.Cancel){
            iconMenu.close();
        }
    }

    private void handleRemoveItemFromSale(Player p, int clickedSlot, ItemStack item, ClickType click){
        if(item == null || clickedSlot == options.Slots.Confirm || clickedSlot == options.Slots.Cancel || clickedSlot == options.Slots.Total)
            return;

        int amount = getAmountFromClickType(click, item);
        if(amount == 0)
            return;

        //Move item Up
        Inventory inventory = iconMenu.getInventory(p);
        ItemStack moved = item.clone();
        moved.setAmount(amount);
        if(amount == item.getAmount())
            inventory.remove(item);
        else
            item.setAmount(item.getAmount()-amount);

        //Update inventory items
        Inventory playerInventory = p.getInventory();
        playerInventory.addItem(moved);
        p.updateInventory();

        //Update Total Value
        totalValue -= SellManager.getValueOfItem(moved);
        updateTotal();
    }


    public boolean playerInventoryClick(Player clicker, Inventory inventory, int slot, ItemStack item, ClickType click) {
        if (item == null)
            return true;

        if(options.IgnoreHotbarClick && slot <= 8){
            return true;
        }

        handleAddItemToSale(clicker, inventory, slot, item, click);

        return true;
    }

    private void handleAddItemToSale(Player p, Inventory inventory, int slot, ItemStack item, ClickType click){
        int amount = getAmountFromClickType(click, item);
        if(amount == 0)
            return;

        //Move item Up
        ItemStack moved = item.clone();
        moved.setAmount(amount);
        if(amount == item.getAmount())
            inventory.remove(item);
        else
            item.setAmount(item.getAmount()-amount);

        //Update inventory items
        Inventory sellInventory = iconMenu.getInventory(p);
        sellInventory.addItem(moved);
        p.updateInventory();

        //Update Total Value
        totalValue += SellManager.getValueOfItem(moved);
        updateTotal();
    }

    private int getAmountFromClickType(ClickType clickType, ItemStack item){
        if(clickType == options.SingleSelectClick)
            return 1;
        else if(clickType == options.MultiSelectClick)
            return item.getAmount();

        return 0;
    }

    private String getTitle(){
        return options.Items.Total.getItemMeta().getDisplayName().replace("{total}", String.format("%.2f", totalValue));
    }

    private void updateTotal(){
        ItemStack newTotal = options.Items.Total.clone();
        ItemMeta meta = newTotal.getItemMeta();
        meta.setDisplayName(getTitle());
        newTotal.setItemMeta(meta);
        iconMenu.getInventory(player).setItem(options.Slots.Total, newTotal);
        player.updateInventory();
    }

    public void playerInventoryClose(Player player, Inventory inventory){
        if(hasSold)
            return;
        for(ItemStack item : getSoldItems(player)){
            givePlayerItem(player, item);
        }
    }

    private void givePlayerItem(Player player, ItemStack item){
        if(player != null && item != null && item.getType() != Material.AIR){
            if(player.getInventory().firstEmpty() != -1) {
                player.getInventory().addItem(item);
            }
            else{
                World world = player.getWorld();
                Item itemEntity = world.spawn(player.getLocation(), Item.class);
                itemEntity.setItemStack(item);
                NmsUtils.addOwner(RpgItemsPlugin.instance, itemEntity, player);

                RpgItemsOutputHandler.PrintInfo(player, "Your inventory is full, dropping at your feet");
            }
        }
    }

    private List<ItemStack> getSoldItems(Player player){
        if(hasSold)
            return new ArrayList<>();

        List<ItemStack> soldItems = new ArrayList<>();
        for(ItemStack item : iconMenu.getInventory(player).getContents()){
            if(item == null || item.getType() == Material.AIR)
                continue;
            if(item.hasItemMeta() && item.getItemMeta().getDisplayName().equalsIgnoreCase(getTitle()))
                continue;
            if(!item.isSimilar(options.Items.Confirm) && !item.isSimilar(options.Items.DoubleConfirm) && !item.isSimilar(options.Items.Cancel))
                soldItems.add(item);
        }
        return soldItems;
    }

}
