package me.lorinth.rpgitems.gui;

import org.bukkit.event.HandlerList;

public class TemporaryIconMenu extends IconMenu {

    public TemporaryIconMenu(String name, int size, onClick click) {
        super(name, size, click);
    }

    @Override
    protected void removeViewer(String name){
        super.removeViewer(name);

        if(viewing.isEmpty())
            HandlerList.unregisterAll(this);
    }
}