package me.lorinth.rpgitems.heroes;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.entity.Player;

public class HeroHandler {

    private Hero hero;
    private boolean isValid = true;

    public HeroHandler(Player player){
        try{
            hero = Heroes.getInstance().getCharacterManager().getHero(player);
        }
        catch(Exception exception){
            exception.printStackTrace();
            isValid = false;
        }
    }

    public boolean isValid(){
        return isValid;
    }

    public Hero getHero(){
        return hero;
    }

    public void addMana(int mana){
        hero.setMana(hero.getMana() + mana);
    }

    public void addStamina(int stamina){
        hero.setStamina(hero.getStamina() + stamina);
    }

}
