package me.lorinth.rpgitems.heroes;

import com.herocraftonline.heroes.attributes.AttributeType;
import com.herocraftonline.heroes.characters.effects.common.AttributeIncreaseEffect;

public class IncreasedAttributeEffect extends AttributeIncreaseEffect
{
    public IncreasedAttributeEffect(AttributeType type, int delta)
    {
        super(RpgItemsPassiveSkill.instance, "RpgItems_" + type.name(), null, Integer.MAX_VALUE, type, delta, null, null);
    }
}