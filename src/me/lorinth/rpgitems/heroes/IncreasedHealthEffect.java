package me.lorinth.rpgitems.heroes;

import com.herocraftonline.heroes.characters.effects.common.MaxHealthIncreaseEffect;

public class IncreasedHealthEffect extends MaxHealthIncreaseEffect
{
    public IncreasedHealthEffect(int delta)
    {
        super(RpgItemsPassiveSkill.instance, "RpgItems_Health", null, Integer.MAX_VALUE, Integer.valueOf(delta), null, null);
    }
}
