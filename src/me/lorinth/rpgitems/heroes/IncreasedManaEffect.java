package me.lorinth.rpgitems.heroes;

import com.herocraftonline.heroes.characters.effects.common.MaxManaIncreaseEffect;

public class IncreasedManaEffect extends MaxManaIncreaseEffect
{
    public IncreasedManaEffect(int delta)
    {
        super(RpgItemsPassiveSkill.instance, "RpgItems_Mana", null, Integer.MAX_VALUE, Integer.valueOf(delta), null, null);
    }
}
