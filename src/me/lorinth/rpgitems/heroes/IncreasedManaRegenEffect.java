package me.lorinth.rpgitems.heroes;

import com.herocraftonline.heroes.characters.effects.common.ManaRegenIncreaseEffect;

public class IncreasedManaRegenEffect extends ManaRegenIncreaseEffect {
    public IncreasedManaRegenEffect(int delta)
    {
        super(RpgItemsPassiveSkill.instance, "RpgItems_ManaRegen", null, Integer.MAX_VALUE, Integer.valueOf(delta), null, null);
    }
}
