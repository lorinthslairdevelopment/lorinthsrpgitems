package me.lorinth.rpgitems.heroes;

import com.herocraftonline.heroes.characters.effects.common.MaxStaminaIncreaseEffect;

public class IncreasedStaminaEffect extends MaxStaminaIncreaseEffect
{
    public IncreasedStaminaEffect(int delta)
    {
        super(RpgItemsPassiveSkill.instance, "RpgItems_Stamina", null, Integer.MAX_VALUE, Integer.valueOf(delta), null, null);
    }
}
