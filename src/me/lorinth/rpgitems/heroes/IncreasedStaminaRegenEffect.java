package me.lorinth.rpgitems.heroes;

import com.herocraftonline.heroes.characters.effects.common.StaminaRegenIncreaseEffect;

public class IncreasedStaminaRegenEffect extends StaminaRegenIncreaseEffect {
    public IncreasedStaminaRegenEffect(int delta)
    {
        super(RpgItemsPassiveSkill.instance, "RpgItems_StaminaRegen", null, Integer.MAX_VALUE, Integer.valueOf(delta), null, null);
    }
}
