package me.lorinth.rpgitems.heroes;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;

public class RpgItemsPassiveSkill extends PassiveSkill {

    public static RpgItemsPassiveSkill instance;

    public RpgItemsPassiveSkill(Heroes plugin) {
        super(plugin, "RpgItemsPassiveSkill");
        instance = this;
    }

    @Override
    public String getDescription(Hero hero) {
        return "Passive skill that provides the bonuses from Rpg Items to your hero!";
    }
}
