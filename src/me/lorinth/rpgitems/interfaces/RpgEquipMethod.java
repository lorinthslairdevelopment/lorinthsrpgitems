package me.lorinth.rpgitems.interfaces;

import me.lorinth.rpgitems.objects.RpgItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface RpgEquipMethod {

    RpgItemStack equip(ItemStack item, Player player);

}
