package me.lorinth.rpgitems.listener;

import fr.xephi.authme.events.LoginEvent;
import fr.xephi.authme.events.LogoutEvent;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class AuthMePlayerListener implements Listener {

    @EventHandler
    public void onPlayerLogin(LoginEvent loginEvent){
        Player player = loginEvent.getPlayer();
        PlayerCharacterManager.loadCharacter(player);
    }

    @EventHandler
    public void onPlayerLogout(LogoutEvent logoutEvent){
        Player player = logoutEvent.getPlayer();
        PlayerCharacterManager.unloadCharacter(player);
    }

}
