package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.DamageEffectType;
import me.lorinth.rpgitems.enums.EffectType;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.managers.CombatManager;
import me.lorinth.rpgitems.managers.EffectManager;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.*;
import me.lorinth.rpgitems.tasks.LastingEffect;
import me.lorinth.utils.BooleanCalculator;
import me.lorinth.rpgitems.util.FormulaHelper;
import me.lorinth.rpgitems.util.SettingsHelper;
import me.lorinth.utils.Calculator;
import me.lorinth.utils.FormulaMethod;
import me.lorinth.utils.javascript.JavascriptEngine;
import org.bukkit.Bukkit;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.projectiles.ProjectileSource;

public class CombatListener implements Listener{

    private CombatManager combatManager = CombatManager.instance;

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPvpEvent(EntityDamageByEntityEvent event){
        Entity target = event.getEntity();
        Entity damager = event.getDamager();
        if(damager instanceof Projectile){
            ProjectileSource source = ((Projectile) damager).getShooter();
            if(source instanceof Entity)
                damager = (Entity) source;
        }

        if(target instanceof Player)
            combatManager.markPlayerInCombat((Player) target);
        if(damager instanceof Player)
            combatManager.markPlayerInCombat((Player) damager);

        boolean isPvP = target instanceof Player && damager instanceof Player;
        if(isPvP){
            Player targetPlayer = (Player) target;
            Player damagingPlayer = (Player) damager;

            if(!combatManager.playerPvpEnabled(targetPlayer) || !combatManager.playerPvpEnabled(damagingPlayer)){
                event.setCancelled(true);

                String message = combatManager.getCantAttackPlayerMessage();
                if(!message.equalsIgnoreCase("") && !message.equalsIgnoreCase("N/A"))
                    damagingPlayer.sendMessage(message);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onTridentThrow(ProjectileLaunchEvent event){
        if(event.getEntity() instanceof Trident){

        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onDamageEvent(EntityDamageByEntityEvent event){
        if(event.getCause() == EntityDamageEvent.DamageCause.THORNS)
            return;
        Entity damager = event.getDamager();
        if(damager instanceof Projectile)
            damager = (Entity) ((Projectile) damager).getShooter();

        Entity target = event.getEntity();

        if(damager instanceof LivingEntity && target instanceof LivingEntity)
            handleDamageEvent(event, (LivingEntity) damager, (LivingEntity) target);
    }

    private void handleDamageEvent(EntityDamageByEntityEvent event, LivingEntity attacker, LivingEntity defender){
        boolean playerAttacker = attacker instanceof Player;
        boolean playerDefender = defender instanceof Player;
        boolean isPvp = playerAttacker && playerDefender;

        if(!playerAttacker && !playerDefender)
            return;

        DamageEventStatistics eventStatistics = new DamageEventStatistics();
        eventStatistics.eventDamage = event.getDamage();
        eventStatistics.totalDamage = event.getDamage();

        if(attacker instanceof Player){
            RpgCharacter character = PlayerCharacterManager.getCharacter((Player) attacker);
            AttributeMap attrMap = character.getStats();
            if(RpgAttribute.Damage.isEnabled())
                eventStatistics.bonusDamage = RpgAttribute.Damage.isRange() ? ((ValueRange) attrMap.get(RpgAttribute.Damage)).getValue() : (Double) attrMap.get(RpgAttribute.Damage);
            if(RpgAttribute.Penetration.isEnabled())
                eventStatistics.penetration = (Double) attrMap.get(RpgAttribute.Penetration);
            if(RpgAttribute.PvEDamage.isEnabled())
                eventStatistics.pveDamage = (Double) attrMap.get(RpgAttribute.PvEDamage);
            if(RpgAttribute.PvPDamage.isEnabled())
                eventStatistics.pvpDamage = (Double) attrMap.get(RpgAttribute.PvPDamage);
            if(RpgAttribute.CriticalChance.isEnabled())
                eventStatistics.criticalChance = (Double) attrMap.get(RpgAttribute.CriticalChance);
            if(RpgAttribute.CriticalDamage.isEnabled())
                eventStatistics.criticalDamage = (Double) attrMap.get(RpgAttribute.CriticalDamage);
            if(RpgAttribute.Vampirism.isEnabled())
                eventStatistics.vampirism = (Double) attrMap.get(RpgAttribute.Vampirism);
            if(RpgAttribute.Accuracy.isEnabled())
                eventStatistics.accuracy = (Double) attrMap.get(RpgAttribute.Accuracy);
            if(RpgAttribute.BurnChance.isEnabled())
                eventStatistics.burnChance = (Double) attrMap.get(RpgAttribute.BurnChance);
            if(RpgAttribute.BurnDamage.isEnabled())
                eventStatistics.burnDamage = (Double) attrMap.get(RpgAttribute.BurnDamage);
            if(RpgAttribute.PoisonChance.isEnabled())
                eventStatistics.poisonChance = (Double) attrMap.get(RpgAttribute.PoisonChance);
            if(RpgAttribute.PoisonDamage.isEnabled())
                eventStatistics.poisonDamage = (Double) attrMap.get(RpgAttribute.PoisonDamage);
        }

        if(defender instanceof Player) {
            RpgCharacter character = PlayerCharacterManager.getCharacter((Player) defender);
            AttributeMap attrMap = character.getStats();
            if (RpgAttribute.Defense.isEnabled())
                eventStatistics.defense = (Double) attrMap.get(RpgAttribute.Defense);
            if (RpgAttribute.PvEDefense.isEnabled())
                eventStatistics.pveDefense = (Double) attrMap.get(RpgAttribute.PvEDefense);
            if (RpgAttribute.PvPDefense.isEnabled())
                eventStatistics.pvpDefense = (Double) attrMap.get(RpgAttribute.PvPDefense);
            if (RpgAttribute.DodgeChance.isEnabled())
                eventStatistics.dodgeChance = (Double) attrMap.get(RpgAttribute.DodgeChance);
            if (RpgAttribute.BlockChance.isEnabled() && RpgAttribute.BlockDamage.isEnabled()) {
                eventStatistics.blockChance = (Double) attrMap.get(RpgAttribute.BlockChance);
                eventStatistics.blockDamage = (Double) attrMap.get(RpgAttribute.BlockDamage);
            }
            if(((Player) defender).isBlocking()){
                eventStatistics.isBlocking = 1.0d;
            }
        }

        if(SettingsHelper.formulaMethod == FormulaMethod.Java){
            eventStatistics.totalDamage = Calculator.eval(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.BaseDamage, eventStatistics));
            if(isPvp){
                eventStatistics.pvpDamageResult = Calculator.eval(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.PvPDamage, eventStatistics));
                eventStatistics.pvpDefenseModifier = Calculator.eval(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.PvPDefenseModifier, eventStatistics));
                eventStatistics.totalDamage = Calculator.eval(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.TotalPvPDamage, eventStatistics));
            }
            else{
                eventStatistics.pveDamageResult = Calculator.eval(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.PvEDamage, eventStatistics));
                eventStatistics.pveDefenseModifier = Calculator.eval(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.PvEDefenseModifier, eventStatistics));
                eventStatistics.totalDamage = Calculator.eval(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.TotalPvEDamage, eventStatistics));
            }

            if(BooleanCalculator.evaluate(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.CriticalChance, eventStatistics))) {
                eventStatistics.totalDamage = Calculator.eval(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.CriticalDamage, eventStatistics));

                EffectInfo info = EffectManager.getEffectInfo(EffectType.CRITICAL);
                if(info != null)
                    info.play(attacker, defender, null);
            }

            if(BooleanCalculator.evaluate(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.DodgeChance, eventStatistics))){
                event.setCancelled(true);
                defender.setNoDamageTicks(14);

                EffectInfo info = EffectManager.getEffectInfo(EffectType.DODGE);
                if(info != null)
                    info.play(defender, attacker, null);
                return;
            }

            if(BooleanCalculator.evaluate(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.BlockChance, eventStatistics))) {
                eventStatistics.totalDamage = Calculator.eval(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.BlockDamage, eventStatistics));

                EffectInfo info = EffectManager.getEffectInfo(EffectType.BLOCK);
                if(info != null)
                    info.play(defender, attacker, null);
            }

            if(BooleanCalculator.evaluate(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.BurnChance, eventStatistics))) {
                new LastingEffect(new DamageEffectInfo(attacker, defender, eventStatistics.burnDamage, DamageEffectType.BURNING, 5));

                EffectInfo info = EffectManager.getEffectInfo(EffectType.BURN);
                if(info != null)
                    info.play(attacker, defender, null);
            }

            if(BooleanCalculator.evaluate(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.PoisonChance, eventStatistics))) {
                new LastingEffect(new DamageEffectInfo(attacker, defender, eventStatistics.poisonDamage, DamageEffectType.POISON, 5));
                EffectInfo info = EffectManager.getEffectInfo(EffectType.POISON);
                if(info != null)
                    info.play(attacker, defender, null);
            }

            double vampAmount = Calculator.eval(SettingsHelper.formulaMethod, FormulaHelper.parseWithStats(FormulaHelper.VampirismAmount, eventStatistics));
            attacker.setHealth(Math.min(attacker.getMaxHealth(), attacker.getHealth() + vampAmount));

            double damage = Math.max(0.1, eventStatistics.totalDamage);
            event.setDamage(damage);

            if(defender instanceof Player){
                //Is Blocking fix
                if(((Player) defender).isHandRaised() && ((Player) defender).isBlocking()){
                    Bukkit.getScheduler().runTaskLater(RpgItemsPlugin.instance, () -> {
                        defender.setNoDamageTicks(10);
                        defender.setHealth(Math.max(0.0d, defender.getHealth() - damage));

                        CauseDamageEffect(defender, damage);
                    }, 1);
                }
            }
        }
        else if(SettingsHelper.formulaMethod == FormulaMethod.Javascript){
            JavascriptEngine.put("combatEvent", event);
            JavascriptEngine.put("eventStats", eventStatistics);
            JavascriptEngine.put("attacker", attacker);
            JavascriptEngine.put("defender", defender);
            JavascriptEngine.put("isPvP", isPvp);
            JavascriptEngine.eval("onCombat(combatEvent, eventStats, attacker, defender, isPvP)");
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onGeneralDamageEvent(EntityDamageEvent event){
        if(event.getDamage() > 0.0d){
            CauseDamageEffect(event.getEntity(), event.getFinalDamage());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onRegainHealth(EntityRegainHealthEvent event){
        if(event.getAmount() >= 0.5d){
            EffectInfo info = EffectManager.getEffectInfo(EffectType.HEAL);
            if(info != null)
                info.play(event.getEntity(), event.getAmount());
        }
    }

    private void CauseDamageEffect(Entity entity, Double damage){
        EffectInfo info = EffectManager.getEffectInfo(EffectType.DAMAGE);
        if(info != null)
            info.play(entity, damage);
    }

}
