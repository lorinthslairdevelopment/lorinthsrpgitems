package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.objects.consumable.RpgConsumable;
import me.lorinth.rpgitems.util.ConsumableHelper;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class ConsumableListener implements Listener {

    public ArrayList<Player> playersEating = new ArrayList<>();

    @EventHandler
    public void onPlayerConsume(PlayerInteractEvent event){
        Player player = event.getPlayer();

        if(playersEating.contains(player)) {
            event.setCancelled(true);
            return;
        }

        if(!TryConsume(event, player, player.getEquipment().getItemInMainHand()))
            TryConsume(event, player, player.getEquipment().getItemInOffHand());

    }

    @EventHandler
    public void onPlayerConsumeItem(PlayerItemConsumeEvent event){
        if(event.getItem().getType() == Material.MILK_BUCKET){
            RpgCharacter character = PlayerCharacterManager.getCharacter(event.getPlayer());
            if(character == null)
                return;

            character.removeAllBuffs();
        }
    }

    private boolean TryConsume(PlayerInteractEvent event, Player player, ItemStack item){
        RpgConsumable consumable = ConsumableHelper.getConsumableIdFromItemStack(item);
        if(consumable == null)
            return false;

        //If it is a consumable dont let anything else happen
        event.setCancelled(true);

        RpgCharacter character = PlayerCharacterManager.getCharacter(player);
        if(character == null)
            return false;

        if(!consumable.canConsume(character)){
            return false;
        }

        if(!character.canAddBuff(consumable.getBuff())){
            return false;
        }

        playersEating.add(player);
        item.setAmount(item.getAmount()-1);
        consumable.consume(character);

        if(consumable.getEatingDuration() <= 0)
            Bukkit.getScheduler().runTaskLater(RpgItemsPlugin.instance, () -> playersEating.remove(player), 10L);
        else
            Bukkit.getScheduler().runTaskLater(RpgItemsPlugin.instance, () -> playersEating.remove(player), consumable.getEatingDurationInTicks() + 5L);

        return true;
    }
}
