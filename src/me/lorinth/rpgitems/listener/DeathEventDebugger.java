package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.objects.DeathEventLootInfo;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.plugin.RegisteredListener;

import java.util.ArrayList;
import java.util.HashMap;

public class DeathEventDebugger implements Listener {

    private static HashMap<Player, DeathEventLootInfo> playerDeathEventLootInfoHashMap = new HashMap<>();

    public DeathEventDebugger(){
        RpgItemsOutputHandler.PrintInfo("Plugin Listeners listening to EntityDeathEvent");
        for(RegisteredListener listener : EntityDeathEvent.getHandlerList().getRegisteredListeners()){
            RpgItemsOutputHandler.PrintInfo(listener.getPlugin().getName() + ": " + listener.getListener().getClass().getName() + " - " + listener.getPriority());
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDeathEventLowest(EntityDeathEvent event){
        if(event.getEntity().getKiller() != null){
            playerDeathEventLootInfoHashMap.put(event.getEntity().getKiller(), new DeathEventLootInfo(event, new HashMap<>()));
            playerDeathEventLootInfoHashMap.get(event.getEntity().getKiller()).getItems().put(EventPriority.LOWEST, new ArrayList<>(event.getDrops()));
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onDeathEventLow(EntityDeathEvent event){
        if(event.getEntity().getKiller() != null)
            playerDeathEventLootInfoHashMap.get(event.getEntity().getKiller()).getItems().put(EventPriority.LOW, new ArrayList<>(event.getDrops()));
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onDeathEventNormal(EntityDeathEvent event){
        if(event.getEntity().getKiller() != null)
            playerDeathEventLootInfoHashMap.get(event.getEntity().getKiller()).getItems().put(EventPriority.NORMAL, new ArrayList<>(event.getDrops()));
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onDeathEventHigh(EntityDeathEvent event){
        if(event.getEntity().getKiller() != null)
            playerDeathEventLootInfoHashMap.get(event.getEntity().getKiller()).getItems().put(EventPriority.HIGH, new ArrayList<>(event.getDrops()));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDeathEventHighest(EntityDeathEvent event){
        if(event.getEntity().getKiller() != null)
            playerDeathEventLootInfoHashMap.get(event.getEntity().getKiller()).getItems().put(EventPriority.HIGHEST, new ArrayList<>(event.getDrops()));
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDeathEventMonitor(EntityDeathEvent event){
        if(event.getEntity().getKiller() != null)
            playerDeathEventLootInfoHashMap.get(event.getEntity().getKiller()).getItems().put(EventPriority.MONITOR, new ArrayList<>(event.getDrops()));
    }

    public static DeathEventLootInfo getLatestEventData(Player player){
        return playerDeathEventLootInfoHashMap.get(player);
    }

}
