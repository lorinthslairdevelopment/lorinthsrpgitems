package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.enums.DurabilityDamageType;
import me.lorinth.rpgitems.enums.EquipmentSlot;
import me.lorinth.rpgitems.managers.DurabilityManager;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.objects.RpgItemStack;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;

public class DurabilityListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerAttack(EntityDamageByEntityEvent event){
        if(event.getDamager() instanceof Player){
            Player player = (Player) event.getDamager();
            RpgCharacter character = PlayerCharacterManager.getCharacter(player);
            DurabilityManager.causeDurabilityDamage(DurabilityDamageType.Melee_Attack, character.getEquipment());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onProjectileShootEvent(ProjectileLaunchEvent event){
        if(event.getEntity() instanceof Arrow){
            Arrow arrow = (Arrow) event.getEntity();
            if(arrow.getShooter() instanceof Player){
                Player player = (Player) arrow.getShooter();
                RpgCharacter character = PlayerCharacterManager.getCharacter(player);
                DurabilityManager.causeDurabilityDamage(DurabilityDamageType.Ranged_Shoot, character.getEquipment());
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerHit(EntityDamageByEntityEvent event){
        if(event.getEntity() instanceof Player){
            //Decrement Armor Durability
            Player player = (Player) event.getEntity();
            RpgCharacter character = PlayerCharacterManager.getCharacter(player);
            if(player.isBlocking()){
                DurabilityManager.causeDurabilityDamage(DurabilityDamageType.Shield_Block, character.getEquipment());
            }
            else{
                DurabilityManager.causeDurabilityDamage(DurabilityDamageType.Armor_Damage, character.getEquipment());
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event){
        Player player = event.getPlayer();
        Block block = event.getBlock();
        RpgCharacter rpgCharacter = PlayerCharacterManager.getCharacter(player);

        RpgItemStack rpgItemStack = rpgCharacter.getEquipment().getItemInSlot(EquipmentSlot.MAIN_HAND);
        if(rpgItemStack != null && rpgItemStack.isBroken()){
            event.setCancelled(true);
            RpgItemsOutputHandler.PrintError(player, "Unable to use tool because it is broken");
            return;
        }

        if(rpgCharacter != null)
            DurabilityManager.causeBlockBreakDamage(player.getEquipment().getItemInMainHand(), block, rpgCharacter.getEquipment());
    }
}
