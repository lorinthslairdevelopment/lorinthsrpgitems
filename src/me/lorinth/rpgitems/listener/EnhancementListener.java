package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.gui.EnhancementMenu;
import me.lorinth.rpgitems.managers.EnhancementManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class EnhancementListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event){
        if(!EnhancementManager.useMultiBlockStructure)
            return;

        if(EnhancementManager.getEnhancementStructure().isKeystone(event.getBlock())){
            if(EnhancementManager.getEnhancementStructure().createdMultiblockStructure(event.getBlock())){
                event.getPlayer().sendMessage(EnhancementManager.createdMultiblockStructure);
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event){
        if(!EnhancementManager.useMultiBlockStructure)
            return;

        if(EnhancementManager.getEnhancementStructure().destroyMultiblockStructure(event.getBlock())){
            event.getPlayer().sendMessage(EnhancementManager.destroyedMultiblockStructure);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockUse(PlayerInteractEvent event){
        if(!EnhancementManager.useMultiBlockStructure) {
            return;
        }
        if(event.getAction() != Action.RIGHT_CLICK_BLOCK || event.getPlayer().isSneaking()) {
            return;
        }

        if(EnhancementManager.getEnhancementStructure().isMultiblockStructure(event.getClickedBlock())){
            event.setCancelled(true);

            new EnhancementMenu(event.getPlayer());
        }
    }

}
