package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.enums.MonsterExperienceHook;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.events.PlayerExperienceGainEvent;
import me.lorinth.rpgitems.managers.EffectManager;
import me.lorinth.rpgitems.managers.ExperienceManager;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.DamageEffectInfo;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.tasks.LastingEffect;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.List;

public class ExperienceListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityDeath(EntityDeathEvent event){
        if(ExperienceManager.getMonsterExperienceHook() == MonsterExperienceHook.None ||
                ExperienceManager.getMonsterExperienceHook() == MonsterExperienceHook.LorinthsRpgMobs)
            return;

        EntityDamageEvent lastDamageEvent = event.getEntity().getLastDamageCause();

        Player killer = event.getEntity().getKiller();
        killer = killer != null ? killer : getKillerFromTamedEntity(lastDamageEvent);
        killer = killer != null ? killer : getKillerFromLastEffect(event.getEntity());

        if(killer != null && killer.getGameMode() != GameMode.CREATIVE && killer.getGameMode() != GameMode.SPECTATOR) {
            RpgCharacter character = PlayerCharacterManager.getCharacter(killer);
            Integer experience = 0;

            if(ExperienceManager.getMonsterExperienceHook() == MonsterExperienceHook.Vanilla)
                experience = event.getDroppedExp();
            else if(ExperienceManager.getMonsterExperienceHook() == MonsterExperienceHook.BuiltIn)
                experience = ExperienceManager.getExpForEntity(event.getEntity());

            PlayerExperienceGainEvent expEvent = new PlayerExperienceGainEvent(character, event.getEntity(), experience);
            Bukkit.getPluginManager().callEvent(expEvent);

            if(!expEvent.isCancelled()) {
                if(RpgAttribute.ExpGain.isPercent()){
                    Double modifier = ((Double) character.getStats().getOrDefault(RpgAttribute.ExpGain, 0d) + 100d) / 100d;
                    experience = (int) (experience * modifier);
                }
                else
                    experience = (int) (experience + ((Double) character.getStats().getOrDefault(RpgAttribute.ExpGain, 0d)));

                character.addExp(experience);
            }
        }
    }

    private Player getKillerFromTamedEntity(EntityDamageEvent lastDamageEvent){
        if(lastDamageEvent instanceof EntityDamageByEntityEvent){
            Entity damager = ((EntityDamageByEntityEvent) lastDamageEvent).getDamager();
            if(damager instanceof Player){
                return (Player) damager;
            }
            if(damager instanceof Tameable){
                AnimalTamer tamer = ((Tameable) damager).getOwner();
                if(tamer instanceof Player)
                    return (Player) tamer;
            }
        }

        return null;
    }

    private Player getKillerFromLastEffect(Entity entity){
        List<LastingEffect> effects = EffectManager.getLastingEffects(entity);
        if(effects.size() == 0)
            return null;
        else{
            for(int i = effects.size()-1; i >= 0; i--){
                LastingEffect effect = effects.get(i);
                DamageEffectInfo info = effect.getDamageEffectInfo();
                LivingEntity owner = info.getOwner();
                if(owner != null && owner instanceof Player)
                    return (Player) owner;
            }
        }

        return null;
    }

}
