package me.lorinth.rpgitems.listener;

import com.meowj.langutils.lang.LanguageHelper;
import me.lorinth.rpgitems.enums.ShowDroppedItemNames;
import me.lorinth.rpgitems.util.GlowHelper;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.rpgitems.util.SettingsHelper;
import org.bukkit.Bukkit;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

public class ItemDropDisplayListener implements Listener {

    private String dropLocale = "en_us";
    private ShowDroppedItemNames showDroppedItemNames = ShowDroppedItemNames.Named;

    public ItemDropDisplayListener(ShowDroppedItemNames showDroppedItemNames, String locale){
        if(showDroppedItemNames != null)
            this.showDroppedItemNames = showDroppedItemNames;
        if(locale != null && !locale.equalsIgnoreCase(""))
            dropLocale = locale;

        if(Bukkit.getPluginManager().getPlugin("LangUtils") == null && showDroppedItemNames == ShowDroppedItemNames.All) {
            this.showDroppedItemNames = ShowDroppedItemNames.Named;
            RpgItemsOutputHandler.PrintError("Unable to use " + RpgItemsOutputHandler.HIGHLIGHT + "ShowDroppedItemNames: All " + RpgItemsOutputHandler.ERROR + " LangUtils required!!!");
        }else{
            RpgItemsOutputHandler.PrintInfo("Using Item Drop Displays (" + dropLocale + ")");
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onItemDrop(ItemSpawnEvent event){
        if(showDroppedItemNames != ShowDroppedItemNames.None)
            itemName(event.getEntity(), null);
        if(SettingsHelper.itemGlow)
            GlowHelper.AddGlowToItemEntity(event.getEntity());
    }

    @EventHandler(ignoreCancelled = true)
    public void onItemPickup(PlayerPickupItemEvent event){
        Item target = event.getItem();
        ItemStack stack = target.getItemStack();

        String name = null;
        if(stack.hasItemMeta())
            if(stack.getItemMeta().hasDisplayName())
                name = stack.getItemMeta().getDisplayName();
        if(name == null && showDroppedItemNames == ShowDroppedItemNames.All)
            name = LanguageHelper.getItemName(stack, dropLocale);

        if(event.getRemaining() > 1)
            name += " x" + event.getRemaining();

        target.setCustomName(name);
        target.setCustomNameVisible(true);
    }

    @EventHandler(ignoreCancelled = true)
    public void onItemJoin(ItemMergeEvent event){
        itemName(event.getTarget(), event.getEntity());
    }

    private void itemName(Item target, Item joined){
        if(showDroppedItemNames == ShowDroppedItemNames.None)
            return;

        ItemStack stack = target.getItemStack();
        ItemStack otherStack = null;
        if(joined != null)
            otherStack = joined.getItemStack();

        String name = null;
        if(stack.hasItemMeta())
            if(stack.getItemMeta().hasDisplayName())
                name = stack.getItemMeta().getDisplayName();
        if(name == null && showDroppedItemNames == ShowDroppedItemNames.All)
            name = LanguageHelper.getItemName(stack, dropLocale);

        if(name == null)
            return;
        if(stack.getAmount() > 1 || (otherStack != null && otherStack.getAmount() >= 1))
            name += " x" + (stack.getAmount() + (otherStack != null ? otherStack.getAmount() : 0));

        target.setCustomName(name);
        target.setCustomNameVisible(true);
    }

}
