package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.events.ArmorEquipEvent;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.RpgItemStack;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;

public class ItemEquipListener implements Listener{

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onArmorEquip(ArmorEquipEvent event){
        event.setOldArmorPiece(resetGearSetLore(event.getOldArmorPiece()));
        triggerPlayerUpdate(event.getPlayer());
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onWeaponChange(PlayerItemHeldEvent event){
        Player player = event.getPlayer();
        PlayerInventory inv = player.getInventory();
        inv.setItem(event.getPreviousSlot(), resetGearSetLore(inv.getItem(event.getPreviousSlot())));
        player.updateInventory();
        triggerPlayerUpdate(event.getPlayer());
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onItemChange(PlayerSwapHandItemsEvent event){
        Player player = event.getPlayer();
        EntityEquipment equipment = player.getEquipment();
        if(equipment != null){
            equipment.setItemInMainHand(resetGearSetLore(equipment.getItemInMainHand()));
            equipment.setItemInOffHand(resetGearSetLore(equipment.getItemInOffHand()));
            player.updateInventory();
        }
        triggerPlayerUpdate(event.getPlayer());
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onHandItemClicked(InventoryClickEvent event){
        if(event.getClickedInventory() instanceof PlayerInventory &&
                event.getWhoClicked() instanceof Player){
            Player player = (Player) event.getWhoClicked();
            if(player.getInventory().getHeldItemSlot() == event.getSlot()){
                EntityEquipment equipment = player.getEquipment();
                if(equipment != null){
                    equipment.setItemInMainHand(resetGearSetLore(equipment.getItemInMainHand()));
                    player.updateInventory();
                }
                triggerPlayerUpdate(player);
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onMainHandChanged(InventoryClickEvent event){
        Player player = (Player) event.getWhoClicked();
        PlayerInventory inventory = player.getInventory();
        if(event.getSlotType() == InventoryType.SlotType.CONTAINER){
            if(event.getClick() == ClickType.SHIFT_LEFT || event.getClick() == ClickType.SHIFT_RIGHT){
                int openSlot = inventory.firstEmpty();
                if(openSlot < 9 && openSlot == inventory.getHeldItemSlot()){
                    Bukkit.getScheduler().scheduleSyncDelayedTask(RpgItemsPlugin.instance,
                            () -> PlayerCharacterManager.getCharacter(player).update(), 1);
                }
            }
        }
    }

    private void triggerPlayerUpdate(Player player){
        Bukkit.getScheduler().scheduleSyncDelayedTask(RpgItemsPlugin.instance,
                () -> PlayerCharacterManager.getCharacter(player).update(), 1);
    }

    private ItemStack resetGearSetLore(ItemStack item){
        if(item == null)
            return null;
        RpgItemStack rpgItemStack = new RpgItemStack(item);
        rpgItemStack.updateGearSetLore(new HashMap<>());
        return rpgItemStack.getBukkitItem();
    }

}
