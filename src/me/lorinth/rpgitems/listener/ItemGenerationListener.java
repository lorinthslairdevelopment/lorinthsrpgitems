package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.managers.ItemGenerationManager;
import me.lorinth.rpgitems.managers.LootManager;
import me.lorinth.rpgitems.managers.RarityManager;
import me.lorinth.rpgitems.managers.SocketManager;
import me.lorinth.rpgitems.objects.RpgGeneratedItem;
import me.lorinth.rpgitems.objects.sockets.SocketGem;
import me.lorinth.rpgitems.util.GlowHelper;
import me.lorinth.rpgitems.util.LevelHelper;
import me.lorinth.rpgitems.util.SettingsHelper;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemGenerationListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityDeathEvent(EntityDeathEvent event){
        if(event.getEntity().getKiller() != null && event.getEntity() instanceof Monster){
            int level = LevelHelper.getMonsterLevel(event.getEntity());

            dropRpgItem(level, event);
            dropSocketGem(level, event);
        }
    }

    private void dropRpgItem(Integer level, EntityDeathEvent event){
        RpgGeneratedItem generatedItem = LootManager.getRpgItemDrop(level);
        if(generatedItem == null) {
            return;
        }

        if(SettingsHelper.itemGlow) {
            GlowHelper.DropWithGlow(event.getEntity().getLocation(), generatedItem.createItemStack());
        }
        else {
            event.getDrops().add(generatedItem.createItemStack());
        }
    }

    private void dropSocketGem(Integer level, EntityDeathEvent event){
        SocketGem gem = SocketManager.GetNextSocketGem(level);
        if(gem == null){
            return;
        }

        event.getDrops().add(gem.getItemStack());
    }


    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onItemCraftEvent(CraftItemEvent event){
        ItemStack result = event.getCurrentItem();
        if(result.getDurability() != 0){
            return;
        }
        else if(result.hasItemMeta()){
            ItemMeta meta = result.getItemMeta();
            if(meta.hasLore())
                return;
        }
        else if(ItemGenerationManager.isEquipmentItem(event.getCurrentItem())){
            RpgGeneratedItem item = new RpgGeneratedItem(event.getCurrentItem(), LevelHelper.getPlayerLevel((Player) event.getWhoClicked()));
            item.setRarityTier(RarityManager.getNextCraftedRarity(item.getLevel(), result));
            event.setCurrentItem(item.createItemStack());
        }
    }

}
