package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.managers.ItemGenerationManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.inventory.ItemStack;

public class ItemGenerationTagListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onDropGeneration(ItemSpawnEvent event){
        ItemStack itemStack = event.getEntity().getItemStack();

        ItemStack newItemStack = ItemGenerationManager.applyGenerationTags(itemStack);
        if(newItemStack != null)
            event.getEntity().setItemStack(itemStack);
    }

}
