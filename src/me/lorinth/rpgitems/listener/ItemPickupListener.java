package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.enums.RpgConsumableAction;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.consumable.RpgConsumable;
import me.lorinth.rpgitems.objects.consumable.RpgConsumableEvents;
import me.lorinth.rpgitems.util.ConsumableHelper;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.inventory.ItemStack;

public class ItemPickupListener implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void onPickup(EntityPickupItemEvent event){
        if(!(event.getEntity() instanceof Player))
            return;

        Player player = (Player) event.getEntity();
        Item item = event.getItem();
        ItemStack itemStack = item.getItemStack();

        RpgConsumable consumable = ConsumableHelper.getConsumableIdFromItemStack(itemStack);
        if(consumable == null)
            return;

        event.setCancelled(true);

        RpgConsumableEvents events = consumable.getEvents();
        if(events.OnPickup != null){
            ConsumableHelper.handleEvents(PlayerCharacterManager.getCharacter(player), consumable, events.OnPickup);
            if(events.OnPickup == RpgConsumableAction.CONSUME)
                item.remove();
        }
    }

}
