package me.lorinth.rpgitems.listener;

import me.lorinth.rpgmobs.Events.RpgItemsGetLevelEvent;
import me.lorinth.rpgmobs.Events.RpgMobDeathEvent;
import me.lorinth.rpgitems.enums.MonsterExperienceHook;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.managers.EffectManager;
import me.lorinth.rpgitems.managers.ExperienceManager;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.DamageEffectInfo;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.tasks.LastingEffect;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.util.List;

public class LorinthsRpgMobsListener implements Listener {

    @EventHandler
    public void onGetRpgLevel(RpgItemsGetLevelEvent event){
        event.setLevel(PlayerCharacterManager.getCharacter(event.getPlayer()).getLevel());
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onRpgMobsDeath(RpgMobDeathEvent event){
        Player killer = event.getKiller();
        if(event.getKiller() == null){
            killer = getKillerFromLastEffect(event.getEntity());
        }

        if(killer != null){
            RpgCharacter rpgCharacter = PlayerCharacterManager.getCharacter(killer);
            if(rpgCharacter != null) {
                if(RpgAttribute.GoldGain.isEnabled()){
                    Double goldGain = (Double) rpgCharacter.getStats().get(RpgAttribute.GoldGain);
                    double currency = (event.getCurrencyValue() * 100.0 + goldGain) / 100.0;
                    event.setCurrencyValue(currency);
                }

                if(event.getExp() > 0 && ExperienceManager.getMonsterExperienceHook() == MonsterExperienceHook.LorinthsRpgMobs) {
                    rpgCharacter.addExp(event.getExp());
                }
            }
        }
    }

    private Player getKillerFromLastEffect(Entity entity){
        List<LastingEffect> effects = EffectManager.getLastingEffects(entity);
        if(effects.size() == 0)
            return null;
        else{
            for(int i = effects.size()-1; i >= 0; i--){
                LastingEffect effect = effects.get(i);
                DamageEffectInfo info = effect.getDamageEffectInfo();
                LivingEntity owner = info.getOwner();
                if(owner != null && owner instanceof Player)
                    return (Player) owner;
            }
        }

        return null;
    }

}
