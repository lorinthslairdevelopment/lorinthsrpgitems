package me.lorinth.rpgitems.listener;

import com.gmail.nossr50.config.Config;
import com.gmail.nossr50.datatypes.player.McMMOPlayer;
import com.gmail.nossr50.datatypes.skills.PrimarySkillType;
import com.gmail.nossr50.locale.LocaleLoader;
import com.gmail.nossr50.mcMMO;
import com.gmail.nossr50.skills.mining.MiningManager;
import com.gmail.nossr50.skills.repair.Repair;
import com.gmail.nossr50.skills.repair.RpgItemsRepairManager;
import com.gmail.nossr50.skills.salvage.Salvage;
import com.gmail.nossr50.skills.salvage.SalvageManager;
import com.gmail.nossr50.util.player.UserManager;
import com.gmail.nossr50.util.skills.SkillUtils;
import me.lorinth.rpgitems.managers.RepairManager;
import me.lorinth.rpgitems.objects.RpgItemStack;
import me.lorinth.rpgitems.util.LoreHelper;
import me.lorinth.utils.MathHelper;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredListener;

import java.util.ArrayList;
import java.util.List;

public class McmmoRepairListener implements Listener {

    public McmmoRepairListener(){
        if(RepairManager.useMcMMoRepair){
            List<RegisteredListener> targetListeners = new ArrayList<>();

            for(RegisteredListener listener : PlayerInteractEvent.getHandlerList().getRegisteredListeners()){
                if(listener.getListener().getClass() == com.gmail.nossr50.listeners.PlayerListener.class) {
                    if (listener.getPriority() == EventPriority.LOWEST)
                        targetListeners.add(listener);
                }
            }

            if(targetListeners.size() > 0)
                for(RegisteredListener listener : targetListeners)
                    PlayerInteractEvent.getHandlerList().unregister(listener);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerInteractLowest(PlayerInteractEvent event)
    {
        if(!RepairManager.useMcMMoRepair)
            return;

        Player player = event.getPlayer();
        if ((event.getHand() != EquipmentSlot.HAND) || (!UserManager.hasPlayerDataKey(player)) || (player.getGameMode() == GameMode.CREATIVE)) {
            return;
        }
        McMMOPlayer mcMMOPlayer = UserManager.getPlayer(player);
        MiningManager miningManager = mcMMOPlayer.getMiningManager();
        Block block = event.getClickedBlock();
        ItemStack heldItem = player.getInventory().getItemInMainHand();
        Material type = block.getType();
        switch (event.getAction())
        {
            case RIGHT_CLICK_BLOCK:
                if ((!Config.getInstance().getAbilitiesOnlyActivateWhenSneaking()) || (player.isSneaking()))
                {
                    if ((type == Repair.anvilMaterial) && (PrimarySkillType.REPAIR.getPermissions(player)) && (mcMMO.getRepairableManager().isRepairable(heldItem)))
                    {
                        RpgItemsRepairManager repairManager = new RpgItemsRepairManager(mcMMOPlayer);
                        event.setCancelled(true);
                        if ((heldItem.getEnchantments().size() <= 0) || (repairManager.checkConfirmation(true)))
                        {
                            repairManager.handleRepair(heldItem);
                            player.updateInventory();
                        }
                    }
                    else if ((type == Salvage.anvilMaterial) && (PrimarySkillType.SALVAGE.getPermissions(player)) && (mcMMO.getSalvageableManager().isSalvageable(heldItem)))
                    {
                        SalvageManager salvageManager = UserManager.getPlayer(player).getSalvageManager();
                        event.setCancelled(true);
                        if ((heldItem.getEnchantments().size() <= 0) || (salvageManager.checkConfirmation(true)))
                        {
                            SkillUtils.handleAbilitySpeedDecrease(player);
                            salvageManager.handleSalvage(block.getLocation(), heldItem);
                            player.updateInventory();
                        }
                    }
                }
                else if (miningManager.canDetonate()) {
                    if (type == Material.TNT) {
                        event.setCancelled(true);
                    } else {
                        miningManager.remoteDetonation();
                    }
                }
                break;
            case LEFT_CLICK_BLOCK:
                if ((!Config.getInstance().getAbilitiesOnlyActivateWhenSneaking()) || (player.isSneaking())) {
                    if ((type == Repair.anvilMaterial) && (PrimarySkillType.REPAIR.getPermissions(player)) && (mcMMO.getRepairableManager().isRepairable(heldItem)))
                    {
                        RpgItemsRepairManager repairManager = new RpgItemsRepairManager(mcMMOPlayer);
                        if (repairManager.checkConfirmation(false))
                        {
                            repairManager.setLastAnvilUse(0);
                            player.sendMessage(LocaleLoader.getString("Skills.Cancelled", LocaleLoader.getString("Repair.Pretty.Name")));
                        }
                    }
                    else if ((type == Salvage.anvilMaterial) && (PrimarySkillType.SALVAGE.getPermissions(player)) && (mcMMO.getSalvageableManager().isSalvageable(heldItem)))
                    {
                        SalvageManager salvageManager = mcMMOPlayer.getSalvageManager();
                        if (salvageManager.checkConfirmation(false))
                        {
                            salvageManager.setLastAnvilUse(0);
                            player.sendMessage(LocaleLoader.getString("Skills.Cancelled", LocaleLoader.getString("Salvage.Pretty.Name")));
                        }
                    }
                }
                break;
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInventoryClickEvent(InventoryClickEvent event){
        if(!RepairManager.allowItemClickRepair)
            return;

        if(event.getClick() != ClickType.RIGHT)
            return;

        Player player = (Player) event.getWhoClicked();

        ItemStack repairMaterial = event.getCursor();
        ItemStack targetItem = event.getCurrentItem();

        if(repairMaterial == null || repairMaterial.getType() == Material.AIR)
            return;
        if(targetItem == null || targetItem.getType() == Material.AIR)
            return;

        Double repairAmount = RepairManager.getRepairValue(repairMaterial);
        if(repairAmount == null)
            return;

        RpgItemStack rpgItemStack = new RpgItemStack(targetItem);
        if(!rpgItemStack.hasDurability())
            return;
        if(MathHelper.equals(rpgItemStack.getCustomDurability(), rpgItemStack.getCustomMaxDurability())){
            if(!RepairManager.fullDurabilityMessage.equalsIgnoreCase(""))
                player.sendMessage(RepairManager.fullDurabilityMessage);

            event.setCancelled(true);
            return;
        }

        //Reduce repair material by 1
        repairMaterial.setAmount(repairMaterial.getAmount()-1);
        repairAmount = rpgItemStack.addDurability(repairAmount);
        ItemStack finalItem = rpgItemStack.getBukkitItem();

        targetItem.setItemMeta(finalItem.getItemMeta());
        if(!RepairManager.repairMessage.equalsIgnoreCase(""))
            player.sendMessage(RepairManager.repairMessage.replace("{amount}", LoreHelper.DecimalFormat.format(repairAmount)));
        event.setCancelled(true);
    }

}
