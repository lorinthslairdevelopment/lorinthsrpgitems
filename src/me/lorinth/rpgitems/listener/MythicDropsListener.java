package me.lorinth.rpgitems.listener;

import com.tealcube.minecraft.bukkit.mythicdrops.MythicDropsPlugin;
import com.tealcube.minecraft.bukkit.mythicdrops.api.MythicDrops;
import com.tealcube.minecraft.bukkit.mythicdrops.api.items.CustomItem;
import com.tealcube.minecraft.bukkit.mythicdrops.events.CustomItemGenerationEvent;
import com.tealcube.minecraft.bukkit.mythicdrops.events.RandomItemGenerationEvent;
import com.tealcube.minecraft.bukkit.mythicdrops.utils.BroadcastMessageUtil;
import me.lorinth.rpgitems.managers.DungeonsXLManager;
import me.lorinth.rpgitems.managers.ItemGenerationManager;
import me.lorinth.rpgitems.managers.LootManager;
import me.lorinth.rpgitems.util.LevelHelper;
import me.lorinth.rpgitems.util.PluginHelper;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MythicDropsListener implements Listener {

    private MythicDrops mythicDrops;

    public MythicDropsListener(){
        RpgItemsOutputHandler.PrintInfo("Mythic Drops Listener loaded");
        mythicDrops = MythicDropsPlugin.getInstance();
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onMythicDrop(RandomItemGenerationEvent event){
        ItemStack generated = event.getItemStack();
        if(generated != null){
            ItemStack newItem = ItemGenerationManager.applyGenerationTags(generated);
            event.setItemStack(removeUnbreakable(newItem));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onMythicCustomItem(CustomItemGenerationEvent event){
        ItemStack result = event.getResult();
        event.setResult(removeUnbreakable(ItemGenerationManager.applyGenerationTags(result)));
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityDeath(EntityDeathEvent event) {
        if (event.getEntity().getKiller() != null && event.getEntity() instanceof Monster) {
            int level = LevelHelper.getMonsterLevel(event.getEntity());
            ItemStack itemStack = null;

            if(PluginHelper.DungeonsXLEnabled && DungeonsXLManager.isDungeonWorld(event.getEntity().getWorld())) {
                return;
            }
            else if (event.getEntity().getKiller() == null) {
                return;
            }

            String customItemId = LootManager.getMythicDropsCustomItem(level);

            CustomItem ci = mythicDrops.getCustomItemManager().getById(customItemId);
            if (ci == null)
            {
                RpgItemsOutputHandler.PrintError("Failed to find Mythic Drops Custom Item by id, " + RpgItemsOutputHandler.HIGHLIGHT + customItemId);
                return;
            }

            CustomItemGenerationEvent customItemGenerationEvent =
                    new CustomItemGenerationEvent(ci, ci.toItemStack());
            Bukkit.getPluginManager().callEvent(customItemGenerationEvent);
            if (!customItemGenerationEvent.isCancelled()) {
                itemStack = ci.toItemStack();
                if (ci.isBroadcastOnFind()) {
                    BroadcastMessageUtil.INSTANCE.broadcastItem(
                            mythicDrops.getSettingsManager().getLanguageSettings(),
                            event.getEntity().getKiller(),
                            itemStack);
                }
            }

            if (itemStack != null) {
                World w = event.getEntity().getWorld();
                org.bukkit.Location l = event.getEntity().getLocation();
                w.dropItemNaturally(l, removeUnbreakable(itemStack));
            }
        }
    }

    private ItemStack removeUnbreakable(ItemStack item){
        ItemMeta meta = item.getItemMeta();
        if(meta.isUnbreakable()){
            meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        }
        item.setItemMeta(meta);
        return item;
    }

}
