package me.lorinth.rpgitems.listener;

import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.adapters.bukkit.BukkitPlayer;
import io.lumine.xikage.mythicmobs.api.bukkit.events.MythicMobDeathEvent;
import io.lumine.xikage.mythicmobs.drops.Drop;
import io.lumine.xikage.mythicmobs.drops.DropMetadata;
import io.lumine.xikage.mythicmobs.drops.DropTable;
import io.lumine.xikage.mythicmobs.mobs.ActiveMob;
import io.lumine.xikage.mythicmobs.mobs.MythicMob;
import me.lorinth.rpgitems.commands.ItemGenerateCommand;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.regex.Pattern;

public class MythicMobsListener implements Listener{

    private MythicMobs mythicMobs;

    public MythicMobsListener(){
        mythicMobs = MythicMobs.inst();
        RpgItemsOutputHandler.PrintInfo("Hooked into Mythic Mobs!");
    }

    @EventHandler(priority= EventPriority.LOWEST)
    public void onMythicMobsDeath(MythicMobDeathEvent event)
    {
        MythicMob localMythicMob = event.getMobType();
        ActiveMob activeMob = event.getMob();
        ArrayList localArrayList = new ArrayList(event.getDrops());
        if (localArrayList.isEmpty()) {
            return;
        }
        if ((event.getKiller() == null) || (!(event.getKiller() instanceof Player))) {
            return;
        }
        String str;
        Player player = (Player) event.getKiller();
        for (Iterator localIterator = event.getDrops().iterator(); localIterator.hasNext();)
        {
            str = (String)localIterator.next();
            Optional<DropTable> optTable = getDropTable(str);
            if(optTable != null && optTable.isPresent()){
                DropTable table = optTable.get();
                Collection<Drop> drops = table.generate(new DropMetadata(activeMob, new BukkitPlayer(player))).getDrops();
                for(Drop drop : drops){
                    String line = drop.getLine();
                    if (line.startsWith("lri gen") || line.startsWith("lri generate"))
                    {
                        ItemStack item = getItem(line);
                        if ((item != null)) {
                            event.getDrops().add(item);
                        }
                    }
                }
            }
        }
    }

    private Optional<DropTable> getDropTable(String str){
        return mythicMobs.getDropManager().getDropTable(str);
    }

    private ItemStack getItem(String command){
        String[] args = command.split(Pattern.quote(" "));

        args = Arrays.copyOfRange(args, 2, args.length);
        return ItemGenerateCommand.executeCommand(args, null);
    }

}
