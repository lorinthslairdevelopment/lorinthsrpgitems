package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.MathHelper;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

public class PlayerListener implements Listener {

    private static boolean AuthMeEnabled = false;

    public PlayerListener(){
        Plugin authMe = Bukkit.getPluginManager().getPlugin("AuthMe");
        if (authMe != null && authMe.isEnabled()){
            AuthMeEnabled = true;
            Bukkit.getPluginManager().registerEvents(new AuthMePlayerListener(), RpgItemsPlugin.instance);
            RpgItemsOutputHandler.PrintInfo("Detected AuthMe for player login!");
        }
    }

    public static boolean isUsingAuthMe(){
        return AuthMeEnabled;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        if(AuthMeEnabled)
            return;

        Player player = event.getPlayer();
        PlayerCharacterManager.loadCharacter(player);
    }

    @EventHandler
    public void onPlayerKick(PlayerKickEvent event){
        Player player = event.getPlayer();
        PlayerCharacterManager.unloadCharacter(player);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();
        PlayerCharacterManager.unloadCharacter(player);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerRespawn(PlayerRespawnEvent event){
        Bukkit.getScheduler().runTaskLater(RpgItemsPlugin.instance, () -> {
            Player player = event.getPlayer();
            PlayerCharacterManager.getCharacter(player).update(true);
        }, 10);
    }

    @EventHandler(priority=EventPriority.MONITOR)
    public void onPlayerJump(PlayerMoveEvent event)
    {
        if(!RpgAttribute.Jump.isEnabled())
            return;

        Player player = event.getPlayer();
        Vector velocity = player.getVelocity();
        if (!MathHelper.equals(velocity.getY(), 0.41999998688697815D))
            return;

        Double modifier = (Double) PlayerCharacterManager.getCharacter(player).getStats().get(RpgAttribute.Jump);
        if(modifier == null || MathHelper.equals(0.0D, modifier))
            return;

        if(RpgAttribute.Jump.isPercent())
            modifier = modifier / 100.0;

        velocity.setY(Math.sqrt(2 * 0.08 * (1.0 + modifier)));
        player.setVelocity(velocity);
    }

}
