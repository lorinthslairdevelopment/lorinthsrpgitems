package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.events.PlayerLevelUpEvent;
import me.lorinth.rpgitems.managers.ExperienceManager;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.util.CommandHelper;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerRpgListener implements Listener {

    @EventHandler
    public void onPlayerLevelUp(PlayerLevelUpEvent event){
        Player player = event.getPlayer();
        Integer level = event.getNewLevel();

        CommandHelper.ExecuteCommands(ExperienceManager.getApplicableLevelupMessages(event.getOriginalLevel(), event.getNewLevel()), player, level);

        RpgCharacter character = PlayerCharacterManager.getCharacter(player);
        character.update(true);
    }

}
