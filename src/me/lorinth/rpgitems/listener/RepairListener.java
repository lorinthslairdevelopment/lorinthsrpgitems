package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.gui.RepairMenu;
import me.lorinth.rpgitems.managers.RepairManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class RepairListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event){
        if(!RepairManager.useMultiBlockStructure)
            return;

        if(RepairManager.getRepairStructure().isKeystone(event.getBlock())){
            if(RepairManager.getRepairStructure().createdMultiblockStructure(event.getBlock())){
                event.getPlayer().sendMessage(RepairManager.createdMultiblockStructure);
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event){
        if(!RepairManager.useMultiBlockStructure)
            return;

        if(RepairManager.getRepairStructure().destroyMultiblockStructure(event.getBlock())){
            event.getPlayer().sendMessage(RepairManager.destroyedMultiblockStructure);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockUse(PlayerInteractEvent event){
        if(!RepairManager.useMultiBlockStructure) {
            return;
        }
        if(event.getAction() != Action.RIGHT_CLICK_BLOCK || event.getPlayer().isSneaking()) {
            return;
        }

        if(RepairManager.getRepairStructure().isMultiblockStructure(event.getClickedBlock())){
            event.setCancelled(true);

            new RepairMenu(event.getPlayer());
        }
    }

}
