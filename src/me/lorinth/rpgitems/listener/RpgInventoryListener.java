package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import ru.endlesscode.rpginventory.api.InventoryAPI;

public class RpgInventoryListener implements Listener{

    public RpgInventoryListener(){
        RpgItemsOutputHandler.PrintInfo("Hooked into RpgInventory");
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onInventoryOpen(InventoryCloseEvent event) {
        if (InventoryAPI.isRPGInventory(event.getInventory())) {
            PlayerCharacterManager.getCharacter((Player) event.getPlayer()).update();
        }
    }

}
