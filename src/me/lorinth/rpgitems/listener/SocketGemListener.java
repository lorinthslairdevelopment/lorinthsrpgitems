package me.lorinth.rpgitems.listener;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.managers.SocketManager;
import me.lorinth.rpgitems.objects.RpgItemStack;
import me.lorinth.rpgitems.objects.sockets.SocketFailureReason;
import me.lorinth.rpgitems.objects.sockets.SocketGem;
import me.lorinth.utils.NmsUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class SocketGemListener implements Listener {

    @EventHandler
    public void onSocketGemPlace(InventoryClickEvent event){
        Inventory clickedInventory = event.getClickedInventory();
        if(clickedInventory == null){
            return;
        }
        else if(event.getClickedInventory().getType() != InventoryType.PLAYER){
            return;
        }
        else if(!(event.getWhoClicked() instanceof Player)){
            return;
        }

        //Cursor will be the gem
        ItemStack cursor = event.getCursor();
        //Current item will be the gear piece
        ItemStack target = event.getCurrentItem();
        if(cursor == null || target == null ||
            cursor.getType() == Material.AIR || target.getType() == Material.AIR){
            return;
        }

        String gemId = getSocketGemId(cursor);
        if(gemId == null){
            return;
        }

        SocketGem socketGem = SocketManager.GetSocketGemById(gemId);
        if(socketGem == null){
            return;
        }

        RpgItemStack rpgItemStack = new RpgItemStack(target);
        if(!rpgItemStack.isRpgItem()){
            return;
        }

        event.setCancelled(true);

        Player player = (Player) event.getWhoClicked();
        SocketFailureReason failureReason = rpgItemStack.socket(socketGem, null);
        if(failureReason != null){
            sendErrorMessage(player, failureReason);
            return;
        }

        cursor.setAmount(cursor.getAmount()-1);
        event.setCursor(cursor.getAmount() == 0 ? null : cursor);
        event.setCurrentItem(rpgItemStack.getBukkitItem());
        PlayerCharacterManager.getCharacter(player).update();
    }

    private String getSocketGemId(ItemStack itemStack){
        return NmsUtils.getTag(RpgItemsPlugin.instance, itemStack, "SocketGemId");
    }

    private void sendErrorMessage(Player player, SocketFailureReason failureReason){
        switch(failureReason){
            case NoOpenSlots:
                player.sendMessage(SocketManager.GetSocketFailureMessages().NoOpenSlots);
                break;
            case InvalidSlot:
                player.sendMessage(SocketManager.GetSocketFailureMessages().InvalidSlot);
                break;
            case FilledSlot:
                player.sendMessage(SocketManager.GetSocketFailureMessages().FilledSlot);
                break;
            case GearLowLevel:
                player.sendMessage(SocketManager.GetSocketFailureMessages().GearLowLevel);
                break;
        }
    }

}
