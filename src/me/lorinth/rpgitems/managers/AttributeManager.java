package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.objects.ValueRange;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;

public class AttributeManager {

    public static AttributeMap EMPTY;
    public static AttributeManager instance;
    private HashMap<String, RpgAttribute> attributeMap = new HashMap<>();

    public AttributeManager(FileConfiguration config){
        instance = this;
        loadAttributes(config);
    }

    private void loadAttributes(FileConfiguration config){
        String path = "Attributes.";
        for(RpgAttribute attribute : RpgAttribute.values()){
            try{
                String subPath = path + attribute.name() + ".";
                String displayName = ChatColor.translateAlternateColorCodes('&', config.getString(subPath + "Display"));
                attribute.setDisplayName(displayName);
                if(TryParse.parseEnum(Material.class, config.getString(subPath + "DisplayMaterial")))
                    attribute.setDisplayMaterial(Material.valueOf(config.getString(subPath + "DisplayMaterial")));

                String defaultValue = config.getString(subPath + "DefaultValue");
                if(attribute.isRange())
                    attribute.setDefaultValue(new ValueRange(defaultValue));
                else
                    attribute.setDefaultValue(Double.parseDouble(defaultValue));

                if(ConfigHelper.ConfigContainsPath(config, subPath + "MaxValue")){
                    attribute.setMaxValue(config.getDouble(subPath + "MaxValue"));
                }

                attribute.setIsDecimal(config.getBoolean(subPath + "IsDecimal"));
                attribute.setIsPercent(config.getBoolean(subPath + "IsPercent"));
                attribute.setEnabled(config.getBoolean(subPath + "Enabled"));
                attributeMap.put(ChatColor.stripColor(displayName), attribute);
            }
            catch(Exception exception){
                System.out.println("Error loading attribute, " + attribute.name());
            }
        }
    }

    public static RpgAttribute getAttributeByName(String display){
        return instance.attributeMap.get(display);
    }

}