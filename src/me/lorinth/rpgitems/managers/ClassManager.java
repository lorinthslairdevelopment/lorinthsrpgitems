package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.objects.character.CharacterClass;
import me.lorinth.rpgitems.objects.gui.ClassGuiOptions;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class ClassManager {

    private static boolean enabled = false;
    private static HashMap<String, CharacterClass> characterClassesById;
    private static ArrayList<CharacterClass> baseClasses;
    private static CharacterClass defaultClass;
    private static ClassGuiOptions classGuiOptions;

    public ClassManager(FileConfiguration config){
        enabled = config.getBoolean("Enabled");
        if (!enabled) {
            return;
        }

        loadClasses(config, "Classes");
        loadGuiOptions(config, "GUI");
        RpgItemsOutputHandler.PrintInfo("Class System Enabled!");
    }

    public static boolean isEnabled(){
        return enabled;
    }

    public static CharacterClass getClassById(String id){
        return characterClassesById.get(id);
    }

    public static Collection<CharacterClass> getAllClasses(){
        return characterClassesById.values();
    }

    public static Collection<CharacterClass> getAllBaseClasses(){
        return baseClasses;
    }

    public static CharacterClass getDefaultClass(){
        return defaultClass;
    }

    public static ClassGuiOptions getGuiOptions(){
        return classGuiOptions;
    }

    private void loadClasses(FileConfiguration config, String path){
        characterClassesById = new HashMap<>();
        for(String classId: config.getConfigurationSection(path).getKeys(false)){
            CharacterClass characterClass = new CharacterClass(config, path, classId);
            //Default to first class in case Default: is not defined by users
            if(defaultClass == null){
                defaultClass = characterClass;
            }

            RpgItemsOutputHandler.PrintInfo(classId + " class was loaded!");
            characterClassesById.put(classId, characterClass);

            //If Default: true for this class, set it as the default
            if(characterClass.getMetadata().Default){
                defaultClass = characterClass;
            }
        }

        baseClasses = new ArrayList<>();
        for(CharacterClass characterClass : characterClassesById.values()){
            characterClass.loadSubclassList();
            if(characterClass.isBaseClass()){
                baseClasses.add(characterClass);
            }
        }

    }

    private void loadGuiOptions(FileConfiguration config, String path){
        classGuiOptions = new ClassGuiOptions(config, path);
    }

}
