package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.utils.ResourceHelper;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CombatManager {

    private boolean enabled;
    private List<String> enabledWorlds;
    private List<String> disabledWorlds;

    private List<String> enabledPlayers;
    private List<String> disabledPlayers;

    private static boolean togglingEnabled;
    private long toggleDelay;
    private HashMap<String, Long> playerToggledTimestamp;
    private long combatDelay;
    private HashMap<String, Long> playerInCombat;

    private String togglingDisabledMessage;
    private String toggleDelayMessage;
    private String combatDelayMessage;
    private String cantAttackPlayerMessage;
    private String pvpEnabledMessage;
    private String pvpDisabledMessage;

    public static CombatManager instance;

    public CombatManager(FileConfiguration config){
        instance = this;

        enabledWorlds = new ArrayList<>();
        disabledWorlds = new ArrayList<>();
        enabledPlayers = new ArrayList<>();
        disabledPlayers = new ArrayList<>();
        playerToggledTimestamp = new HashMap<>();
        playerInCombat = new HashMap<>();

        load(config);
    }

    private void load(FileConfiguration config){
        String path = "PvP.";
        enabled = config.getBoolean(path + "Enabled");
        enabledWorlds = config.getStringList(path + "EnabledWorlds");
        disabledWorlds = config.getStringList(path + "DisabledWorlds");

        path = path + "Players.";
        togglingEnabled = config.getBoolean(path + "TogglingEnabled");
        toggleDelay = config.getInt(path + "TogglingDelay");
        combatDelay = config.getInt(path + "CombatDelay");

        enabledPlayers = config.getStringList(path + "Enabled");
        disabledPlayers = config.getStringList(path + "Disabled");

        path = "Messages.";
        togglingDisabledMessage = ChatColor.translateAlternateColorCodes('&', config.getString(path + "TogglingIsDisabled"));
        toggleDelayMessage = ChatColor.translateAlternateColorCodes('&', config.getString(path + "ToggleDelay"));
        combatDelayMessage = ChatColor.translateAlternateColorCodes('&', config.getString(path + "CombatDelay"));
        cantAttackPlayerMessage = ChatColor.translateAlternateColorCodes('&', config.getString(path + "CantAttackPlayer"));
        pvpEnabledMessage = ChatColor.translateAlternateColorCodes('&', config.getString(path + "PvpEnabled"));
        pvpDisabledMessage = ChatColor.translateAlternateColorCodes('&', config.getString(path + "PvpDisabled"));
    }

    public void save(){
        ResourceHelper.copy(RpgItemsPlugin.instance.getResource("resources/combat.yml"), new File(RpgItemsPlugin.instance.getDataFolder(), "combat.yml"));

        File file = new File(RpgItemsPlugin.instance.getDataFolder(), "combat.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);

        config.set("PvP.Players.Enabled", enabledPlayers);
        config.set("PvP.Players.Disabled", disabledPlayers);

        try{
            config.save(file);
        } catch(Exception exception){
            exception.printStackTrace();
        }
    }

    public static boolean isTogglingEnabled(){
        return togglingEnabled;
    }

    public String getCantAttackPlayerMessage(){
        return cantAttackPlayerMessage;
    }

    public boolean playerInCombat(Player p){
        return getPlayerCombatWait(p) <= 0;
    }

    public void markPlayerInCombat(Player p){
        playerInCombat.put(p.getUniqueId().toString(), getCurrentTime() + combatDelay);
    }

    private Long getPlayerCombatWait(Player p){
        return getCurrentTime() - (playerInCombat.getOrDefault(p.getUniqueId().toString(), 0L));
    }

    public boolean playerCanToggle(Player p){
        return getPlayerToggleWait(p) >= 0;
    }

    private Long getPlayerToggleWait(Player p){
        return getCurrentTime() - (playerToggledTimestamp.getOrDefault(p.getUniqueId().toString(), 0L));
    }

    public boolean playerPvpEnabled(Player p){
        if(!WorldEnabled(p))
            return false;
        String uuid = p.getUniqueId().toString();
        if(enabledPlayers.contains(uuid))
            return true;
        if(disabledPlayers.contains(uuid))
            return false;
        return enabled;
    }

    public String changePlayerStatus(Player p){
        if(!togglingEnabled)
            return togglingDisabledMessage;
        if(!playerCanToggle(p))
            return toggleDelayMessage.replace("{delay}", (-1 * getPlayerToggleWait(p)) + "");
        if(playerInCombat(p))
            return combatDelayMessage.replace("{delay}", (-1 * getPlayerCombatWait(p)) + "");

        String uuid = p.getUniqueId().toString();
        boolean pvpEnabled = enabled;
        if(enabledPlayers.contains(uuid))
            pvpEnabled = true;
        if(disabledPlayers.contains(uuid))
            pvpEnabled = false;

        if(pvpEnabled) {
            enabledPlayers.remove(uuid);
            disabledPlayers.add(uuid);
            p.sendMessage(pvpDisabledMessage);
        }
        else{
            enabledPlayers.add(uuid);
            disabledPlayers.remove(uuid);
            p.sendMessage(pvpEnabledMessage);
        }

        playerToggledTimestamp.put(p.getUniqueId().toString(), getCurrentTime() + toggleDelay);
        return "";
    }

    private boolean WorldEnabled(Player p){
        String worldName = p.getWorld().getName();
        if(enabledWorlds.contains(worldName))
            return true;
        if(disabledWorlds.contains(worldName))
            return false;
        return enabled;
    }

    private Long getCurrentTime(){
        return System.currentTimeMillis() / 1000;
    }

}
