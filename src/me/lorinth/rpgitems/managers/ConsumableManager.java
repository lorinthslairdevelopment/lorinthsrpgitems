package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.objects.consumable.RpgConsumable;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.Set;

public class ConsumableManager {

    private HashMap<String, RpgConsumable> consumables = new HashMap<>();
    private static ConsumableManager instance;

    public ConsumableManager(FileConfiguration config){
        instance = this;

        for(String key : config.getKeys(false)){
            consumables.put(key, new RpgConsumable(config, key));
        }

        RpgItemsOutputHandler.PrintInfo("Loaded, " + RpgItemsOutputHandler.HIGHLIGHT + consumables.keySet().size() + RpgItemsOutputHandler.INFO + " consumables!");
    }

    public static ConsumableManager getInstance(){
        return instance;
    }

    public Set<String> getConsumableIds(){
        return consumables.keySet();
    }

    public RpgConsumable getConsumableById(String id){
        return consumables.get(id);
    }

}
