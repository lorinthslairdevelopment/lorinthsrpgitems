package me.lorinth.rpgitems.managers;

import de.erethon.dungeonsxl.game.Game;
import org.bukkit.World;

public class DungeonsXLManager {

    public static boolean isEnabled = false;

    public static boolean isDungeonWorld(World world){
        return isEnabled && Game.getByWorld(world).getDungeon() != null;
    }

}
