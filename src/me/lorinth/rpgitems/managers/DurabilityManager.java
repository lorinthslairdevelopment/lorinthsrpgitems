package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.DurabilityDamageCost;
import me.lorinth.rpgitems.enums.DurabilityDamageType;
import me.lorinth.rpgitems.listener.DurabilityListener;
import me.lorinth.rpgitems.objects.*;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import net.md_5.bungee.api.ChatMessageType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class DurabilityManager {

    private static HashMap<MaterialDurability, ValueRange> itemDurabilities;
    private static boolean enabled = false;
    private static boolean doCustomItemsDisappearWhenBroken = false;
    public static boolean useDecimalDurability = false;
    private static HashMap<DurabilityDamageCost, Double> durabilityCosts;
    private static Random random = new Random();
    private static ChatMessageType messageType = ChatMessageType.ACTION_BAR;
    private static ArrayList<DurabilityMessage> durabilityValueMessages;

    private static ValueRange defaultBlockDurabilityCost;
    private static HashMap<MaterialDurability, ValueRange> blockDurabilityCost;

    public DurabilityManager(FileConfiguration config) {
        itemDurabilities = new HashMap<>();
        if(config.getBoolean("UseCustomDurability")){
            useDecimalDurability = config.getBoolean("UseDecimalDurability");
            doCustomItemsDisappearWhenBroken = config.getBoolean("DoCustomItemsDisappearWhenBroken");
            enabled = true;
            RpgItemsOutputHandler.PrintInfo("Using Custom Durability System");

            Bukkit.getPluginManager().registerEvents(new DurabilityListener(), RpgItemsPlugin.instance);

            //Load Durability Costs
            durabilityCosts = new HashMap<>();
            for(DurabilityDamageCost cost : DurabilityDamageCost.values()){
                durabilityCosts.put(cost, config.getDouble("DurabilityCost." + cost.name().replaceAll("_", ".")));
            }

            blockDurabilityCost = new HashMap<>();
            String blockBreakPrefix = "DurabilityCost.BlockBreak";
            if(ConfigHelper.ConfigContainsPath(config, blockBreakPrefix)){
                for(String key : config.getConfigurationSection(blockBreakPrefix).getKeys(false)){
                    if(key.equalsIgnoreCase("Default")){
                        ValueRange valueRange = new ValueRange(config.getString(blockBreakPrefix + "." + key));
                        if(valueRange.isValid())
                            defaultBlockDurabilityCost = valueRange;
                        else {
                            RpgItemsOutputHandler.PrintError("Failed to load " + RpgItemsOutputHandler.HIGHLIGHT + blockBreakPrefix + ".Default" + RpgItemsOutputHandler.ERROR + " in durability.yml. Using default value = 1");
                            defaultBlockDurabilityCost = new ValueRange("1");
                        }
                    }
                    else{
                        MaterialDurability md = new MaterialDurability(key);
                        if(md.isValid()){
                            ValueRange valueRange = new ValueRange(config.getString(blockBreakPrefix + "." + key));
                            if(valueRange.isValid())
                                blockDurabilityCost.put(md, valueRange);
                        }
                    }
                }
            }
            else{
                RpgItemsOutputHandler.PrintError("Failed to load " + RpgItemsOutputHandler.HIGHLIGHT + blockBreakPrefix + RpgItemsOutputHandler.ERROR + " in durability.yml. Please regen this file");
            }

            loadDurabilities(config);
            loadMessages(config);
        }else
            RpgItemsOutputHandler.PrintInfo("Not using Custom Durability system");
    }

    private void loadDurabilities(FileConfiguration config){
        // Load Material Durabilities
        for(String key : config.getConfigurationSection("Durability").getKeys(false)){
            MaterialDurability md = new MaterialDurability(key);
            if(md.isValid()){
                String value = config.getString("Durability." + key);
                ValueRange range = new ValueRange(value);
                if(!range.isValid()){
                    RpgItemsOutputHandler.PrintError("Invalid Value Range, " + value + ". '" + key + "' will have 100 durability. [durability.yml]");
                    itemDurabilities.put(md, new ValueRange("100"));
                }
                else
                    itemDurabilities.put(md, range);
            }
            else{
                RpgItemsOutputHandler.PrintError("Invalid Material/Durability config value, '" + key + "'. This key will not be considered in custom durability. [durability.yml]");
            }
        }
    }

    private void loadMessages(FileConfiguration config){
        String prefix = "Messages.";
        String messageTypeString = config.getString(prefix + "DeliveryType");

        if(TryParse.parseEnum(ChatMessageType.class, messageTypeString))
            messageType = ChatMessageType.valueOf(messageTypeString);
        else
            RpgItemsOutputHandler.PrintError("Can't parse value, " + messageTypeString + ", at " + prefix + "DeliveryType, in [durability.yml]");

        durabilityValueMessages = new ArrayList<>();
        if(ConfigHelper.ConfigContainsPath(config, prefix + "DurabilityWarnings")){
            for(String key : config.getConfigurationSection(prefix + "DurabilityWarnings").getKeys(false)){
                durabilityValueMessages.add(new DurabilityMessage(new Value(key), ChatColor.translateAlternateColorCodes('&', config.getString(prefix + "DurabilityWarnings." + key))));
            }
        }
    }

    public static ArrayList<DurabilityMessage> getDurabilityValueMessages(){
        return durabilityValueMessages;
    }

    public static ChatMessageType getChatMessageType(){
        return messageType;
    }

    public static boolean IsEnabled(){
        return enabled;
    }

    public static Double getDurability(MaterialDurability md){
        if(enabled) {
            ValueRange range = itemDurabilities.get(md);
            if (range != null)
                return range.getValue();
        }
        return null;
    }

    public static void causeDurabilityDamage(DurabilityDamageType damageType, RpgCharacterEquipment equipment){
        RpgCharacter character = equipment.getCharacter();
        if(character.isPlayer()){
            Player player = (Player) character.getEntity();
            EntityEquipment entityEquipment = player.getEquipment();

            switch(damageType){
                case Melee_Attack:
                    handleMeleeAttack(entityEquipment, equipment);
                    break;
                case Shield_Block:
                    handleDamageSpecificType(entityEquipment, equipment, Material.SHIELD, DurabilityDamageCost.Weapons_Shield_Block);
                    break;
                case Ranged_Shoot:
                    handleDamageSpecificType(entityEquipment, equipment, Material.BOW, DurabilityDamageCost.Weapons_Bow_Shoot);
                    break;
                case Armor_Damage:
                    if(random.nextDouble() * 100.0 < getDurabilityCost(DurabilityDamageCost.Armor_DamageChance_Head))
                        equipment.dealDurabilityDamage(entityEquipment.getHelmet(), getDurabilityCost(DurabilityDamageCost.Armor_DamageAmount_Head), doCustomItemsDisappearWhenBroken);
                    if(random.nextDouble() * 100.0 < getDurabilityCost(DurabilityDamageCost.Armor_DamageChance_Chest))
                        equipment.dealDurabilityDamage(entityEquipment.getChestplate(), getDurabilityCost(DurabilityDamageCost.Armor_DamageAmount_Chest), doCustomItemsDisappearWhenBroken);
                    if(random.nextDouble() * 100.0 < getDurabilityCost(DurabilityDamageCost.Armor_DamageChance_Legs))
                        equipment.dealDurabilityDamage(entityEquipment.getLeggings(), getDurabilityCost(DurabilityDamageCost.Armor_DamageAmount_Legs), doCustomItemsDisappearWhenBroken);
                    if(random.nextDouble() * 100.0 < getDurabilityCost(DurabilityDamageCost.Armor_DamageChance_Feet))
                        equipment.dealDurabilityDamage(entityEquipment.getBoots(), getDurabilityCost(DurabilityDamageCost.Armor_DamageAmount_Feet), doCustomItemsDisappearWhenBroken);

                    ItemStack mainhand = entityEquipment.getItemInMainHand();
                    ItemStack offhand = entityEquipment.getItemInOffHand();

                    if(mainhand.getType() == Material.SHIELD)
                        dealDurabilityCost(DurabilityDamageCost.Weapons_Shield_Passive, mainhand, equipment);
                    if(offhand.getType() == Material.SHIELD)
                        dealDurabilityCost(DurabilityDamageCost.Weapons_Shield_Passive, offhand, equipment);

                    break;
            }
        }
    }

    public static void causeBlockBreakDamage(ItemStack item, Block block, RpgCharacterEquipment equipment){
        if(!item.hasItemMeta())
            return;
        if(!item.getItemMeta().hasLore() || item.getItemMeta().getLore().size() == 0)
            return;

        Double cost;
        ValueRange value = blockDurabilityCost.get(new MaterialDurability(block));
        if(value != null)
            cost = value.getValue();
        else
            cost = defaultBlockDurabilityCost.getValue();

        equipment.dealDurabilityDamage(item, cost, doCustomItemsDisappearWhenBroken);
    }

    private static void handleMeleeAttack(EntityEquipment equipment, RpgCharacterEquipment rpgEquipment){
        ItemStack mainhand = equipment.getItemInMainHand();
        ItemStack offhand = equipment.getItemInOffHand();

        if(!handleMeleeItem(mainhand, rpgEquipment)){
            dealDurabilityCost(DurabilityDamageCost.Weapons_Melee_MainHand, mainhand, rpgEquipment);
        }
        if(!handleMeleeItem(offhand, rpgEquipment)){
            dealDurabilityCost(DurabilityDamageCost.Weapons_Melee_OffHand, offhand, rpgEquipment);
        }
    }

    //Return if durability damage was done to item
    private static boolean handleMeleeItem(ItemStack hand, RpgCharacterEquipment rpgEquipment){
        if(hand == null)
            return true;
        if(!hand.hasItemMeta())
            return true;
        if(!hand.getItemMeta().hasLore() || hand.getItemMeta().getLore().size() == 0)
            return true;

        if(hand.getType() == Material.BOW) {
            dealDurabilityCost(DurabilityDamageCost.Weapons_Bow_Melee, hand, rpgEquipment);
            return true;
        }
        else return hand.getType() == Material.SHIELD;

    }

    private static void handleDamageSpecificType(EntityEquipment equipment, RpgCharacterEquipment rpgEquipment, Material targetType, DurabilityDamageCost damageCost){
        ItemStack mainhand = equipment.getItemInMainHand();
        if(mainhand != null && mainhand.getType() == targetType){
            dealDurabilityCost(damageCost, mainhand, rpgEquipment);
            return;
        }

        ItemStack offhand = equipment.getItemInOffHand();
        if(offhand != null && offhand.getType() == targetType)
            dealDurabilityCost(damageCost, offhand, rpgEquipment);
    }

    private static void dealDurabilityCost(DurabilityDamageCost cost, ItemStack item, RpgCharacterEquipment equipment){
        equipment.dealDurabilityDamage(item, getDurabilityCost(cost), doCustomItemsDisappearWhenBroken);
    }

    private static Double getDurabilityCost(DurabilityDamageCost cost){
        if(enabled)
            return durabilityCosts.getOrDefault(cost, 0.0d);
        return null;
    }

}
