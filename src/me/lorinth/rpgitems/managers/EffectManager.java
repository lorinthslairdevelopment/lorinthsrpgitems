package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.enums.EffectType;
import me.lorinth.rpgitems.objects.EffectInfo;
import me.lorinth.rpgitems.tasks.LastingEffect;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EffectManager {

    private static HashMap<EffectType, EffectInfo> effectInfo;
    private static HashMap<String, ArrayList<LastingEffect>> monsterLastingEffects;

    public EffectManager(FileConfiguration config){
        effectInfo = new HashMap<>();
        for(String effectName : config.getConfigurationSection("").getKeys(false)){
            if(TryParse.parseEnum(EffectType.class, effectName.toUpperCase())){
                EffectInfo info = new EffectInfo(config, effectName);
                effectInfo.put(info.getType(), info);
            }
            else{
                RpgItemsOutputHandler.PrintError("Failed to load effect, '" + effectName + "' in effects.yml");
            }
        }

        monsterLastingEffects = new HashMap<>();
    }

    public static EffectInfo getEffectInfo(EffectType effectType){
        return effectInfo.get(effectType);
    }

    public static void addEffect(Entity entity, LastingEffect lastingEffect){
        ArrayList<LastingEffect> existingEffects = monsterLastingEffects.getOrDefault(entity.getUniqueId().toString(), new ArrayList<>());
        existingEffects.add(lastingEffect);
        monsterLastingEffects.put(entity.getUniqueId().toString(), existingEffects);
    }

    public static void removeEffect(Entity entity, LastingEffect lastingEffect){
        ArrayList<LastingEffect> existingEffects = monsterLastingEffects.getOrDefault(entity.getUniqueId().toString(), new ArrayList<>());
        existingEffects.remove(lastingEffect);
        monsterLastingEffects.put(entity.getUniqueId().toString(), existingEffects);
    }

    public static List<LastingEffect> getLastingEffects(Entity entity){
        return monsterLastingEffects.getOrDefault(entity.getUniqueId().toString(), new ArrayList<>());
    }

    public static void clearEntity(Entity entity){
        monsterLastingEffects.remove(entity.getUniqueId().toString());
    }

}
