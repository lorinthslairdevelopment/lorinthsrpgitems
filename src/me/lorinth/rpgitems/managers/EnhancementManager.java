package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.listener.EnhancementListener;
import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.objects.enhancement.EnhancementData;
import me.lorinth.rpgitems.objects.enhancement.EnhancementTierInfo;
import me.lorinth.rpgitems.objects.gui.EnhancementGuiOptions;
import me.lorinth.rpgitems.objects.structure.MultiBlockStructure;
import me.lorinth.rpgitems.util.*;
import me.lorinth.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class EnhancementManager {

    public static boolean enabled = false;
    public static boolean useMultiBlockStructure = false;

    public static String enhancementSuccessMessage;
    public static String enhancementFailureMessage;
    public static String enhancementDestroyMessage;
    public static String createdMultiblockStructure;
    public static String destroyedMultiblockStructure;

    private static MultiBlockStructure enhancementStructure;
    private static EnhancementListener enhancementListener;

    private static EnhancementGuiOptions enhancementGuiOptions;

    private static HashMap<ItemStack, EnhancementData> enhancementItems;

    public EnhancementManager(FileConfiguration config){
        enabled = config.getBoolean("Enabled");
        if(!enabled)
            return;

        RpgItemsOutputHandler.PrintInfo("Enhancement system enabled!");

        useMultiBlockStructure = config.getBoolean("useMultiBlockStructure");
        enhancementSuccessMessage = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.EnhancementSuccess"));
        enhancementFailureMessage = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.EnhancementFailure"));
        enhancementDestroyMessage = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.EnhancementDestroy"));
        createdMultiblockStructure = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.CreatedMultiblockStructure"));
        destroyedMultiblockStructure = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.DestroyedMultiblockStructure"));

        enhancementItems = new HashMap<>();
        for(String key : config.getConfigurationSection("EnhancementItems").getKeys(false)){
            String path = "EnhancementItems";
            enhancementItems.put(ItemStackHelper.makeItemFromConfig(config, path, key, "enhancement.yml", RpgItemsOutputHandler.getInstance()),
                    getEnhancementDataFromConfig(config, path + "." + key));
        }

        if(useMultiBlockStructure){
            JavaPlugin rpgItems = RpgItemsPlugin.instance;
            String path = rpgItems.getDataFolder() + File.separator + "MultiblockStructures";

            File file = new File(path, "EnhancementStructure.json");
            if(!file.exists()){
                ResourceHelper.copy(rpgItems.getResource("resources/EnhancementStructure.json"), new File(path, "EnhancementStructure.json"));
            }

            enhancementStructure = new MultiBlockStructure(new File(path, "EnhancementStructure.json"));
            loadEnhancementUI(config);

            enhancementListener = new EnhancementListener();
            Bukkit.getPluginManager().registerEvents(enhancementListener, RpgItemsPlugin.instance);
        }
    }

    private EnhancementData getEnhancementDataFromConfig(FileConfiguration config, String path){
        EnhancementData data = new EnhancementData();

        data.setMinimumLevel(config.getInt(path + ".MinimumLevel"));
        data.setMaximumLevel(config.getInt(path + ".MaximumLevel"));
        data.setNumberOfUpgrades(config.getInt(path + ".NumberOfUpgrades"));
        data.setTierChances(getEnhancementTierChancesFromConfig(config, path));
        data.setAllAttributePercentIncrease(config.getDouble(path + ".AllAttributePercentIncrease"));
        data.setAttributeSpecificPercentIncrease(AttributeMap.load(config, path + ".AttributeSpecificPercentIncrease"));
        data.setAttributeMaximumValueIncrease(AttributeMap.load(config, path + ".AttributeMaximumValueIncrease"));
        data.setAttributeMinimumValueIncrease(AttributeMap.load(config, path + ".AttributeMinimumValueIncrease"));
        return data;
    }

    private HashMap<Integer, EnhancementTierInfo> getEnhancementTierChancesFromConfig(FileConfiguration config, String path){
        HashMap<Integer, EnhancementTierInfo> chances = new HashMap<>();
        path = path + ".TierChances";
        for(String key : config.getConfigurationSection(path).getKeys(false)){
            chances.put(Integer.parseInt(key), getEnhancementTierInfoFromConfig(config, path + "." + key));
        }

        return chances;
    }

    private EnhancementTierInfo getEnhancementTierInfoFromConfig(FileConfiguration config, String path){
        EnhancementTierInfo info = new EnhancementTierInfo();

        info.setOverallSuccessChance(config.getDouble(path + ".Success"));
        info.setOverallDestoryChance(config.getDouble(path + ".Destroy"));

        if(ConfigHelper.ConfigContainsPath(config, path + ".RaritySpecificSuccess")){
            for(String rarityTier : config.getConfigurationSection(path + ".RaritySpecificSuccess").getKeys(false)){
                if(!TryParse.parseEnum(RarityTier.class, rarityTier)){
                    RpgItemsOutputHandler.PrintError("Invalid Rarity Tier: " + RpgItemsOutputHandler.HIGHLIGHT + rarityTier + RpgItemsOutputHandler.ERROR + " in enhancement.yml [" + path + ".RaritySpecificSuccess]");
                    continue;
                }

                info.setRaritySpecificSuccessChance(RarityTier.valueOf(rarityTier), config.getDouble(path + ".RaritySpecificSuccess." + rarityTier));
            }
        }
        if(ConfigHelper.ConfigContainsPath(config, path + ".RaritySpecificDestroy")){
            for(String rarityTier : config.getConfigurationSection(path + ".RaritySpecificDestroy").getKeys(false)){
                if(!TryParse.parseEnum(RarityTier.class, rarityTier)){
                    RpgItemsOutputHandler.PrintError("Invalid Rarity Tier: " + RpgItemsOutputHandler.HIGHLIGHT + rarityTier + RpgItemsOutputHandler.ERROR + " in enhancement.yml [" + path + ".RaritySpecificDestroy]");
                    continue;
                }

                info.setRaritySpecificDestroyChance(RarityTier.valueOf(rarityTier), config.getDouble(path + ".RaritySpecificDestroy." + rarityTier));
            }
        }

        return info;
    }

    private void loadEnhancementUI(FileConfiguration config){
        if(ConfigHelper.ConfigContainsPath(config, "GUI")){
            enhancementGuiOptions = new EnhancementGuiOptions();

            String path = "GUI.";

            enhancementGuiOptions.Title = ChatColor.translateAlternateColorCodes('&', config.getString(path + "Title"));
            enhancementGuiOptions.Rows = config.getInt(path + "Rows");

            // Load Slots
            path = "GUI.Slots.";
            enhancementGuiOptions.Slots.Instruction = config.getInt(path + "Instruction");
            enhancementGuiOptions.Slots.EnhancementMaterial = config.getInt(path + "EnhancementMaterial");
            enhancementGuiOptions.Slots.TargetItem = config.getInt(path + "TargetItem");
            enhancementGuiOptions.Slots.ChanceInfo = config.getInt(path + "ChanceInfo");
            enhancementGuiOptions.Slots.Result = config.getInt(path + "Result");

            // Load Items
            path = "GUI.Items.";
            enhancementGuiOptions.Instructions = ItemStackHelper.makeItemFromConfig(config, path, "Instruction", "enhancement.yml", RpgItemsOutputHandler.getInstance());
            enhancementGuiOptions.ChanceInfo = ItemStackHelper.makeItemFromConfig(config, path, "ChanceInfo", "enhancement.yml", RpgItemsOutputHandler.getInstance());
            enhancementGuiOptions.LockedSlot = ItemStackHelper.makeItemFromConfig(config, path, "LockedSlot", "enhancement.yml", RpgItemsOutputHandler.getInstance());
            enhancementGuiOptions.NotValidEnhancementItem = ItemStackHelper.makeItemFromConfig(config, path, "NotValidEnhancementItem", "enhancement.yml", RpgItemsOutputHandler.getInstance());
        }
        else{
            RpgItemsOutputHandler.PrintError("Repair.yml, has no existing configuration options for GUI");
        }
    }

    public static EnhancementGuiOptions getEnhancementGuiOptions(){
        return enhancementGuiOptions;
    }

    public static EnhancementData getEnhancementData(ItemStack enhancementMaterial){
        for(Map.Entry<ItemStack, EnhancementData> entry : enhancementItems.entrySet()){
            if(entry.getKey().isSimilar(enhancementMaterial)){
                return entry.getValue();
            }
        }
        return null;
    }

    public static MultiBlockStructure getEnhancementStructure(){
        return enhancementStructure;
    }

    public static void onDisable(){
        if(!enabled)
            return;

        if(enhancementStructure != null) {
            enhancementStructure.save();
        }
        if(enhancementStructure != null){
            HandlerList.unregisterAll(enhancementListener);
        }
    }

}
