package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.enums.MonsterExperienceHook;
import me.lorinth.rpgitems.enums.PlayerExperienceMode;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.objects.Experienced;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.objects.ValueRange;
import me.lorinth.rpgitems.objects.character.CharacterClassInstance;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.rpgitems.util.SettingsHelper;
import me.lorinth.utils.Calculator;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import net.md_5.bungee.api.ChatMessageType;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.regex.Pattern;

public class ExperienceManager {

    private static String playerLevelFormula = "(({level}-1) * 20) + 50";
    public static String gainExperienceMessage = "";
    public static String lostExperienceMessage = "";
    private static MonsterExperienceHook monsterExperienceHook = MonsterExperienceHook.Vanilla;
    public static List<String> levelUpMessages;
    public static ChatMessageType experienceMessageDelivery = ChatMessageType.ACTION_BAR;
    public static PlayerExperienceMode experienceMode = PlayerExperienceMode.FORMULA;

    private static int levelCap = Integer.MAX_VALUE;
    private static HashMap<Integer, Integer> experienceLevelMap;
    private static HashMap<EntityType, ValueRange> experienceRewardsMap;
    private static HashMap<Integer, List<String>> levelUpEvents;
    private static TreeMap<Integer, AttributeMap> levelPassiveStats;

    public ExperienceManager(FileConfiguration config){
        if(ConfigHelper.ConfigContainsPath(config, "PlayerLevelFormula"))
            playerLevelFormula = config.getString("PlayerLevelFormula");
        if(ConfigHelper.ConfigContainsPath(config, "ExperienceMessage"))
            gainExperienceMessage = ChatColor.translateAlternateColorCodes('&', config.getString("ExperienceMessage"));
        if(ConfigHelper.ConfigContainsPath(config, "LostExperienceMessage"))
            lostExperienceMessage = ChatColor.translateAlternateColorCodes('&', config.getString("LostExperienceMessage"));
        if(ConfigHelper.ConfigContainsPath(config, "ExperienceMessageDelivery"))
            if(TryParse.parseEnum(ChatMessageType.class, config.getString("ExperienceMessageDelivery")))
                experienceMessageDelivery = ChatMessageType.valueOf(config.getString("ExperienceMessageDelivery"));
        if(ConfigHelper.ConfigContainsPath(config, "LevelCap"))
            levelCap = Integer.parseInt(config.getString("LevelCap"));
        levelUpMessages = new ArrayList<>();
        if(ConfigHelper.ConfigContainsPath(config, "LevelUpMessages")){
            for(String line : config.getStringList("LevelUpMessages")){
                levelUpMessages.add(ChatColor.translateAlternateColorCodes('&', line));
            }
        }
        if(ConfigHelper.ConfigContainsPath(config, "MonsterExperienceHook")){
            if(TryParse.parseEnum(MonsterExperienceHook.class, config.getString("MonsterExperienceHook"))){
                monsterExperienceHook = MonsterExperienceHook.valueOf(config.getString("MonsterExperienceHook"));
            }
            else{
                RpgItemsOutputHandler.PrintError("Unable to parse, 'MonsterExperienceHook' in experience.yml. Using Vanilla experience hook");
                monsterExperienceHook = MonsterExperienceHook.Vanilla;
            }
        }
        else{
            RpgItemsOutputHandler.PrintError("Couldn't find, 'MonsterExperienceHook' in experience.yml. Using Vanilla experience hook");
            monsterExperienceHook = MonsterExperienceHook.Vanilla;
        }

        if(ConfigHelper.ConfigContainsPath(config, "PlayerExperienceMode")){
            experienceMode = PlayerExperienceMode.valueOf(config.getString("PlayerExperienceMode"));
        }
        if(experienceMode == PlayerExperienceMode.LEVEL_DEFINITION){
            experienceLevelMap = new HashMap<>();
            Integer lastLevel = 0;
            Integer currentLevel = 1;
            Integer lastExp = 0;
            Integer currentExp = 0;
            for(String key : config.getConfigurationSection("LevelExperienceDefinitions").getKeys(false)){
                if(!TryParse.parseInt(key)){
                    RpgItemsOutputHandler.PrintError("Error in experience.yml, LevelExperienceDefinitions." + key);
                    RpgItemsOutputHandler.PrintError(key + " is not a number");
                }
                else {
                    currentLevel = Integer.parseInt(key);
                    currentExp = config.getInt("LevelExperienceDefinitions." + key);
                    if(lastLevel + 1 != currentLevel){
                        int addition = (currentExp - lastExp) / (currentLevel - lastLevel);

                        for(int i = 1; i < (currentLevel-lastLevel); i++)
                            experienceLevelMap.put(lastLevel + i, (lastExp + i * addition));
                    }
                    experienceLevelMap.put(currentLevel, currentExp);
                    lastLevel = currentLevel;
                }
            }
        }
        if(ConfigHelper.ConfigContainsPath(config, "ExperienceRewards")){
            experienceRewardsMap = new HashMap<>();
            for(String entityString : config.getConfigurationSection("ExperienceRewards").getKeys(false)){
                if(TryParse.parseEnum(EntityType.class, entityString)){
                    EntityType type = EntityType.valueOf(entityString);
                    String configValue = config.getString("ExperienceRewards." + entityString);
                    ValueRange range = new ValueRange(configValue);
                    if(!range.isValid()){
                        experienceRewardsMap.put(type, new ValueRange("0"));
                        RpgItemsOutputHandler.PrintError("Invalid Value Range, " + configValue + ". " + entityString + " will reward 0 exp. [experience.yml]");
                    }
                    else
                        experienceRewardsMap.put(type, range);
                }
                else
                    RpgItemsOutputHandler.PrintError("Error parsing entity type: " + entityString + " in experience.yml, [ExperienceRewards." + entityString + "]");
            }
        }
        if(ConfigHelper.ConfigContainsPath(config, "LevelUpEvents")){
            levelUpEvents = new HashMap<>();
            for(String key : config.getConfigurationSection("LevelUpEvents").getKeys(false)){
                if(TryParse.parseInt(key)){
                    List<String> commands = config.getStringList("LevelUpEvents." + key);
                    Integer level = Integer.parseInt(key);

                    levelUpEvents.put(level, commands);
                }
                else{
                    RpgItemsOutputHandler.PrintError("Can't parse " + RpgItemsOutputHandler.HIGHLIGHT + "'" + key + "'" + RpgItemsOutputHandler.ERROR + " as type Integer");
                }
            }
        }
        if(ConfigHelper.ConfigContainsPath(config, "LevelPassiveStats")){
            levelPassiveStats = new TreeMap<>();
            for(String levelString : config.getConfigurationSection("LevelPassiveStats").getKeys(false)){
                if(!TryParse.parseInt(levelString)){
                    RpgItemsOutputHandler.PrintError("LevelPassiveStat Profile, has invalid level key, " + RpgItemsOutputHandler.HIGHLIGHT + levelString);
                    continue;
                }

                Integer level = Integer.parseInt(levelString);
                AttributeMap levelPassiveStatProfile = new AttributeMap(false);

                for(String attributeString : config.getConfigurationSection("LevelPassiveStats." + levelString).getKeys(false)){
                    if(!TryParse.parseEnum(RpgAttribute.class, attributeString)){
                        RpgItemsOutputHandler.PrintError("LevelPassiveStat Profile, " + RpgItemsOutputHandler.HIGHLIGHT + levelString + RpgItemsOutputHandler.ERROR +
                                " has invalid attribute, " + RpgItemsOutputHandler.HIGHLIGHT + attributeString);
                        continue;
                    }
                    else{
                        RpgAttribute rpgAttribute = RpgAttribute.valueOf(attributeString);
                        if(rpgAttribute.isEnabled()){
                            String value = config.getString("LevelPassiveStats." + levelString + "." + attributeString);
                            if(!TryParse.parseDouble(value))
                                levelPassiveStatProfile.put(rpgAttribute, Double.parseDouble(value));
                            else
                                levelPassiveStatProfile.put(rpgAttribute, new ValueRange(value));
                        }
                    }
                }

                levelPassiveStats.put(level, levelPassiveStatProfile);
            }
        }
        else{
            levelPassiveStats = new TreeMap<>();
            levelPassiveStats.put(0, new AttributeMap(false));
        }
    }

    public static List<String> getApplicableLevelupMessages(Integer originalLevel, Integer newLevel){
        ArrayList<String> levelUpList = new ArrayList<>();
        for(int i=originalLevel+1; i<=newLevel; i++){
            if(levelUpEvents.containsKey(i)){
                levelUpList.addAll(levelUpEvents.get(i));
            }
        }
        return levelUpList;
    }

    public static MonsterExperienceHook getMonsterExperienceHook(){
        return monsterExperienceHook;
    }

    public static int getExpToNextLevel(Integer level){
        if(experienceMode == PlayerExperienceMode.FORMULA)
            return (int) Calculator.eval(SettingsHelper.formulaMethod, playerLevelFormula.replaceAll(Pattern.quote("{level}"), level.toString()));
        else
            return experienceLevelMap.get(level);
    }

    public static int getExpToNextLevel(Integer level, Double modifier){
        if(modifier == null){
            return getExpToNextLevel(level);
        }
        return (int) Math.floor(getExpToNextLevel(level) * modifier);
    }

    public static boolean checkLevelUp(Player player){
        RpgCharacter character = PlayerCharacterManager.getCharacter(player);
        return checkLevelUp(character);
    }

    public static boolean checkLevelUp(RpgCharacter character){
        if(ClassManager.isEnabled()){
            CharacterClassInstance classInstance = character.getCurrentCharacterClass();
            return classInstance.getExperience() >=
                    getExpToNextLevel(classInstance.getLevel(),
                            classInstance.getCharacterClass().getMetadata().ExperienceRequiredModifier);
        }
        else{
            return character.getInfo().getExperience() >= getExpToNextLevel(character.getLevel());
        }
    }

    public static int getExpForEntity(Entity entity){
        ValueRange range = experienceRewardsMap.get(entity.getType());
        if(range == null)
            return 0;
        else
            return (int) Math.floor(range.getValue());
    }

    public static AttributeMap getLevelPassiveStats(Integer level){
        if(levelPassiveStats == null)
            return AttributeMap.empty;

        Map.Entry<Integer, AttributeMap> entry = levelPassiveStats.floorEntry(level);
        if(entry == null)
            return AttributeMap.empty;

        return entry.getValue();
    }

    public static int getLevelCap(Experienced experienced){
        if(experienced instanceof CharacterClassInstance){
            CharacterClassInstance classInstance = (CharacterClassInstance) experienced;
            Integer classMaxLevel = classInstance.getCharacterClass().getMetadata().LevelCap;

            if(classMaxLevel != null){
                return classMaxLevel;
            }
        }
        return levelCap;
    }

}
