package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.objects.GearSet;
import me.lorinth.rpgitems.objects.MaterialDurability;
import me.lorinth.rpgitems.objects.RandomCollection;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Random;
import java.util.TreeMap;

public class GearSetsManager {

    private static boolean enabled;
    private static String itemDisplayNameLine; //TODO
    private static String itemDisplayValueLine; //TODO
    private static Random random = new Random();
    private static HashMap<String, GearSet> gearSetsById;
    private static HashMap<String, GearSet> gearSetsByName;
    private static HashMap<Integer, RandomCollection<GearSet>> gearSetsByLevel;
    private static TreeMap<Integer, HashMap<RarityTier, Double>> rarityGearSetChance;

    public GearSetsManager(FileConfiguration config){
        enabled = config.getBoolean("Enabled");
        if(!enabled)
            return;

        loadDisplaySettings(config);
        loadGearSets(config);
        loadRarityChances(config);
    }

    private void loadDisplaySettings(FileConfiguration config){
        String path = "ItemDisplay.";
        itemDisplayNameLine = ChatColor.translateAlternateColorCodes('&', config.getString(path + "Name"));
        itemDisplayValueLine = ChatColor.translateAlternateColorCodes('&', config.getString(path + "Values"));
    }

    private void loadGearSets(FileConfiguration config){
        gearSetsById = new HashMap<>();
        gearSetsByName = new HashMap<>();
        gearSetsByLevel = new HashMap<>();
        for(String gearSetId : config.getConfigurationSection("GearSets").getKeys(false)){
            GearSet gearSet = new GearSet(config, "GearSets." + gearSetId, gearSetId);
            gearSetsById.put(gearSetId, gearSet);
            gearSetsByName.put(ChatColor.stripColor(gearSet.getDisplayName()), gearSet);
        }
        RpgItemsOutputHandler.PrintInfo("Loaded, " + RpgItemsOutputHandler.HIGHLIGHT + gearSetsById.size() + RpgItemsOutputHandler.INFO + " Gear Sets!");
    }

    private void loadRarityChances(FileConfiguration config){
        rarityGearSetChance = new TreeMap<>();

        String path = "RarityChances";
        for(String level : config.getConfigurationSection(path).getKeys(false)){
            if(!TryParse.parseInt(level)) {
                RpgItemsOutputHandler.PrintError("Invalid level value, " + RpgItemsOutputHandler.HIGHLIGHT + level + RpgItemsOutputHandler.ERROR + ", under " + path + ", in gearsets.yml");
                continue;
            }

            Integer lvl = Integer.parseInt(level);
            HashMap<RarityTier, Double> map = new HashMap<>();

            String levelPath = path + "." + level;
            for(String rarity : config.getConfigurationSection(levelPath).getKeys(false)){
                if(!TryParse.parseEnum(RarityTier.class, rarity)) {
                    RpgItemsOutputHandler.PrintError("Invalid rarity, " + RpgItemsOutputHandler.HIGHLIGHT + rarity + RpgItemsOutputHandler.ERROR + ", under " + levelPath + ", in gearsets.yml");
                    continue;
                }

                RarityTier rarityTier = RarityTier.valueOf(rarity);
                String valuePath = levelPath + "." + rarity;
                String value = config.getString(valuePath).replace("%", "");

                if(!TryParse.parseDouble(value)){
                    RpgItemsOutputHandler.PrintError("Invalid value, " + RpgItemsOutputHandler.HIGHLIGHT + value + RpgItemsOutputHandler.ERROR + ", at " + valuePath + ", in gearsets.yml");
                    continue;
                }
                map.put(rarityTier, Double.parseDouble(value));
            }

            rarityGearSetChance.put(lvl, map);
        }
    }

    public static String getItemDisplayNameLine(){
        return itemDisplayNameLine;
    }

    public static String getItemDisplayValueLine(){
        return itemDisplayValueLine;
    }

    public static boolean isEnabled(){
        return enabled;
    }

    public static double getRarityGearSetChance(Integer level, RarityTier tier){
        if(!enabled)
            return 0;

        HashMap<RarityTier, Double> chances = rarityGearSetChance.floorEntry(level).getValue();
        if(chances == null)
            return 0;

        return chances.getOrDefault(tier, 0.0);
    }

    public static GearSet getGearSetById(String gearSetId){
        if(!enabled)
            return null;
        return gearSetsById.get(gearSetId);
    }

    public static GearSet getGearSetByName(String gearSetName){
        if(!enabled)
            return null;
        return gearSetsByName.get(gearSetName);
    }

    public static GearSet getGearSetByLevelAndRarityForItem(Integer level, RarityTier rarityTier, ItemStack itemStack){
        if(!enabled)
            return null;
        if(random.nextDouble() * 100.0 >= getRarityGearSetChance(level, rarityTier))
            return null;

        RandomCollection<GearSet> levelGearSets;
        if(!gearSetsByLevel.containsKey(level)){
            levelGearSets = new RandomCollection<>();

            for(GearSet gs : gearSetsById.values()){
                if(gs.getStartLevel() <= level && level <= gs.getEndLevel())
                    levelGearSets.add(gs.getWeight(), gs);
            }

            gearSetsByLevel.put(level, levelGearSets);
        }
        else{
            levelGearSets = gearSetsByLevel.get(level);
        }

        GearSet gearSet = null;
        MaterialDurability materialDurability = new MaterialDurability(itemStack);
        for(int i=0; i<5; i++){
            GearSet gs = levelGearSets.next();

            if(gs.getGeneratonInfo().canApply(rarityTier, materialDurability)){
                gearSet = gs;
                break;
            }
        }

        return gearSet;
    }

}
