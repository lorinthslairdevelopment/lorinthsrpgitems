package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.objects.*;
import me.lorinth.rpgitems.util.LoreHelper;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ItemGenerationManager {

    private static Random random = new Random();
    private static ArrayList<MaterialDurability> weaponList;
    private static ArrayList<MaterialDurability> armorList;

    private static ItemGenerationManager instance;

    public static boolean isEquipmentItem(ItemStack item){
        return isWeapon(new MaterialDurability(item)) || isArmor(new MaterialDurability(item));
    }

    public static ArrayList<MaterialDurability> getWeaponMaterials(){
        return weaponList;
    }

    public static ArrayList<MaterialDurability> getArmorMaterials(){
        return armorList;
    }

    public ItemGenerationManager(FileConfiguration config){
        instance = this;
        weaponList = new ArrayList<>();
        armorList = new ArrayList<>();

        for(String weapon : config.getStringList("WeaponList")){
            MaterialDurability md = new MaterialDurability(weapon);
            if(md.isValid())
                weaponList.add(md);
            else
                RpgItemsOutputHandler.PrintError("Can't parse weapon, " + weapon + ", in itemgeneration.yml");
        }
        for(String armor : config.getStringList("ArmorList")){
            MaterialDurability md = new MaterialDurability(armor);
            if(md.isValid())
                armorList.add(md);
            else
                RpgItemsOutputHandler.PrintError("Can't parse armor, " + armor + ", in itemgeneration.yml");
        }
    }

    public ItemGenerationManager getInstance(){
        return instance;
    }

    public static RpgGeneratedItem generateItem(int level){
        return generateItem(level, null, null);
    }

    public static RpgGeneratedItem generateItem(int level, RarityTier tier, GearSet gearSet){
        if(tier == null)
            tier = RarityManager.getNextRarity(level);

        if(random.nextDouble() * 100 < 40)
            return getRandomWeapon(level, tier, gearSet);

        return getRandomArmor(level, tier, gearSet);
    }

    public static RpgGeneratedItem generateItemAtRarityOrAbove(int level, RarityTier tier, GearSet gearSet){
        return generateItem(level, RarityManager.getNextRarityAtOrAbove(level, tier), gearSet);
    }

    public static RpgGeneratedItem generateItemWithGivenItemStack(int level, RarityTier tier, ItemStack itemStack, GearSet gearSet){
        if(tier == null)
            tier = RarityManager.getNextRarityAtOrAbove(level, RarityTier.Common);

        MaterialDurability md = new MaterialDurability(itemStack);
        RpgGeneratedItem item = new RpgGeneratedItem(new ItemStack(md.getMaterial(), 1, md.getDurability()), level);
        item.setRarityTier(tier);
        item.setGearSet(gearSet);
        item.setSocketCount(SocketManager.GetNextSocketCount(level, tier));
        return item;
    }

    public static RpgGeneratedItem generateItemWithGivenItemStackAtRarityOrAbove(int level, RarityTier tier, ItemStack itemStack, GearSet gearSet){
        return generateItemWithGivenItemStack(level, RarityManager.getNextRarityAtOrAbove(level, tier), itemStack, gearSet);
    }

    private static RpgGeneratedItem getRandomWeapon(int level, RarityTier tier, GearSet gearSet){
        MaterialDurability md = tier.getRandomWeapon();
        ItemStack itemStack = new ItemStack(md.getMaterial(), 1, md.getDurability());
        RpgGeneratedItem item = new RpgGeneratedItem(itemStack, level);
        item.setRarityTier(tier);
        if(gearSet != null)
            item.setGearSet(gearSet);
        else
            item.setGearSet(GearSetsManager.getGearSetByLevelAndRarityForItem(level, tier, itemStack));
        item.setSocketCount(SocketManager.GetNextSocketCount(level, tier));
        return item;
    }

    private static RpgGeneratedItem getRandomArmor(int level, RarityTier tier, GearSet gearSet){
        MaterialDurability md = tier.getRandomArmor();
        ItemStack itemStack = new ItemStack(md.getMaterial(), 1, md.getDurability());
        RpgGeneratedItem item = new RpgGeneratedItem(itemStack, level);
        item.setRarityTier(tier);
        if(gearSet != null)
            item.setGearSet(gearSet);
        else
            item.setGearSet(GearSetsManager.getGearSetByLevelAndRarityForItem(level, tier, itemStack));
        item.setSocketCount(SocketManager.GetNextSocketCount(level, tier));
        return item;
    }

    public static boolean isWeapon(MaterialDurability material){
        return weaponList.contains(material);
    }

    public static boolean isArmor(MaterialDurability material){
        return armorList.contains(material);
    }

    public static ItemStack applyGenerationTags(ItemStack itemStack){
        if(itemStack == null)
            return null;
        if(itemStack.hasItemMeta()){
            ItemMeta meta = itemStack.getItemMeta();
            if(meta.hasLore()){
                List<String> lore = meta.getLore();
                if(lore == null || lore.size() == 0)
                    return null;

                int generationTagLevel = getGenerationTagLevel(lore);
                if(generationTagLevel == -1)
                    return null;

                //Check for rarity
                RarityTier tier = null;
                boolean tierAndAbove = false;

                int rarityIndex = LoreHelper.getRarityGenerationTagLine(lore);
                if(rarityIndex >= 0){
                    String rarityLine = lore.get(rarityIndex);
                    for(String piece : GenerationTags.rarityTagPieces){
                        rarityLine = rarityLine.replace(piece, "");
                    }

                    if(rarityLine.contains("+"))
                        tierAndAbove = true;
                    rarityLine = rarityLine.replace("+", "");

                    if(TryParse.parseEnum(RarityTier.class, rarityLine))
                        tier = RarityTier.valueOf(rarityLine);

                    lore.remove(rarityIndex);
                }

                GearSet gearSet = null;
                int gearSetIndex = LoreHelper.getGearSetGenerationTagLine(lore);
                if(gearSetIndex >= 0){
                    String gearSetLine = lore.get(gearSetIndex);
                    for(String piece : GenerationTags.gearSetTagPieces){
                        gearSetLine = gearSetLine.replace(piece, "");
                    }

                    gearSet = GearSetsManager.getGearSetById(gearSetLine);
                }

                //Generate Item
                RpgItemStack rpgItemStack = new RpgItemStack(itemStack);
                RpgGeneratedItem generatedItem;

                if(tier != null){
                    if(tierAndAbove)
                        generatedItem = ItemGenerationManager.generateItemWithGivenItemStackAtRarityOrAbove(generationTagLevel, tier, itemStack, gearSet);
                    else
                        generatedItem = ItemGenerationManager.generateItemWithGivenItemStack(generationTagLevel, tier, itemStack, gearSet);
                }
                else
                    generatedItem = ItemGenerationManager.generateItemWithGivenItemStack(generationTagLevel, null, itemStack, gearSet);

                if(generatedItem == null)
                    return itemStack;

                generatedItem.getModifications();
                generatedItem.addAttributes(rpgItemStack.getAttributes());

                //Check for boosted tag
                int boostedIndex = LoreHelper.getBoostedGenerationTagLine(lore);
                if(boostedIndex >= 0){
                    String boostedLine = ChatColor.stripColor(lore.get(boostedIndex));

                    for(String piece : GenerationTags.boostedTagPieces){
                        boostedLine = boostedLine.replace(piece, "");
                    }

                    boostedLine = boostedLine.trim();

                    if(!TryParse.parseDouble(boostedLine)) {
                        return itemStack;
                    }
                    double boostAmount = Double.parseDouble(boostedLine);
                    generatedItem.boostStats(boostAmount);

                    lore.remove(boostedIndex);
                }

                lore = LoreHelper.stripAttributeLore(lore);

                //Replace tags with data
                int unbreakableTag = LoreHelper.getIndexOfTag(lore, "[LriGeneration-Unbreakable]");
                if(unbreakableTag != -1){
                    lore.remove(unbreakableTag);
                    meta.setUnbreakable(true);
                }

                int levelTag = LoreHelper.getIndexOfTag(lore, "[LriGeneration-Level]");
                if(levelTag != -1){
                    lore.set(levelTag, generatedItem.getLevelLine());
                    String itemLevelLine = generatedItem.getItemLevelLine();

                    if(itemLevelLine != null)
                        lore.add(levelTag+1, itemLevelLine);
                }

                int rarityTag = LoreHelper.getIndexOfTag(lore, "[LriGeneration-Rarity]");
                if(rarityTag != -1)
                    lore.set(rarityTag, generatedItem.getRarityLine());

                //Use existing durability if it exists otherwise use generated
                int existingDurabilityIndex = LoreHelper.getDurabilityLine(lore);
                if(generatedItem.getDurabilityLine() != null) {
                    int durabilityTag = LoreHelper.getIndexOfTag(lore, "[LriGeneration-Durability]");

                    if(durabilityTag != -1) {
                        if(existingDurabilityIndex != -1)
                            lore.remove(durabilityTag);
                        else
                            lore.set(durabilityTag, generatedItem.getDurabilityLine());
                    }
                }
                //Add unbreakable if there is any durability tag
                if(existingDurabilityIndex != -1 || generatedItem.getDurabilityLine() != null){
                    meta.setUnbreakable(true);
                }

                int statsTag = LoreHelper.getIndexOfTag(lore, "[LriGeneration-Attributes]");
                if(statsTag != -1){
                    List<String> attributeLore = generatedItem.getAttributeLines();

                    lore.remove(statsTag);
                    lore.addAll(statsTag, attributeLore);
                }

                int nameTag = LoreHelper.getIndexOfTag(lore, "[LriGeneration-Name]");
                if(nameTag != -1){
                    lore.remove(nameTag);
                    meta.setDisplayName(generatedItem.getDisplayName(meta.getDisplayName()));
                }

                meta.setLore(lore);
                itemStack.setItemMeta(meta);
            }
        }
        return itemStack;
    }

    private static int getGenerationTagLevel(List<String> lore){
        int level = LoreHelper.getGenerationTagLevel(lore);
        if(level == -1)
            return -1;

        int line = LoreHelper.getLevelGenerationTagLine(lore);
        lore.remove(line);
        return level;
    }

}
