package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.objects.RandomNullCollection;
import me.lorinth.rpgitems.util.ColorHelper;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.Color;
import org.bukkit.configuration.file.FileConfiguration;

public class LeatherManager {

    private static LeatherManager instance;
    private static RandomNullCollection<Color> colorChances;

    public LeatherManager(FileConfiguration config) {
        instance = this;

        loadColorChances(config);
    }

    private void loadColorChances(FileConfiguration config){
        if(ConfigHelper.ConfigContainsPath(config, "ColorChances")){
            colorChances = new RandomNullCollection<>();

            for(String colorValue : config.getConfigurationSection("ColorChances").getKeys(false)){
                Color color = ColorHelper.fromString(colorValue);
                if(color != null){
                    String chanceValue = config.getString("ColorChances." + colorValue).replace('%', ' ').trim();
                    if(TryParse.parseDouble(chanceValue)){
                        colorChances.add(Double.parseDouble(chanceValue), color);
                    }
                    else{
                        RpgItemsOutputHandler.PrintError("Unable to load chance, " + RpgItemsOutputHandler.HIGHLIGHT + chanceValue + RpgItemsOutputHandler.ERROR + " in leather.yml");
                    }

                } else{
                    RpgItemsOutputHandler.PrintError("Unable to load color, " + RpgItemsOutputHandler.HIGHLIGHT + colorValue + RpgItemsOutputHandler.ERROR + " in leather.yml");
                }
            }
        }
    }

    public static Color getNextLeatherColor(){
        if(colorChances == null)
            return null;
        return colorChances.next();
    }

}
