package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.listener.DeathEventDebugger;
import me.lorinth.rpgitems.objects.MythicDropsCustomItemsInfo;
import me.lorinth.rpgitems.objects.MythicDropsTierInfo;
import me.lorinth.rpgitems.objects.RpgGeneratedItem;
import me.lorinth.rpgitems.util.PluginHelper;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class LootManager {

    private static TreeMap<Integer, Double> rpgItemsDropChances;

    private static boolean mythicDropsChancesEnabled;
    private static TreeMap<Integer, MythicDropsTierInfo> mythicDropsTierInfo;

    private static boolean mythicDropsCustomItemsEnabled;
    private static TreeMap<Integer, MythicDropsCustomItemsInfo> mythicDropsCustomItemsInfo;

    private static Random random = new Random();
    private static boolean debugging = false;

    public LootManager(FileConfiguration config){
        load(config);
    }

    public void load(FileConfiguration config){
        rpgItemsDropChances = new TreeMap<>();
        if(ConfigHelper.ConfigContainsPath(config, "Debug")){
            if(config.getBoolean("Debug")){
                debugging = true;
                Bukkit.getScheduler().runTaskLater(RpgItemsPlugin.instance, () ->
                        Bukkit.getPluginManager().registerEvents(new DeathEventDebugger(), RpgItemsPlugin.instance), 10);
            }
        }

        String pathPrefix = "RpgItemsDropChances";
        for(String key : config.getConfigurationSection(pathPrefix).getKeys(false)){
            if(TryParse.parseInt(key)){
                if(TryParse.parseDouble(config.getString(pathPrefix + "." + key))){
                    rpgItemsDropChances.put(Integer.parseInt(key), config.getDouble(pathPrefix + "." + key));
                }
            }
        }

        mythicDropsChancesEnabled = false;
        pathPrefix = "MythicDropsTierChances";
        if(ConfigHelper.ConfigContainsPath(config, pathPrefix)){
            mythicDropsTierInfo = new TreeMap<>();
            for(String key : config.getConfigurationSection(pathPrefix).getKeys(false)){
                if(key.equalsIgnoreCase("Enabled"))
                    mythicDropsChancesEnabled = config.getBoolean(pathPrefix + ".Enabled");
                else if(TryParse.parseInt(key)){
                    Integer level = Integer.parseInt(key);
                    mythicDropsTierInfo.put(level, new MythicDropsTierInfo(config, level, pathPrefix + "." + key));
                }
            }
        }

        mythicDropsCustomItemsEnabled = false;
        pathPrefix = "MythicDropsCustomItems";
        if(ConfigHelper.ConfigContainsPath(config, pathPrefix)){
            mythicDropsCustomItemsInfo = new TreeMap<>();
            for(String key : config.getConfigurationSection(pathPrefix).getKeys(false)){
                if(key.equalsIgnoreCase("Enabled"))
                    mythicDropsCustomItemsEnabled = config.getBoolean(pathPrefix + ".Enabled");
                else if(TryParse.parseInt(key)){
                    Integer level = Integer.parseInt(key);
                    mythicDropsCustomItemsInfo.put(level, new MythicDropsCustomItemsInfo(config, level, pathPrefix + "." + key));
                }
            }
        }
    }

    public static boolean isDebugging(){
        return debugging;
    }

    public static RpgGeneratedItem getRpgItemDrop(Integer level){
        if(random.nextDouble() * 100 < getDropChance(level))
            return ItemGenerationManager.generateItem(level);

        return null;
    }

    public static Double getDropChance(Integer level){
        Map.Entry<Integer, Double> entry = rpgItemsDropChances.floorEntry(level);
        if(entry != null)
            return entry.getValue();
        return 10.0;
    }

    public static String getMythicDropsTier(Integer level){
        if(level == null)
            return null;
        if(!mythicDropsChancesEnabled)
            return null;
        if(!PluginHelper.MythicDropsEnabled)
            return null;

        Map.Entry<Integer, MythicDropsTierInfo> entry = mythicDropsTierInfo.floorEntry(level);
        if(entry == null)
            return null;

        MythicDropsTierInfo info = entry.getValue();
        if(random.nextDouble() * 100.0 > info.getDropChance())
            return null;

        return info.getNextTier();
    }

    public static String getMythicDropsCustomItem(Integer level){
        if(level == null)
            return null;
        if(!mythicDropsCustomItemsEnabled)
            return null;
        if(!PluginHelper.MythicDropsEnabled)
            return null;

        Map.Entry<Integer, MythicDropsCustomItemsInfo> entry = mythicDropsCustomItemsInfo.floorEntry(level);
        if(entry == null)
            return null;

        MythicDropsCustomItemsInfo info = entry.getValue();
        if(random.nextDouble() * 100.0 > info.getDropChance())
            return null;

        return info.getNextCustomItem();
    }
}
