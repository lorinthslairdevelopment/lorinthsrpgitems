package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.enums.ArmorTier;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.enums.WeaponTier;
import me.lorinth.rpgitems.objects.*;
import me.lorinth.utils.TryParse;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MaterialTierManager {

    private static HashMap<MaterialDurability, List<RpgAttribute>> blockedAttributes;
    private static HashMap<WeaponTier, WeaponTierInfo> weaponTierInfo;
    private static HashMap<ArmorTier, ArmorTierInfo> armorTierInfo;
    private static HashMap<MaterialDurability, CustomTierInfo> customTierInfo;
    private static ArrayList<MaterialDurability> weaponMaterials;
    private static ArrayList<MaterialDurability> armorMaterials;
    private static ArrayList<MaterialDurability> customMaterials;

    private static ArrayList<RpgAttribute> emptylist = new ArrayList<>();

    public MaterialTierManager(FileConfiguration config){
        blockedAttributes = new HashMap<>();
        weaponTierInfo = new HashMap<>();
        armorTierInfo = new HashMap<>();
        customTierInfo = new HashMap<>();
        weaponMaterials = new ArrayList<>();
        armorMaterials = new ArrayList<>();
        customMaterials = new ArrayList<>();

        for(Material mat : Material.values()){
            if(mat.name().contains("_SWORD"))
                weaponMaterials.add(new MaterialDurability(mat, (short) 0));
            else if(mat.name().contains("_AXE"))
                weaponMaterials.add(new MaterialDurability(mat, (short) 0));
            else if(mat.name().equalsIgnoreCase("BOW"))
                weaponMaterials.add(new MaterialDurability(mat, (short) 0));
            else if(mat.name().contains("HELMET"))
                armorMaterials.add(new MaterialDurability(mat, (short) 0));
            else if(mat.name().contains("CHESTPLATE"))
                armorMaterials.add(new MaterialDurability(mat, (short) 0));
            else if(mat.name().contains("LEGGINGS"))
                armorMaterials.add(new MaterialDurability(mat, (short) 0));
            else if(mat.name().contains("BOOTS"))
                armorMaterials.add(new MaterialDurability(mat, (short) 0));
        }

        for(String weaponTier : config.getConfigurationSection("Weapon").getKeys(false)){
            WeaponTierInfo info = new WeaponTierInfo(config, "Weapon.", weaponTier);
            if(info.isValid())
                weaponTierInfo.put(info.getTier(), info);
        }
        for(String armorTier : config.getConfigurationSection("Armor").getKeys(false)){
            ArmorTierInfo info = new ArmorTierInfo(config, "Armor.", armorTier);
            if(info.isValid())
                armorTierInfo.put(info.getTier(), info);
        }
        for(String custom : config.getConfigurationSection("Custom").getKeys(false)){
            CustomTierInfo info = new CustomTierInfo(config, "Custom.", custom);
            if(info.isValid() && info.getTier().isValid()) {
                customTierInfo.put(info.getTier(), info);
                customMaterials.add(info.getTier());
            }
        }

        for(String key : config.getConfigurationSection("AttributeBlacklist").getKeys(false)){
            List<String> blocked = config.getStringList("AttributeBlacklist." + key);
            List<RpgAttribute> blockedAttributeList = new ArrayList<>();
            for(String block : blocked){
                if(TryParse.parseEnum(RpgAttribute.class, block))
                    blockedAttributeList.add(RpgAttribute.valueOf(block));
            }

            //Handle Custom Items or explicit items like WOODEN_SWORD
            MaterialDurability md = new MaterialDurability(key);
            if(md.isValid())
                blockedAttributes.put(md, blockedAttributeList);
            else{
                //Handle Vanilla Items
                key = key.toUpperCase();
                for(Material material : Material.values()){
                    if(material.name().endsWith(key)){
                        blockedAttributes.put(new MaterialDurability(material, (short) 0), blockedAttributeList);
                    }
                }
            }
        }
    }

    public static IItemTier getItemTierInfo(ItemStack itemStack){
        MaterialDurability md = new MaterialDurability(itemStack);

        if(customMaterials.contains(md))
            return getCustomTierInfo(itemStack);
        else if(weaponMaterials.contains(md))
            return getWeaponTierInfo(itemStack);
        else if(armorMaterials.contains(md))
            return getArmorTierInfo(itemStack);
        return null;
    }

    private static WeaponTierInfo getWeaponTierInfo(ItemStack itemStack){
        MaterialDurability md = new MaterialDurability(itemStack);
        for(WeaponTier tier : WeaponTier.values()){
            if(md.getMaterial().name().startsWith(tier.name().toUpperCase())){
                return weaponTierInfo.get(tier);
            }
        }
        return null;
    }

    private static ArmorTierInfo getArmorTierInfo(ItemStack itemStack){
        MaterialDurability md = new MaterialDurability(itemStack);
        for(ArmorTier tier : ArmorTier.values()){
            if(md.getMaterial().name().startsWith(tier.name().toUpperCase())){
                return armorTierInfo.get(tier);
            }
        }
        return null;
    }

    private static CustomTierInfo getCustomTierInfo(ItemStack itemStack){
        MaterialDurability md = new MaterialDurability(itemStack);
        return customTierInfo.get(md);
    }

    public static List<RpgAttribute> getBlockedAttributes(ItemStack item){
        MaterialDurability md = new MaterialDurability(item);
        if(blockedAttributes.containsKey(md))
            return blockedAttributes.get(md);
        return emptylist;
    }

}
