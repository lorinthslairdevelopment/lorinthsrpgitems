package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.objects.AttributeRangeMap;
import me.lorinth.rpgitems.objects.Modification;
import me.lorinth.rpgitems.objects.RandomCollection;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;

public class ModificationManager {

    private static HashMap<Integer, RandomCollection<Modification>> modificationsByLevel;
    private static int minLevel = 0;
    private static int maxLevel = 100;
    private static int maxItemLevel = 200;

    public ModificationManager(FileConfiguration config){
        modificationsByLevel = new HashMap<>();
        loadConfig(config);
        for(String key : config.getConfigurationSection("").getKeys(false)){
            if(!key.equalsIgnoreCase("Config"))
                loadModification(config, key);
        }
    }

    public static int getMaxLevel(){
        return maxLevel;
    }

    public static boolean isItemLevelDisabled(){
        return maxItemLevel == -1 || maxItemLevel == maxLevel;
    }

    public static int getMaxItemLevel(){
        return maxItemLevel;
    }

    private void loadConfig(FileConfiguration config){
        if(ConfigHelper.ConfigContainsPath(config, "Config.MinimumLevel"))
            minLevel = config.getInt("Config.MinimumLevel");
        if(ConfigHelper.ConfigContainsPath(config, "Config.MaximumLevel"))
            maxLevel = config.getInt("Config.MaximumLevel");
        if(ConfigHelper.ConfigContainsPath(config, "Config.MaximumItemLevel"))
            maxItemLevel = config.getInt("Config.MaximumItemLevel");
    }

    private void loadModification(FileConfiguration config, String key){
        Modification modification = new Modification();

        key += ".";
        if(ConfigHelper.ConfigContainsPath(config, key + "Prefix"))
            modification.setPrefix(config.getString(key + "Prefix"));
        if(ConfigHelper.ConfigContainsPath(config, key + "Suffix"))
            modification.setSuffix(config.getString(key + "Suffix"));
        if(ConfigHelper.ConfigContainsPath(config, key + "StartLevel"))
            modification.setStartLevel(config.getInt(key + "StartLevel"));
        if(ConfigHelper.ConfigContainsPath(config, key + "EndLevel"))
            modification.setEndLevel(config.getInt(key + "EndLevel"));
        if(ConfigHelper.ConfigContainsPath(config, key + "CanDuplicate"))
            modification.setCanDuplicate(config.getBoolean(key + "CanDuplicate"));

        String path = key + "Attributes";
        modification.setAttributes(AttributeRangeMap.FromConfig(config, path));

        addModificationToLevels(modification);
    }

    private void addModificationToLevels(Modification modification){
        int start = minLevel;
        int end = maxLevel;
        if(modification.getStartLevel() != null)
            start = modification.getStartLevel();
        if(modification.getEndLevel() != null)
            end = modification.getEndLevel();

        if(end == maxLevel)
            end ++;
        for(int i=start; i<end; i++){
            addModificationAtLevel(modification, i);
        }
    }

    private void addModificationAtLevel(Modification modification, int level){
        if(!modificationsByLevel.containsKey(level))
            modificationsByLevel.put(level, new RandomCollection<>());

        modificationsByLevel.get(level).add(modification.getWeight(), modification);
    }

    public static Modification nextModification(Integer level){
        if(modificationsByLevel.containsKey(level))
            return modificationsByLevel.get(level).next();
        return null;
    }
}
