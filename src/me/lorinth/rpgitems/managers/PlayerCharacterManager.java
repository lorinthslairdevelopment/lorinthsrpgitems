package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.objects.RpgCharacter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class PlayerCharacterManager {

    private static HashMap<String, RpgCharacter> loadedPlayerCharacters;

    public PlayerCharacterManager(){
        loadedPlayerCharacters = new HashMap<>();
        loadCurrentPlayers();
    }

    private void loadCurrentPlayers(){
        Bukkit.getScheduler().runTaskAsynchronously(RpgItemsPlugin.instance, () -> {
            Bukkit.getOnlinePlayers().forEach(PlayerCharacterManager::loadCharacter);
        });
    }

    public static void saveLoadedPlayers(){
        Bukkit.getScheduler().runTaskAsynchronously(RpgItemsPlugin.instance, () -> {
            for(RpgCharacter rpgCharacter : loadedPlayerCharacters.values()){
                rpgCharacter.saveCharacter(true);
            }
        });
    }

    public static void saveSynchronously(){
        for(RpgCharacter rpgCharacter : loadedPlayerCharacters.values()){
            rpgCharacter.saveCharacter(false);
        }
    }

    public static RpgCharacter getCharacter(Player player){
        String key = player.getUniqueId().toString();
        return loadedPlayerCharacters.get(key);
    }

    public static RpgCharacter loadCharacter(Player player){
        String key = player.getUniqueId().toString();
        if(loadedPlayerCharacters.containsKey(key)){
            return loadedPlayerCharacters.get(key);
        }

        RpgCharacter character = new RpgCharacter(player);
        character.loadCharacter();
        loadedPlayerCharacters.put(key, character);

        return character;
    }

    public static boolean unloadCharacter(Player player){
        String key = player.getUniqueId().toString();
        if(loadedPlayerCharacters.containsKey(key)){
            RpgCharacter character = loadedPlayerCharacters.remove(key);
            character.saveCharacter(true);
            return true;
        }
        return false;
    }

}
