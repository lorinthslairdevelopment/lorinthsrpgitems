package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.objects.MaterialDurability;
import me.lorinth.rpgitems.objects.RandomCollection;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class RarityManager {

    public static RarityManager instance;
    private static Random random = new Random();
    private static TreeMap<Integer, RandomCollection<RarityTier>> rarityChancesMobDrops = new TreeMap<>();
    private static TreeMap<Integer, RandomCollection<RarityTier>> rarityChancesItemCraft = new TreeMap<>();
    private static boolean useCraftedChances = true;

    public RarityManager(FileConfiguration config){
        instance = this;
        loadRarities(config);
    }

    private void loadRarities(FileConfiguration config){
        String path = "Rarity.";
        for(RarityTier tier : RarityTier.values()){
            String subPath = path + tier.name() + ".";
            tier.setDisplayName(config.getString(subPath + "Display"));
            tier.setColorCode(ChatColor.translateAlternateColorCodes('&', config.getString(subPath + "ColorCode")));
            tier.setModificationCount(config.getInt(subPath + "Modifications"));
            tier.setAttributeMultiplier(config.getDouble(subPath + "AttributeMultiplier"));

            ArrayList<MaterialDurability> materials = new ArrayList<>();
            for(String material : config.getStringList(subPath + "Materials")){
                //Custom item with durability
                MaterialDurability md = new MaterialDurability(material);
                if(md.isValid())
                    materials.add(md);
                else
                    RpgItemsOutputHandler.PrintError("Can't parse material, '" + material + "', in rarity, '" + tier + "', in rarity.yml");
            }

            ArrayList<MaterialDurability> weapons = new ArrayList<>();
            ArrayList<MaterialDurability> armor = new ArrayList<>();

            for(MaterialDurability mat : materials){
                if(ItemGenerationManager.isWeapon(mat))
                    weapons.add(mat);
                else if(ItemGenerationManager.isArmor(mat))
                    armor.add(mat);
                else
                    RpgItemsOutputHandler.PrintError(tier.name() + " has invalid material, " + mat.toString());
            }

            tier.setWeapons(weapons);
            tier.setArmor(armor);
        }

        loadRarityChances(config, "RarityChance.MobDrops", rarityChancesMobDrops);
        loadRarityChances(config, "RarityChance.Crafted", rarityChancesItemCraft);

        if(ConfigHelper.ConfigContainsPath(config, "RarityChance.Crafted.Enabled")){
            useCraftedChances = config.getBoolean("RarityChance.Crafted.Enabled");
        }
    }

    private void loadRarityChances(FileConfiguration config, String path, TreeMap<Integer, RandomCollection<RarityTier>> chanceMap){
        for(String level : config.getConfigurationSection(path).getKeys(false)){
            if(TryParse.parseInt(level)){
                Integer Level = Integer.parseInt(level);
                RandomCollection<RarityTier> localRarityChances = new RandomCollection<>();
                for(RarityTier rarityTier : RarityTier.values()){
                    if(ConfigHelper.ConfigContainsPath(config, path + "." + level + "." + rarityTier.name())){
                        localRarityChances.add(config.getDouble(path + "." + level + "." + rarityTier.name()), rarityTier);
                    }
                }

                chanceMap.put(Level, localRarityChances);
            }
        }
    }

    public static RarityTier getNextRarity(Integer level){
        return rarityChancesMobDrops.floorEntry(level).getValue().next();
    }

    public static RarityTier getNextRarityAtOrAbove(Integer level, RarityTier rarityTier){
        RandomCollection<RarityTier> tiers = rarityChancesMobDrops.floorEntry(level).getValue();
        RandomCollection<RarityTier> newTierList = new RandomCollection<>(random);
        for(Map.Entry<RarityTier, Double> entry : tiers.getChances().entrySet()){
            if(entry.getKey().ordinal() >= rarityTier.ordinal()) {
                newTierList.add(entry.getValue(), entry.getKey());
            }
        }
        return newTierList.next();
    }

    public static RarityTier getNextCraftedRarity(Integer level, ItemStack itemStack){
        if(useCraftedChances){
            return rarityChancesItemCraft.floorEntry(level).getValue().next();
        } else {
            return getNextRarityTierForItem(level, new MaterialDurability(itemStack));
        }
    }

    public static RarityTier getNextRarityTierForItem(Integer level, MaterialDurability materialDurability){
        int ordinal = -1;
        RarityTier lowest = null;
        for(RarityTier rarityTier : RarityTier.values()){
            if(rarityTier.hasItem(materialDurability)){
                if(lowest == null){
                    lowest = rarityTier;
                    ordinal = rarityTier.ordinal();
                }
                else if(rarityTier.ordinal() < ordinal){
                    lowest = rarityTier;
                    ordinal = rarityTier.ordinal();
                }
            }
        }

        return getNextRarityAtOrAbove(level, lowest);
    }

}
