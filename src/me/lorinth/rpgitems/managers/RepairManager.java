package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.listener.RepairListener;
import me.lorinth.rpgitems.objects.MaterialDurability;
import me.lorinth.rpgitems.objects.gui.RepairGuiOptions;
import me.lorinth.rpgitems.objects.structure.MultiBlockStructure;
import me.lorinth.rpgitems.util.*;
import me.lorinth.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.*;

public class RepairManager {

    public static boolean enabled = false;
    public static boolean useMcMMoRepair = true;
    public static boolean allowItemClickRepair = false;
    public static boolean useMultiBlockStructure = false;

    public static String repairMessage;
    public static String fullDurabilityMessage;
    public static String createdMultiblockStructure;
    public static String destroyedMultiblockStructure;

    private static MultiBlockStructure repairStructure;
    private static RepairListener repairListener;

    private static RepairGuiOptions repairGuiOptions;

    private static HashMap<MaterialDurability, Double> materialRepairValue;
    private static HashMap<ItemStack, Double> customRepairMaterialValue;

    public RepairManager(FileConfiguration config){
        if(ConfigHelper.ConfigContainsPath(config, "Enabled")){
            enabled = config.getBoolean("Enabled");
            if(!enabled)
                return;

            RpgItemsOutputHandler.PrintInfo("Repair system enabled!");
        }

        useMcMMoRepair = config.getBoolean("useMcMMoRepair");
        allowItemClickRepair = config.getBoolean("allowItemClickRepair");
        useMultiBlockStructure = config.getBoolean("useMultiBlockStructure");
        repairMessage = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.Repair"));
        fullDurabilityMessage = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.FullDurability"));
        createdMultiblockStructure = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.CreatedMultiblockStructure"));
        destroyedMultiblockStructure = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.DestroyedMultiblockStructure"));

        materialRepairValue = new HashMap<>();
        for(String key : config.getConfigurationSection("RepairMaterials").getKeys(false)){
            MaterialDurability md = new MaterialDurability(key);
            if(md.isValid()){
                materialRepairValue.put(md, config.getDouble("RepairMaterials." + key));
            }
        }

        customRepairMaterialValue = new HashMap<>();
        if(ConfigHelper.ConfigContainsPath(config, "CustomRepairMaterials")){
            for(String key : config.getConfigurationSection("CustomRepairMaterials").getKeys(false)){
                Double repairValue = config.getDouble("CustomRepairMaterials." + key + ".RepairValue");
                ItemStack customMaterial = ItemStackHelper.makeItemFromConfig(config, "CustomRepairMaterials", key, "repair.yml", RpgItemsOutputHandler.getInstance());
                customRepairMaterialValue.put(customMaterial, repairValue);
            }
        }

        if(useMultiBlockStructure){
            JavaPlugin rpgItems = RpgItemsPlugin.instance;
            String path = rpgItems.getDataFolder() + File.separator + "MultiblockStructures";

            File file = new File(path, "RepairStructure.json");
            if(!file.exists()){
                ResourceHelper.copy(rpgItems.getResource("resources/RepairStructure.json"), new File(path, "RepairStructure.json"));
            }

            repairStructure = new MultiBlockStructure(new File(path, "RepairStructure.json"));
            loadRepairUI(config);

            repairListener = new RepairListener();
            Bukkit.getPluginManager().registerEvents(repairListener, RpgItemsPlugin.instance);
        }
    }

    private void loadRepairUI(FileConfiguration config){
        if(ConfigHelper.ConfigContainsPath(config, "GUI")){
            repairGuiOptions = new RepairGuiOptions();

            String path = "GUI.";

            // Load Clicks
            if(TryParse.parseEnum(ClickType.class, config.getString(path + "SingleRepairClick")))
                repairGuiOptions.SingleRepairClick = ClickType.valueOf(config.getString(path + "SingleRepairClick"));
            else
                RpgItemsOutputHandler.PrintError("Repair.yml, SingleRepairClick is not a valid ClickType");

            if(TryParse.parseEnum(ClickType.class, config.getString(path + "MultiRepairClick")))
                repairGuiOptions.MultiRepairClick = ClickType.valueOf(config.getString(path + "MultiRepairClick"));
            else
                RpgItemsOutputHandler.PrintError("Repair.yml, MultiRepairClick is not a valid ClickType");

            repairGuiOptions.Title = ChatColor.translateAlternateColorCodes('&', config.getString(path + "Title"));
            repairGuiOptions.Rows = config.getInt(path + "Rows");

            // Load Slots
            path = "GUI.Slots.";
            repairGuiOptions.Slots.Instruction = config.getInt(path + "Instruction");
            repairGuiOptions.Slots.RepairValues = config.getInt(path + "RepairValues");
            repairGuiOptions.Slots.TargetRepairItem = config.getInt(path + "TargetRepairItem");
            repairGuiOptions.Slots.RepairMaterial = config.getInt(path + "RepairMaterial");
            repairGuiOptions.Slots.Result = config.getInt(path + "Result");

            // Load Items
            path = "GUI.Items";
            repairGuiOptions.Instructions = ItemStackHelper.makeItemFromConfig(config, path, "Instructions", "repair.yml", RpgItemsOutputHandler.getInstance());
            repairGuiOptions.RepairValues = parseRepairValuesItem(ItemStackHelper.makeItemFromConfig(config, path, "RepairValues", "repair.yml", RpgItemsOutputHandler.getInstance()));
            repairGuiOptions.LockedSlot = ItemStackHelper.makeItemFromConfig(config, path, "LockedSlot", "repair.yml", RpgItemsOutputHandler.getInstance());
        }
        else{
            RpgItemsOutputHandler.PrintError("Repair.yml, has no existing configuration options for GUI");
        }
    }

    private ItemStack parseRepairValuesItem(ItemStack item){
        if(item == null)
            return null;
        ItemMeta meta = item.getItemMeta();

        List<String> finalLore = meta.getLore();
        String format = "";
        Integer line = 0;

        for(int i=0; i<finalLore.size(); i++){
            String loreLine = finalLore.get(i);
            if(loreLine.contains("{material}") && loreLine.contains("{value}")){
                line = i;
                format = loreLine;
                break;
            }
        }

        finalLore.remove(format);

        List<String> lore = new ArrayList<>();

        List<Map.Entry<MaterialDurability, Double>> sortedEntries = new ArrayList<>(RepairManager.getMaterialRepairValues().entrySet());
        Collections.sort(sortedEntries,
                Comparator.comparing(Map.Entry::getValue)
        );

        for(Map.Entry<MaterialDurability, Double> entry : sortedEntries){
            lore.add(format.replace("{material}", entry.getKey().getMaterial().name())
                            .replace("{value}", LoreHelper.DecimalFormat.format(entry.getValue())));
        }

        finalLore.addAll(line, lore);
        meta.setLore(finalLore);
        item.setItemMeta(meta);

        return item;
    }

    public static RepairGuiOptions getRepairGuiOptions(){
        return repairGuiOptions;
    }

    public static HashMap<MaterialDurability, Double> getMaterialRepairValues(){
        return materialRepairValue;
    }

    public static Double getRepairValue(ItemStack repairMaterial){
        Double repairValue = getCustomRepairValue(repairMaterial);
        if(repairValue != null)
            return repairValue;

        return materialRepairValue.getOrDefault(new MaterialDurability(repairMaterial), null);
    }

    private static Double getCustomRepairValue(ItemStack customRepairItem){
        for(Map.Entry<ItemStack, Double> entry : customRepairMaterialValue.entrySet()){
            if(entry.getKey().isSimilar(customRepairItem)){
                return entry.getValue();
            }
        }
        return null;
    }

    public static MultiBlockStructure getRepairStructure(){
        return repairStructure;
    }

    public static void onDisable(){
        if(repairStructure != null) {
            repairStructure.save();
        }
        if(repairListener != null){
            HandlerList.unregisterAll(repairListener);
        }
    }

}
