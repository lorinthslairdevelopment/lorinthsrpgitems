package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.objects.MaterialDurability;
import me.lorinth.rpgitems.objects.RpgItemStack;
import me.lorinth.rpgitems.objects.gui.SellGuiOptions;
import me.lorinth.rpgitems.objects.SellMessages;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.rpgitems.util.SettingsHelper;
import me.lorinth.utils.Calculator;
import me.lorinth.utils.ItemStackHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class SellManager {

    private static boolean enabled;
    private static SellGuiOptions sellGuiOptions;
    private static SellMessages sellMessages;
    private static String rpgValueFormula;
    private static HashMap<RpgAttribute, Double> attributeValues;
    private static HashMap<MaterialDurability, Double> materialValues;
    private static HashMap<RarityTier, Double> rarityValues;
    private static VaultManager vaultManager;

    public SellManager(FileConfiguration config) {
        enabled = config.getBoolean("Enabled");
        vaultManager = new VaultManager(enabled);

        if(enabled){
            RpgItemsOutputHandler.PrintInfo("SellManager Enabled");
            loadSellMessages(config, "Messages");
            loadSellGuiOptions(config, "GUI");
            rpgValueFormula = config.getString("RpgValueFormula");
            loadAttributeValues(config, "AttributeValues");
            loadRarityValues(config, "RarityValues");
            loadValues(config, "Values");
        }
    }

    private void loadSellMessages(FileConfiguration config, String path){
        sellMessages = new SellMessages();
        sellMessages.Sold = ChatColor.translateAlternateColorCodes('&', config.getString(path + ".Sold"));
    }

    private void loadSellGuiOptions(FileConfiguration config, String path){
        sellGuiOptions = new SellGuiOptions();

        sellGuiOptions.Title = ChatColor.translateAlternateColorCodes('&', config.getString(path + ".Title"));
        sellGuiOptions.Rows = config.getInt(path + ".Rows");
        sellGuiOptions.SingleSelectClick = ClickType.valueOf(config.getString(path + ".SingleSelectClick"));
        sellGuiOptions.MultiSelectClick = ClickType.valueOf(config.getString(path + ".MultiSelectClick"));
        sellGuiOptions.UseDoubleConfirm = config.getBoolean(path + ".UseDoubleConfirm");
        sellGuiOptions.IgnoreHotbarClick = config.getBoolean(path + ".IgnoreHotbarClick");

        String slotPath = path + ".Slots.";
        sellGuiOptions.Slots.Total = config.getInt(slotPath + "Total");
        sellGuiOptions.Slots.Confirm = config.getInt(slotPath + "Confirm");
        sellGuiOptions.Slots.Cancel = config.getInt(slotPath + "Cancel");

        String itemPath = path + ".Items";
        sellGuiOptions.Items.Total = ItemStackHelper.makeItemFromConfig(config, itemPath, "Total", "sell.yml", RpgItemsOutputHandler.getInstance());
        sellGuiOptions.Items.Confirm = ItemStackHelper.makeItemFromConfig(config, itemPath, "Confirm", "sell.yml", RpgItemsOutputHandler.getInstance());
        sellGuiOptions.Items.DoubleConfirm = ItemStackHelper.makeItemFromConfig(config, itemPath, "DoubleConfirm", "sell.yml", RpgItemsOutputHandler.getInstance());
        sellGuiOptions.Items.Cancel = ItemStackHelper.makeItemFromConfig(config, itemPath, "Cancel", "sell.yml", RpgItemsOutputHandler.getInstance());
    }

    private void loadAttributeValues(FileConfiguration config, String path){
        attributeValues = new HashMap<>();
        for(String attributeName : config.getConfigurationSection(path).getKeys(false)){
            if(TryParse.parseEnum(RpgAttribute.class, attributeName)){
                attributeValues.put(RpgAttribute.valueOf(attributeName), config.getDouble(path + "." + attributeName));
            }
            else{
                RpgItemsOutputHandler.PrintError("Failed to parse, " + RpgItemsOutputHandler.HIGHLIGHT + attributeName + RpgItemsOutputHandler.ERROR + " to RpgAttributeType in sell.yml");
            }
        }
    }

    private void loadRarityValues(FileConfiguration config, String path){
        rarityValues = new HashMap<>();
        for(String rarityName : config.getConfigurationSection(path).getKeys(false)){
            if(TryParse.parseEnum(RarityTier.class, rarityName)){
                rarityValues.put(RarityTier.valueOf(rarityName), config.getDouble(path + "." + rarityName));
            }
            else{
                RpgItemsOutputHandler.PrintError("Failed to parse, " + RpgItemsOutputHandler.HIGHLIGHT + rarityName + RpgItemsOutputHandler.ERROR + " to RarityTier in sell.yml");
            }
        }
    }

    private void loadValues(FileConfiguration config, String path){
        materialValues = new HashMap<>();
        for(String material : config.getConfigurationSection(path).getKeys(false)){
            MaterialDurability md = new MaterialDurability(material);
            if(md.isValid()){
                materialValues.put(md, config.getDouble(path + "." + material));
            }
            else{
                RpgItemsOutputHandler.PrintError("Failed to parse, " + RpgItemsOutputHandler.HIGHLIGHT + material + RpgItemsOutputHandler.ERROR + " to MaterialDurability in sell.yml");
            }
        }
    }

    public static boolean isEnabled(){
        return enabled;
    }

    public static SellGuiOptions getSellGuiOptions(){
        return sellGuiOptions;
    }

    public static SellMessages getSellMessages(){ return sellMessages; }

    public static Double getValueOfItem(ItemStack item){
        return getValueofRpgItemStack(new RpgItemStack(item));
    }

    private static Double getValueOfItemStack(ItemStack item){
        MaterialDurability md = new MaterialDurability(item);
        return materialValues.getOrDefault(md, 0.0) * item.getAmount();
    }

    private static Double getValueofRpgItemStack(RpgItemStack rpgItem){
        if(!rpgItem.isRpgItem())
            return getValueOfItemStack(rpgItem.getBukkitItem());

        String formulaInstance = rpgValueFormula.replaceAll(Pattern.quote("{level}"), rpgItem.getILevel().toString())
                                                .replaceAll(Pattern.quote("{rarityValue}"), getValueOfRarityTier(rpgItem.getRarityTier()).toString())
                                                .replaceAll(Pattern.quote("{attributeValues}"), getAttributeValues(rpgItem).toString())
                                                .replaceAll(Pattern.quote("{rawValue}"), getValueOfItemStack(rpgItem.getBukkitItem()).toString());
        return Calculator.eval(SettingsHelper.formulaMethod, formulaInstance);
    }

    private static Double getValueOfRarityTier(RarityTier rarity){
        return rarityValues.getOrDefault(rarity, 1.0);
    }

    private static Double getAttributeValues(RpgItemStack rpgItemStack){
        Double multiplier = 1.0;
        for(RpgAttribute attribute : rpgItemStack.getAttributes().getNonZeroAttributes().keySet()){
            multiplier *= attributeValues.getOrDefault(attribute, 1.0);
        }
        return multiplier;
    }

    public static Double sellItems(Player player, List<ItemStack> items){
        Double total = 0.0d;
        for(ItemStack item : items){
            total += getValueOfItem(item);
        }

        return vaultManager.addMoney(player, total) ? total : 0.0d;
    }

}
