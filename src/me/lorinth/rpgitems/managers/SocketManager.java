package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.listener.SocketGemListener;
import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.objects.RandomCollection;
import me.lorinth.rpgitems.objects.RandomNullCollection;
import me.lorinth.rpgitems.objects.sockets.*;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.ItemStackHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class SocketManager {

    private static boolean enabled = false;
    private static SocketFailureMessages socketFailureMessages;
    private static SocketDisplaySettings socketDisplaySettings;
    private static HashMap<String, SocketGem> socketGemsById;
    private static TreeMap<Integer, HashMap<RarityTier, RandomNullCollection<Integer>>> rarityTierSlotChances;
    private static TreeMap<Integer, Double> socketDropChances;
    private static TreeMap<Integer, RandomCollection<SocketGem>> socketGemDropChances;
    private static Random random = new Random();

    public SocketManager(FileConfiguration config) {
        enabled = config.getBoolean("Enabled");
        if (!enabled) {
            return;
        }

        loadMessages(config);
        loadDisplay(config);
        loadGems(config);
        loadSocketSlotInfo(config);
        loadDropInfo(config);

        Bukkit.getPluginManager().registerEvents(new SocketGemListener(), RpgItemsPlugin.instance);

        RpgItemsOutputHandler.PrintInfo("Socket System Enabled!");
    }

    public static SocketDisplaySettings GetSocketDisplaySettings(){
        return socketDisplaySettings;
    }

    public static SocketFailureMessages GetSocketFailureMessages() {
        return socketFailureMessages;
    }

    public static int GetNextSocketCount(int level, RarityTier rarityTier){
        if(!enabled){
            return 0;
        }

        Map.Entry<Integer, HashMap<RarityTier, RandomNullCollection<Integer>>> levelSettings = rarityTierSlotChances.floorEntry(level);
        if(levelSettings == null){
            return 0;
        }

        RandomCollection<Integer> slotChances = levelSettings.getValue().get(rarityTier);
        if(slotChances == null){
            return 0;
        }

        Integer slotRoll = slotChances.next();
        if(slotRoll == null){
            return 0;
        }
        return slotRoll;
    }

    public static SocketGem GetNextSocketGem(int level){
        if(!enabled){
            return null;
        }
        Map.Entry<Integer, Double> entry = socketDropChances.floorEntry(level);
        if(entry == null){
            return null;
        }

        if(random.nextDouble() * 100 > entry.getValue()){
            return null;
        }

        Map.Entry<Integer, RandomCollection<SocketGem>> socketGemChances = socketGemDropChances.floorEntry(level);
        if(socketGemChances == null){
            return null;
        }

        return socketGemChances.getValue().next();
    }

    public static SocketGem GetSocketGemById(String id){
        return socketGemsById.get(id);
    }

    private void loadMessages(FileConfiguration config){
        socketFailureMessages = new SocketFailureMessages();

        String path = "Messages.";
        socketFailureMessages.NoOpenSlots = ChatColor.translateAlternateColorCodes('&', config.getString(path + "NoOpenSlots"));
        socketFailureMessages.GearLowLevel = ChatColor.translateAlternateColorCodes('&', config.getString(path + "GearLowLevel"));
        socketFailureMessages.InvalidSlot = ChatColor.translateAlternateColorCodes('&', config.getString(path + "InvalidSlot"));
        socketFailureMessages.FilledSlot = ChatColor.translateAlternateColorCodes('&', config.getString(path + "FilledSlot"));
    }

    private void loadDisplay(FileConfiguration config){
        socketDisplaySettings = new SocketDisplaySettings();

        String path = "Display.";
        socketDisplaySettings.FilledSlot = ChatColor.translateAlternateColorCodes('&', config.getString(path + "FilledSlot"));
        socketDisplaySettings.OpenSlot = ChatColor.translateAlternateColorCodes('&', config.getString(path + "OpenSlot"));

        socketDisplaySettings.SocketHeader = new SocketHeader(
                config.getBoolean(path + "Header.Enabled"),
                ChatColor.translateAlternateColorCodes('&', config.getString(path + "Header.Text"))
        );
        socketDisplaySettings.SocketFooter = new SocketFooter(
                config.getBoolean(path + "Footer.Enabled"),
                ChatColor.translateAlternateColorCodes('&', config.getString(path + "Footer.Text"))
        );
    }

    private void loadGems(FileConfiguration config){
        socketGemsById = new HashMap<>();

        String path = "Gems";
        for(String id : config.getConfigurationSection(path).getKeys(false)){
            ItemStack item = ItemStackHelper.makeItemFromConfig(config, path, id, "sockets.yml", RpgItemsOutputHandler.getInstance());
            if(item == null){
                continue;
            }

            AttributeMap bonuses = AttributeMap.load(config, path + "." + id + ".Bonuses");
            SocketGem gem = new SocketGem(id, bonuses, item);
            socketGemsById.put(id, gem);
        }
    }

    private void loadSocketSlotInfo(FileConfiguration config){
        rarityTierSlotChances = new TreeMap<>();

        String path = "Chances.Slots";
        for(String levelValue : config.getConfigurationSection(path).getKeys(false)){
            if(!TryParse.parseInt(levelValue)){
                RpgItemsOutputHandler.PrintError("Invalid integer value, " + RpgItemsOutputHandler.HIGHLIGHT + levelValue + RpgItemsOutputHandler.INFO +  ", at " + path);
                continue;
            }

            Integer level = Integer.parseInt(levelValue);
            HashMap<RarityTier, RandomNullCollection<Integer>> levelRarityChances = new HashMap<>();
            for(String rarityValue : config.getConfigurationSection(path + "." + levelValue).getKeys(false)){
                if(!TryParse.parseEnum(RarityTier.class, rarityValue)){
                    RpgItemsOutputHandler.PrintError("Invalid rarity tier, " + RpgItemsOutputHandler.HIGHLIGHT + rarityValue + RpgItemsOutputHandler.INFO + ", at " + path + "." + levelValue);
                    continue;
                }

                RarityTier tier = RarityTier.valueOf(rarityValue);
                RandomNullCollection<Integer> socketChances = new RandomNullCollection<>();

                for (String socketCountValue : config.getConfigurationSection(path + "." + levelValue + "." + rarityValue).getKeys(false)){
                    if(!TryParse.parseInt(socketCountValue)){
                        RpgItemsOutputHandler.PrintError("Invalid integer value, " + RpgItemsOutputHandler.HIGHLIGHT + socketCountValue + RpgItemsOutputHandler.INFO +  ", at " + path + "." + levelValue + "." + rarityValue);
                        continue;
                    }

                    Integer socketCount = Integer.parseInt(socketCountValue);
                    String doubleChance = config.getString(path + "." + levelValue + "." + rarityValue + "." + socketCountValue).replace('%', ' ').trim();
                    double chance = Double.parseDouble(doubleChance);
                    socketChances.add(chance, socketCount);
                }
                levelRarityChances.put(tier, socketChances);
            }
            rarityTierSlotChances.put(level, levelRarityChances);
        }
    }

    private void loadDropInfo(FileConfiguration config){
        socketDropChances = new TreeMap<>();
        socketGemDropChances = new TreeMap<>();

        String path = "Chances.Drops";
        for(String levelValue : config.getConfigurationSection(path).getKeys(false)){
            if(!TryParse.parseInt(levelValue)){
                RpgItemsOutputHandler.PrintError("Invalid integer value, " + RpgItemsOutputHandler.HIGHLIGHT + levelValue + RpgItemsOutputHandler.INFO +  ", at " + path);
                continue;
            }

            Integer level = Integer.parseInt(levelValue);
            Double dropChance = 0.0d;
            RandomCollection<SocketGem> socketGemDropChance = new RandomCollection<>();
            for(String key : config.getConfigurationSection(path + "." + levelValue).getKeys(false)){
                if(key.equalsIgnoreCase("chance")){
                    String dropValue = config.getString(path + "." + levelValue + "." + key).replace('%', ' ').trim();
                    dropChance = Double.parseDouble(dropValue);
                }
                else{
                    SocketGem gem = socketGemsById.get(key);
                    if(gem == null){
                        RpgItemsOutputHandler.PrintError("Invalid socket gem id, " + RpgItemsOutputHandler.HIGHLIGHT + key + RpgItemsOutputHandler.INFO +  ", at " + path + "." + levelValue);
                        continue;
                    }

                    socketGemDropChance.add(config.getDouble(path + "." + levelValue + "." + key), gem);
                }
            }
            socketGemDropChances.put(level, socketGemDropChance);
            socketDropChances.put(level, dropChance);
        }
    }

    public static boolean isEnabled(){
        return enabled;
    }

}
