package me.lorinth.rpgitems.managers;

import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import static org.bukkit.Bukkit.getServer;

public class VaultManager {

    private boolean enabled = false;
    public Economy economy;

    public VaultManager(boolean enabled){
        this.enabled = enabled && setupEconomy();
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            RpgItemsOutputHandler.PrintInfo("No Vault Plugin");
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            RpgItemsOutputHandler.PrintInfo("No RegisteredServiceProvider");
            return false;
        }
        economy = rsp.getProvider();
        return true;
    }

    public boolean isEnabled(){
        return enabled;
    }

    public boolean addMoney(Player player, Double money){
        if(isEnabled()) {
            economy.depositPlayer(player, money);
            return true;
        }
        return false;
    }

}
