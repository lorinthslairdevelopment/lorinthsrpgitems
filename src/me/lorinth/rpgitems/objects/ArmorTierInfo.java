package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.enums.ArmorTier;
import me.lorinth.utils.TryParse;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.TreeMap;

public class ArmorTierInfo implements IItemTier<ArmorTier>{

    private ArmorTier armorTier;
    private TreeMap<Integer, AttributeRangeMap> levelAttributes = new TreeMap<>();
    private boolean valid = false;

    public ArmorTierInfo(FileConfiguration config, String prefix, String tierName){
        if(TryParse.parseEnum(ArmorTier.class, tierName)){
            armorTier = ArmorTier.valueOf(tierName);
            valid = true;

            loadAttributes(config, prefix + tierName);
        }
    }

    private void loadAttributes(FileConfiguration config, String prefix){
        for(String level : config.getConfigurationSection(prefix).getKeys(false)){
            if(TryParse.parseInt(level)){
                Integer Level = Integer.parseInt(level);
                levelAttributes.put(Level, AttributeRangeMap.FromConfig(config, prefix + "." + level));
            }
        }
    }

    public void applyLevelAttributes(AttributeMap map, Integer level){
        map.applyAttributeRangeMap(levelAttributes.floorEntry(level).getValue());
    }

    public ArmorTier getTier(){
        return armorTier;
    }

    public boolean isValid(){
        return valid;
    }

}
