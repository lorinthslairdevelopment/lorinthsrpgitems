package me.lorinth.rpgitems.objects;

import me.lorinth.utils.ConfigHelper;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Map;
import java.util.TreeMap;

public class AttributeBuffProfile {

    private TreeMap<Integer, AttributeMap> buffsPerLevel;
    private TreeMap<Integer, AttributeMap> totalsPerLevel;
    private boolean isAdditive = false;

    public AttributeBuffProfile(FileConfiguration config, String path){
        buffsPerLevel = new TreeMap<>();
        totalsPerLevel = new TreeMap<>();

        if(ConfigHelper.ConfigContainsPath(config, path + ".IsAdditive")){
            isAdditive = config.getBoolean(path + ".IsAdditive");
        }

        for(String levelValue: config.getConfigurationSection(path).getKeys(false)){
            if(levelValue.equalsIgnoreCase("IsAdditive")){
                continue;
            }

            int level = Integer.parseInt(levelValue);
            buffsPerLevel.put(level, AttributeMap.load(config, path + "." + level));
        }
    }

    public AttributeMap getProfileAtLevel(int level){
        if(!isAdditive){
            Map.Entry<Integer, AttributeMap> entry = buffsPerLevel.floorEntry(level);
            if(entry == null){
                return AttributeMap.empty.clone();
            }
            return entry.getValue().clone();
        }

        if(!totalsPerLevel.containsKey(level)){
            totalsPerLevel.put(level, calculateTotalAtLevel(level));
        }

        return totalsPerLevel.get(level).clone();
    }

    private AttributeMap calculateTotalAtLevel(int level){
        Map.Entry<Integer, AttributeMap> closest = totalsPerLevel.floorEntry(level);
        int startLevel = closest != null ? closest.getKey() : 0;
        AttributeMap attributes = closest != null && closest.getValue() != null ? closest.getValue().clone() : new AttributeMap(false);

        for(int i=startLevel+1; i<= level; i++){
            if(buffsPerLevel.containsKey(i)){
                attributes.applyAttributeMap(buffsPerLevel.get(i));
            }

            totalsPerLevel.put(i, attributes.clone());
        }

        return attributes;
    }

}
