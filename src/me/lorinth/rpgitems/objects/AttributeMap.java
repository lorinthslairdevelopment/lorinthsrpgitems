package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.MathHelper;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;

public class AttributeMap extends HashMap<RpgAttribute, Object> {

    public static AttributeMap empty = new AttributeMap(false);

    public AttributeMap(boolean useDefaultValue){
        for(RpgAttribute attr : RpgAttribute.values()){
            if(attr.isRange()){
                ValueRange range = useDefaultValue ? (ValueRange) attr.getDefaultValue() : new ValueRange();
                range.setIsDecimal(attr.isDecimal());
                this.put(attr, range);
            }
            else
                this.put(attr, useDefaultValue ? attr.getDefaultValue() : 0.0);
        }
    }

    public void add(RpgAttribute attribute, Object value){
        if(value == null)
            return;

        Object storedValue = get(attribute);
        if(storedValue instanceof Double){
            if(value instanceof Double) {
                put(attribute, ((Double) storedValue) + ((Double) value));
            }
            else if(value instanceof ValueRange){
                put(attribute, ((Double)storedValue) + ((ValueRange) value).getValue());
            }
            else if(value instanceof AttributeRange) {
                    put(attribute, ((Double) storedValue) + ((AttributeRange) value).getValue());
                }
        }
        else if(storedValue instanceof ValueRange){
            if(value instanceof Double) {
                put(attribute, ((ValueRange) storedValue).add(new ValueRange(((Double) value))));
            }
            else if(value instanceof ValueRange){
                put(attribute, ((ValueRange)storedValue).add((ValueRange) value));
            }
            else if(value instanceof AttributeRange) {
                put(attribute, ((ValueRange) storedValue).add((AttributeRange) value));
            }
        }
    }

    public void addAll(Object value){
        for(RpgAttribute attribute : this.keySet()){
            this.add(attribute, value);
        }
    }

    public void subtract(RpgAttribute attribute, Object value){
        if(value == null)
            return;

        Object storedValue = get(attribute);
        if(storedValue instanceof Double){
            if(value instanceof Double)
                put(attribute, ((Double)storedValue) - ((Double)value));
            else if(value instanceof ValueRange)
                put(attribute, ((Double)storedValue) - ((ValueRange) value).getValue());
            else if(value instanceof AttributeRange)
                put(attribute, ((Double)storedValue) - ((AttributeRange) value).getValue());
        }
        else if(storedValue instanceof ValueRange){
            if(value instanceof Double)
                put(attribute, ((ValueRange)storedValue).subtract(new ValueRange(((Double) value))));
            else if(value instanceof ValueRange)
                put(attribute, ((ValueRange)storedValue).subtract((ValueRange) value));
            else if(value instanceof AttributeRange)
                put(attribute, ((ValueRange)storedValue).subtract((AttributeRange) value));
        }
    }

    public void subtractAll(Object value){
        for(RpgAttribute attribute : this.keySet()){
            this.subtract(attribute, value);
        }
    }

    public void multiply(RpgAttribute attribute, Double value){
        if(value == null)
            return;

        Object storedValue = get(attribute);
        if(storedValue instanceof Double) {
            put(attribute, ((Double)storedValue) * ((Double)value));
        }
        else if(storedValue instanceof ValueRange){
            put(attribute, ((ValueRange)storedValue).multiply((value)));
        }
    }

    public void multiplyObject(RpgAttribute attribute, Object value){
        if(value == null)
            return;


        Object storedValue = get(attribute);
        if(storedValue instanceof Double){
            if(value instanceof Double)
                put(attribute, ((Double)storedValue) * ((Double)value));
            else if(value instanceof ValueRange)
                put(attribute, ((Double)storedValue) * ((ValueRange) value).getValue());
            else if(value instanceof AttributeRange)
                put(attribute, ((Double)storedValue) * ((AttributeRange) value).getValue());
        }
        else if(storedValue instanceof ValueRange){
            if(value instanceof Double)
                put(attribute, ((ValueRange)storedValue).multiply((Double) value));
            else if(value instanceof ValueRange)
                put(attribute, ((ValueRange)storedValue).multiply((ValueRange) value));
            else if(value instanceof AttributeRange)
                put(attribute, ((ValueRange)storedValue).multiply((AttributeRange) value));
        }
    }

    public void multiplyAll(Double value){
        for(RpgAttribute attribute : this.keySet()){
            this.multiply(attribute, value);
        }
    }

    @Override
    public Object get(Object key) {
        if(key instanceof RpgAttribute){
            if(!((RpgAttribute) key).isEnabled())
                return 0.0;
            return super.get(key);
        }
        return null;
    }

    public HashMap<RpgAttribute, Object> getNonZeroAttributes(){
        HashMap<RpgAttribute, Object> values = new HashMap<>();
        for(Entry<RpgAttribute, Object> entry : this.entrySet()){
            Object value = entry.getValue();
            if(value instanceof Double){
                if(!MathHelper.equals(((Double) value), 0.0))
                    values.put(entry.getKey(), entry.getValue());
            }
            else if(value instanceof ValueRange){
                if(!((ValueRange) value).isZero())
                    values.put(entry.getKey(), entry.getValue());
            }
        }
        return values;
    }

    public void applyAttributeMap(AttributeMap map){
        if(map == null)
            return;
        for(RpgAttribute attribute : map.getNonZeroAttributes().keySet()){
            if(attribute.isEnabled()){
                add(attribute, map.get(attribute));
            }
        }
    }

    public void removeAttributeMap(AttributeMap map){
        if(map == null)
            return;
        for(RpgAttribute attribute : RpgAttribute.values()){
            if(attribute.isEnabled())
                subtract(attribute, map.get(attribute));
        }
    }

    public void applyAttributeRangeMap(AttributeRangeMap map){
        if(map == null)
            return;
        for(Entry<RpgAttribute, AttributeRange> entry : map.entrySet()){
            add(entry.getKey(), entry.getValue());
        }
    }

    public AttributeMap clone(){
        AttributeMap newMap = new AttributeMap(false);
        for(Entry<RpgAttribute, Object> entry : this.entrySet()){
            newMap.add(entry.getKey(), entry.getValue());
        }
        return newMap;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(Entry<RpgAttribute, Object> entry : this.entrySet()) {
            if (entry.getValue() instanceof Double) {
                if (!MathHelper.equals(0.0, (Double) entry.getValue()))
                    sb.append("{" + entry.getKey().name() + ", " + entry.getValue() + "} ");
            }
            else if(entry.getValue() instanceof ValueRange){
                if(!((ValueRange)entry.getValue()).isZero())
                    sb.append("{" + entry.getKey().name() + ", " + entry.getValue() + "} ");
            }
        }
        return sb.toString();
    }

    public void save(FileConfiguration config, String path){
        path += ".";
        for(RpgAttribute attr : RpgAttribute.values()){
            if(attr.isEnabled()){
                Object value = get(attr);
                if(!MathHelper.isZero(value))
                    config.set(path + attr.name(), get(attr).toString());
                else
                    config.set(path + attr.name(), null);
            }
        }
    }

    public static AttributeMap load(FileConfiguration config, String path){
        AttributeMap am = new AttributeMap(false);
        for(RpgAttribute attr : RpgAttribute.values()){
            if(attr.isEnabled() && ConfigHelper.ConfigContainsPath(config, path + "." + attr.name())){
                String value = config.getString(path + "." + attr.name());
                if(value != null){
                    if(value.contains("")){
                        am.add(attr, new ValueRange(value));
                    }
                    else{
                        am.add(attr, config.getDouble(path + "." + attr.name()));
                    }
                }
            }
        }
        return am;
    }

    @Override
    public Object put(RpgAttribute attribute, Object value){
        if(value instanceof Double){
            value = Math.min((Double) value, attribute.getMaxValue());
            return super.put(attribute, value);
        }
        else if(value instanceof ValueRange){
            ValueRange range = ((ValueRange) value);
            range.cap(attribute.getMaxValue());
            return super.put(attribute, range);
        }
        return null;
    }

}
