package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.enums.RpgAttribute;

import java.util.Random;

public class AttributeRange {

    private RpgAttribute attribute;
    private double minimum;
    private double maximum;
    private Random random = new Random();

    public AttributeRange(RpgAttribute attribute, double minimum, double maximum){
        this.attribute = attribute;
        this.minimum = Math.min(minimum, maximum);
        this.maximum = Math.max(minimum, maximum);
    }

    public double getValue(){
        if(minimum == maximum)
            return maximum;

        double value = (random.nextDouble() * (maximum - minimum)) + minimum;
        if(!attribute.isDecimal())
            return (int) value;
        return value;
    }

    public double getMinimum(){
        return minimum;
    }

    public double getMaximum(){
        return maximum;
    }

    @Override
    public String toString() {
        return minimum + " - " + maximum;
    }
}
