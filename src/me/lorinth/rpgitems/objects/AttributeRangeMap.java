package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.regex.Pattern;

public class AttributeRangeMap extends HashMap<RpgAttribute, AttributeRange> {

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(Entry<RpgAttribute, AttributeRange> entry : this.entrySet())
            sb.append("{" + entry.getKey().name() + ", " + entry.getValue() + "} ");
        return sb.toString();
    }

    public static AttributeRangeMap FromConfig(FileConfiguration config, String path){
        AttributeRangeMap attributes = new AttributeRangeMap();
        if(ConfigHelper.ConfigContainsPath(config, path)){
            for(String attribute : config.getConfigurationSection(path).getKeys(false)){
                try{
                    if(TryParse.parseEnum(RpgAttribute.class, attribute)){
                        RpgAttribute rpgAttribute = RpgAttribute.valueOf(attribute);
                        if(rpgAttribute.isEnabled()){
                            String value = config.getString(path + "." + attribute);
                            if(TryParse.parseDouble(value))
                                attributes.put(rpgAttribute, new AttributeRange(rpgAttribute, Double.parseDouble(value), Double.parseDouble(value)));
                            else{
                                String[] args = value.split(Pattern.quote("-"));
                                if(args.length == 2 && TryParse.parseDouble(args[0]) && TryParse.parseDouble(args[1]))
                                    attributes.put(rpgAttribute, new AttributeRange(rpgAttribute, Double.parseDouble(args[0]), Double.parseDouble(args[1])));
                                else
                                    attributes.put(rpgAttribute, new AttributeRange(rpgAttribute, Double.parseDouble(value), Double.parseDouble(value)));
                            }
                        }
                    }
                    else{
                        RpgItemsOutputHandler.PrintError("AttributeRangeMap, has invalid attribute, " + RpgItemsOutputHandler.HIGHLIGHT + attribute);
                    }
                }
                catch(Exception exception){
                    RpgItemsOutputHandler.PrintError("AttributeRangeMap, has invalid attribute, " + RpgItemsOutputHandler.HIGHLIGHT + attribute);
                }
            }
        }
        return attributes;
    }

}
