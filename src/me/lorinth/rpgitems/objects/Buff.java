package me.lorinth.rpgitems.objects;

import java.util.List;

public class Buff {

    private String id;
    private AttributeMap stats;
    private List<String> tags;
    private Long duration;
    private boolean removeOnDeath;

    public Buff(String id, AttributeMap stats, List<String> tags, Long duration){
        this.id = id;
        this.stats = stats;
        this.tags = tags;
        this.duration = duration;
        this.removeOnDeath = true;
    }

    public Buff(String id, AttributeMap stats, List<String> tags, Long duration, boolean removeOnDeath){
        this.id = id;
        this.stats = stats;
        this.tags = tags;
        this.duration = duration;
        this.removeOnDeath = removeOnDeath;
    }

    public String getId(){
        return id;
    }

    public AttributeMap getStats(){
        return stats;
    }

    public List<String> getTags(){
        return tags;
    }

    public Long getRawDuration(){
        return duration;
    }

    public Long getDurationInMilliseconds(){
        return duration * 1000;
    }

    public Long getDurationInTicks(){
        return duration * 20;
    }

    public boolean shouldRemoveOnDeath(){
        return removeOnDeath;
    }

}
