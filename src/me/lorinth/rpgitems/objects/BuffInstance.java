package me.lorinth.rpgitems.objects;

public class BuffInstance {

    private Buff buff;
    private AttributeMap buffStats;
    private Long TimeApplied;
    private Long Duration;

    public BuffInstance(Buff buff){
        this.buff = buff;
        this.buffStats = buff.getStats();
        this.TimeApplied = System.currentTimeMillis();
        this.Duration = buff.getDurationInMilliseconds();
    }

    public Buff getBuff(){
        return buff;
    }

    public Long getWearTime(){
        return TimeApplied + Duration;
    }

}
