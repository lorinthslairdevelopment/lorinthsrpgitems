package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.TreeMap;

public class CustomTierInfo implements IItemTier<MaterialDurability>{

    private MaterialDurability materialDurability;
    private TreeMap<Integer, AttributeRangeMap> levelAttributes = new TreeMap<>();

    public CustomTierInfo(FileConfiguration config, String prefix, String id){
        materialDurability = new MaterialDurability(id);

        if(materialDurability.isValid())
            loadAttributes(config, prefix + id);
        else
            RpgItemsOutputHandler.PrintError("Material.yml, Custom Material/Durability, " + RpgItemsOutputHandler.HIGHLIGHT + id + RpgItemsOutputHandler.ERROR +
                    " is invalid");
    }

    private void loadAttributes(FileConfiguration config, String prefix){
        for(String level : config.getConfigurationSection(prefix).getKeys(false)){
            if(TryParse.parseInt(level)){
                Integer Level = Integer.parseInt(level);
                levelAttributes.put(Level, AttributeRangeMap.FromConfig(config, prefix + "." + level));
            }
        }
    }

    public void applyLevelAttributes(AttributeMap map, Integer level){
        map.applyAttributeRangeMap(levelAttributes.floorEntry(level).getValue());
    }

    public AttributeRangeMap getAttributes(){
        return levelAttributes.floorEntry(1).getValue();
    }

    public MaterialDurability getTier(){
        return materialDurability;
    }

    public boolean isValid(){
        return true;
    }

}
