package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.enums.DamageEffectType;
import org.bukkit.entity.LivingEntity;

public class DamageEffectInfo {

    private LivingEntity owner;
    private LivingEntity entity;
    private Double damage;
    private DamageEffectType type;
    private int duration;
    private boolean enabled;

    public DamageEffectInfo(LivingEntity owner, LivingEntity entity, Double damage, DamageEffectType type, int duration){
        this.owner = owner;
        this.entity = entity;
        this.damage = damage;
        this.type = type;
        this.duration = duration;
        setEnabled(true);
    }

    public LivingEntity getOwner(){
        return owner;
    }

    public LivingEntity getEntity(){
        return entity;
    }

    public Double getDamage(){
        return damage;
    }

    public DamageEffectType getDamageEffectType(){
        return type;
    }

    public int getDuration() { return duration; }

    public void setEnabled(boolean enabled){
        this.enabled = enabled;
    }

    public boolean isEnabled(){
        return enabled;
    }

}
