package me.lorinth.rpgitems.objects;

public class DamageEventStatistics {

    //Event Info
    public Double totalDamage = 0.0;
    public Double eventDamage = 0.0;
    public Double pveDamageResult = 0.0;
    public Double pveDefenseModifier = 0.0;
    public Double pvpDamageResult = 0.0;
    public Double pvpDefenseModifier = 0.0;

    //Offensive
    public Double penetration = 0.0;
    public Double bonusDamage = 0.0;
    public Double pveDamage = 0.0;
    public Double pvpDamage = 0.0;
    public Double vampirism = 0.0;
    public Double accuracy = 0.0;
    public Double burnChance = 0.0;
    public Double burnDamage = 0.0;
    public Double poisonChance = 0.0;
    public Double poisonDamage = 0.0;
    public Double criticalChance = 0.0;
    public Double criticalDamage = 0.0;

    //Defensive
    public Double defense = 0.0;
    public Double pveDefense = 0.0;
    public Double pvpDefense = 0.0;
    public Double dodgeChance = 0.0;
    public Double blockChance = 0.0;
    public Double blockDamage = 0.0;
    public Double isBlocking = 0.0;

}
