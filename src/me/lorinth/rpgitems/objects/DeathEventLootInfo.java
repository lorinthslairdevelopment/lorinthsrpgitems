package me.lorinth.rpgitems.objects;

import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;

public class DeathEventLootInfo {

    EntityDeathEvent event;
    HashMap<EventPriority, List<ItemStack>> items;

    public DeathEventLootInfo(EntityDeathEvent event, HashMap<EventPriority, List<ItemStack>> items){
        this.event = event;
        this.items = items;
    }

    public EntityDeathEvent getEvent(){
        return event;
    }

    public HashMap<EventPriority, List<ItemStack>> getItems(){
        return items;
    }

}
