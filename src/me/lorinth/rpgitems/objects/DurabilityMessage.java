package me.lorinth.rpgitems.objects;

public class DurabilityMessage {

    private Value value;
    private String message;

    public DurabilityMessage(Value value, String message){
        this.value = value;
        this.message = message;
    }

    public Value getValue(){
        return value;
    }

    public String getMessage(){
        return message;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj instanceof DurabilityMessage){
            return ((DurabilityMessage) obj).value == value && ((DurabilityMessage) obj).message.equalsIgnoreCase(message);
        }
        return false;
    }

    @Override
    public String toString() {
        return value.toString() + " - " + message;
    }
}
