package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.enums.EffectType;
import me.lorinth.rpgitems.util.LoreHelper;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.rpgitems.util.holograms.HologramManager;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.List;

public class EffectInfo {

    private EffectType effectType;
    private String userText;
    private String targetText;
    private Sound sound;
    private ParticleInfo particleInfo;
    private boolean useHologram;
    private String[] hologramLines;
    private double hologramRadius = 10.0d;

    public EffectInfo(FileConfiguration config, String effectName){
        if(TryParse.parseEnum(EffectType.class, effectName.toUpperCase())){
            effectType = EffectType.valueOf(effectName.toUpperCase());

            String prefix = effectName + ".";
            userText = ChatColor.translateAlternateColorCodes('&', config.getString(prefix + "UserText"));
            targetText = ChatColor.translateAlternateColorCodes('&', config.getString(prefix + "TargetText"));

            if(ConfigHelper.ConfigContainsPath(config, prefix + "Sound")){
                String soundText = config.getString(prefix + "Sound");
                if(TryParse.parseEnum(Sound.class, soundText)){
                    sound = Sound.valueOf(soundText);
                }
                else{
                    RpgItemsOutputHandler.PrintError("Failed to load sound, '" + soundText + "' in effects.yml (" + effectName + ")");
                }
            }

            if(ConfigHelper.ConfigContainsPath(config, prefix + "Particles"))
                particleInfo = new ParticleInfo(config, prefix + "Particles");

            if(ConfigHelper.ConfigContainsPath(config, prefix + "UseHologram")){
                useHologram = config.getBoolean(prefix + "UseHologram");
            }
            if(ConfigHelper.ConfigContainsPath(config, prefix + "HologramLines")){
                List<String> hologramLines = config.getStringList(prefix + "HologramLines");
                for(int i=0; i<hologramLines.size(); i++){
                    hologramLines.set(i, ChatColor.translateAlternateColorCodes('&', hologramLines.get(i)));
                }
                this.hologramLines = hologramLines.toArray(new String[hologramLines.size()]);
            }
            if(ConfigHelper.ConfigContainsPath(config, prefix + "HologramRadius")){
                hologramRadius = config.getDouble(prefix + "HologramRadius");
            }
        }
    }

    public EffectType getType(){
        return effectType;
    }

    public String getUserText(){
        return userText;
    }

    public String getTargetText(){
        return targetText;
    }

    public Sound getSound(){
        return sound;
    }

    public void play(Entity user, Entity target, Double value){
        this.playPrivate(user, target, value);
    }

    public void play(Entity target, Double value){
        this.playPrivate(null, target, value);
    }

    private void playPrivate(Entity user, Entity target, Double value){
        if(!userText.equalsIgnoreCase("") && user != null) {
            user.sendMessage(userText);
        }
        if(!targetText.equalsIgnoreCase("") && target != null) {
            target.sendMessage(targetText);
        }

        double height = -1.0d;

        switch(effectType){
            case BLOCK:
            case DODGE:
                if(sound != null && user != null) {
                    user.getWorld().playSound(user.getLocation(), sound, 1.0f, 1.0f);
                }
                if(particleInfo != null && user != null) {
                    particleInfo.play(user.getLocation().add(0, 1, 0));
                }
                if(target instanceof Player && useHologram) {
                    HologramManager.setHoloTime((Player) target, getHologramLines(value), user.getLocation(), 20, height);
                }
                break;
            case BURN:
                height += 0.2d;
            case CRITICAL:
                height += 0.2d;
            case POISON:
                height += 0.2d;
                if(sound != null && target != null){
                    target.getWorld().playSound(target.getLocation(), sound, 1.0f, 1.0f);
                }
                if(particleInfo != null && target != null) {
                    particleInfo.play(target.getLocation().add(0, 1, 0));
                }
                if(user instanceof Player && useHologram) {
                    HologramManager.setHoloTime((Player) user, getHologramLines(value), target.getLocation(), 20, height);
                }
                break;
            case DAMAGE:
                height -= 0.2d;
            case HEAL:
                if(target == null){
                    return;
                }
                if(sound != null) {
                    target.getWorld().playSound(target.getLocation(), sound, 1.0f, 1.0f);
                }
                if(particleInfo != null) {
                    particleInfo.play(target.getLocation().add(0, 1, 0));
                }
                if(useHologram) {
                    HologramManager.setGlobalHologram(getHologramLines(value), target.getLocation(), 20, height, hologramRadius);
                }
                break;
        }
    }

    public boolean isUseHologram(){
        return useHologram;
    }

    public String[] getHologramLines(Double value){
        if(value == null)
            return hologramLines;

        String[] lines = hologramLines.clone();
        String[] parsedLines = new String[lines.length];
        for(int i=0; i<lines.length; i++){
            parsedLines[i] = lines[i].replace("{value}", LoreHelper.DecimalFormat.format(value));
        }
        return parsedLines;
    }

}
