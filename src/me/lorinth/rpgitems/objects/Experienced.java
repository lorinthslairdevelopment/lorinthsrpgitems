package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.events.PlayerLevelUpEvent;
import me.lorinth.rpgitems.managers.ExperienceManager;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.util.regex.Pattern;

public class Experienced {

    private int level = 1;
    private int experience = 0;
    private double expNeededModifier = 1.0;
    private Player player;

    public Experienced(Player player, double expNeededModifier){
        this.player = player;
        this.expNeededModifier = expNeededModifier;
    }

    public Integer getLevel(){
        return level;
    }

    protected void setLevel(int level){
        int levelCap = ExperienceManager.getLevelCap(this);
        if(level > levelCap) {
            level = levelCap;
        }

        this.level = level;
    }

    public int getExperience(){
        return experience;
    }

    protected void setExperience(int experience){
        this.experience = experience;
    }

    public void addExperience(Integer exp){
        if(exp == null || exp <= 0){
            return;
        }

        int levelCap = ExperienceManager.getLevelCap(this);
        if(level >= levelCap){
            this.setExperience(0);
            return;
        }

        if(player.getGameMode() != GameMode.SURVIVAL && player.getGameMode() != GameMode.ADVENTURE)
            return;

        experience += exp;
        if(ExperienceManager.checkLevelUp(player)){
            levelUp();
        }

        player.spigot().sendMessage(ExperienceManager.experienceMessageDelivery,
                TextComponent.fromLegacyText(ExperienceManager.gainExperienceMessage.replaceAll(Pattern.quote("{exp}"), exp.toString())));
    }

    public void removeExperience(Integer exp){
        if(exp == null || exp <= 0)
            return;

        experience -= exp;
        if(experience < 0)
            experience = 0;

        player.spigot().sendMessage(ExperienceManager.experienceMessageDelivery,
                TextComponent.fromLegacyText(ExperienceManager.lostExperienceMessage.replaceAll(Pattern.quote("{exp}"), exp.toString())));
    }

    public void levelUp(){
        int originalLevel = level;
        while(experience >= ExperienceManager.getExpToNextLevel(level, expNeededModifier) && level < ExperienceManager.getLevelCap(this)){
            experience -= ExperienceManager.getExpToNextLevel(level, expNeededModifier);
            level ++;
        }
        if(originalLevel != level){
            for(String line : ExperienceManager.levelUpMessages){
                player.sendMessage(line.replaceAll(Pattern.quote("{level}"), level + "").replaceAll(Pattern.quote("{player_name}"), player.getDisplayName()));
            }

            Bukkit.getPluginManager().callEvent(new PlayerLevelUpEvent(player, originalLevel, level));
        }
    }

    public void reset(){
        level = 1;
        experience = 0;
    }

}
