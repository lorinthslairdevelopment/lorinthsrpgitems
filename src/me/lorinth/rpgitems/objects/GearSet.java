package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.List;

public class GearSet {

    private Integer startLevel;
    private Integer endLevel;
    private Integer weight;
    private String id;
    private String displayName;
    private List<String> lore;
    private HashMap<Integer, GearSetBuff> buffs = new HashMap<>();
    private GearSetGenerationInfo generationInfo;

    public GearSet(FileConfiguration config, String pathPrefix, String id){
        this.id = id;
        pathPrefix += ".";

        startLevel = config.getInt(pathPrefix + "StartLevel");
        endLevel = config.getInt(pathPrefix + "EndLevel");
        weight = config.getInt(pathPrefix + "Weight");
        displayName = ChatColor.translateAlternateColorCodes('&', config.getString(pathPrefix + "DisplayName"));
        lore = config.getStringList(pathPrefix + "Lore");

        for(String tier : config.getConfigurationSection(pathPrefix + "Buffs").getKeys(false)){
            String buffPrefix = pathPrefix + "Buffs." + tier;
            if(!TryParse.parseInt(tier)){
                RpgItemsOutputHandler.PrintError("Invalid gear count, " + RpgItemsOutputHandler.HIGHLIGHT + tier + RpgItemsOutputHandler.ERROR + ", under " + pathPrefix + "Buffs, in gearsets.yml");
                continue;
            }

            buffs.put(Integer.parseInt(tier), new GearSetBuff(config, buffPrefix));
        }

        generationInfo = new GearSetGenerationInfo(config, pathPrefix + "Generation");
    }

    public Integer getStartLevel(){
        return startLevel;
    }

    public Integer getEndLevel(){
        return endLevel;
    }

    public Integer getWeight(){
        return weight;
    }

    public String getId(){
        return id;
    }

    public String getDisplayName(){
        return displayName;
    }

    public HashMap<Integer, GearSetBuff> getBuffs(){
        return buffs;
    }

    public List<String> getLore(){
        return lore;
    }

    public GearSetGenerationInfo getGeneratonInfo(){
        return generationInfo;
    }

}
