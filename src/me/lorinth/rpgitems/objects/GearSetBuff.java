package me.lorinth.rpgitems.objects;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public class GearSetBuff {

    private String inactiveLore;
    private String activeLore;
    private boolean doesOverridePrevious;
    private AttributeMap buffs;

    public GearSetBuff(FileConfiguration config, String prefix){
        prefix += ".";
        inactiveLore = ChatColor.translateAlternateColorCodes('&', config.getString(prefix + "InactiveLore"));
        activeLore = ChatColor.translateAlternateColorCodes('&', config.getString(prefix + "ActiveLore"));
        doesOverridePrevious = config.getBoolean(prefix + "Override");
        buffs = AttributeMap.load(config, prefix + "Stats");
    }

    public String getInactiveLore(){
        return inactiveLore;
    }

    public String getActiveLore(){
        return activeLore;
    }

    public boolean doesOverridePrevious(){
        return doesOverridePrevious;
    }

    public AttributeMap getBuffs(){
        return buffs;
    }

}
