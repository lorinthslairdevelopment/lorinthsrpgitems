package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

public class GearSetGenerationInfo {

    public List<RarityTier> validRarities;
    public List<MaterialDurability> invalidItemTypes;

    public GearSetGenerationInfo(FileConfiguration config, String prefix){
        validRarities = new ArrayList<>();
        invalidItemTypes = new ArrayList<>();

        prefix += ".";
        for(String rarity : config.getStringList(prefix + "Rarity_Whitelist")){
            if(!TryParse.parseEnum(RarityTier.class, rarity)){
                RpgItemsOutputHandler.PrintError("Invalid Rarity Tier " + RpgItemsOutputHandler.HIGHLIGHT + rarity + RpgItemsOutputHandler.ERROR + ", under " + prefix + "Rarity_Whitelist, in gearsets.yml");
                continue;
            }
            validRarities.add(RarityTier.valueOf(rarity));
        }
        for(String item : config.getStringList(prefix + "Items_Blacklist")){
            MaterialDurability md = new MaterialDurability(item);
            if(!md.isValid()){
                RpgItemsOutputHandler.PrintError("Invalid Material Durability, " + RpgItemsOutputHandler.HIGHLIGHT + item + RpgItemsOutputHandler.ERROR + ", under " + prefix + "Items_Blacklist, in gearsets.yml");
                continue;
            }
            invalidItemTypes.add(md);
        }
    }

    public boolean canApply(RarityTier rarityTier, MaterialDurability md){
        return validRarities.contains(rarityTier) && !invalidItemTypes.contains(md);
    }

}
