package me.lorinth.rpgitems.objects;

import java.util.regex.Pattern;

public class GenerationTags {

    public static String levelTag = "[LriGeneration:{level}]";
    public static String rarityTag = "[LriGeneration-Rarity:{rarity}]";
    public static String gearSetTag = "[LriGeneration-GearSet:{gearSet}]";
    public static String boostedTag = "[LriGeneration-Boosted:{boost}]";

    public static String[] levelTagPieces = levelTag.split(Pattern.quote("{level}"));
    public static String[] rarityTagPieces = rarityTag.split(Pattern.quote("{rarity}"));
    public static String[] gearSetTagPieces = gearSetTag.split(Pattern.quote("{gearSet}"));
    public static String[] boostedTagPieces = boostedTag.split(Pattern.quote("{boost}"));

}
