package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.enums.HealthScaleActivation;

public class HealthScaleOptions{

    public boolean Enabled;
    public Double Scale;
    public HealthScaleActivation Activation = HealthScaleActivation.Greater;

}
