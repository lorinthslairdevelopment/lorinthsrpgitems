package me.lorinth.rpgitems.objects;

public interface IItemTier<T> {

    void applyLevelAttributes(AttributeMap map, Integer level);

    T getTier();

    boolean isValid();

}
