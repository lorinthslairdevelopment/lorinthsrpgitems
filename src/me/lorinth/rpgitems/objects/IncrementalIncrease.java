package me.lorinth.rpgitems.objects;

public class IncrementalIncrease {

    private double baseValue;
    private double increasePerLevel;

    public IncrementalIncrease(double base, double increasePerLevel){
        this.baseValue = base;
        this.increasePerLevel = increasePerLevel;
    }

    public double getResult(double level){
        return this.baseValue + (this.increasePerLevel * level);
    }

    public int getIntResult(double level){
        return (int) Math.floor(getResult(level));
    }

}
