package me.lorinth.rpgitems.objects;

import me.idlibrary.main.IDLibrary;
import me.lorinth.rpgitems.managers.DurabilityManager;
import me.lorinth.utils.TryParse;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

import java.util.regex.Pattern;

public class MaterialDurability {

    private Material material;
    private Short durability = 0;
    private boolean hasDurability = false;
    private boolean valid = true;

    public MaterialDurability(String info){
        if(info == null){
            valid = false;
            return;
        }

        if(info.contains(":")){
            String[] args = info.split(Pattern.quote(":"));
            this.material = IDLibrary.getMaterial(args[0]);
            this.durability = Short.parseShort(args[1]);
            this.hasDurability = true;
        }
        else {
            if(TryParse.parseEnum(Material.class, info))
                this.material = Material.valueOf(info);
            else
                this.material = IDLibrary.getMaterial(info);
        }

        if(this.material == null)
            valid = false;
    }

    public MaterialDurability(ItemStack item){
        this(item.getType(), DurabilityManager.IsEnabled() ? item.getDurability() : 0);
    }

    public MaterialDurability(Block block){
        this.material = block.getType();
        this.durability = 0;
    }

    public MaterialDurability(Material material, Short durability){
        this.material = material;
        this.durability = durability;
    }

    public Material getMaterial(){
        return material;
    }

    public Short getDurability(){
        return durability;
    }

    public ItemStack toItemStack(int count){
        return new ItemStack(material, count, durability);
    }

    public ItemStack toItemStack(){
        return this.toItemStack(1);
    }

    public boolean isValid(){
        return valid;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MaterialDurability) {
            MaterialDurability other = (MaterialDurability) obj;
            return other.getDurability() == durability && other.getMaterial() == material;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (material.getKey().hashCode() * 1000 + 10000) + durability;
    }

    @Override
    public String toString(){
        return material.name() + (this.hasDurability ? ":" + durability : "" );
    }

}
