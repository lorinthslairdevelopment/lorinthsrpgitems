package me.lorinth.rpgitems.objects;

public class Modification {

    private Integer startLevel;
    private Integer endLevel;
    private String prefix = null;
    private String suffix = null;
    private boolean canDuplicate = true;
    private Integer weight = 1;
    private AttributeRangeMap attributes = new AttributeRangeMap();

    public Integer getStartLevel(){
        return startLevel;
    }

    public void setStartLevel(int startLevel){
        this.startLevel = startLevel;
    }

    public Integer getEndLevel(){
        return endLevel;
    }

    public void setEndLevel(int endLevel){
        this.endLevel = endLevel;
    }

    public boolean hasPrefix(){ return prefix != null; }

    public String getPrefix(){
        return prefix;
    }

    public void setPrefix(String prefix){
        this.prefix = prefix;
    }

    public boolean hasSuffix(){ return suffix != null; }

    public String getSuffix(){
        return suffix;
    }

    public void setSuffix(String suffix){ this.suffix = suffix; }

    public boolean canDuplicate(){ return this.canDuplicate; }

    public void setCanDuplicate(boolean canDuplicate){
        this.canDuplicate = canDuplicate;
    }

    public Integer getWeight(){
        return weight;
    }

    public void setWeight(Integer weight){
        this.weight = weight;
    }

    public AttributeRangeMap getAttributes(){
        return attributes;
    }

    public void setAttributes(AttributeRangeMap attributes){
        this.attributes = attributes;
    }

}
