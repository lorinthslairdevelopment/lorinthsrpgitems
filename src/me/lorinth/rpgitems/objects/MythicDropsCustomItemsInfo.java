package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.Random;

public class MythicDropsCustomItemsInfo {

    private Integer level;
    private Double dropChance;
    private ArrayList<CustomItemInfo> customItemInfos;
    private Random random = new Random();

    public MythicDropsCustomItemsInfo(FileConfiguration config, Integer level, String prefix){
        this.level = level;
        customItemInfos = new ArrayList<>();
        String dropChanceValue = config.getString(prefix + ".DropChance").replace('%', ' ');

        if(TryParse.parseDouble(dropChanceValue)){
            dropChance = Double.parseDouble(dropChanceValue);
            prefix += ".Drops";
            for(String customItemId : config.getConfigurationSection(prefix).getKeys(false)){
                dropChanceValue = config.getString(prefix + "." + customItemId).replace('%', ' ');
                if(TryParse.parseDouble(dropChanceValue))
                    customItemInfos.add(new CustomItemInfo(customItemId, Double.parseDouble(dropChanceValue)));
                else
                    RpgItemsOutputHandler.PrintError("Failed to parse value as double at, " + RpgItemsOutputHandler.HIGHLIGHT + prefix + "." + customItemId +
                            RpgItemsOutputHandler.ERROR + " in loot.yml");
            }
        }
        else{
            RpgItemsOutputHandler.PrintError("Failed to parse value as double at, " + RpgItemsOutputHandler.HIGHLIGHT + prefix + ".DropChance" +
                    RpgItemsOutputHandler.ERROR + " in loot.yml");
        }
    }

    public Double getDropChance(){
        return dropChance;
    }

    public String getNextCustomItem(){
        double roll = random.nextDouble() * 100.0;
        double current = 0.0d;
        for(CustomItemInfo info : customItemInfos){
            current += info.chance;

            if(current > roll)
                return info.customItemId;
        }

        return null;
    }

    private class CustomItemInfo{

        private String customItemId;
        private Double chance;

        public CustomItemInfo(String customItemId, Double chance){
            this.customItemId = customItemId;
            this.chance = chance;
        }
    }

}
