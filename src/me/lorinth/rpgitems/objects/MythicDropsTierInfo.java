package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MythicDropsTierInfo {

    private Integer level;
    private Double chance;
    private HashMap<String, Double> tierChances;
    private Random random = new Random();

    public MythicDropsTierInfo(FileConfiguration config, Integer level, String pathPrefix){
        this.level = level;
        tierChances = new HashMap<>();
        for(String tiername : config.getConfigurationSection(pathPrefix).getKeys(false)){
            String value;
            if(tiername.equalsIgnoreCase("DropChance")){
                value = config.getString(pathPrefix + "." + tiername).replace('%', ' ');
                if(TryParse.parseDouble(value))
                    chance = Double.parseDouble(value);
                else
                    RpgItemsOutputHandler.PrintError("Failed to parse value as double at, " + RpgItemsOutputHandler.HIGHLIGHT + pathPrefix + "." + tiername +
                            RpgItemsOutputHandler.ERROR + " in loot.yml");

            }
            else{
                value = config.getString(pathPrefix + "." + tiername).replace('%', ' ');
                if(TryParse.parseDouble(value))
                    tierChances.put(tiername, Double.parseDouble(value));
                else
                    RpgItemsOutputHandler.PrintError("Failed to parse value as double at, " + RpgItemsOutputHandler.HIGHLIGHT + pathPrefix + "." + tiername +
                            RpgItemsOutputHandler.ERROR + " in loot.yml");

            }
        }
    }

    public Double getDropChance(){
        return chance;
    }

    public String getNextTier(){
        if(tierChances == null)
            return null;

        double roll = random.nextDouble() * 100.0;
        double current = 0.0d;
        for(Map.Entry<String, Double> tierChance : tierChances.entrySet()){
            current += tierChance.getValue();
            if(current > roll)
                return tierChance.getKey();
        }
        return null;
    }

}
