package me.lorinth.rpgitems.objects;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

public class ParticleInfo {

    private int count;
    private float spread;
    private Particle particle;
    private Object data = null;

    public ParticleInfo(FileConfiguration config, String prefix){
        count = config.getInt(prefix + ".Count");
        spread = (float) config.getDouble(prefix + ".Spread");

        String particleName = config.getString(prefix + ".Particle");

        if(TryParse.parseEnum(Particle.class, particleName)) {
            particle = Particle.valueOf(particleName);
            data = loadData(config, prefix + ".Data");
        }
        else
            RpgItemsOutputHandler.PrintError("Failed to load particle, '" + particleName + "' in effects.yml (" + prefix + ")");
    }

    private Object loadData(FileConfiguration config, String path){
        String value = config.getString(path);

        if(TryParse.parseEnum(Material.class, value)){
            Material material = Material.valueOf(value.toUpperCase());
            switch(particle){
                case REDSTONE:
                    return new Particle.DustOptions(org.bukkit.Color.fromRGB(Integer.parseInt(value)), 1);
                case ITEM_CRACK:
                    return new ItemStack(material, 1);
                case BLOCK_CRACK:
                case BLOCK_DUST:
                case FALLING_DUST:
                    return Material.valueOf(value).createBlockData();
                default:
                    if(TryParse.parseInt(value))
                        return Short.parseShort(value);
            }
        }
        return null;
    }

    public void play(Location location){
        if(data != null)
            if(data instanceof Short)
                location.getWorld().spawnParticle(particle, location, count, spread, spread, spread, (short) data);
            else
                location.getWorld().spawnParticle(particle, location, count, spread, spread, spread, 0, data);
        else
            location.getWorld().spawnParticle(particle, location, count, spread, spread, spread);
    }

}
