package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.HealthScaleActivation;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.events.PlayerLevelUpEvent;
import me.lorinth.rpgitems.heroes.HeroHandler;
import me.lorinth.rpgitems.managers.ClassManager;
import me.lorinth.rpgitems.managers.ExperienceManager;
import me.lorinth.rpgitems.objects.character.CharacterClass;
import me.lorinth.rpgitems.objects.character.CharacterClassInstance;
import me.lorinth.rpgitems.util.*;
import me.lorinth.utils.ConfigHelper;
import net.Indyuce.mmocore.api.player.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.*;

public class RpgCharacter {

    private Experienced info = null;
    private CharacterClassInstance currentCharacterClass = null;
    private List<CharacterClassInstance> allClassInfo = new ArrayList<>();

    private AttributeMap passiveStats = new AttributeMap(false);
    private AttributeMap buffStats = new AttributeMap(false);
    private AttributeMap levelPassiveStats = new AttributeMap(false);
    private AttributeMap totalStats = new AttributeMap(false);
    private AttributeMap oldStats = new AttributeMap(false);

    private ArrayList<BuffInstance> buffs = new ArrayList<>();
    private HashMap<String, Long> cooldowns = new HashMap<>();

    private double health;
    private RpgCharacterEquipment equipment;
    private Entity entity;
    private HeroHandler hero;
    private boolean isLoaded = false;

    public RpgCharacter(Player player){
        entity = player;
        getHero();

        equipment = new RpgCharacterEquipment(this);
    }

    public RpgCharacter(Entity entity){
        this.entity = entity;
    }

    public boolean isLoaded(){
        return isLoaded;
    }

    public boolean isPlayer(){
        return entity instanceof Player;
    }

    public Player getPlayer(){
        if(!isPlayer())
            return null;
        return (Player) entity;
    }

    public CharacterClassInstance getInfoForClass(CharacterClass characterClass){
        Optional<CharacterClassInstance> instance = allClassInfo.stream()
                .filter(characterClassInstance -> characterClass.getId()
                        .equalsIgnoreCase(characterClassInstance.getCharacterClass().getId())).findFirst();

        return instance.orElse(null);
    }

    public CharacterClassInstance getCurrentCharacterClass(){
        return currentCharacterClass;
    }

    public CharacterClassInstance setCurrentCharacterClass(CharacterClass characterClass, Integer level){
        if(characterClass == null){
            return null;
        }

        CharacterClassInstance targetClass = null;
        for(CharacterClassInstance instance : allClassInfo){
            if(instance.getCharacterClass().getId().equalsIgnoreCase(characterClass.getId())){
                targetClass = instance;
                break;
            }
        }

        if(targetClass == null){
            targetClass = new CharacterClassInstance((Player) entity, characterClass);
        }

        if(level != null){
            targetClass.setLevel(level);
        }
        String message = MessageHelper.getMessage("Class.Change")
                .replace("{level}", targetClass.getLevel().toString())
                .replace("{className}", targetClass.getCharacterClass().getDisplayName());
        entity.sendMessage(message);

        Bukkit.getScheduler().runTask(RpgItemsPlugin.instance, () -> {
            equipment = new RpgCharacterEquipment(this);
            equipment.update();
            update(false);
        });

        currentCharacterClass = targetClass;
        allClassInfo.add(targetClass);
        return targetClass;
    }

    public List<CharacterClassInstance> getAllClassInfo(){
        return allClassInfo;
    }

    public void update(){
        this.update(false);
    }

    public void update(boolean fullRefresh){
        if(isPlayer()){
            if(!isLoaded){
                return;
            }

            if(fullRefresh) {
                getHero();
            }

            oldStats = totalStats.clone();
            Player player = (Player) entity;
            double health = player.getHealth();
            removeBonuses(player);

            totalStats = new AttributeMap(false);

            //Add core stats to equipment stats
            if(ClassManager.isEnabled()){
                totalStats.applyAttributeMap(currentCharacterClass.getAttributes());
            }
            else{
                levelPassiveStats = ExperienceManager.getLevelPassiveStats(info.getLevel());
                totalStats.applyAttributeMap(levelPassiveStats);
            }

            totalStats.applyAttributeMap(passiveStats);
            totalStats.applyAttributeMap(equipment.update());

            buffStats = new AttributeMap(false);
            for(BuffInstance b : buffs){
                buffStats.applyAttributeMap(b.getBuff().getStats());
            }

            totalStats.applyAttributeMap(buffStats);

            addBonuses(player, fullRefresh);
            health = Math.min(health, player.getMaxHealth());
            if(health > 0){
                player.setHealth(Math.min(health, player.getMaxHealth()));
            }
            updateHealthScale();
        }
    }

    private void getHero(){
        if(!isPlayer() ||
                !PluginHelper.HeroesEnabled) {
            return;
        }

        hero = new HeroHandler((Player) entity);

        if(!hero.isValid()){
            hero = null;
        }
    }

    public boolean canAddBuff(Buff buff){
        for(BuffInstance b : buffs){
            if(ListHelper.containsOneSimilarElement(buff.getTags(), b.getBuff().getTags())){
                RpgItemsOutputHandler.PrintError(getPlayer(), "You have a conflicting buff effecting you");
                return false;
            }
        }
        if(cooldowns.containsKey(buff.getId())){
            Long delay = cooldowns.get(buff.getId()) - System.currentTimeMillis();
            if(delay > 0){
                double roundedDelay = Math.round(delay * 10) / 10.0;
                RpgItemsOutputHandler.PrintError(getPlayer(), "You can't eat that again so soon! Wait " + RpgItemsOutputHandler.HIGHLIGHT + roundedDelay + RpgItemsOutputHandler.ERROR + " more seconds!");
                return false;
            }
        }

        return true;
    }

    public boolean addBuff(Buff buff){
        if(!canAddBuff(buff)){
            return false;
        }

        BuffInstance instance = new BuffInstance(buff);

        buffs.add(instance);
        buffStats.applyAttributeMap(buff.getStats());
        totalStats.applyAttributeMap(buff.getStats());

        Bukkit.getScheduler().runTaskLater(RpgItemsPlugin.instance, () -> {
            removeBuff(buff.getId());
        }, buff.getDurationInTicks());

        return true;
    }

    public boolean removeBuff(String id){
        BuffInstance buffInstance = null;
        for(BuffInstance b : buffs){
            if(b.getBuff().getId().equalsIgnoreCase(id)){
                buffInstance = b;
            }
        }

        if(buffInstance != null){
            buffs.remove(buffInstance);
            buffStats.removeAttributeMap(buffInstance.getBuff().getStats());
            totalStats.removeAttributeMap(buffInstance.getBuff().getStats());
            return true;
        }
        return false;
    }

    public void removeAllBuffs(){
        buffs = new ArrayList<>();

        update();
    }

    public void removeBonuses(Player player){
        removeBonus(player, Attribute.GENERIC_ATTACK_SPEED, ModifierHelper.attackSpeedModifierName);
        removeBonus(player, Attribute.GENERIC_MAX_HEALTH, ModifierHelper.healthModifierName);
        removeBonus(player, Attribute.GENERIC_MOVEMENT_SPEED, ModifierHelper.moveSpeedModifierName);
    }

    private void removeBonus(Player player, Attribute attribute, String modifierName){
        AttributeInstance attackSpeedAttribute = player.getAttribute(attribute);
        List<AttributeModifier> removeMods = new ArrayList<>();
        for(AttributeModifier mod : attackSpeedAttribute.getModifiers())
            if(mod.getName().equalsIgnoreCase(modifierName))
                removeMods.add(mod);

        for(AttributeModifier mod : removeMods)
            attackSpeedAttribute.removeModifier(mod);
    }

    public void addCooldown(String id, double durationInSeconds){
        cooldowns.put(id, (long) (System.currentTimeMillis() + (durationInSeconds * 1000)));
    }

    private void addBonuses(Player player, boolean fullRefresh){
        addBonus(player, Attribute.GENERIC_ATTACK_SPEED, RpgAttribute.AttackSpeed, ModifierHelper.attackSpeedModifierName, null);
        addBonus(player, Attribute.GENERIC_MOVEMENT_SPEED, RpgAttribute.MovementSpeed, ModifierHelper.moveSpeedModifierName, 0.2);

        if(hero != null){
            CharacterHelper.UpdateCharacterHero(hero.getHero(), oldStats, totalStats, fullRefresh);
        }
        else{
            addBonus(player, Attribute.GENERIC_MAX_HEALTH, RpgAttribute.Health, ModifierHelper.healthModifierName, 20.0);
        }
    }

    private void addBonus(Player player, Attribute attribute, RpgAttribute rpgAttribute, String modifierName, Double normalValue){
        Double value = (Double) totalStats.get(rpgAttribute);
        if(rpgAttribute.isPercent() && normalValue != null)
            value = (value / 100.0) * normalValue;
        else{
            if(rpgAttribute == RpgAttribute.MovementSpeed)
                value = value / 100.0;
        }

        AttributeInstance attributeInstance = player.getAttribute(attribute);
        AttributeModifier attributeModifier = new AttributeModifier(modifierName, value, AttributeModifier.Operation.ADD_NUMBER);
        attributeInstance.addModifier(attributeModifier);
    }

    public Entity getEntity(){
        return entity;
    }

    public RpgCharacterEquipment getEquipment(){
        return equipment;
    }

    public AttributeMap getStats(){
        return totalStats;
    }

    public void addMana(int mana){
        if(PluginHelper.SkillAPIEnabled){
            SkillApiHelper.addMana(getPlayer(), mana);
        }
        else if(this.hero != null){
            hero.addMana(mana);
        }
        else if(PluginHelper.MMOCoreEnabled){
            PlayerData.get(getPlayer()).giveMana(mana);
        }
    }

    public void addStamina(int stamina){
        if(this.hero != null){
            hero.addStamina(stamina);
        }
        else if(PluginHelper.MMOCoreEnabled){
            PlayerData.get(getPlayer()).giveStamina(stamina);
        }
    }

    public void addStellium(int stellium){
        if(PluginHelper.MMOCoreEnabled){
            PlayerData.get(getPlayer()).giveStellium(stellium);
        }
    }

    private void updateHealthScale(){
        HealthScaleOptions healthScaleOptions = DisplayHelper.getHealthScaleOptions();

        if(healthScaleOptions.Enabled){
            Player player = ((Player) entity);

            if(healthScaleOptions.Activation == HealthScaleActivation.Greater)
                player.setHealthScale(Math.min(player.getMaxHealth(), healthScaleOptions.Scale));
            else if(healthScaleOptions.Activation == HealthScaleActivation.Always)
                player.setHealthScale(healthScaleOptions.Scale);
        }
    }

    public void saveCharacter(boolean isAsync){
        if(isPlayer()){
            if(!isAsync){
                save();
                return;
            }

            Bukkit.getScheduler().runTaskAsynchronously(RpgItemsPlugin.instance, this::save);
        }
    }

    private void save(){
        File file = new File(RpgItemsPlugin.instance.getDataFolder() + File.separator + "PlayerData", entity.getUniqueId().toString() + ".yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);

        config.set("Name", this.getPlayer().getDisplayName());
        config.set("Health", this.getPlayer().getHealth());

        if(ClassManager.isEnabled()){
           for(CharacterClassInstance classInstance : allClassInfo){
               classInstance.save(config, "Classes");
           }
           config.set("CurrentClassId", currentCharacterClass.getCharacterClass().getId());
        }
        else{
            config.set("Level", info.getLevel());
            config.set("Experience", info.getExperience());
        }

        config.set("Attributes", null);
        passiveStats.save(config, "Attributes");

        config.set("Buffs", null);
        for (BuffInstance buffInstance : buffs) {
            if (buffInstance.getWearTime() > System.currentTimeMillis() + 1000) {
                String path = "Buffs." + buffInstance.getBuff().getId() + ".";
                config.set(path + "RemainingDuration", (buffInstance.getWearTime() - System.currentTimeMillis()) / 1000.000);
                config.set(path + "Tags", buffInstance.getBuff().getTags());
                buffInstance.getBuff().getStats().save(config, path + ".Stats");
            }
        }

        config.set("Cooldowns", null);
        for (Map.Entry<String, Long> entry : cooldowns.entrySet()) {
            Long delay = entry.getValue() - System.currentTimeMillis();

            if (delay > 0)
                config.set("Cooldowns." + entry.getKey(), delay);
        }

        try {
            if (!file.getParentFile().exists())
                file.getParentFile().mkdirs();
            config.save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RpgItemsOutputHandler.PrintInfo("Saved, " + entity.getName());
    }

    public void loadCharacter(){
        Bukkit.getScheduler().runTaskAsynchronously(RpgItemsPlugin.instance, () -> {
            info = new Experienced((Player) entity, 1.0);

            //Open File
            File file = new File(RpgItemsPlugin.instance.getDataFolder() + File.separator + "PlayerData", entity.getUniqueId().toString() + ".yml");
            if(!file.exists()){
                RpgItemsOutputHandler.PrintInfo("User " + getPlayer().getUniqueId() + " (" + getPlayer().getName() + ") account not found. Creating new account...");

                if(ClassManager.isEnabled()) {
                    setDefaultClass();
                }

                isLoaded = true;
                return;
            }

            FileConfiguration config = YamlConfiguration.loadConfiguration(file);
            if(ClassManager.isEnabled()) {
                String currentClassId = config.getString("CurrentClassId");
                if(ConfigHelper.ConfigContainsPath(config, "Classes")){
                    for(String classId : config.getConfigurationSection("Classes").getKeys(false)){
                        CharacterClass characterClass = ClassManager.getClassById(classId);

                        if(characterClass == null){
                            RpgItemsOutputHandler.PrintInfo("Player has invalid data with classId, " + classId);
                            continue;
                        }

                        CharacterClassInstance classInstance = new CharacterClassInstance((Player) entity, characterClass);
                        classInstance.setLevel(config.getInt("Classes." + classId + ".Level"));
                        classInstance.setExperience(config.getInt("Classes." + classId + ".Experience"));
                        allClassInfo.add(classInstance);

                        if(classId.equalsIgnoreCase(currentClassId)){
                            currentCharacterClass = classInstance;
                        }
                    }
                }

                setDefaultClass();
            }
            else {
                int level = config.getInt("Level");
                int exp = config.getInt("Experience");
                info.setLevel(level);
                info.setExperience(exp);

                int levelCap = ExperienceManager.getLevelCap(info);
                if(info.getLevel() > levelCap){
                    RpgItemsOutputHandler.PrintInfo("User " + getPlayer().getUniqueId() + " (" + getPlayer().getName() + ") level is over level cap. Setting to level cap");
                    this.setLevel(levelCap);
                }
            }

            passiveStats = AttributeMap.load(config, "Attributes");

            if(ConfigHelper.ConfigContainsPath(config, "Buffs")){
                String path = "Buffs.";
                for(String buffId : config.getConfigurationSection("Buffs").getKeys(false)){
                    String buffPath = path + buffId + ".";
                    addBuff(new Buff(buffId, AttributeMap.load(config, buffPath + "Stats"), config.getStringList(buffPath + "Tags"), (long) config.getDouble(buffPath + "RemainingDuration")));
                }
            }
            if(ConfigHelper.ConfigContainsPath(config, "Cooldowns")){
                for(String cooldown : config.getConfigurationSection("Cooldowns").getKeys(false)){
                    cooldowns.put(cooldown, System.currentTimeMillis() + config.getLong("Cooldowns." + cooldown));
                }
            }

            update(true);
            if(ConfigHelper.ConfigContainsPath(config, "Health")){
                double health = config.getDouble("Health");
                if(health > 0){
                    ((Player) entity).setHealth(health);
                }
            }
            RpgItemsOutputHandler.PrintInfo("Loaded, " + entity.getName());
        });

        isLoaded = true;
    }

    public void addExp(Integer exp){
        if(ClassManager.isEnabled()){
            currentCharacterClass.addExperience(exp);
        }
        else{
            info.addExperience(exp);
        }
    }

    public void setLevel(Integer level){
        if(level == null)
            level = 1;

        if(ClassManager.isEnabled()){
            currentCharacterClass.setLevel(level);
            currentCharacterClass.setExperience(0);
        }
        else{
            info.setLevel(level);
            info.setExperience(0);

            if(isPlayer())
                Bukkit.getPluginManager().callEvent(new PlayerLevelUpEvent((Player) entity, info.getLevel(), level));
        }
    }

    public Experienced getInfo(){
        return info;
    }

    public void reset(){
        if(ClassManager.isEnabled()){
            currentCharacterClass.reset();
        }
        else{
            info.reset();
        }
    }

    public int getLevel(){
        if(ClassManager.isEnabled()){
            return currentCharacterClass.getLevel();
        }
        else{
            return info.getLevel();
        }
    }

    public int getExperience(){
        if(ClassManager.isEnabled()){
            return currentCharacterClass.getExperience();
        }
        else{
            return info.getExperience();
        }
    }

    private void setDefaultClass(){
        if(currentCharacterClass == null){
            CharacterClass defaultClass = ClassManager.getDefaultClass();
            RpgItemsOutputHandler.PrintInfo("Player has no active class, using the default " + defaultClass.getId());

            currentCharacterClass = new CharacterClassInstance((Player) entity, defaultClass);
            allClassInfo.add(currentCharacterClass);
        }
    }

}
