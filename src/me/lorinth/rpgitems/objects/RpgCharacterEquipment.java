package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.enums.EquipmentSlot;
import me.lorinth.rpgitems.interfaces.RpgEquipMethod;
import me.lorinth.rpgitems.managers.ClassManager;
import me.lorinth.rpgitems.managers.ItemGenerationManager;
import me.lorinth.rpgitems.util.MessageHelper;
import me.lorinth.utils.ExtensionMethods;
import me.lorinth.rpgitems.util.PluginHelper;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import ru.endlesscode.rpginventory.api.InventoryAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RpgCharacterEquipment{

    private RpgCharacter character;
    private AttributeMap equipmentStats = new AttributeMap(false);
    private AttributeMap gearSetStats = new AttributeMap(false);
    private HashMap<EquipmentSlot, RpgItemStack> itemsInSlots = new HashMap<>();
    private HashMap<GearSet, Integer> gearSetValues = new HashMap<>();
    private List<RpgItemStack> accessories = new ArrayList<>();

    private Integer totalGearScore = 0;
    private Double averageGearScore = 0d;
    private Integer itemsEquipped = 0;

    public RpgCharacterEquipment(RpgCharacter rpgCharacter){
        character = rpgCharacter;

        for(EquipmentSlot slot : EquipmentSlot.values()){
            itemsInSlots.put(slot, null);
        }
    }

    public AttributeMap update(){
        AttributeMap attributes = parseItems();
        updateGearScore();
        updateGearSetLore();
        character.getPlayer().updateInventory();

        return attributes;
    }

    public HashMap<GearSet, Integer> getGearSetValues(){
        return gearSetValues;
    }

    private void updateGearScore(){
        totalGearScore = 0;
        averageGearScore = 0d;
        itemsEquipped = 0;

        for(RpgItemStack rpgItem : itemsInSlots.values()) {
            if (rpgItem != null) {
                totalGearScore += rpgItem.getILevel();
                itemsEquipped++;
            }
        }
        for(RpgItemStack rpgItem : accessories){
            if(rpgItem != null){
                totalGearScore += rpgItem.getILevel();
                itemsEquipped ++;
            }
        }

        averageGearScore = (totalGearScore != null ? totalGearScore : 1) / (itemsEquipped > 0 ? itemsEquipped * 1.0d : 1.0d);
    }

    private void updateGearSetLore(){
        for(Map.Entry<EquipmentSlot, RpgItemStack> entry : itemsInSlots.entrySet()){
            EquipmentSlot slot = entry.getKey();
            RpgItemStack rpgItem = entry.getValue();
            if(rpgItem != null) {
                rpgItem.updateGearSetLore(this.gearSetValues);
                if(rpgItem.hasChanged()){
                    updateGearInSlot(slot, rpgItem.getBukkitItem());
                }
            }
        }
    }

    private void updateGearInSlot(EquipmentSlot slot, ItemStack item){
        if(item == null)
            return;

        EntityEquipment equipment = character.getPlayer().getEquipment();
        switch(slot){
            case MAIN_HAND:
                equipment.setItemInMainHand(item);
                break;
            case OFF_HAND:
                equipment.setItemInOffHand(item);
                break;
            case HEAD:
                equipment.setHelmet(item);
                break;
            case CHEST:
                equipment.setChestplate(item);
                break;
            case LEGS:
                equipment.setLeggings(item);
                break;
            case FEET:
                equipment.setBoots(item);
                break;
        }
    }

    public RpgItemStack getItemInSlot(EquipmentSlot slot){
        return itemsInSlots.get(slot);
    }

    public RpgCharacter getCharacter(){
        return character;
    }

    public Integer getTotalGearScore(){
        return (totalGearScore != null ? totalGearScore : 0);
    }

    public Double getAverageGearScore(){
        return (averageGearScore != null ? averageGearScore : 0d);
    }

    public void dealDurabilityDamage(ItemStack item, Double amount, boolean canDisappearWhenBroken){
        for(Map.Entry<EquipmentSlot, RpgItemStack> entry : itemsInSlots.entrySet()){
            RpgItemStack value = entry.getValue();
            if(value == null || value.getBukkitItem() == null)
                continue;
            if(value.getBukkitItem().isSimilar(item)){
                value.removeDurability(amount);

                if(value.isBroken() && canDisappearWhenBroken)
                    updatePlayerSlot(entry.getKey(), new ItemStack(Material.AIR));
                else
                    updatePlayerSlot(entry.getKey(), value.getBukkitItem());

                return;
            }
        }
    }

    private void updatePlayerSlot(EquipmentSlot slot, ItemStack item){
        if(slot == null || item == null)
            return;

        if(character.isPlayer()){
            Player player = (Player) character.getEntity();
            EntityEquipment equipment = player.getEquipment();
            switch(slot){
                case MAIN_HAND:
                    equipment.setItemInMainHand(item);
                    break;
                case OFF_HAND:
                    equipment.setItemInOffHand(item);
                    break;
                case HEAD:
                    equipment.setHelmet(item);
                    break;
                case CHEST:
                    equipment.setChestplate(item);
                    break;
                case LEGS:
                    equipment.setLeggings(item);
                    break;
                case FEET:
                    equipment.setBoots(item);
                    break;
            }
        }
    }

    private AttributeMap parseItems(){
        equipmentStats = new AttributeMap(false);
        gearSetValues = new HashMap<>();
        gearSetStats = new AttributeMap(false);

        if(character.isPlayer()){
            Player player = (Player) character.getEntity();

            equipPrimaryGear(player);
            equipAccessories(player);
        }

        if(gearSetValues.size() > 0){
            for(Map.Entry<GearSet, Integer> entry : gearSetValues.entrySet()){
                AttributeMap gearSetAttributeMap = new AttributeMap(false);
                if(entry != null && entry.getKey() != null && entry.getValue() != 0){
                    HashMap<Integer, GearSetBuff> gearSetBuffs = entry.getKey().getBuffs();
                    for(int i=1; i<=entry.getValue(); i++){
                        GearSetBuff gearSetBuff = gearSetBuffs.get(i);
                        if(gearSetBuff != null){
                            if(gearSetBuff.doesOverridePrevious())
                                gearSetAttributeMap = gearSetBuff.getBuffs();
                            else
                                gearSetAttributeMap.applyAttributeMap(gearSetBuff.getBuffs());
                        }
                    }
                    gearSetStats.applyAttributeMap(gearSetAttributeMap);
                }
            }
        }
        gearSetStats.applyAttributeMap(equipmentStats);
        return gearSetStats;
    }

    public void equipPrimaryGear(Player player){
        EntityEquipment equip = player.getEquipment();
        compareAndUpdate(EquipmentSlot.MAIN_HAND, equip.getItemInMainHand(), player, this::equipHandItem);
        compareAndUpdate(EquipmentSlot.OFF_HAND, equip.getItemInOffHand(), player, this::equipHandItem);
        compareAndUpdate(EquipmentSlot.HEAD, equip.getHelmet(), player, this::equipArmor);
        compareAndUpdate(EquipmentSlot.CHEST, equip.getChestplate(), player, this::equipArmor);
        compareAndUpdate(EquipmentSlot.LEGS, equip.getLeggings(), player, this::equipArmor);
        compareAndUpdate(EquipmentSlot.FEET, equip.getBoots(), player, this::equipArmor);

        for(RpgItemStack rpgItemStack : itemsInSlots.values()){
            if(rpgItemStack != null && !rpgItemStack.isBroken() && rpgItemStack.isEquipped()){
                equipmentStats.applyAttributeMap(rpgItemStack.getAttributes());
                gearSetValues.put(rpgItemStack.getGearSet(), gearSetValues.getOrDefault(rpgItemStack.getGearSet(), 0) + 1);
            }
        }
    }

    public void equipAccessories(Player player){
        if(PluginHelper.RPGInventoryEnabled) {
            List<ItemStack> passives = InventoryAPI.getPassiveItems(player);
            accessories = ExtensionMethods.setSize(accessories, passives.size());

            for(int i=0; i<passives.size(); i++){
                ItemStack itemStack = passives.get(i);
                compareAndUpdateAccessory(i, itemStack, player, this::equipItem);
            }

            for(RpgItemStack rpgItemStack : accessories){
                if(rpgItemStack != null){
                    equipmentStats.applyAttributeMap(rpgItemStack.getAttributes());
                    gearSetValues.put(rpgItemStack.getGearSet(), gearSetValues.getOrDefault(rpgItemStack.getGearSet(), 0) + 1);
                }
            }
        }
    }

    private void compareAndUpdate(EquipmentSlot slot, ItemStack playerItem, Player player, RpgEquipMethod method){
        RpgItemStack equipped = itemsInSlots.get(slot);
        if(playerItem == null)
            itemsInSlots.put(slot, null);
        else if(equipped == null || !equipped.getBukkitItem().isSimilar(playerItem))
            itemsInSlots.put(slot, method.equip(playerItem, player));
    }

    private void compareAndUpdateAccessory(Integer index, ItemStack playerItem, Player player, RpgEquipMethod method){
        RpgItemStack equipped = accessories.size() > index ? accessories.get(index) : null;
        if(playerItem == null)
            ExtensionMethods.addOrSet(accessories, index, null);
        else if(equipped == null || !equipped.getBukkitItem().isSimilar(playerItem))
            ExtensionMethods.addOrSet(accessories, index, method.equip(playerItem, player));
    }

    private RpgItemStack equipHandItem(ItemStack item, Player player){
        if(item == null)
            return null;
        else if(item.getType() == Material.SHIELD || ItemGenerationManager.getWeaponMaterials().contains(new MaterialDurability(item)))
            return equipItem(item, player);
        return null;
    }

    private RpgItemStack equipArmor(ItemStack item, Player player){
        if(item == null)
            return null;
        else if(ItemGenerationManager.getArmorMaterials().contains(new MaterialDurability(item)))
            return equipItem(item, player);
        return null;
    }

    private RpgItemStack equipItem(ItemStack item, Player player){
        if(item == null)
            return null;
        if(ClassManager.isEnabled() && ItemGenerationManager.isEquipmentItem(item)){
            if(!character.getCurrentCharacterClass().getCharacterClass().canEquipItem(new MaterialDurability(item))){
                player.sendMessage(MessageHelper.getMessage("Class.NotProficient"));
                return null;
            }
        }
        RpgItemStack rpgItemStack = new RpgItemStack(item, player);
        return rpgItemStack;
    }
}
