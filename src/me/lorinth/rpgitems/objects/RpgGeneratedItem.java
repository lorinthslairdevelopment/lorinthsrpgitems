package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.managers.*;
import me.lorinth.rpgitems.util.*;
import me.lorinth.utils.NmsUtils;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static me.lorinth.rpgitems.util.LoreHelper.makeDurabilityLine;

public class RpgGeneratedItem {

    private ItemStack itemStack;
    private int level = 1;
    private Double durability;
    private RarityTier rarityTier = null;
    private Modification prefixModification = null;
    private Modification suffixModification = null;
    private GearSet gearSet = null;
    private AttributeMap attributes = new AttributeMap(false);
    private ArrayList<Modification> modsApplied = new ArrayList<>();
    private int socketCount = 0;

    public RpgGeneratedItem(ItemStack item, int level){
        this.itemStack = item;
        this.durability = DurabilityManager.getDurability(new MaterialDurability(item));
        processLeatherColor();
        processLevel(level);
    }

    private void processLeatherColor(){
        if(itemStack.getItemMeta() instanceof LeatherArmorMeta){
           Color color = LeatherManager.getNextLeatherColor();
           if(color == null)
               return;

           LeatherArmorMeta meta = (LeatherArmorMeta) itemStack.getItemMeta();
           meta.setColor(color);
           itemStack.setItemMeta(meta);
        }
    }

    private void processLevel(int level){
        if(level > ModificationManager.getMaxLevel()){
            if(ModificationManager.isItemLevelDisabled())
                this.level = Math.min(level, ModificationManager.getMaxLevel());
            else
                this.level = Math.min(level, ModificationManager.getMaxItemLevel());
        }
        else
            this.level = level;
    }

    public ItemStack getItemStack(){
        return itemStack;
    }

    public int getLevel(){
        return level;
    }

    public Modification getPrefixModification(){
        return prefixModification;
    }

    public Modification getSuffixModification(){
        return suffixModification;
    }

    public RarityTier getRarityTier(){ return rarityTier; }

    public void setRarityTier(RarityTier tier){
        rarityTier = tier;
    }

    public void setPrefixModification(Modification modification){
        prefixModification = modification;
    }

    public void setSuffixModification(Modification modification){
        suffixModification = modification;
    }

    public void addModification(Modification modification){
        if(modification == null)
            return;

        if(modification.hasSuffix() && suffixModification == null){
            setSuffixModification(modification);
        }
        else if(modification.hasPrefix() && prefixModification == null){
            setPrefixModification(modification);
        }

        modsApplied.add(modification);
        attributes.applyAttributeRangeMap(modification.getAttributes());
    }

    public void setSocketCount(int socketCount){
        this.socketCount = socketCount;
    }

    public ItemStack createItemStack(){
        getModifications();

        ItemMeta meta = itemStack.getItemMeta();
        List<String> lore = meta.getLore();
        if(lore == null)
            lore = new ArrayList<>();
        meta.setDisplayName(getDisplayName(NameHelper.getName(itemStack)).trim());
        addLevelAndRarity(lore);
        addDurability(lore);
        lore.add("");
        lore.addAll(getAttributeLines());

        //Gear Sets
        if(gearSet != null && GearSetsManager.isEnabled()){
            lore.add("");
            lore.addAll(getGearSetLines());
        }
        addSockets(lore);
        meta.setLore(lore);
        meta.setUnbreakable(DurabilityManager.IsEnabled());
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        itemStack.setItemMeta(meta);

        if(DisplayHelper.RemoveVanillaAttributes){
            if(ItemGenerationManager.isWeapon(new MaterialDurability(itemStack)))
                NmsUtils.RemoveDamageAttribute(itemStack);
            else if(ItemGenerationManager.isArmor(new MaterialDurability(itemStack)))
                NmsUtils.RemoveArmorAttribute(itemStack);
        }

        if(gearSet != null && GearSetsManager.isEnabled()){
            NmsUtils.addTag(RpgItemsPlugin.instance, itemStack, "GearSet", this.gearSet.getId());
        }
        if(socketCount > 0 && SocketManager.isEnabled()){
            NmsUtils.addTag(RpgItemsPlugin.instance, itemStack, "SocketCount", socketCount + "");
        }

        return itemStack;
    }

    private void addLevelAndRarity(List<String> lore){
        String levelLine = getLevelLine();
        if(levelLine != null)
            lore.add(levelLine);
        if(level > ModificationManager.getMaxLevel() && !ModificationManager.isItemLevelDisabled())
            lore.add(getItemLevelLine());
        lore.add(getRarityLine());
    }

    public String getLevelLine(){
        if(ModificationManager.getMaxLevel() == 0)
            return null;
        return DisplayHelper.Level.replace("{level}", "" + Math.min(level, ModificationManager.getMaxLevel()));
    }

    public String getItemLevelLine(){
        if(level > ModificationManager.getMaxLevel() && !ModificationManager.isItemLevelDisabled())
            return DisplayHelper.iLevel.replace("{ilevel}", "" + level);
        return null;
    }

    public String getRarityLine(){
        return DisplayHelper.Rarity.replace("{rarity}", rarityTier.getFullDisplayName());
    }

    public String getDurabilityLine(){
        if(durability != null){
            return makeDurabilityLine(durability, durability);
        }
        return null;
    }

    public List<String> getAttributeLines(){
        List<String> attributeLines = new ArrayList<>();
        HashMap<RpgAttribute, Object> attributes = this.attributes.getNonZeroAttributes();
        for(RpgAttribute attribute : RpgAttribute.values()){
            if(attributes.containsKey(attribute)){
                Object value;
                if(attribute.isRange()){
                    value = ((ValueRange) attributes.get(attribute)).multiply(this.rarityTier.getAttributeMultiplier());
                    this.attributes.put(attribute, value);
                }
                else{
                    value = (Double) attributes.get(attribute) * this.rarityTier.getAttributeMultiplier();
                    this.attributes.put(attribute, value);
                }
            }
        }

        attributeLines.addAll(LoreHelper.makeAttributeLines(this.attributes, null));
        return attributeLines;
    }

    private void addDurability(List<String> lore){
        String durabilityLine = getDurabilityLine();
        if(durabilityLine != null) {
            lore.add(durabilityLine);
        }
    }

    private void addSockets(List<String> lore){
        if(socketCount > 0 && SocketManager.isEnabled()){
            if(SocketManager.GetSocketDisplaySettings().SocketHeader.Enabled){
                lore.add(SocketManager.GetSocketDisplaySettings().SocketHeader.Text);
            }
            for(int i=0; i<socketCount; i++){
                lore.add(SocketManager.GetSocketDisplaySettings().OpenSlot);
            }
            if(SocketManager.GetSocketDisplaySettings().SocketFooter.Enabled){
                lore.add(SocketManager.GetSocketDisplaySettings().SocketFooter.Text);
            }
        }
    }

    public void getModifications(){
        IItemTier tier = MaterialTierManager.getItemTierInfo(itemStack);
        if(tier != null)
            tier.applyLevelAttributes(attributes, level);

        int rarityModCount = rarityTier.getModificationCount();
        int maxTryCount = 5;
        int tries = 0;

        for(int i=0; i<rarityModCount; i++){
            Modification mod = null;
            boolean valid = false;
            while(!valid && tries < maxTryCount){
                mod = ModificationManager.nextModification(level);
                if(mod == null){
                    break;
                }

                valid = true;
                if(!mod.canDuplicate())
                    if(modsApplied.contains(mod))
                        valid = false;

                if(valid)
                    for(RpgAttribute attribute : MaterialTierManager.getBlockedAttributes(itemStack)){
                        if(mod.getAttributes().containsKey(attribute))
                            valid = false;
                    }

                if(!valid)
                    tries++;
            }
            tries = 0;
            if(valid) {
                addModification(mod);
            }
        }
    }

    public String getDisplayName(String baseName){
        String displayName = DisplayHelper.Name;
        if(prefixModification != null)
            displayName = displayName.replace("{prefix}", prefixModification.getPrefix());
        else {
            displayName = displayName.replace("{prefix}", "");
        }
        if(suffixModification != null)
            displayName = displayName.replace("{suffix}", suffixModification.getSuffix());
        else {
            displayName = displayName.replace("{suffix}", "");
        }
        displayName = displayName.replace("{name}", ChatColor.stripColor(baseName));
        displayName = displayName.replace("{rarityColor}", "").trim();

        return rarityTier.getColorCode() + displayName;
    }

    public AttributeMap getAttributes(){
        return attributes;
    }

    public void addAttributes(AttributeMap attrMap){
        attributes.applyAttributeMap(attrMap);
    }

    public GearSet getGearSet(){
        return gearSet;
    }

    public void setGearSet(GearSet gearSet){
        this.gearSet = gearSet;
    }

    public List<String> getGearSetLines(){
        ArrayList<String> gearSetLore = new ArrayList<>();
        gearSetLore.add(GearSetsManager.getItemDisplayNameLine().replace("{name}", this.gearSet.getDisplayName()));
        for(Map.Entry<Integer, GearSetBuff> entry : this.gearSet.getBuffs().entrySet()){
            Integer level = entry.getKey();
            GearSetBuff gearSetBuff = entry.getValue();

            gearSetLore.add(parseGearSetLine(GearSetsManager.getItemDisplayValueLine(), level, gearSetBuff.getInactiveLore()));
        }

        return gearSetLore;
    }

    private String parseGearSetLine(String gearSetDisplayLine, Integer equipped, String loreLine){
        return gearSetDisplayLine.replace("{name}", this.gearSet.getDisplayName())
                .replace("{equipped}", equipped.toString())
                .replace("{lore}", loreLine);
    }


    public void boostStats(double multiplier){
        for(RpgAttribute attribute : attributes.keySet()){
            attributes.multiply(attribute, multiplier);
        }
    }

}
