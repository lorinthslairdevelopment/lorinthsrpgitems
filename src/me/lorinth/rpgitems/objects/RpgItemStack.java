package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.managers.AttributeManager;
import me.lorinth.rpgitems.managers.DurabilityManager;
import me.lorinth.rpgitems.objects.sockets.ISocketable;
import me.lorinth.rpgitems.objects.sockets.SocketFailureReason;
import me.lorinth.rpgitems.objects.sockets.SocketGem;
import me.lorinth.rpgitems.util.LevelHelper;
import me.lorinth.rpgitems.util.MessageHelper;
import me.lorinth.rpgitems.util.RpgItemStackParser;
import me.lorinth.utils.MathHelper;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class RpgItemStack implements ISocketable {

    //Bukkit info
    private Player wielder;
    private ItemStack itemStack;

    //Rpg Info
    private String baseName = null;
    private Integer level = 0;
    private Integer iLevel = null;
    private Integer enhancementLevel = 0;
    private RarityTier rarityTier = RarityTier.Common;
    private AttributeMap attributes = new AttributeMap(false);
    private AttributeMap visualBonus = null;
    private boolean hasDurability = false;
    private Double durability = 1.0;
    private Double maxDurability = 1.0;
    private GearSet gearSet;
    private HashMap<GearSet, Integer> equippedGearSets;
    private int lastEquippedGearSetCount = 0;
    private boolean equipped = true;
    private boolean changed = false;
    private boolean isRpgItem = false;
    private ArrayList<SocketGem> socketGems = new ArrayList<>();

    private List<DurabilityMessage> processedMessages = new ArrayList<>();

    public RpgItemStack(){}

    public RpgItemStack(ItemStack item){
        this(item, null);
    }

    public RpgItemStack(ItemStack item, Player player){
        itemStack = item;
        wielder = player;
        RpgItemStackParser.loadRpgItemStack(itemStack, this);
    }

    public boolean hasDurability(){
        return hasDurability;
    }

    public String getBaseName(){
        return baseName;
    }

    public void setBaseName(String baseName){
        this.baseName = baseName;
        this.changed = true;
    }

    public Double getCustomDurability(){
        return durability;
    }

    public void setDurability(Double durability){
        this.durability = durability;
        if(this.maxDurability < this.durability)
            this.maxDurability = this.durability;

        this.hasDurability = true;
        this.changed = true;

        if(durability == 0) {
            if (wielder != null)
                wielder.sendMessage(MessageHelper.getMessage("Item.Broken").replace("{itemname}", itemStack.getItemMeta().getDisplayName()));
            equipped = false;
        }
    }

    public Double getCustomMaxDurability() {
        return maxDurability;
    }

    public void setMaxDurability(Double maxDurability){
        this.maxDurability = maxDurability;
        this.changed = true;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
        this.isRpgItem = true;
        this.changed = true;

        if(wielder != null && level > 1){
            if(LevelHelper.getPlayerLevel(wielder) < level) {
                wielder.sendMessage(MessageHelper.getMessage("Item.HigherLevel").replace("{itemname}", itemStack.getItemMeta().getDisplayName()));
                equipped = false;
            }
        }
    }

    public Integer getILevel(){
        return iLevel != null ? iLevel : level;
    }

    public void setILevel(Integer iLevel) {
        this.iLevel = iLevel;
        this.isRpgItem = true;
        this.changed = true;
    }

    public Integer getEnhancementLevel(){
        return enhancementLevel;
    }

    public void setEnhancementLevel(Integer enhancementLevel){
        this.enhancementLevel = enhancementLevel;
        this.changed = true;
    }

    public RarityTier getRarityTier() { return rarityTier; }

    public void setRarityTier(RarityTier tier){
        this.rarityTier = tier;
        this.changed = true;
    }

    public boolean isRpgItem() { return isRpgItem; }

    public boolean hasMaxDurability(){
        return MathHelper.equals(durability, maxDurability) || !hasDurability;
    }

    public boolean isBroken(){
        return durability <= 0.0d;
    }

    public ItemStack getRawItemStack(){
        return itemStack;
    }

    public void setRawItemStack(ItemStack item){
        this.itemStack = item;
    }

    public ItemStack getBukkitItem(){
        if(changed && isRpgItem)
            rebuild();
        return itemStack;
    }

    public AttributeMap getAttributes(){
        return attributes;
    }

    public List<SocketGem> getSocketGems(){
        return socketGems;
    }

    public void setAttributes(AttributeMap attributes){
        changed = true;
        this.attributes = attributes;
    }

    public AttributeMap getVisualBonus(){
        return this.visualBonus;
    }

    public void setVisualBonus(AttributeMap visualBonus){
        changed = true;
        this.visualBonus = visualBonus;
    }

    public GearSet getGearSet(){
        return gearSet;
    }

    public boolean hasChanged() {
        return changed;
    }

    public boolean isEquipped() { return equipped; }

    public void setGearSet(GearSet gs){
        changed = true;
        gearSet = gs;
    }

    public void updateGearSetLore(HashMap<GearSet, Integer> equippedGearSets){
        int equippedGearSetCount = equippedGearSets.getOrDefault(gearSet, 0);
        changed = lastEquippedGearSetCount != equippedGearSetCount;
        this.equippedGearSets = equippedGearSets;
        lastEquippedGearSetCount = equippedGearSetCount;
    }

    // Durability
    public double addDurability(Double durability){
        durability = Math.min(this.maxDurability - this.durability, durability);
        this.durability = Math.min(this.durability + durability, this.maxDurability);

        this.processedMessages = this.processedMessages.stream().filter(message -> message.getValue().isInRange(this.durability, maxDurability)).collect(Collectors.toList());

        changed = true;
        return durability;
    }

    public void removeDurability(Double durability){
        if(wielder.getGameMode() != GameMode.SURVIVAL && wielder.getGameMode() != GameMode.ADVENTURE)
            return;

        this.durability = Math.max(0.0d, this.durability - durability);

        if(durability == 0 && itemStack.hasItemMeta()){
            wielder.sendMessage(MessageHelper.getMessage("Item.Broken").replace("{itemname}", itemStack.getItemMeta().getDisplayName()));
            attributes = AttributeManager.EMPTY;
        }

        changed = true;
    }

    //Rebuild itemstack with changed values
    private void rebuild(){
        changed = false;
        itemStack = RpgItemStackParser.updateItemFromRpgItem(this.itemStack, this, equippedGearSets);
    }

    public RpgItemStack clone(){
        RpgItemStack rpgItemStack = new RpgItemStack();
        rpgItemStack.setLevel(this.level);
        rpgItemStack.setILevel(this.iLevel);
        rpgItemStack.setRarityTier(this.rarityTier);
        rpgItemStack.setRawItemStack(this.itemStack.clone());
        rpgItemStack.setEnhancementLevel(this.enhancementLevel);
        rpgItemStack.setAttributes(this.attributes);
        rpgItemStack.setVisualBonus(this.visualBonus);
        rpgItemStack.setGearSet(this.gearSet);
        rpgItemStack.setDurability(this.durability);
        rpgItemStack.setMaxDurability(this.maxDurability);
        rpgItemStack.setBaseName(this.baseName);
        return rpgItemStack;
    }

    public void checkForDurabilityMessage(){
        if(wielder == null)
            return;
        if(!hasDurability)
            return;
        if(DurabilityManager.IsEnabled()){
            String lowest = null;
            for(DurabilityMessage message : DurabilityManager.getDurabilityValueMessages()){
                if(!processedMessages.contains(message) && message.getValue().isInRange(durability, maxDurability)){
                    lowest = message.getMessage();
                    if(!message.getValue().isNegative())
                        processedMessages.add(message);
                }
            }

            if(lowest != null && itemStack != null){
                BaseComponent[] component = TextComponent.fromLegacyText(
                        lowest.replace("{name}", itemStack.getItemMeta().getDisplayName())
                                .replace("{durability}", ((int) (double) this.durability) + ""));

                wielder.spigot().sendMessage(DurabilityManager.getChatMessageType(), component);
            }
        }
    }

    public void setSocketGems(ArrayList<SocketGem> socketGems){
        this.socketGems = socketGems;
    }

    @Override
    public SocketFailureReason socket(SocketGem socketGem, Integer slot) {
        if(socketGem.getMinimumLevel() > level){
            return SocketFailureReason.GearLowLevel;
        }

        if(slot == null){
            for(int i=0; i< socketGems.size(); i++){
                SocketGem socket = socketGems.get(i);
                if(socket == null){
                    applySocketGem(socketGem, i);
                    return null;
                }
            }
            return SocketFailureReason.NoOpenSlots;
        }
        else{
            if(socketGems.size() >= slot){
                return SocketFailureReason.InvalidSlot;
            }
            else if(socketGems.get(slot) != null){
                return SocketFailureReason.FilledSlot;
            }

            applySocketGem(socketGem, slot);
            return null;
        }
    }

    private void applySocketGem(SocketGem socketGem, int i){
        socketGems.set(i, socketGem);
        attributes.applyAttributeMap(socketGem.getBonuses());
        changed = true;
    }

    @Override
    public SocketGem replaceSocket(SocketGem socketGem, Integer slot){
        //TODO: Replace a gem
        return null;
    }

    @Override
    public void unsocket(SocketGem socketGem, Integer slot) {
        //TODO: Remove a gem
    }
}
