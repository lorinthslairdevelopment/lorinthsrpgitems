package me.lorinth.rpgitems.objects;

import me.lorinth.utils.TryParse;

public class Value {

    private String stringValue;
    private double value = 0.0d;
    private boolean isPercent = false;
    private boolean isInteger = true;
    private boolean isNegative = false;

    public Value(String value){
        stringValue = value;

        isPercent = value.contains("%");

        isInteger = !value.contains(".");

        isNegative = value.contains("-");

        value = value.replace('%', ' ').trim();
        if(TryParse.parseDouble(value))
            this.value = Math.abs(Double.parseDouble(value));
    }

    public Object getValue(){
        if(isInteger)
            return (int) value;
        else
            return value;
    }

    public boolean isPercent(){
        return isPercent;
    }

    public boolean isNegative() {
        return isNegative;
    }

    public boolean isInRange(double value, double maxValue){
        if(isPercent){
            return (value / maxValue) <= (this.value / 100.0);
        }
        else{
            if(isNegative){
                return value <= this.value;
            }
            else{
                return Math.floor(value) <= this.value;
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj instanceof Value){
            return ((Value) obj).isInteger == isInteger && ((Value) obj).isPercent == isPercent && ((Value) obj).value == value && ((Value) obj).stringValue == stringValue;
        }
        return false;
    }

    @Override
    public int hashCode() {
        if(isPercent)
            return (int) this.value + 1000000;
        else
            return (int) this.value;
    }

    @Override
    public String toString(){
        return stringValue;
    }

}
