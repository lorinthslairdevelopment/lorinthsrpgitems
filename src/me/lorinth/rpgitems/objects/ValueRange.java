package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.util.LoreHelper;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.MathHelper;
import me.lorinth.utils.TryParse;

import java.util.Random;

public class ValueRange {

    private double minimum;
    private double maximum;
    private boolean isDecimal;
    private boolean valid = true;
    private boolean reportError;
    private Random random = new Random();

    public ValueRange(){
        this(0d, 0d);
    }

    public ValueRange(Double value){
        minimum = value;
        maximum = value;
    }

    public ValueRange(Double min, Double max){
        this.minimum = min;
        this.maximum = max;
    }

    public ValueRange(String configValue){
        this(configValue, true);
    }

    public ValueRange(String configValue, boolean reportError){
        this.reportError = reportError;
        if(configValue.contains("-")){
            String[] args = configValue.split("-");
            minimum = parse(args[0]);
            maximum = parse(args[1]);
        }
        else{
            minimum = parse(configValue);
            maximum = parse(configValue);
        }
    }

    private double parse(String value){
        if(value.contains(".")){
            if(TryParse.parseDouble(value)){
                isDecimal = true;
                return Double.parseDouble(value);
            }
        }
        else if(TryParse.parseInt(value))
                return Integer.parseInt(value);

        if(reportError)
            RpgItemsOutputHandler.PrintError("Error parsing value, " + value);

        valid = false;
        return 0;
    }

    public ValueRange add(ValueRange range){
        minimum += range.getMinimum();
        maximum += range.getMaximum();
        return this;
    }

    public ValueRange add(AttributeRange range){
        minimum += range.getMinimum();
        maximum += range.getMaximum();
        return this;
    }

    public ValueRange subtract(ValueRange range){
        minimum -= range.minimum;
        maximum -= range.maximum;
        return this;
    }

    public ValueRange subtract(AttributeRange range){
        minimum -= range.getMinimum();
        maximum -= range.getMaximum();
        return this;
    }

    public ValueRange multiply(double value){
        minimum = minimum * value;
        maximum = maximum * value;
        return this;
    }

    public ValueRange multiply(ValueRange value){
        minimum = minimum * value.getMinimum();
        maximum = maximum * value.getMaximum();
        return this;
    }

    public ValueRange multiply(AttributeRange value){
        minimum = minimum * value.getMinimum();
        maximum = maximum * value.getMaximum();
        return this;
    }

    public double getMinimum(){
        return minimum;
    }

    public double getMaximum(){
        return maximum;
    }

    public double getValue(){
        if(minimum == maximum)
            return maximum;

        if(isDecimal)
            return random.nextDouble() * (maximum - minimum) + minimum;
        else
            return random.nextInt(((int) maximum) - ((int) minimum)) + ((int) minimum);
    }

    public boolean isValid(){
        return valid;
    }

    public void setIsDecimal(boolean isDecimal){
        this.isDecimal = isDecimal;
    }

    public boolean isZero(){
        return MathHelper.equals(minimum, 0) && MathHelper.equals(maximum, 0);
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof ValueRange){
            ValueRange other = (ValueRange) obj;
            return MathHelper.equals(minimum, other.minimum) && MathHelper.equals(maximum, other.maximum);
        }
        return false;
    }

    @Override
    public String toString() {
        if(!isDecimal)
            return ((int) minimum) + "-" + ((int) maximum);
        return LoreHelper.DecimalFormat.format(minimum) + "-" + LoreHelper.DecimalFormat.format(maximum);
    }

    public void cap(Double maximum){
        this.minimum = Math.min(this.minimum, maximum);
        this.maximum = Math.min(this.maximum, maximum);
    }
}
