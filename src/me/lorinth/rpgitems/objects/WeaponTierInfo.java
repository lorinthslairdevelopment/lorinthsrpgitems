package me.lorinth.rpgitems.objects;

import me.lorinth.rpgitems.enums.WeaponTier;
import me.lorinth.utils.TryParse;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.TreeMap;

public class WeaponTierInfo  implements IItemTier<WeaponTier>{

    private WeaponTier weaponTier;
    private TreeMap<Integer, AttributeRangeMap> levelAttributes = new TreeMap<>();
    private boolean valid = false;

    public WeaponTierInfo(FileConfiguration config, String prefix, String tierName){
        if(TryParse.parseEnum(WeaponTier.class, tierName)){
            weaponTier = WeaponTier.valueOf(tierName);
            valid = true;

            loadAttributes(config, prefix + tierName);
        }
    }

    private void loadAttributes(FileConfiguration config, String prefix){
        for(String level : config.getConfigurationSection(prefix).getKeys(false)){
            if(TryParse.parseInt(level)){
                Integer Level = Integer.parseInt(level);
                levelAttributes.put(Level, AttributeRangeMap.FromConfig(config, prefix + "." + level));
            }
        }
    }

    public void applyLevelAttributes(AttributeMap map, Integer level){
        map.applyAttributeRangeMap(levelAttributes.floorEntry(level).getValue());
    }

    public WeaponTier getTier(){
        return weaponTier;
    }

    public boolean isValid(){
        return valid;
    }

}