package me.lorinth.rpgitems.objects.character;

import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.managers.ClassManager;
import me.lorinth.rpgitems.managers.ItemGenerationManager;
import me.lorinth.rpgitems.objects.AttributeBuffProfile;
import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.objects.MaterialDurability;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.ItemStackHelper;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class CharacterClass {

    private String id;
    private String displayName;
    private ItemStack displayItem;
    private ArrayList<MaterialDurability> weaponList;
    private ArrayList<MaterialDurability> armorList;
    private ArrayList<CharacterClass> subclasses;
    private ArrayList<CharacterClassPrerequisite> classPrerequisites;
    private List<String> subclassIds;
    private CharacterClassPermission permission;
    private CharacterClassVitalInformation vitalInformation;
    private CharacterClassMetadata metadata;
    private AttributeBuffProfile attributeBuffProfile;

    public CharacterClass(FileConfiguration config, String path, String id){
        path += "." + id;

        this.id = id;
        loadAttributeBuffProfile(config, path);
        loadDisplayItem(config, path);
        loadMetadata(config, path);
        loadPermission(config, path);
        loadPrerequisite(config, path);
        loadRestrictions(config, path);
        loadSubclassIds(config, path);
        loadVitalInformation(config, path);
    }

    public String getId(){
        return id;
    }

    public String getDisplayName(){
        return displayName;
    }

    public ItemStack getDisplayItem(){
        return displayItem;
    }

    public boolean canEquipItem(MaterialDurability materialDurability){
        boolean noRestrictions =
                (ItemGenerationManager.isWeapon(materialDurability) && weaponList.isEmpty()) ||
                        (ItemGenerationManager.isArmor(materialDurability) && armorList.isEmpty());
        return noRestrictions || weaponList.contains(materialDurability) || armorList.contains(materialDurability);
    }

    public boolean hasPermission(Player player){
        return permission.hasPermission(player);
    }

    public CharacterClassMetadata getMetadata(){
        return metadata;
    }

    public boolean isUnlocked(RpgCharacter rpgCharacter){
        if(hasPrerequisites()){
            for(CharacterClassPrerequisite classPrerequisite : classPrerequisites){
                if(!classPrerequisite.isSatisfied(rpgCharacter)){
                    return false;
                }
            }
        }

        return hasPermission(rpgCharacter.getPlayer());
    }

    public boolean hasPrerequisites(){
        return classPrerequisites.size() > 0;
    }

    public boolean isBaseClass(){
        return !hasPrerequisites();
    }

    public List<CharacterClass> getSubClasses(){
        return subclasses;
    }

    public boolean hasAccessToSubClass(RpgCharacter rpgCharacter){
        return subclasses.stream()
                .anyMatch(characterClass -> characterClass.isUnlocked(rpgCharacter));
    }

    private AttributeMap getVitalMap(int level){
        AttributeMap vitalMap = new AttributeMap(false);
        vitalMap.put(RpgAttribute.Health, vitalInformation.getHealth(level) - 20);
        vitalMap.put(RpgAttribute.Mana, vitalInformation.getMana(level));
        vitalMap.put(RpgAttribute.Stamina, vitalInformation.getStamina(level));
        return vitalMap;
    }

    public AttributeMap getAttributes(int level){
        AttributeMap attributeMap = attributeBuffProfile.getProfileAtLevel(level);
        attributeMap.applyAttributeMap(getVitalMap(level));
        return attributeMap;
    }

    private void loadAttributeBuffProfile(FileConfiguration config, String path){
        attributeBuffProfile = new AttributeBuffProfile(config, path + ".Buffs");
    }

    private void loadDisplayItem(FileConfiguration config, String path){
        displayItem = ItemStackHelper.makeItemFromConfig(config, path, "Display", "classes.yml", RpgItemsOutputHandler.getInstance());
        displayName = displayItem.getItemMeta().getDisplayName();
    }

    private void loadMetadata(FileConfiguration config, String path){
        this.metadata = new CharacterClassMetadata(config, path);
    }

    private void loadPermission(FileConfiguration config, String path){
        permission = new CharacterClassPermission(config, path);
    }

    private void loadPrerequisite(FileConfiguration config, String path){
        classPrerequisites = new ArrayList<>();

        path += ".ClassPrerequisites";
        if(ConfigHelper.ConfigContainsPath(config, path)){
            for(String classId : config.getConfigurationSection(path).getKeys(false)){
                Integer level = config.getInt(path + "." + classId);
                CharacterClass characterClass = ClassManager.getClassById(classId);
                if(characterClass == null){
                    RpgItemsOutputHandler.PrintError("Invalid class prerequisite, " +
                            RpgItemsOutputHandler.HIGHLIGHT + classId +
                            RpgItemsOutputHandler.ERROR + ", for class " +
                            RpgItemsOutputHandler.HIGHLIGHT + id);
                }
                else{
                    classPrerequisites.add(new CharacterClassPrerequisite(characterClass, level));
                }
            }
        }
    }

    private void loadRestrictions(FileConfiguration config, String path){
        path += ".Restrictions";
        loadWeaponList(config, path + ".Weapons");
        loadArmorList(config, path + ".Armor");
    }

    private void loadWeaponList(FileConfiguration config, String path){
        weaponList = new ArrayList<>();

        if(ConfigHelper.ConfigContainsPath(config, path)){
            for(String mdValue : config.getStringList(path)){
                MaterialDurability md = new MaterialDurability(mdValue);
                weaponList.add(md);
            }
        }
    }

    private void loadArmorList(FileConfiguration config, String path){
        armorList = new ArrayList<>();

        if(ConfigHelper.ConfigContainsPath(config, path)) {
            for (String mdValue : config.getStringList(path)) {
                MaterialDurability md = new MaterialDurability(mdValue);
                armorList.add(md);
            }
        }
    }

    private void loadSubclassIds(FileConfiguration config, String path){
        if(ConfigHelper.ConfigContainsPath(config, path + ".Subclasses")){
            subclassIds = config.getStringList(path + ".Subclasses");
        } else {
            subclassIds = new ArrayList<>();
        }
    }

    public void loadSubclassList(){
        subclasses = new ArrayList<>();
        for(String classId : subclassIds){
            CharacterClass subClass = ClassManager.getClassById(classId);
            if(subClass != null){
                subclasses.add(subClass);
            }
            else {
                RpgItemsOutputHandler.PrintError("Invalid subclass Id, " +
                        RpgItemsOutputHandler.HIGHLIGHT + classId +
                        RpgItemsOutputHandler.ERROR + " under class, " +
                        RpgItemsOutputHandler.HIGHLIGHT + id);
            }
        }
    }

    private void loadVitalInformation(FileConfiguration config, String path){
        vitalInformation = new CharacterClassVitalInformation(config, path);
    }

}
