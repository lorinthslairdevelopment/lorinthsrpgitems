package me.lorinth.rpgitems.objects.character;

import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.objects.Experienced;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class CharacterClassInstance extends Experienced {

    private CharacterClass characterClass;

    public CharacterClassInstance(Player player, CharacterClass characterClass){
        super(player, characterClass.getMetadata().ExperienceRequiredModifier);
        this.characterClass = characterClass;
        this.setLevel(characterClass.getMetadata().StartLevel);
    }

    public CharacterClass getCharacterClass(){
        return characterClass;
    }

    public AttributeMap getAttributes(){
        return characterClass.getAttributes(this.getLevel());
    }

    public void save(FileConfiguration config, String prefix){
        prefix += "." + characterClass.getId() + ".";
        config.set(prefix + "Level", this.getLevel());
        config.set(prefix + "Experience", this.getExperience());
    }

}
