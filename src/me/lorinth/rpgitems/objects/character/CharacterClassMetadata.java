package me.lorinth.rpgitems.objects.character;

import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.configuration.file.FileConfiguration;

import java.lang.reflect.Field;

public class CharacterClassMetadata {

    public boolean Default = false;
    public double ExperienceRequiredModifier = 1.0;
    public int GuiSlot = -1;
    public int StartLevel = 1;
    public Integer LevelCap = null;

    public CharacterClassMetadata(FileConfiguration config, String path){
        load(config, path);
    }

    private void load(FileConfiguration config, String path){
        for(Field field : this.getClass().getFields()){
            Object value = config.get(path + "." + field.getName());
            try{
                if(value != null){
                    field.set(this, value);
                }
            } catch(Exception ex){
                RpgItemsOutputHandler.PrintInfo("Error mapping value, "
                    + RpgItemsOutputHandler.HIGHLIGHT + value.toString()
                    + RpgItemsOutputHandler.INFO + " to type, "
                    + RpgItemsOutputHandler.HIGHLIGHT + field.getType());
            }
        }
    }

}
