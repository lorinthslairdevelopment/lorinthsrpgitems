package me.lorinth.rpgitems.objects.character;

import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CharacterClassPermission {

    private boolean required;
    private List<String> nodes;

    public CharacterClassPermission(FileConfiguration config, String path){
        path += ".Permission";
        required = false;
        if(ConfigHelper.ConfigContainsPath(config, path)) {
            required = config.getBoolean(path + ".Required");
            if(required){
                if(!ConfigHelper.ConfigContainsPath(config, path + ".Nodes")){
                    RpgItemsOutputHandler.PrintError("Missing permission setting "
                    + RpgItemsOutputHandler.HIGHLIGHT + "'Nodes' "
                    + RpgItemsOutputHandler.ERROR + "at path, "
                    + RpgItemsOutputHandler.HIGHLIGHT + path
                    + RpgItemsOutputHandler.ERROR + " when Required is set to 'true' in file, "
                    + RpgItemsOutputHandler.HIGHLIGHT + "classes.yml");
                    nodes = new ArrayList<String>(){{
                        add("INVALID_PERMISSION_NODE");
                    }};
                }
                else{
                    nodes = config.getStringList(path + ".Node");
                }
            }
        }
    }

    public boolean hasPermission(Player player){
        return !required || player.isOp() || nodes.stream().allMatch(player::hasPermission);
    }

}
