package me.lorinth.rpgitems.objects.character;

import me.lorinth.rpgitems.objects.RpgCharacter;

public class CharacterClassPrerequisite  {

    public CharacterClass characterClass;
    public Integer level;

    public CharacterClassPrerequisite(CharacterClass characterClass, Integer level){
        this.characterClass = characterClass;
        this.level = level;
    }

    public boolean isSatisfied(RpgCharacter character){
        return character.getAllClassInfo()
                .stream()
                .anyMatch(characterClassInstance ->
                        characterClassInstance.getCharacterClass().getId().equals(characterClass.getId()) &&
                        characterClassInstance.getLevel() >= level);
    }

}
