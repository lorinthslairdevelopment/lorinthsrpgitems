package me.lorinth.rpgitems.objects.character;

import me.lorinth.rpgitems.objects.IncrementalIncrease;
import org.bukkit.configuration.file.FileConfiguration;

public class CharacterClassVitalInformation {

    public IncrementalIncrease health;
    public IncrementalIncrease mana;
    public IncrementalIncrease stamina;

    private final String healthPrefix = "Health.";
    private final String manaPrefix = "Mana.";
    private final String staminaPrefix = "Stamina.";

    private final String baseSuffix = "Base";
    private final String perLevelSuffix = "PerLevel";

    public CharacterClassVitalInformation(FileConfiguration config, String path){
        path += ".";
        health = new IncrementalIncrease(
                config.getDouble(path + healthPrefix + baseSuffix),
                config.getDouble(path + healthPrefix + perLevelSuffix));
        mana = new IncrementalIncrease(
                config.getDouble(path + manaPrefix + baseSuffix),
                config.getDouble(path + manaPrefix + perLevelSuffix));
        stamina = new IncrementalIncrease(
                config.getDouble(path + staminaPrefix + baseSuffix),
                config.getDouble(path + staminaPrefix + perLevelSuffix));
    }

    public double getHealth(int level){
        return health.getResult(level-1);
    }

    public double getMana(int level){
        return mana.getResult(level-1);
    }

    public double getStamina(int level){
        return stamina.getResult(level-1);
    }

}
