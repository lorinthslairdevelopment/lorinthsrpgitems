package me.lorinth.rpgitems.objects.config;

import org.bukkit.configuration.file.FileConfiguration;

public interface IConfigBased {

    void load(String path, FileConfiguration config);

    void save(String path, FileConfiguration config);

}
