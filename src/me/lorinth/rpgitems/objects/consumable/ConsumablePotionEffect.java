package me.lorinth.rpgitems.objects.consumable;

import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ConsumablePotionEffect {

    public static PotionEffect MakePotionEffect(String typeName, double duration, int level){
        PotionEffectType type = PotionEffectType.getByName(typeName);
        if(type != null)
            return new PotionEffect(type, (int) duration * 20, level);
        else
            RpgItemsOutputHandler.PrintError("Unable to parse PotionEffectType, " + RpgItemsOutputHandler.HIGHLIGHT + typeName + RpgItemsOutputHandler.ERROR + ", in consumables.yml");

        return null;
    }

}
