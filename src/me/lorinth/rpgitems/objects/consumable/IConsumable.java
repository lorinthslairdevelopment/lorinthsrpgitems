package me.lorinth.rpgitems.objects.consumable;

import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.objects.RpgCharacter;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public interface IConsumable {

    boolean consume(RpgCharacter character);

    String getId();

    Integer getLevel();

    ItemStack getItemStack();

    List<String> getTags();

    double getCooldown();

    double getEatingDuration();

    Long getEatingDurationInTicks();

    Sound getSound();

    AttributeMap getBuffs();

}
