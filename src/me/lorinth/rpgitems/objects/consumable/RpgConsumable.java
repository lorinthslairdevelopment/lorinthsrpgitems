package me.lorinth.rpgitems.objects.consumable;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.RpgConsumableAction;
import me.lorinth.rpgitems.objects.*;
import me.lorinth.rpgitems.util.CommandHelper;
import me.lorinth.rpgitems.util.LoreHelper;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RpgConsumable implements IConsumable {

    private String id;
    private int level;
    private ItemStack itemStack;
    private double cooldown = 0;
    private double radius = 0;
    private double eatingDuration = 0;
    private double duration = 0;
    private double heal = 0;
    private double mana = 0;
    private double stamina = 0;
    private int stellium = 0;
    private int food = 0;
    private boolean consumed = true;
    private boolean removeOnDeath = true;
    private Sound sound = Sound.ENTITY_GENERIC_EAT;
    private AttributeRangeMap buffs;
    private List<String> tags = new ArrayList<>();
    private ArrayList<PotionEffect> potionEffects = new ArrayList<>();
    private List<String> commands = new ArrayList<>();
    private RpgConsumableEvents events = new RpgConsumableEvents();

    public RpgConsumable(FileConfiguration config, String id){
        this.id = id;

        itemStack = createItemStack(config, id);

        Set<String> options = config.getConfigurationSection(id).getKeys(false);
        for(String option : options){
            String path = id + "." + option;
            switch(option.toLowerCase()){
                case "level":
                    level = config.getInt(path);
                    break;
                case "eatingduration":
                    eatingDuration = config.getDouble(path);
                    break;
                case "cooldown":
                    cooldown = config.getDouble(path);
                    break;
                case "duration":
                    duration = config.getDouble(path);
                    break;
                case "heal":
                    heal = config.getInt(path);
                    break;
                case "mana":
                    mana = config.getInt(path);
                    break;
                case "stamina":
                    stamina = config.getInt(path);
                    break;
                case "stellium":
                    stellium = config.getInt(path);
                    break;
                case "food":
                    food = config.getInt(path);
                    break;
                case "tags":
                    tags = config.getStringList(path);
                    break;
                case "buffs":
                    buffs = AttributeRangeMap.FromConfig(config, path);
                    break;
                case "potioneffects":
                    for(String effect : config.getConfigurationSection(path).getKeys(false)){
                        PotionEffectType potionType = PotionEffectType.getByName(effect.toUpperCase());
                        if(potionType == null){
                            RpgItemsOutputHandler.PrintError("Invalid potion type, " + RpgItemsOutputHandler.HIGHLIGHT + effect +
                                    RpgItemsOutputHandler.ERROR + ",in consumables.yml under consumable, " +
                                    RpgItemsOutputHandler.HIGHLIGHT + id + RpgItemsOutputHandler.ERROR);
                            continue;
                        }

                        this.potionEffects.add(new PotionEffect(potionType, (int) (duration * 20), config.getInt(path + "." + effect)));
                    }
                    break;
                case "commands":
                    commands = config.getStringList(path);
                    break;
                case "radius":
                    radius = config.getDouble(path);
                    break;
                case "sound":
                    if(TryParse.parseEnum(Sound.class, config.getString(path)))
                        sound = Sound.valueOf(config.getString(path));
                    break;
                case "removeondeath":
                    removeOnDeath = config.getBoolean(path);
                    break;
                case "events":
                    loadEvents(config, path);
                    break;
                default:
                    break;
            }
        }
    }

    private void loadEvents(FileConfiguration config, String path){
        String onPickup = config.getString(path + ".OnPickup");
        if(!TryParse.parseEnum(RpgConsumableAction.class, onPickup)){
            RpgItemsOutputHandler.PrintError("Wrong event type, " + RpgItemsOutputHandler.HIGHLIGHT + onPickup);
        }
        else{
            this.events.OnPickup = RpgConsumableAction.valueOf(onPickup);
        }
    }

    private ItemStack createItemStack(FileConfiguration config, String id){
        String path = id + ".Item.";
        ItemStack itemStack = new ItemStack(Material.APPLE, 1);
        MaterialDurability md = new MaterialDurability(config.getString(path + "Id"));
        if(md.isValid())
            itemStack = new ItemStack(md.getMaterial(), 1, md.getDurability());

        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', config.getString(path + "DisplayName")));

        List<String> lore = new ArrayList<>();
        lore.addAll(config.getStringList(path + "Lore"));
        lore.add("");
        lore.add("&8[Consumable: " + id + "]");

        itemMeta.setLore(LoreHelper.translateColorCodes(lore));
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public boolean canConsume(RpgCharacter character){
        Player player = character.getPlayer();
        if(character.getLevel() < level){
            RpgItemsOutputHandler.PrintError(player, "Your level isn't high enough to consume that!");
            return false;
        }

        return true;
    }

    @Override
    public boolean consume(RpgCharacter character) {
        if(!canConsume(character))
            return false;

        Player player = character.getPlayer();

        for(int i = 0; i <= (getEatingDurationInTicks() / 4); i++){
            Bukkit.getScheduler().scheduleSyncDelayedTask(RpgItemsPlugin.instance, () ->
                    player.getWorld().playSound(player.getLocation(), getSound(), 1f, (float) (Math.random() * 2)), 4L * i);
        }

        Bukkit.getScheduler().runTaskLater(RpgItemsPlugin.instance, () -> apply(character), getEatingDurationInTicks());

        return true;
    }

    private boolean apply(RpgCharacter character){
        Player player = character.getPlayer();
        CommandHelper.ExecuteCommands(commands, character.getPlayer(), character.getLevel());

        for(PotionEffect potionEffect : potionEffects){
            character.getPlayer().addPotionEffect(potionEffect, true);
        }

        if(duration > 0L)
            consumed = character.addBuff(getBuff());

        if(cooldown > 0L)
            character.addCooldown(getId(), cooldown);

        if(food != 0)
            player.setFoodLevel(player.getFoodLevel() + food);
        if(heal != 0)
            player.setHealth(Math.min(player.getHealth() + heal, player.getMaxHealth()));
        if(mana != 0)
            character.addMana((int) mana);
        if(stamina != 0)
            character.addStamina((int) stamina);
        if(stellium != 0)
            character.addStellium(stellium);

        return true;
    }

    public Buff getBuff(){
        return new Buff(getId(), getBuffs(), getTags(), (long) duration, removeOnDeath);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Integer getLevel() {
        return level;
    }

    @Override
    public ItemStack getItemStack() {
        return itemStack;
    }

    @Override
    public List<String> getTags(){
        return tags;
    }

    @Override
    public double getEatingDuration(){
        return eatingDuration;
    }

    @Override
    public Long getEatingDurationInTicks(){
        return (long) eatingDuration * 20;
    }

    @Override
    public double getCooldown(){
        return cooldown;
    }

    @Override
    public Sound getSound() { return sound; }

    @Override
    public AttributeMap getBuffs() {
        AttributeMap map = new AttributeMap(false);
        map.applyAttributeRangeMap(buffs);

        return map;
    }

    public RpgConsumableEvents getEvents(){
        return events;
    }

}
