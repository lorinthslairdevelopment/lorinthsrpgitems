package me.lorinth.rpgitems.objects.enhancement;

import me.lorinth.rpgitems.objects.AttributeMap;

import java.util.HashMap;

public class EnhancementData {

    private Integer MinimumLevel;
    private Integer MaximumLevel;
    private Integer NumberOfUpgrades = 1;
    private HashMap<Integer, EnhancementTierInfo> TierChances;
    private double AllAttributePercentIncrease;
    private AttributeMap AttributeSpecificPercentIncrease;
    private AttributeMap AttributeMinimumValueIncrease;
    private AttributeMap AttributeMaximumValueIncrease;

    public Integer getMinimumLevel() {
        return MinimumLevel;
    }

    public void setMinimumLevel(Integer minimumLevel) {
        MinimumLevel = minimumLevel;
    }

    public Integer getMaximumLevel() {
        return MaximumLevel;
    }

    public void setMaximumLevel(Integer maximumLevel) {
        MaximumLevel = maximumLevel;
    }

    public HashMap<Integer, EnhancementTierInfo> getTierChances() {
        return TierChances;
    }

    public void setTierChances(HashMap<Integer, EnhancementTierInfo> tierChances) {
        TierChances = tierChances;
    }

    public double getAllAttributePercentIncrease() {
        return AllAttributePercentIncrease;
    }

    public void setAllAttributePercentIncrease(double allAttributePercentIncrease) {
        AllAttributePercentIncrease = allAttributePercentIncrease / 100.0;
    }

    public AttributeMap getAttributeSpecificPercentIncrease() {
        return AttributeSpecificPercentIncrease;
    }

    public void setAttributeSpecificPercentIncrease(AttributeMap attributeSpecificPercentIncrease) {
        attributeSpecificPercentIncrease.multiplyAll(1.0 / 100.0);
        AttributeSpecificPercentIncrease = attributeSpecificPercentIncrease;
    }

    public AttributeMap getAttributeMaximumValueIncrease() {
        return AttributeMaximumValueIncrease;
    }

    public void setAttributeMaximumValueIncrease(AttributeMap attributeMaximumValueIncrease) {
        AttributeMaximumValueIncrease = attributeMaximumValueIncrease;
    }

    public AttributeMap getAttributeMinimumValueIncrease() {
        return AttributeMinimumValueIncrease;
    }

    public void setAttributeMinimumValueIncrease(AttributeMap attributeMinimumValueIncrease) {
        AttributeMinimumValueIncrease = attributeMinimumValueIncrease;
    }

    public Integer getNumberOfUpgrades() {
        return NumberOfUpgrades;
    }

    public void setNumberOfUpgrades(Integer numberOfUpgrades) {
        if(numberOfUpgrades != null){
            if(numberOfUpgrades < 1) {
                numberOfUpgrades = 1;
            }

            NumberOfUpgrades = numberOfUpgrades;
        }
    }
}
