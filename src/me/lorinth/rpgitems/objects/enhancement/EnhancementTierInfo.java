package me.lorinth.rpgitems.objects.enhancement;

import me.lorinth.rpgitems.enums.RarityTier;

import java.util.HashMap;

public class EnhancementTierInfo {

    private double SuccessChance;
    private double DestoryChance;
    private HashMap<RarityTier, Double> RaritySpecificSuccess = new HashMap<>();
    private HashMap<RarityTier, Double> RaritySpecificDestroy = new HashMap<>();

    public double getSuccessChance(RarityTier rarityTier) {
        return RaritySpecificSuccess.getOrDefault(rarityTier, SuccessChance);
    }

    public void setOverallSuccessChance(double successChance) {
        SuccessChance = successChance;
    }

    public void setRaritySpecificSuccessChance(RarityTier tier, Double chance){
        RaritySpecificSuccess.put(tier, chance);
    }

    public double getDestoryChance(RarityTier rarityTier) {
        return RaritySpecificDestroy.getOrDefault(rarityTier, DestoryChance);
    }

    public void setOverallDestoryChance(double destoryChance) {
        DestoryChance = destoryChance;
    }

    public void setRaritySpecificDestroyChance(RarityTier tier, Double chance){
        RaritySpecificDestroy.put(tier, chance);
    }
}
