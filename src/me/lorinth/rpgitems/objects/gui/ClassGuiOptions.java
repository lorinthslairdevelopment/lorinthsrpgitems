package me.lorinth.rpgitems.objects.gui;

import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.ConfigHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.inventory.ClickType;

public class ClassGuiOptions {

    public boolean AutoPlace = false;
    public String Title;
    public String ClassTitle;
    public String SelectText;
    public String OpenText;
    public ClickType SelectClickType;
    public ClickType OpenSubClassMenu;

    public ClassGuiOptions(FileConfiguration config, String path){
        Title = getColoredMessage(config, path + ".Title", "&aClass Menu");
        ClassTitle = getColoredMessage(config, path + ".ClassTitle", "&a{className} Sub-Classes");
        SelectText = getColoredMessage(config, path + ".Select.Text", "&eLeft click &ato select");
        OpenText = getColoredMessage(config, path + ".Open.Text", "&eRight click &ato Open");

        SelectClickType = getClickType(config, path + ".Select.ClickType", ClickType.LEFT);
        OpenSubClassMenu = getClickType(config, path + ".Open.ClickType", ClickType.RIGHT);
    }

    private String getColoredMessage(FileConfiguration config, String path, String value){
        if(ConfigHelper.ConfigContainsPath(config, path)){
            value = config.getString(path);
        }

        return ChatColor.translateAlternateColorCodes('&', value);
    }

    private ClickType getClickType(FileConfiguration config, String path, ClickType value){
        String configValue = null;
        if(ConfigHelper.ConfigContainsPath(config, path + ".Open.ClickType")){
            configValue = config.getString(path + ".Open.ClickType");
        }

        if(configValue == null){
            return value;
        }
        else if(!TryParse.parseEnum(ClickType.class, configValue)){
            RpgItemsOutputHandler.PrintError("Invalid Click Type, " +
                    RpgItemsOutputHandler.HIGHLIGHT + configValue +
                    RpgItemsOutputHandler.INFO + " in classes.yml at " +
                    RpgItemsOutputHandler.HIGHLIGHT);
            return value;
        }
        else{
            return ClickType.valueOf(configValue);
        }
    }

}
