package me.lorinth.rpgitems.objects.gui;

import org.bukkit.inventory.ItemStack;

public class EnhancementGuiOptions {

    public String Title;
    public int Rows;

    public EnhancementGuiOptions.Slots Slots = new EnhancementGuiOptions.Slots();

    public ItemStack Instructions;
    public ItemStack ChanceInfo;
    public ItemStack LockedSlot;
    public ItemStack NotValidEnhancementItem;

    public class Slots{
        public int Instruction;
        public int TargetItem;
        public int EnhancementMaterial;
        public int Result;
        public int ChanceInfo;
    }

}
