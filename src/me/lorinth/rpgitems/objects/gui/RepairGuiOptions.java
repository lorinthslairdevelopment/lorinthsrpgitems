package me.lorinth.rpgitems.objects.gui;

import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public class RepairGuiOptions {

    public String Title;
    public int Rows;

    public ClickType SingleRepairClick;
    public ClickType MultiRepairClick;

    public Slots Slots = new Slots();

    public ItemStack Instructions;
    public ItemStack RepairValues;
    public ItemStack LockedSlot;

    public class Slots{
        public int Instruction;
        public int RepairValues;
        public int TargetRepairItem;
        public int RepairMaterial;
        public int Result;
    }

}
