package me.lorinth.rpgitems.objects.gui;

import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public class SellGuiOptions {

    public String Title;
    public int Rows;
    public ClickType SingleSelectClick;
    public ClickType MultiSelectClick;
    public boolean UseDoubleConfirm;
    public boolean IgnoreHotbarClick;
    public Slots Slots = new Slots();
    public Items Items = new Items();

    public class Slots{
        public int Total;
        public int Confirm;
        public int Cancel;
    }

    public class Items{
        public ItemStack Total;
        public ItemStack Confirm;
        public ItemStack DoubleConfirm;
        public ItemStack Cancel;
    }

}
