package me.lorinth.rpgitems.objects.placeholder;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.managers.ClassManager;
import me.lorinth.rpgitems.managers.ExperienceManager;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.objects.character.CharacterClassInstance;
import me.lorinth.rpgitems.util.LoreHelper;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.TryParse;
import org.bukkit.entity.Player;

public class RpgItemsPlaceholder extends PlaceholderExpansion {

    public RpgItemsPlaceholder(){
        RpgItemsOutputHandler.PrintInfo("Created RPG Placeholders");
    }

    @Override
    public String getIdentifier() {
        return "rpgitems";
    }

    @Override
    public String getAuthor() {
        return "Lorinthio";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onPlaceholderRequest(Player p, String identifier) {
        RpgCharacter character = PlayerCharacterManager.getCharacter(p);

        if(character == null)
            return "";

        if (identifier.equals("level")) {
            if(ClassManager.isEnabled()){
                return Integer.toString(character.getCurrentCharacterClass().getLevel());
            }
            else{
                return Integer.toString(character.getLevel());
            }
        }
        else if(identifier.equals("experience")){
            return Integer.toString(character.getExperience());
        }
        else if(identifier.equals("experience_percent")){
            double exp = character.getExperience();
            double nextLevel;
            if(ClassManager.isEnabled()){
                CharacterClassInstance classInstance = character.getCurrentCharacterClass();
                nextLevel = ExperienceManager.getExpToNextLevel(classInstance.getLevel(), classInstance.getCharacterClass().getMetadata().ExperienceRequiredModifier);
            }
            else{
                nextLevel = ExperienceManager.getExpToNextLevel(character.getLevel());
            }
            int percent = (int) Math.floor(exp * 100 / nextLevel);
            return Integer.toString(percent) + '%';
        }
        else if(identifier.equals("nextlevel")){
            Integer nextLevel;
            if(ClassManager.isEnabled()){
                CharacterClassInstance classInstance = character.getCurrentCharacterClass();
                nextLevel = ExperienceManager.getExpToNextLevel(classInstance.getLevel(), classInstance.getCharacterClass().getMetadata().ExperienceRequiredModifier);
            }
            else{
                nextLevel = ExperienceManager.getExpToNextLevel(character.getLevel());
            }
            return nextLevel.toString();
        }
        else if(identifier.equals("total_gearscore")){
            return character.getEquipment().getTotalGearScore().toString();
        }
        else if(identifier.equals("average_gearscore")){
            return LoreHelper.DecimalFormat.format(character.getEquipment().getAverageGearScore());
        }
        else if(identifier.equals("class")){
            return character.getCurrentCharacterClass().getCharacterClass().getDisplayName();
        }
        else{
            identifier = identifier.substring(0, 1).toUpperCase() + identifier.substring(1);
            if(TryParse.parseEnum(RpgAttribute.class, identifier)){
                RpgAttribute attr = RpgAttribute.valueOf(identifier);
                return attr.getValueAsString(character.getStats());
            }
        }

        return "";
    }

}
