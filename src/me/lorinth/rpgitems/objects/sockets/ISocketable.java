package me.lorinth.rpgitems.objects.sockets;

public interface ISocketable {

    SocketFailureReason socket(SocketGem socketGem, Integer slot);

    SocketGem replaceSocket(SocketGem socketGem, Integer slot);

    void unsocket(SocketGem socketGem, Integer slot);

}
