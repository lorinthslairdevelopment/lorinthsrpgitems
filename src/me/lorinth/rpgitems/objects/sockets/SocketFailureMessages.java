package me.lorinth.rpgitems.objects.sockets;

public class SocketFailureMessages {

    public String NoOpenSlots;
    public String GearLowLevel;
    public String InvalidSlot;
    public String FilledSlot;

}
