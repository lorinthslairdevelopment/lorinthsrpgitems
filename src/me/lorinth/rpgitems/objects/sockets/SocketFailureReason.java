package me.lorinth.rpgitems.objects.sockets;

public enum SocketFailureReason {

    NoOpenSlots, InvalidSlot, FilledSlot, GearLowLevel

}
