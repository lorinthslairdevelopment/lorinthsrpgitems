package me.lorinth.rpgitems.objects.sockets;

public class SocketFooter{
    public boolean Enabled;
    public String Text;

    public SocketFooter(boolean enabled, String text){
        this.Enabled = enabled;
        this.Text = text;
    }
}
