package me.lorinth.rpgitems.objects.sockets;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.managers.SocketManager;
import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.util.LoreHelper;
import me.lorinth.utils.NmsUtils;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class SocketGem {

    private String id;
    private AttributeMap bonuses;
    private ItemStack itemStack;
    private Integer minimumLevel;
    private boolean canStack = true;

    public SocketGem(String id, AttributeMap bonuses, ItemStack itemStack){
        this.id = id;
        this.bonuses = bonuses;
        this.itemStack = itemStack;

        updateLore();
    }

    public String getId(){
        return id;
    }

    public AttributeMap getBonuses(){
        return bonuses;
    }

    public ItemStack getItemStack(){
        return itemStack.clone();
    }

    public void setMinimumLevel(Integer minLevel){
        minimumLevel = minLevel;
    }

    public int getMinimumLevel(){
        if(minimumLevel == null){
            return 0;
        }
        return minimumLevel;
    }

    public void setCanStack(Boolean canStack){
        if(canStack != null){
            this.canStack = canStack;
        }
    }

    public boolean canStack(){
        return this.canStack;
    }

    private void updateLore() {
        ItemMeta meta = this.itemStack.getItemMeta();
        List<String> lore = meta.getLore();
        for(int i=0; i<lore.size(); i++){
            String line = lore.get(i);
            if(line.contains("{OpenSlot}")){
                lore.set(i, line.replace("{OpenSlot}", SocketManager.GetSocketDisplaySettings().OpenSlot));
            }

            if(line.equalsIgnoreCase("{Bonuses}")){
                lore.remove(i);
                lore.addAll(i, LoreHelper.makeAttributeLines(this.bonuses, null));
            }
        }
        meta.setLore(lore);
        itemStack.setItemMeta(meta);
        NmsUtils.addTag(RpgItemsPlugin.instance, itemStack, "SocketGemId", id);
    }

}
