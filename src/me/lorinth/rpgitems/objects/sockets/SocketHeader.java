package me.lorinth.rpgitems.objects.sockets;

public class SocketHeader{
    public boolean Enabled;
    public String Text;

    public SocketHeader(boolean enabled, String text){
        this.Enabled = enabled;
        this.Text = text;
    }
}
