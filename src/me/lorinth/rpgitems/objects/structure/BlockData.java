package me.lorinth.rpgitems.objects.structure;

import me.idlibrary.main.IDLibrary;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class BlockData {

    private Byte data = null;
    private Material material = Material.AIR;

    public BlockData(String configValue){
        if(configValue.equalsIgnoreCase(""))
            return;

        material = IDLibrary.getMaterial(configValue);
        if(material == null){
            RpgItemsOutputHandler.PrintInfo("Failed to parse value, " + configValue);
        }
    }

    public BlockData(Block block){
        material = block.getType();
        if(block.getType() == Material.CAVE_AIR){
            material = Material.AIR;
        }
        data = block.getData();
    }

    public Byte getData() {
        return data;
    }

    public void setData(Byte data){
        this.data = data;
    }

    public Material getMaterial() {
        return material;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof BlockData){
            if(getData() == null)
                return ((BlockData) obj).getMaterial() == getMaterial();
            else
                return ((BlockData) obj).getMaterial() == getMaterial() && ((BlockData) obj).getData() == getData();
        }
        return false;
    }

    @Override
    public String toString() {
        if(data == null)
            return material.name();

        return material.name() + ":" + data;
    }
}
