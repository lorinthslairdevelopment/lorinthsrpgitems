package me.lorinth.rpgitems.objects.structure;

import me.lorinth.utils.TryParse;

public class KeystoneLocation {

    private int x, y, z;

    public KeystoneLocation(String configValue){
        String[] args;

        args = configValue.split(",");
        if(args.length == 3){
            if(TryParse.parseInt(args[0]))
                x = Integer.parseInt(args[0]);
            if(TryParse.parseInt(args[1]))
                y = Integer.parseInt(args[1]);
            if(TryParse.parseInt(args[2]))
                z = Integer.parseInt(args[2]);
        }

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    @Override
    public String toString() {
        return x + "," + y + "," + z;
    }
}
