package me.lorinth.rpgitems.objects.structure;

import org.bukkit.block.Block;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Layer {

    private BlockData[][] layerBlocks;
    private Integer sizeX = null;
    private Integer sizeZ = null;

    public Layer(JSONObject json){
        JSONArray jsonArray = (JSONArray) json.get("Blocks");

        sizeZ = jsonArray.size();
        for(int z=0; z<jsonArray.size(); z++){
            JSONArray blockArray = (JSONArray) jsonArray.get(z);

            if(sizeX == null){
                sizeX = blockArray.size();
                layerBlocks = new BlockData[sizeZ][sizeX];
            }

            for(int x=0; x<blockArray.size(); x++){
                layerBlocks[z][x] = new BlockData((String) blockArray.get(x));
            }
        }
    }

    public Layer(Integer sizeX, Integer sizeZ, BlockData[][] layerBlocks){
        this.sizeX = sizeX;
        this.sizeZ = sizeZ;
        this.layerBlocks = layerBlocks;
    }

    public boolean layerIsEqual(Block startBlock){
        for(int x=0; x<sizeX; x++){
            for(int z=0; z<sizeZ; z++){
                Block newBlock = startBlock.getRelative(x, 0, z);
                BlockData newBlockData = new BlockData(newBlock);
                BlockData layerBlockData = getBlockData(x, z);

                if(!layerBlockData.equals(newBlockData))
                    return false;
            }
        }

        return true;
    }

    public JSONObject toJSON(){
        JSONObject layer = new JSONObject();

        JSONArray blocksArray = new JSONArray();
        for(int z=0; z<layerBlocks.length; z++){
            BlockData[] row = layerBlocks[z];
            JSONArray rowArray = new JSONArray();

            for(int x=0; x<row.length; x++){
                rowArray.add(row[x].toString());
            }
            blocksArray.add(rowArray);
        }

        layer.put("Blocks", blocksArray);
        return layer;
    }

    public int getSizeX(){
        return sizeX;
    }

    public int getSizeZ(){
        return sizeZ;
    }

    public BlockData getBlockData(int x, int z){
        return layerBlocks[z][x];
    }

    public void putBlockData(int x, int z, BlockData blockData){
        layerBlocks[z][x] = blockData;
    }

}
