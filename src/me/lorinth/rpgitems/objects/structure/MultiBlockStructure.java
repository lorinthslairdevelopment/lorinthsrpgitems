package me.lorinth.rpgitems.objects.structure;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import me.lorinth.utils.JsonHelper;
import me.lorinth.utils.LocationHelper;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class MultiBlockStructure {

    private Layer[] layers;
    private BlockData keystone;
    private KeystoneLocation keystoneLocationInStructure;
    private ArrayList<Location> keystoneLocations;

    private boolean layersChanged = false;

    private File file;
    private JSONObject jsonObject;

    public MultiBlockStructure(File file) {
        this.file = file;

        keystoneLocations = new ArrayList<>();

        JSONParser parser = new JSONParser();
        try {
            jsonObject = (JSONObject) parser.parse(new FileReader(file));

            keystone = new BlockData((String) jsonObject.get("Keystone"));
            keystoneLocationInStructure = new KeystoneLocation((String) jsonObject.get("KeystoneLocation"));

            JSONArray keystoneLocationArray = (JSONArray) jsonObject.get("KeystoneLocations");
            for (int i = 0; i < keystoneLocationArray.size(); i++) {
                keystoneLocations.add(JsonHelper.parseLocationFromJSON((JSONObject) keystoneLocationArray.get(i)));
            }

            JSONArray jsonLayers = (JSONArray) jsonObject.get("Layers");
            this.layers = new Layer[jsonLayers.size()];
            for (int i = 0; i < jsonLayers.size(); i++) {
                this.layers[i] = new Layer((JSONObject) jsonLayers.get(i));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void save() {
        updateJsonObject();

        try {
            if (file.exists())
                file.delete();

            File newFile = new File(file.getPath());

            FileWriter writer = new FileWriter(newFile);

            // Pretty print
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonParser jp = new JsonParser();
            JsonElement je = jp.parse(jsonObject.toString());
            String prettyJsonString = gson.toJson(je);

            writer.write(prettyJsonString);
            writer.flush();
            writer.close();

            RpgItemsOutputHandler.PrintInfo("Saved, " + file.getName());
        } catch (Exception exception) {
            RpgItemsOutputHandler.PrintError("Failed to save, " + file.getName());
        }
    }

    private void updateJsonObject() {
        JSONParser parser = new JSONParser();
        try {
            jsonObject = (JSONObject) parser.parse(new FileReader(file));

            //Keystone Locations
            JSONArray keystoneLocations = new JSONArray();
            for (Location location : this.keystoneLocations) {
                keystoneLocations.add(JsonHelper.parseLocationToJSON(location));
            }
            jsonObject.put("KeystoneLocations", keystoneLocations);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Layers
        if(this.layersChanged){
            JSONArray layers = new JSONArray();
            for(Layer layer : this.layers){
                layers.add(layer.toJSON());
            }

            jsonObject.put("Layers", layers);
        }
    }

    public void updateLayers(Layer[] layers) {
        this.layers = layers;
        layersChanged = true;
    }

    public boolean isKeystone(Block block) {
        return keystone.equals(new BlockData(block));
    }

    public boolean createdMultiblockStructure(Block block) {
        if (!keystone.equals(new BlockData(block)))
            return false;

        int startX = block.getX() - keystoneLocationInStructure.getX();
        int startY = block.getY() - keystoneLocationInStructure.getY();
        int startZ = block.getZ() - keystoneLocationInStructure.getZ();

        for (Layer layer : layers) {
            if (!layer.layerIsEqual(block.getWorld().getBlockAt(startX, startY, startZ)))
                return false;
            startY++;
        }

        keystoneLocations.add(block.getLocation());
        return true;
    }

    public boolean destroyMultiblockStructure(Block block){
        if (!keystone.equals(new BlockData(block)))
            return false;

        Location blockLoc = block.getLocation();
        int size = keystoneLocations.size();
        keystoneLocations.removeIf((Location loc) ->
            loc.getWorld().getName().equalsIgnoreCase(blockLoc.getWorld().getName()) &&
                    loc.getBlockX() == blockLoc.getBlockX() &&
                    loc.getBlockY() == blockLoc.getBlockY() &&
                    loc.getBlockZ() == blockLoc.getBlockZ()
        );

        return size != keystoneLocations.size();
    }

    public boolean isMultiblockStructure(Block block) {
        Location blockLocation = block.getLocation();

        for (Location loc : keystoneLocations) {
            if (LocationHelper.equal(blockLocation, loc))
                return true;
        }

        return false;
    }

}
