package me.lorinth.rpgitems.objects.structure;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.math.BlockVector3;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class WorldEditHelper {

    private static WorldEditPlugin plugin = null;
    private Player player;
    private boolean noData = true;

    public WorldEditHelper(Player player) {
        try {
            this.player = player;

            if(plugin == null){
                plugin = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
            }
        } catch (Exception exception) {
            RpgItemsOutputHandler.PrintError(exception.toString());
        }
    }

    public void setNoData(boolean noData){
        this.noData = noData;
    }

    public Layer[] getSelectionLayers(){
        List<Layer> layers = new ArrayList<>();

        com.sk89q.worldedit.world.World selectionWorld = plugin.getSession(player).getSelectionWorld();
        com.sk89q.worldedit.regions.Region region = null;
        try{
            region = plugin.getSession(player).getSelection(selectionWorld);
        }
        catch(Exception ex){
            RpgItemsOutputHandler.PrintError(player, "World Guard error, something went wrong!");
        }

        if(region == null){
            RpgItemsOutputHandler.PrintError(player, "You must select a world guard region first!");
            return null;
        }

        BlockVector3 min = region.getMinimumPoint();
        BlockVector3 max = region.getMaximumPoint();

        World world = Bukkit.getWorld(selectionWorld.getName());

        Integer sizeY = (max.getBlockY() - min.getBlockY()) + 1;
        Integer sizeX = (max.getBlockX() - min.getBlockX()) + 1;
        Integer sizeZ = (max.getBlockZ() - min.getBlockZ()) + 1;

        int minY = min.getBlockY();
        int minX = min.getBlockX();
        int minZ = min.getBlockZ();

        if(sizeX != sizeZ)
            RpgItemsOutputHandler.PrintRawInfo(player, "This selection is not symmetrically sized. This will cause weird behavior in multiblock detection");

        for(int y = 0; y < sizeY; y++){
            BlockData[][] data = new BlockData[sizeZ][sizeX];

            for(int z = 0; z < sizeZ; z++){
                for(int x = 0; x < sizeX; x++){
                    data[z][x] = new BlockData(world.getBlockAt(minX + x, minY + y, minZ + z));
                    if(noData)
                        data[z][x].setData(null);
                }
            }

            Layer layer = new Layer(sizeX, sizeZ, data);
            layers.add(layer);
        }

        RpgItemsOutputHandler.PrintInfo(player, "Created, " + layers.size() + " layers");

        return layers.toArray(new Layer[layers.size()]);
    }

}
