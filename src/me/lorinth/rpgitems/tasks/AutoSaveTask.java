package me.lorinth.rpgitems.tasks;

import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.lorinth.rpgitems.util.RpgItemsOutputHandler;
import org.bukkit.scheduler.BukkitRunnable;

public class AutoSaveTask extends BukkitRunnable {
    @Override
    public void run() {
        PlayerCharacterManager.saveLoadedPlayers();
        RpgItemsOutputHandler.PrintInfo("Auto saved!");
    }
}
