package me.lorinth.rpgitems.tasks;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.DamageEffectType;
import me.lorinth.rpgitems.managers.EffectManager;
import me.lorinth.rpgitems.objects.DamageEffectInfo;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class LastingEffect extends BukkitRunnable{

    private DamageEffectInfo info;
    private long endTime;

    public LastingEffect(DamageEffectInfo info){
        this.info = info;
        endTime = (System.currentTimeMillis() / 1000) + info.getDuration();
        this.runTaskTimer(RpgItemsPlugin.instance, 10, 10);
        EffectManager.addEffect(info.getEntity(), this);
    }

    public DamageEffectInfo getDamageEffectInfo(){
        return info;
    }

    @Override
    public void run() {
        if((System.currentTimeMillis() / 1000) > endTime){
            this.cancel();
            return;
        }
        else{
            if(info.getDamageEffectType() == DamageEffectType.BURNING)
                info.getEntity().setFireTicks(4);
            if(info.getDamageEffectType() == DamageEffectType.POISON)
                info.getEntity().addPotionEffect(new PotionEffect(PotionEffectType.POISON, 2, 0), false);

            info.getEntity().damage(info.getDamage());
            info.getEntity().setNoDamageTicks(10);
        }
    }

    @Override
    public void cancel(){
        EffectManager.removeEffect(info.getEntity(), this);
        super.cancel();
    }
}
