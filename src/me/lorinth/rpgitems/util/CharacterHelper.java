package me.lorinth.rpgitems.util;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.attributes.AttributeType;
import com.herocraftonline.heroes.characters.Hero;
import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.heroes.*;
import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.utils.MathHelper;
import org.bukkit.Bukkit;

public class CharacterHelper {

    public static void UpdateCharacterHero(Hero hero, AttributeMap oldStats, AttributeMap totalStats, boolean fullRefresh){
        boolean heroesChanged = fullRefresh;
        if(me.lorinth.rpgitems.heroes.RpgItemsPassiveSkill.instance == null){
            Heroes.getInstance().getSkillManager().addSkill(new me.lorinth.rpgitems.heroes.RpgItemsPassiveSkill(Heroes.getInstance()));
        }

        if(fullRefresh || attributeChanged(RpgAttribute.Health, oldStats, totalStats)){
            heroesChanged = true;
            hero.removeEffect(new IncreasedHealthEffect((int) (double) oldStats.get(RpgAttribute.Health)));
            hero.addEffect(new IncreasedHealthEffect((int) (double) totalStats.get(RpgAttribute.Health)));
        }
        if(fullRefresh || attributeChanged(RpgAttribute.Mana, oldStats, totalStats)){
            heroesChanged = true;
            hero.removeEffect(new IncreasedManaEffect((int) (double) oldStats.get(RpgAttribute.Mana)));
            hero.addEffect(new IncreasedManaEffect((int) (double) totalStats.get(RpgAttribute.Mana)));
        }
        if(fullRefresh || attributeChanged(RpgAttribute.ManaRegeneration, oldStats, totalStats)){
            heroesChanged = true;
            hero.removeEffect(new IncreasedManaRegenEffect((int) (double) oldStats.get(RpgAttribute.ManaRegeneration)));
            hero.addEffect(new IncreasedManaRegenEffect((int) (double) totalStats.get(RpgAttribute.ManaRegeneration)));
        }
        if(fullRefresh || attributeChanged(RpgAttribute.Stamina, oldStats, totalStats)){
            heroesChanged = true;
            hero.removeEffect(new IncreasedStaminaEffect((int) (double) oldStats.get(RpgAttribute.Stamina)));
            hero.addEffect(new IncreasedStaminaEffect((int) (double) totalStats.get(RpgAttribute.Stamina)));
        }
        if(fullRefresh || attributeChanged(RpgAttribute.StaminaRegeneration, oldStats, totalStats)){
            heroesChanged = true;
            hero.removeEffect(new IncreasedStaminaRegenEffect((int) (double) oldStats.get(RpgAttribute.StaminaRegeneration)));
            hero.addEffect(new IncreasedStaminaRegenEffect((int) (double) totalStats.get(RpgAttribute.StaminaRegeneration)));
        }

        heroesChanged = refreshHeroesAttribute(hero, AttributeType.STRENGTH, RpgAttribute.Strength, oldStats, totalStats, fullRefresh) || heroesChanged;
        heroesChanged = refreshHeroesAttribute(hero, AttributeType.CONSTITUTION, RpgAttribute.Constitution, oldStats, totalStats, fullRefresh) || heroesChanged;
        heroesChanged = refreshHeroesAttribute(hero, AttributeType.ENDURANCE, RpgAttribute.Endurance, oldStats, totalStats, fullRefresh) || heroesChanged;
        heroesChanged = refreshHeroesAttribute(hero, AttributeType.DEXTERITY, RpgAttribute.Dexterity, oldStats, totalStats, fullRefresh) || heroesChanged;
        heroesChanged = refreshHeroesAttribute(hero, AttributeType.INTELLECT, RpgAttribute.Intellect, oldStats, totalStats, fullRefresh) || heroesChanged;
        heroesChanged = refreshHeroesAttribute(hero, AttributeType.WISDOM, RpgAttribute.Wisdom, oldStats, totalStats, fullRefresh) || heroesChanged;
        heroesChanged = refreshHeroesAttribute(hero, AttributeType.CHARISMA, RpgAttribute.Charisma, oldStats, totalStats, fullRefresh) || heroesChanged;

        if (heroesChanged) {
            Bukkit.getScheduler().runTaskLater(RpgItemsPlugin.instance, () -> hero.rebuildAttributes(), 2L);
        }
    }

    private static boolean refreshHeroesAttribute(Hero hero, AttributeType attributeType, RpgAttribute rpgAttribute, AttributeMap oldStats, AttributeMap totalStats, boolean fullRefresh){
        if(fullRefresh || attributeChanged(rpgAttribute, oldStats, totalStats)){
            hero.removeEffect(new IncreasedAttributeEffect(attributeType, (int) (double) oldStats.get(rpgAttribute)));
            hero.addEffect(new IncreasedAttributeEffect(attributeType, (int) (double) totalStats.get(rpgAttribute)));
            return true;
        }
        return false;
    }

    private static boolean attributeChanged(RpgAttribute rpgAttribute, AttributeMap oldStats, AttributeMap totalStats){
        if(rpgAttribute.isRange()){
            return !(oldStats.get(rpgAttribute)).equals(totalStats.get(rpgAttribute));
        }
        else
            return !MathHelper.equals((Double) oldStats.get(rpgAttribute), (Double) totalStats.get(rpgAttribute));
    }

}
