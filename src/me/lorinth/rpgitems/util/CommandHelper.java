package me.lorinth.rpgitems.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

public class CommandHelper {

    public static void ExecuteCommands(List<String> commands, Player player, Integer level){
        for(String command : commands){
            command = command.replace("{player}", player.getName()).replace("{level}", level.toString());
            RpgItemsOutputHandler.PrintInfo(command);
            if(command.startsWith("console:"))
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command.replace("console:", ""));
            else if(command.startsWith("player:"))
                Bukkit.getServer().dispatchCommand(player, command.replace("player:", ""));
            else
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
        }
    }

}
