package me.lorinth.rpgitems.util;

import me.lorinth.rpgitems.enums.RpgConsumableAction;
import me.lorinth.rpgitems.managers.ConsumableManager;
import me.lorinth.rpgitems.objects.RpgCharacter;
import me.lorinth.rpgitems.objects.consumable.RpgConsumable;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.regex.Pattern;

public class ConsumableHelper {

    public static RpgConsumable getConsumableIdFromItemStack(ItemStack itemStack){
        if(!itemStack.hasItemMeta()){
            return null;
        }

        ItemMeta meta = itemStack.getItemMeta();
        if(!meta.hasLore())
            return null;

        int consumableLine = LoreHelper.getConsumableLine(meta.getLore());
        if(consumableLine == -1)
            return null;

        String line = ChatColor.stripColor(meta.getLore().get(consumableLine));

        String[] pieces = ChatColor.stripColor(DisplayHelper.Consumable).split(Pattern.quote("{consumable}"));
        for(String remove : pieces){
            line = line.replace(remove, "");
        }

        return ConsumableManager.getInstance().getConsumableById(line.trim());
    }

    public static boolean handleEvents(RpgCharacter character, RpgConsumable consumable, RpgConsumableAction action){
        switch(action){
            case CONSUME:
                if(consumable.canConsume(character))
                    consumable.consume(character);
                return true;
        }

        return false;
    }

}
