package me.lorinth.rpgitems.util;

import me.lorinth.rpgitems.enums.HealthScaleActivation;
import me.lorinth.rpgitems.objects.HealthScaleOptions;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public class DisplayHelper {

    public static String Name = "{rarityColor}{prefix} {name} {suffix}";
    public static String Level = ChatColor.translateAlternateColorCodes('&', "&7Level: &6{level}");
    public static String iLevel = ChatColor.translateAlternateColorCodes('&', "&7iLevel: &6{ilevel}");
    public static String Rarity = ChatColor.translateAlternateColorCodes('&', "&7Rarity: {rarity}");
    public static String Durability = ChatColor.translateAlternateColorCodes('&', "&3Durability: {durability}");
    public static String Consumable = ChatColor.translateAlternateColorCodes('&', "&8[Consumable: {consumable}]");
    public static String Gearset = ChatColor.translateAlternateColorCodes('&', "&aGear Set: {gearset}");
    public static String EnhancementFormat = ChatColor.translateAlternateColorCodes('&', "{basename} + {enhancementTier}");
    public static String DurabilitySeperator = " / ";
    public static String AttributeSeperator = " - ";
    public static boolean RemoveVanillaAttributes = false;

    private static HealthScaleOptions healthScaleOptions;

    public static void load(FileConfiguration config){
        healthScaleOptions = new HealthScaleOptions();

        healthScaleOptions.Enabled = config.getBoolean("HealthScale.Enabled");
        healthScaleOptions.Scale = config.getDouble("HealthScale.Scale");

        String activation = config.getString("HealthScale.Activation");
        if(TryParse.parseEnum(HealthScaleActivation.class, activation)){
            healthScaleOptions.Activation = HealthScaleActivation.valueOf(activation);
        }

    }

    public static HealthScaleOptions getHealthScaleOptions(){
        return healthScaleOptions;
    }

}