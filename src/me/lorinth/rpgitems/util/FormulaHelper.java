package me.lorinth.rpgitems.util;

import me.lorinth.rpgitems.objects.DamageEventStatistics;

public class FormulaHelper {

    public static String BaseDamage;
    public static String PvEDamage;
    public static String PvPDamage;
    public static String PvEDefenseModifier;
    public static String PvPDefenseModifier;
    public static String TotalPvEDamage;
    public static String TotalPvPDamage;
    public static String CriticalChance;
    public static String CriticalDamage;
    public static String DodgeChance;
    public static String BlockChance;
    public static String BlockDamage;
    public static String BurnChance;
    public static String PoisonChance;
    public static String VampirismAmount;

    public static String parseWithStats(String formula, DamageEventStatistics eventStatistics){
        if(formula == null){
            return "0";
        }
        formula = parse(formula, "event", eventStatistics.eventDamage);
        formula = parse(formula, "penetration", eventStatistics.penetration);
        formula = parse(formula, "damage", eventStatistics.bonusDamage);
        formula = parse(formula, "totalDamage", eventStatistics.totalDamage);
        formula = parse(formula, "pveDamage", eventStatistics.pveDamage);
        formula = parse(formula, "pvpDamage", eventStatistics.pvpDamage);
        formula = parse(formula, "pveDamageResult", eventStatistics.pveDamageResult);
        formula = parse(formula, "pveDefenseModifier", eventStatistics.pveDefenseModifier);
        formula = parse(formula, "pvpDamageResult", eventStatistics.pvpDamageResult);
        formula = parse(formula, "pvpDefenseModifier", eventStatistics.pvpDefenseModifier);
        formula = parse(formula, "vampirism", eventStatistics.vampirism);
        formula = parse(formula, "accuracy", eventStatistics.accuracy);
        formula = parse(formula, "burnChance", eventStatistics.burnChance);
        formula = parse(formula, "burnDamage", eventStatistics.burnDamage);
        formula = parse(formula, "poisonChance", eventStatistics.poisonChance);
        formula = parse(formula, "poisonDamage", eventStatistics.poisonDamage);
        formula = parse(formula, "defense", eventStatistics.defense);
        formula = parse(formula, "pveDefense", eventStatistics.pveDefense);
        formula = parse(formula, "pvpDefense", eventStatistics.pvpDefense);
        formula = parse(formula, "dodgeChance", eventStatistics.dodgeChance);
        formula = parse(formula, "blockChance", eventStatistics.blockChance);
        formula = parse(formula, "blockDamage", eventStatistics.blockDamage);
        formula = parse(formula, "isBlocking", eventStatistics.isBlocking);
        formula = parse(formula, "criticalChance", eventStatistics.criticalChance);
        formula = parse(formula, "criticalDamage", eventStatistics.criticalDamage);
        return formula;
    }

    private static String parse(String formula, String tag, Object value){
        if(value != null)
            return formula.replace("{" + tag + "}", value.toString());
        else
            return formula.replace("{" + tag + "}", "0");
    }

}
