package me.lorinth.rpgitems.util;

import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.objects.RpgItemStack;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Team;

public class GlowHelper {

    public static void AddGlowToItemEntity(Item item){
        RpgItemStack rpgItemStack = new RpgItemStack(item.getItemStack());

        if(rpgItemStack.isRpgItem()) {
            AddGlowToItemWithTier(item, rpgItemStack.getRarityTier());
        }
    }

    public static void DropWithGlow(Location location, ItemStack item){
        RpgItemStack rpgItemStack = new RpgItemStack(item);

        if(rpgItemStack.isRpgItem()) {
            Item itemEntity = location.getWorld().dropItem(location, item);
            AddGlowToItemWithTier(itemEntity, rpgItemStack.getRarityTier());
        }
    }

    private static void AddGlowToItemWithTier(Item item, RarityTier tier){
        item.setGlowing(true);

        for(Player player : item.getWorld().getPlayers()){
            Team team = player.getScoreboard().getTeam(tier.getDisplayName());
            if(team == null)
                team = player.getScoreboard().registerNewTeam(tier.getDisplayName());

            ChatColor color = ChatColor.getByChar(tier.getColorCode().substring(tier.getColorCode().length()-1));
            if(team.getColor() != color)
                team.setColor(color);

            team.addEntry(item.getUniqueId().toString());
        }
    }

}
