package me.lorinth.rpgitems.util;

import com.herocraftonline.heroes.Heroes;
import com.sucy.skill.SkillAPI;
import io.lumine.xikage.mythicmobs.MythicMobs;
import me.lorinth.rpgmobs.LorinthsRpgMobs;
import me.lorinth.rpgitems.enums.LevelHook;
import me.lorinth.rpgitems.enums.MonsterLevelHook;
import me.lorinth.rpgitems.managers.PlayerCharacterManager;
import me.robin.battlelevels.api.BattleLevelsAPI;
import net.Indyuce.mmocore.api.player.PlayerData;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class LevelHelper {

    public static LevelHook levelHook = LevelHook.Vanilla;
    public static MonsterLevelHook monsterLevelHook = MonsterLevelHook.LorinthsRpgMobs;

    public static int getPlayerLevel(Player player){
        switch(levelHook){
            case BattleStats:
                return BattleLevelsAPI.getLevel(player.getUniqueId());
            case BuiltIn:
                return PlayerCharacterManager.getCharacter(player).getLevel();
            case Heroes:
                return Heroes.getInstance().getCharacterManager().getHero(player).getHeroLevel();
            case MMOCore:
                return PlayerData.get(player).getLevel();
            case SkillAPI:
                return SkillAPI.getPlayerData(player).getMainClass().getLevel();
            case Vanilla:
                return player.getLevel();
        }
        return 1;
    }

    public static int getMonsterLevel(Entity entity){
        Integer level;
        switch(monsterLevelHook){
            case LorinthsRpgMobs:
                level = LorinthsRpgMobs.GetLevelOfEntity(entity);
                return (level != null ? level : 1);
            case MythicMobs:
                return MythicMobs.inst().getAPIHelper().getMythicMobInstance(entity).getLevel();
            case None:
                return 1;
        }
        return 1;
    }

}
