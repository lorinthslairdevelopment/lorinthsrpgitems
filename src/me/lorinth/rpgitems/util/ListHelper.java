package me.lorinth.rpgitems.util;

import java.util.List;

public class ListHelper {

    public static boolean containsOneSimilarElement(List a, List b){
        for(Object objA : a){
            for(Object objB : b){
                if(objA.equals(objB))
                    return true;
            }
        }
        return false;
    }

}
