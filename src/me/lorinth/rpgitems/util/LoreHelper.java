package me.lorinth.rpgitems.util;

import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.managers.AttributeManager;
import me.lorinth.rpgitems.managers.DurabilityManager;
import me.lorinth.rpgitems.objects.AttributeMap;
import me.lorinth.rpgitems.objects.GenerationTags;
import me.lorinth.rpgitems.objects.ValueRange;
import me.lorinth.utils.MathHelper;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class LoreHelper {

    public static DecimalFormat DecimalFormat = new DecimalFormat("#0.##", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

    private interface isLine{
        boolean check(String line);
    }

    public static Integer getIndexOfTag(List<String> lore, String tag){
        return iterateForLine(lore, (line) -> {
            if(line == null)
                return false;
            return ChatColor.stripColor(line).equalsIgnoreCase(tag);
        });
    }

    public static Integer getGenerationTagLevel(List<String> lore){
        int generationTagLine = getLevelGenerationTagLine(lore);
        if(generationTagLine != -1){
            String line = lore.get(generationTagLine);
            String levelString = ChatColor.stripColor(line).replace(GenerationTags.levelTagPieces[0], "").replace(GenerationTags.levelTagPieces[1], "");
            ValueRange valueRange = new ValueRange(levelString, false);
            valueRange.setIsDecimal(false);

            if(valueRange.isValid())
                return (int) valueRange.getValue();
        }

        return -1;
    }

    public static Integer getLevelGenerationTagLine(List<String> lore){
        return iterateForLine(lore, LoreHelper::isLevelGenerationTagLine);
    }

    public static boolean isLevelGenerationTagLine(String line){
        line = ChatColor.stripColor(line);
        return line.startsWith(GenerationTags.levelTagPieces[0]) && line.endsWith(GenerationTags.levelTagPieces[1]);
    }

    public static RarityTier getGenerationTagRarity(List<String> lore){
        int generationTagLine = getLevelGenerationTagLine(lore);
        if(generationTagLine != -1){
            String line = lore.get(generationTagLine);
            String rarityValue = ChatColor.stripColor(line).replace(GenerationTags.levelTagPieces[0], "").replace(GenerationTags.levelTagPieces[1], "");

            if(TryParse.parseEnum(RarityTier.class, rarityValue)){
                return RarityTier.valueOf(rarityValue);
            }
        }

        return null;
    }

    public static Integer getRarityGenerationTagLine(List<String> lore){
        return iterateForLine(lore, LoreHelper::isRarityGenerationTagLine);
    }

    public static boolean isRarityGenerationTagLine(String line){
        line = ChatColor.stripColor(line);
        return line.startsWith(GenerationTags.rarityTagPieces[0]) && line.endsWith(GenerationTags.rarityTagPieces[1]);
    }

    public static Integer getBoostedGenerationTagLine(List<String> lore){
        return iterateForLine(lore, LoreHelper::isBoostedGenerationTagLine);
    }

    public static boolean isBoostedGenerationTagLine(String line){
        line = ChatColor.stripColor(line);
        return line.startsWith(GenerationTags.boostedTagPieces[0]) && line.endsWith(GenerationTags.boostedTagPieces[1]);
    }

    public static Integer getGearSetGenerationTagLine(List<String> lore){
        return iterateForLine(lore, LoreHelper::isGearSetGenerationTagLine);
    }

    public static boolean isGearSetGenerationTagLine(String line){
        line = ChatColor.stripColor(line);
        return line.startsWith(GenerationTags.gearSetTagPieces[0]) && line.endsWith(GenerationTags.gearSetTagPieces[1]);
    }


    public static List<String> stripAttributeLore(List<String> lore){
        lore.removeIf(LoreHelper::isAttributeLine);
        return lore;
    }

    public static Integer getLevelLine(List<String> lore){
        return iterateForLine(lore, LoreHelper::isLevelLine);
    }

    public static boolean isLevelLine(String line){
        return line.contains(ChatColor.stripColor(DisplayHelper.Level).replace("{level}", ""));
    }

    public static Integer getGearsetLine(List<String> lore){
        return iterateForLine(lore, LoreHelper::isGearsetLine);
    }

    public static boolean isGearsetLine(String line){
        return line.contains(ChatColor.stripColor(DisplayHelper.Gearset).replace("{gearset}", ""));
    }

    public static Integer getItemLevelLine(List<String> lore){
        return iterateForLine(lore, LoreHelper::isItemLevelLine);
    }

    public static boolean isItemLevelLine(String line){
        return line.contains(ChatColor.stripColor(DisplayHelper.iLevel).replace("{ilevel}", ""));
    }

    public static Integer getDurabilityLine(List<String> lore){
        return iterateForLine(lore, LoreHelper::isDurabilityLine);
    }

    public static boolean isDurabilityLine(String line){
        if(line == null)
            return false;
        else
            return line.contains(ChatColor.stripColor(DisplayHelper.Durability).replace("{durability}", "")) && line.contains(DisplayHelper.DurabilitySeperator);
    }

    public static Integer getRarityLine(List<String> lore){
        return iterateForLine(lore, LoreHelper::isRarityLine);
    }

    public static boolean isRarityLine(String line){
        return line.contains(ChatColor.stripColor(DisplayHelper.Rarity).replace("{rarity}", ""));
    }

    public static Integer getConsumableLine(List<String> lore){
        return iterateForLine(lore, LoreHelper::isConsumableLine);
    }

    public static boolean isConsumableLine(String line){

        return line.contains(ChatColor.stripColor(DisplayHelper.Consumable).split(Pattern.quote("{consumable}"))[0]);
    }

    public static boolean isAttributeLine(String line){
        line = ChatColor.stripColor(line).replace("%", "");
        if(LoreHelper.isLevelLine(line))
            return false;
        else if(DurabilityManager.IsEnabled() && LoreHelper.isDurabilityLine(line))
            return false;
        else if(line.contains(DisplayHelper.AttributeSeperator)){
            String[] args = line.split(Pattern.quote(DisplayHelper.AttributeSeperator));
            if(args.length == 2){
                String attributeName = args[0].trim();
                String value = args[1].trim();

                RpgAttribute attribute = AttributeManager.getAttributeByName(attributeName);
                return attribute != null && attribute.isEnabled() && TryParse.parseDouble(value);
            }
        }
        return false;
    }

    private static Integer iterateForLine(List<String> lines, isLine isLineLambda){
        if(lines == null)
            return -1;

        for(int i=0; i<lines.size(); i++) {
            if (isLineLambda.check(lines.get(i)))
                return i;
        }
        return -1;
    }

    public static String makeDurabilityLine(Double min, Double max){
        String valueMin = "" + (DurabilityManager.useDecimalDurability ? min.intValue() : DecimalFormat.format(min));
        String valueMax = "" + (DurabilityManager.useDecimalDurability ? max.intValue() : DecimalFormat.format(max));

        String durabilityInfo = valueMin + DisplayHelper.DurabilitySeperator + valueMax;
        return DisplayHelper.Durability.replace("{durability}", durabilityInfo);
    }

    public static ArrayList<String> makeAttributeLines(AttributeMap map, AttributeMap visualBonuses){
        ArrayList<String> lines = new ArrayList<>();

        HashMap<RpgAttribute, Object> attributes = map.getNonZeroAttributes();
        for(RpgAttribute attribute : RpgAttribute.values()){
            if(attributes.containsKey(attribute)){
                String percentTag = attribute.isPercent() ? "%" : "";
                Object value = attributes.get(attribute);
                String line = "";
                if(attribute.isRange())
                    line = ChatColor.GRAY + attribute.getDisplayName() + DisplayHelper.AttributeSeperator + value.toString();
                else if(attribute.isDecimal())
                    line = ChatColor.GRAY + attribute.getDisplayName() + DisplayHelper.AttributeSeperator + DecimalFormat.format(value) + percentTag;
                else
                    line = ChatColor.GRAY + attribute.getDisplayName() + DisplayHelper.AttributeSeperator + ((int) (double) value) + percentTag;

                if(visualBonuses != null){
                    Object visualBonusValue = visualBonuses.get(attribute);
                    if(!MathHelper.isZero(visualBonusValue)){
                        if(attribute.isRange())
                            line += ChatColor.GREEN + " (+ " + visualBonusValue.toString() + ")";
                        else if(attribute.isDecimal())
                            line += ChatColor.GREEN + " (+ " + DecimalFormat.format(visualBonusValue) + percentTag + ")";
                        else
                            line += ChatColor.GREEN + " (+ " +  ((int) (double) visualBonusValue) + percentTag + ")";
                    }
                }

                lines.add(line);
            }
        }

        return lines;
    }

    public static List<String> translateColorCodes(List<String> lines){
        ArrayList<String> newLore = new ArrayList<>();
        for(String line : lines){
            newLore.add(ChatColor.translateAlternateColorCodes('&', line));
        }
        return newLore;
    }

}
