package me.lorinth.rpgitems.util;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.utils.ConfigHelper;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.InputStreamReader;
import java.util.HashMap;

public class MessageHelper {

    private static HashMap<String, Message> messages;

    public MessageHelper(RpgItemsPlugin plugin){
        load(plugin);
    }

    public void load(RpgItemsPlugin plugin){
        messages = new HashMap<>();

        File messageFile = new File(plugin.getDataFolder(), "messages.yml");
        YamlConfiguration config = YamlConfiguration.loadConfiguration(messageFile);

        boolean newMessageTypes = false;
        FileConfiguration messageConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(plugin.getResource("resources/messages.yml")));
        for(String path : messageConfig.getConfigurationSection("").getKeys(true)){
            if(messageConfig.get(path) instanceof String){
                newMessageTypes = loadMessage(config, path, messageConfig.getString(path)) || newMessageTypes;
            }
        }

        if(newMessageTypes){
            try{
                config.save(messageFile);
            }
            catch(Exception ex){
                RpgItemsOutputHandler.PrintException("Failed to save new messages to messages.yml", ex);
            }
        }
    }

    private boolean loadMessage(FileConfiguration userConfig, String path, String defaultMessage){
        Message message = new Message(path, defaultMessage);
        boolean isNew = false;

        if(!ConfigHelper.ConfigContainsPath(userConfig, path)){
            isNew = true;
            userConfig.set(path, defaultMessage);
        }
        else{
            message.load(userConfig);
        }

        messages.put(path, message);
        return isNew;
    }

    public static String getMessage(String path){
        return messages.get(path).getValue();
    }

    private class Message{

        private String path;
        private String value;

        public Message(String path, String defaultValue){
            this.value = ChatColor.translateAlternateColorCodes('&', defaultValue);
        }

        public void load(FileConfiguration config){
            if(ConfigHelper.ConfigContainsPath(config, path)){
                value = ChatColor.translateAlternateColorCodes('&', config.getString(path));
            }
        }

        public String getValue(){
            return value;
        }
    }
}