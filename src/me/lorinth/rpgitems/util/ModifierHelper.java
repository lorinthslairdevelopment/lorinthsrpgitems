package me.lorinth.rpgitems.util;

public class ModifierHelper {

    public static String attackSpeedModifierName = "RpgItems.AttackSpeed";
    public static String healthModifierName = "RpgItems.Health";
    public static String manaModifierName = "RpgItems.Mana";
    public static String moveSpeedModifierName = "RpgItems.MovementSpeed";

}
