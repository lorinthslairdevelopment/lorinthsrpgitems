package me.lorinth.rpgitems.util;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class NameHelper {

    public static List<String> Material_Wood;
    public static List<String> Material_Leather;
    public static List<String> Material_Stone;
    public static List<String> Material_Iron;
    public static List<String> Material_Gold;
    public static List<String> Material_Diamond;

    public static List<String> Item_Shield;
    public static List<String> Item_Sword;
    public static List<String> Item_Axe;
    public static List<String> Item_Bow;
    public static List<String> Item_Helmet;
    public static List<String> Item_Chest;
    public static List<String> Item_Leg;
    public static List<String> Item_Boot;

    private static HashMap<String, List<String>> Custom_Items;

    private static Random random = new Random();

    public static String getName(ItemStack item){
        // Handle Custom items / Textures based on durability
        String customName = getCustomName(item);
        if(customName != null)
            return customName;

        String display = "";
        // Handle vanilla items
        String mat = item.getType().name();
        if(mat.contains("WOOD"))
            display += getRandomName(Material_Wood);
        else if(mat.contains("LEATHER"))
            display += getRandomName(Material_Leather);
        else if(mat.contains("STONE"))
            display += getRandomName(Material_Stone);
        else if(mat.contains("IRON"))
            display += getRandomName(Material_Iron);
        else if(mat.contains("GOLD"))
            display += getRandomName(Material_Gold);
        else if(mat.contains("DIAMOND"))
            display += getRandomName(Material_Diamond);

        if(display.length() > 0)
            display += " ";

        if(mat.contains("SHIELD"))
            display += getRandomName(Item_Shield);
        else if(mat.contains("SWORD"))
            display += getRandomName(Item_Sword);
        else if(mat.contains("AXE"))
            display += getRandomName(Item_Axe);
        else if(mat.contains("BOW"))
            display += getRandomName(Item_Bow);
        else if(mat.contains("HELMET"))
            display += getRandomName(Item_Helmet);
        else if(mat.contains("CHEST"))
            display += getRandomName(Item_Chest);
        else if(mat.contains("LEGGIN"))
            display += getRandomName(Item_Leg);
        else if(mat.contains("BOOT"))
            display += getRandomName(Item_Boot);

        return display.trim();
    }

    private static String getCustomName(ItemStack item){
        if(item.hasItemMeta()){
            ItemMeta meta = item.getItemMeta();
            if(meta instanceof Damageable){
                Damageable damageable = ((Damageable) item.getItemMeta());
                if(damageable.getDamage() != 0) {
                    String key = item.getType().toString() + ":" + damageable.getDamage();
                    if(Custom_Items.containsKey(key)){
                        return getRandomName(Custom_Items.get(key));
                    }
                }
            }
        }

        String key = item.getType().name();
        if(Custom_Items.containsKey(key)){
            return getRandomName(Custom_Items.get(key));
        }

        return null;
    }

    private static String getRandomName(List<String> names){
        if(names == null)
            return "";
        if(names.size() == 0)
            return "";

        return names.get(random.nextInt(names.size()));
    }

    public static void load(FileConfiguration config){
        Custom_Items = new HashMap<>();
        for(String key : config.getConfigurationSection("Custom").getKeys(false))
            Custom_Items.put(key.toUpperCase(), config.getStringList("Custom." + key));
    }

}
