package me.lorinth.rpgitems.util;

public class PluginHelper {

    public static boolean DungeonsXLEnabled = false;
    public static boolean HeroesEnabled = false;
    public static boolean McMMoEnabled = false;
    public static boolean MMOCoreEnabled = false;
    public static boolean MythicDropsEnabled = false;
    public static boolean MythicMobsEnabled = false;
    public static boolean RPGInventoryEnabled = false;
    public static boolean PlaceholderAPIEnabled = false;
    public static boolean SkillAPIEnabled = false;

}
