package me.lorinth.rpgitems.util;

import me.lorinth.rpgitems.RpgItemsPlugin;
import me.lorinth.rpgitems.enums.RarityTier;
import me.lorinth.rpgitems.enums.RpgAttribute;
import me.lorinth.rpgitems.managers.*;
import me.lorinth.rpgitems.objects.*;
import me.lorinth.rpgitems.objects.sockets.SocketGem;
import me.lorinth.utils.NmsUtils;
import me.lorinth.utils.TryParse;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class RpgItemStackParser {

    public static void loadRpgItemStack(ItemStack item, RpgItemStack rpgItem){
        if(!item.hasItemMeta())
            return;

        ItemMeta meta = item.getItemMeta();
        AttributeMap attributes = new AttributeMap(false);

        //Gearset
        String gearSetId = NmsUtils.getTag(RpgItemsPlugin.instance, item, "GearSet");
        GearSet gearSet = null;
        if(gearSetId != null){
            gearSet = GearSetsManager.getGearSetById(gearSetId);
        }

        if(SocketManager.isEnabled()){
            int socketCount = 0;
            ArrayList<SocketGem> socketGems = new ArrayList<>();

            String socketCountValue = NmsUtils.getTag(RpgItemsPlugin.instance, item, "SocketCount");
            if(socketCountValue != null){
                socketCount = Integer.parseInt(socketCountValue);
                for(int i=0; i<socketCount; i++){
                    String socketGemId = NmsUtils.getTag(RpgItemsPlugin.instance, item, "Socket_" + i);
                    if(socketGemId != null){
                        socketGems.add(SocketManager.GetSocketGemById(socketGemId));
                    }
                    else{
                        socketGems.add(null);
                    }
                }
            }

            rpgItem.setSocketGems(socketGems);
        }

        if(meta.hasLore()){
            for(String line : meta.getLore()){
                line = ChatColor.stripColor(line).replace("%", "");
                if(LoreHelper.isItemLevelLine(line)){
                    String[] pieces = ChatColor.stripColor(DisplayHelper.iLevel).split(Pattern.quote("{ilevel}"));
                    for(String remove : pieces){
                        line = line.replace(remove, "");
                    }
                    if(TryParse.parseInt(line)){
                        rpgItem.setILevel(Integer.parseInt(line));
                    }
                }
                else if(LoreHelper.isLevelLine(line)){
                    String[] pieces = ChatColor.stripColor(DisplayHelper.Level).split(Pattern.quote("{level}"));
                    for(String remove : pieces){
                        line = line.replace(remove, "");
                    }
                    if(TryParse.parseInt(line)) {
                        rpgItem.setLevel(Integer.parseInt(line));
                    }
                }
                else if(LoreHelper.isRarityLine(line)){
                    String[] pieces = ChatColor.stripColor(DisplayHelper.Rarity).split(Pattern.quote("{rarity}"));
                    for(String remove : pieces){
                        line = line.replace(remove, "");
                    }
                    rpgItem.setRarityTier(RarityTier.getTierByDisplayName(line));
                }
                else if(DurabilityManager.IsEnabled() && LoreHelper.isDurabilityLine(line)){
                    String[] pieces = ChatColor.stripColor(DisplayHelper.Durability).split(Pattern.quote("{durability}"));
                    for(String remove : pieces){
                        line = line.replace(remove, "");
                    }

                    String[] values = line.split(Pattern.quote(DisplayHelper.DurabilitySeperator));
                    if(TryParse.parseDouble(values[0].trim()))
                        rpgItem.setDurability(Double.parseDouble(values[0].trim()));
                    if(TryParse.parseDouble(values[1].trim()))
                        rpgItem.setMaxDurability(Double.parseDouble(values[1].trim()));
                }
                else if(GearSetsManager.isEnabled() && LoreHelper.isGearsetLine(line) && gearSet == null){
                    String gearSetLoreLine = ChatColor.stripColor(line);
                    String[] gearSetDisplayPieces = ChatColor.stripColor(DisplayHelper.Gearset).split(Pattern.quote("{gearset}"));
                    for(String piece : gearSetDisplayPieces){
                        gearSetLoreLine = gearSetLoreLine.replace(piece, "");
                    }
                    String gearSetName = gearSetLoreLine;
                    rpgItem.setGearSet(GearSetsManager.getGearSetByName(gearSetName));
                }
                else if(line.contains(DisplayHelper.AttributeSeperator)){
                    String[] args = line.split(Pattern.quote(DisplayHelper.AttributeSeperator));
                    if(args.length >= 2){
                        String attributeName = args[0].trim();
                        String value = line.replace(args[0] + DisplayHelper.AttributeSeperator, "");

                        RpgAttribute attribute = AttributeManager.getAttributeByName(attributeName);
                        if(attribute != null && attribute.isEnabled()) {
                            if(attribute.isRange()){
                                ValueRange range = new ValueRange(value);
                                attributes.add(attribute, range);
                            }
                            else if(TryParse.parseDouble(value))
                                attributes.add(attribute, Double.parseDouble(value));
                        }
                    }
                }
            }

            if(rpgItem.isRpgItem()){
                String baseName = NmsUtils.getTag(RpgItemsPlugin.instance, item, "BaseName");
                if(baseName == null){
                    baseName = meta.getDisplayName();
                    NmsUtils.addTag(RpgItemsPlugin.instance, item, "BaseName", baseName);
                    rpgItem.setRawItemStack(item);
                }
                rpgItem.setBaseName(baseName);
            }

            //Enhancement Level
            String enhancementLevelString = NmsUtils.getTag(RpgItemsPlugin.instance, item, "EnhancementLevel");
            int enhancementLevel = 0;
            if(enhancementLevelString != null){
                if(TryParse.parseInt(enhancementLevelString)) {
                    enhancementLevel = Integer.parseInt(enhancementLevelString);
                }
            }
            rpgItem.setEnhancementLevel(enhancementLevel);

            rpgItem.setAttributes(attributes);
            rpgItem.checkForDurabilityMessage();
        }
    }

    public static ItemStack updateItemFromRpgItem(ItemStack itemStack, RpgItemStack rpgItemStack, HashMap<GearSet, Integer> equippedGearSets){
        if(itemStack == null || rpgItemStack == null)
            return itemStack;
        ItemMeta meta = itemStack.getItemMeta();
        if(meta == null)
            return itemStack;

        List<String> lore = meta.getLore();
        if(DurabilityManager.IsEnabled() && rpgItemStack.hasDurability()){
            lore.set(LoreHelper.getDurabilityLine(lore), LoreHelper.makeDurabilityLine(rpgItemStack.getCustomDurability(), rpgItemStack.getCustomMaxDurability()));

            if(meta.isUnbreakable())
                meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        }

        lore = updateAttributeLore(lore, rpgItemStack);

        GearSet gearSet = rpgItemStack.getGearSet();
        buildGearsetLore(lore, gearSet, equippedGearSets);

        if(SocketManager.isEnabled()){
            buildSocketLore(itemStack, lore, rpgItemStack.getSocketGems());
        }

        meta.setLore(lore);

        if(DisplayHelper.RemoveVanillaAttributes){
            if(ItemGenerationManager.isWeapon(new MaterialDurability(itemStack)))
                NmsUtils.RemoveDamageAttribute(itemStack);
            else if(ItemGenerationManager.isArmor(new MaterialDurability(itemStack)))
                NmsUtils.RemoveArmorAttribute(itemStack);
        }

        itemStack.setItemMeta(meta);
        storeSocketInfo(itemStack, rpgItemStack.getSocketGems());

        if(gearSet != null) {
            NmsUtils.addTag(RpgItemsPlugin.instance, itemStack, "GearSet", gearSet.getId());
        }
        if(rpgItemStack.getEnhancementLevel() > 0){
            NmsUtils.addTag(RpgItemsPlugin.instance, itemStack, "EnhancementLevel", rpgItemStack.getEnhancementLevel().toString());
            meta = itemStack.getItemMeta();

            if(EnhancementManager.enabled){
                meta.setDisplayName(
                        DisplayHelper.EnhancementFormat
                                .replace("{basename}", rpgItemStack.getBaseName())
                                .replace("{enhancementTier}", rpgItemStack.getEnhancementLevel().toString()));
                itemStack.setItemMeta(meta);
            }
        }

        rpgItemStack.checkForDurabilityMessage();
        return itemStack;
    }

    private static List<String> updateAttributeLore(List<String> lore, RpgItemStack rpgItemStack){
        if(lore.size() == 0){
            return lore;
        }
        int startLine = -1;
        int endLine = -1;

        //Target Attribute Map Lore
        for(int i=0; i<lore.size(); i++){
            String line = ChatColor.stripColor(lore.get(i)).replace("%", "");
            if(line.contains(DisplayHelper.AttributeSeperator)){
                String[] args = line.split(Pattern.quote(DisplayHelper.AttributeSeperator));
                if(args.length >= 2){
                    String attributeName = args[0].trim();
                    RpgAttribute attribute = AttributeManager.getAttributeByName(attributeName);
                    if(attribute != null) {
                        if(startLine == -1)
                            startLine = i;
                    }
                }
            }

            //Look for blank line
            if(line.trim().length() == 0 && startLine != -1){
                endLine = i;
                break;
            }
        }

        if(startLine == -1){
            return lore;
        }
        if(endLine == -1){
            endLine = lore.size();
        }

        List<String> strippedLore = lore.subList(0, startLine);
        strippedLore.addAll(lore.subList(endLine, lore.size()));

        strippedLore.addAll(startLine, LoreHelper.makeAttributeLines(rpgItemStack.getAttributes(), rpgItemStack.getVisualBonus()));

        return strippedLore;
    }

    private static void buildGearsetLore(List<String> lore, GearSet gearSet, HashMap<GearSet, Integer> equippedGearSets){
        int gearSetLine = LoreHelper.getGearsetLine(lore);
        if(gearSetLine == -1)
            return;

        if(gearSet == null){
            String gearSetLoreLine = ChatColor.stripColor(lore.get(gearSetLine));
            String[] gearSetDisplayPieces = ChatColor.stripColor(DisplayHelper.Gearset).split(Pattern.quote("{gearset}"));
            for(String piece : gearSetDisplayPieces){
                gearSetLoreLine = gearSetLoreLine.replace(piece, "");
            }
            String gearSetName = gearSetLoreLine;
            gearSet = GearSetsManager.getGearSetByName(gearSetName);
        }
        if(gearSet != null){
            int i = gearSetLine+1;
            while(i < lore.size()){
                if(!lore.get(i).isEmpty()){
                    lore.remove(i);
                }
                if(i == lore.size() || lore.get(i).isEmpty())
                    break;
            }

            ArrayList<String> gearSetLore = new ArrayList<>();

            //Add Gear Set lines
            Integer equipped = equippedGearSets.getOrDefault(gearSet, 0);
            HashMap<Integer, GearSetBuff> gearSetBuffs = gearSet.getBuffs();
            for(Map.Entry<Integer, GearSetBuff> entry : gearSetBuffs.entrySet()){
                Integer required = entry.getKey();
                GearSetBuff gearSetBuff = entry.getValue();
                if(required <= equipped){
                    if(gearSetBuff.doesOverridePrevious()){
                        gearSetLore.clear();
                    }
                    //Active Lore
                    gearSetLore.add(gearSetBuff.getActiveLore());
                }
                else{
                    //InActive Lore
                    gearSetLore.add(gearSetBuff.getInactiveLore());
                }
            }

            for(String line : gearSetLore){
                if(i >= lore.size())
                    lore.add(line);
                else
                    lore.add(i, line);
                i++;
            }
        }
    }

    private static void buildSocketLore(ItemStack itemStack, List<String> lore, List<SocketGem> sockets){
        if(sockets.size() > 0){
            if(SocketManager.GetSocketDisplaySettings().SocketHeader.Enabled){
                lore.add(SocketManager.GetSocketDisplaySettings().SocketHeader.Text);
            }

            for(SocketGem socketGem : sockets){
                if(socketGem == null){
                    lore.add(SocketManager.GetSocketDisplaySettings().OpenSlot);
                }
                else {
                    String name = socketGem.getItemStack().getItemMeta().getDisplayName();
                    lore.add(SocketManager.GetSocketDisplaySettings().FilledSlot.replace("{GemName}", name));
                }
            }

            if(SocketManager.GetSocketDisplaySettings().SocketFooter.Enabled){
                lore.add(SocketManager.GetSocketDisplaySettings().SocketFooter.Text);
            }
        }
    }

    private static void storeSocketInfo(ItemStack itemStack, List<SocketGem> sockets){
        for(int i=0; i<sockets.size(); i++){
            SocketGem socketGem = sockets.get(i);
            if(socketGem != null){
                NmsUtils.addTag(RpgItemsPlugin.instance, itemStack, "Socket_" + i, socketGem.getId());
            } else {
                NmsUtils.removeTag(RpgItemsPlugin.instance, itemStack, "Socket_" + i);
            }
        }
    }

}
