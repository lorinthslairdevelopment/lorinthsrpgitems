package me.lorinth.rpgitems.util;

import me.lorinth.rpgitems.enums.ItemStatsView;
import me.lorinth.utils.FormulaMethod;

public class SettingsHelper {

    public static FormulaMethod formulaMethod = FormulaMethod.Java;
    public static ItemStatsView itemStatsView = ItemStatsView.UiMenu;
    public static boolean itemGlow = false;

}
