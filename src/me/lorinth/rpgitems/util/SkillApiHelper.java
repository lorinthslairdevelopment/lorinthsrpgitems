package me.lorinth.rpgitems.util;

import com.sucy.skill.SkillAPI;
import com.sucy.skill.api.enums.ManaSource;
import com.sucy.skill.api.player.PlayerData;
import org.bukkit.entity.Player;

public class SkillApiHelper {

    public static void addMana(Player player, int mana){
        PlayerData data = SkillAPI.getPlayerData(player);
        data.giveMana(mana, ManaSource.COMMAND);
    }

}
