package me.lorinth.rpgitems.util;

import me.lorinth.rpgitems.util.holograms.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class VersionManager {

    public static void setHoloTime(Player paramPlayer, String[] paramArrayOfString, Location paramLocation, int paramInt, double height)
    {
        Object localObject;
        if (v().equalsIgnoreCase("v1_14_R1"))
        {
            localObject = new PluginHologram_V1_14_R1(paramArrayOfString, paramLocation, height);
            ((PluginHologram_V1_14_R1)localObject).showPlayerTemp(paramPlayer, paramInt);
        }
        else if (v().equalsIgnoreCase("v1_15_R1"))
        {
            localObject = new PluginHologram_V1_15_R1(paramArrayOfString, paramLocation, height);
            ((PluginHologram_V1_15_R1)localObject).showPlayerTemp(paramPlayer, paramInt);
        }
    }

    private static String v()
    {
        String str1 = Bukkit.getServer().getClass().getPackage().getName();
        String str2 = str1.substring(str1.lastIndexOf('.') + 1);
        return str2;
    }
}
