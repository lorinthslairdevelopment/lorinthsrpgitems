package me.lorinth.rpgitems.util.holograms;

import me.lorinth.rpgitems.util.VersionManager;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class HologramManager
{
    public static void setHoloTime(Player paramPlayer, String[] paramArrayOfString, Location paramLocation, int paramInt, double height)
    {
        VersionManager.setHoloTime(paramPlayer, paramArrayOfString, paramLocation, paramInt, height);
    }

    public static void setGlobalHologram(String[] paramArrayOfString, Location location, int paramInt, double height, double radius){
        for(Player player : location.getWorld().getPlayers()){
            if(player.getLocation().distanceSquared(location) < (radius * radius))
                setHoloTime(player, paramArrayOfString, location, paramInt, height);
        }
    }
}

