package me.lorinth.rpgitems.util.holograms;

import me.lorinth.rpgitems.RpgItemsPlugin;
import net.minecraft.server.v1_15_R1.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_15_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class PluginHologram_V1_15_R1
{
    private List<EntityArmorStand> entitylist = new ArrayList();
    private String[] Text;
    private Location location;
    private double DISTANCE = 0.25D;
    private double HEIGHT;
    private int count;

    public PluginHologram_V1_15_R1(String[] paramArrayOfString, Location paramLocation, double height)
    {
        this.HEIGHT = height;
        this.Text = paramArrayOfString;
        this.location = paramLocation;
        create();
    }

    public void showPlayerTemp(final Player paramPlayer, int paramInt)
    {
        showPlayer(paramPlayer);
        new BukkitRunnable()
        {
            public void run()
            {
                hidePlayer(paramPlayer);
            }
        }.runTaskLaterAsynchronously(RpgItemsPlugin.instance, paramInt);
    }

    public void showPlayer(final Player paramPlayer)
    {
        new BukkitRunnable()
        {
            public void run()
            {
                for (EntityArmorStand localEntityArmorStand : entitylist)
                {
                    PacketPlayOutSpawnEntityLiving localPacketPlayOutSpawnEntityLiving = new PacketPlayOutSpawnEntityLiving(localEntityArmorStand);
                    PacketPlayOutEntityMetadata localEntityMetadata = new PacketPlayOutEntityMetadata(localEntityArmorStand.getId(), localEntityArmorStand.getDataWatcher(), true);
                    ((CraftPlayer)paramPlayer).getHandle().playerConnection.sendPacket(localPacketPlayOutSpawnEntityLiving);
                    ((CraftPlayer)paramPlayer).getHandle().playerConnection.sendPacket(localEntityMetadata);
                }
            }
        }.runTaskAsynchronously(RpgItemsPlugin.instance);
    }

    public void hidePlayer(final Player paramPlayer)
    {
        new BukkitRunnable()
        {
            public void run()
            {
                for (EntityArmorStand localEntityArmorStand : entitylist)
                {
                    PacketPlayOutEntityDestroy localPacketPlayOutEntityDestroy = new PacketPlayOutEntityDestroy(localEntityArmorStand.getId());
                    ((CraftPlayer)paramPlayer).getHandle().playerConnection.sendPacket(localPacketPlayOutEntityDestroy);
                }
            }
        }.runTaskAsynchronously(RpgItemsPlugin.instance);
    }

    private void create()
    {
        String[] arrayOfString;
        int k = (arrayOfString = this.Text).length;
        for (int j = 0; j < k; j++)
        {
            String str = arrayOfString[j];
            EntityArmorStand localEntityArmorStand = new EntityArmorStand(((CraftWorld)this.location.getWorld()).getHandle(), this.location.getX(), this.location.getY() + HEIGHT, this.location.getZ());
            localEntityArmorStand.setCustomName(new ChatComponentText(str));
            localEntityArmorStand.setCustomNameVisible(true);
            localEntityArmorStand.setInvisible(true);
            localEntityArmorStand.setNoGravity(true);
            this.entitylist.add(localEntityArmorStand);
            this.location.subtract(0.0D, this.DISTANCE, 0.0D);
            this.count += 1;
        }
        for (int i = 0; i < this.count; i++) {
            this.location.add(0.0D, this.DISTANCE, 0.0D);
        }
        this.count = 0;
    }
}
