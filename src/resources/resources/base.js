var RpgItemsOutputHandler = Java.type('me.lorinth.rpgitems.util.RpgItemsOutputHandler');
var FormulaHelper = Java.type('me.lorinth.rpgitems.util.FormulaHelper');
var EffectManager = Java.type('me.lorinth.rpgitems.managers.EffectManager');
var EffectInfo = Java.type('me.lorinth.rpgitems.objects.EffectInfo');
var EffectType = Java.type('me.lorinth.rpgitems.enums.EffectType');
var LastingEffectClass = Java.type('me.lorinth.rpgitems.tasks.LastingEffect');
var DamageEffectInfo = Java.type('me.lorinth.rpgitems.objects.DamageEffectInfo');
var DamageEffectType = Java.type('me.lorinth.rpgitems.enums.DamageEffectType');

//Selects a random integer between min and max (inclusive)
function range(min, max){
    return Math.floor(Math.random() * (max+1 - min)) + min;
}
// Selects a random number from 0 to max (exclusive)
function rand(max){
    return Math.floor(Math.random() * max);
}

//This function is used for combat events if FormulaMethod = Javascript in config.yml
function onCombat(combatEvent, eventStats, attacker, defender, isPvP){
	eventStats.totalDamage = eval(FormulaHelper.parseWithStats(FormulaHelper.BaseDamage, eventStats));
    if(isPvp){
        eventStats.pvpDamageResult = eval(FormulaHelper.parseWithStats(FormulaHelper.PvPDamage, eventStats));
        eventStats.pvpDefenseModifier = eval(FormulaHelper.parseWithStats(FormulaHelper.PvPDefenseModifier, eventStats));
        eventStats.totalDamage = eval(FormulaHelper.parseWithStats(FormulaHelper.TotalPvPDamage, eventStats));
    }
    else{
        eventStats.pveDamageResult = eval(FormulaHelper.parseWithStats(FormulaHelper.PvEDamage, eventStats));
        eventStats.pveDefenseModifier = eval(FormulaHelper.parseWithStats(FormulaHelper.PvEDefenseModifier, eventStats));
        eventStats.totalDamage = eval(FormulaHelper.parseWithStats(FormulaHelper.TotalPvEDamage, eventStats));
    }

	//Dodge
	if(eval(FormulaHelper.parseWithStats(FormulaHelper.DodgeChance, eventStats))){
		combatEvent.setCancelled(true);
		defender.setNoDamageTicks(14);

		var effectInfo = EffectManager.getEffectInfo(EffectType.DODGE);
		if(effectInfo != null)
			effectInfo.play(defender, attacker);
		return;
	}

	//Critical
	if(eval(FormulaHelper.parseWithStats(FormulaHelper.CriticalChance, eventStats))){
		eventStats.totalDamage = eval(FormulaHelper.parseWithStats(FormulaHelper.CriticalDamage, eventStats));

		var effectInfo = EffectManager.getEffectInfo(EffectType.CRITICAL);
		if(effectInfo != null)
			effectInfo.play(attacker, defender);
	}

	//Block
	if(eval(FormulaHelper.parseWithStats(FormulaHelper.BlockChance, eventStats))) {
		eventStats.totalDamage = eval(FormulaHelper.parseWithStats(FormulaHelper.BlockDamage, eventStats));

		var effectInfo = EffectManager.getEffectInfo(EffectType.BLOCK);
		if(effectInfo != null)
			effectInfo.play(defender, attacker);
	}

	//Burn
	if(eval(FormulaHelper.parseWithStats(FormulaHelper.BurnChance, eventStats))) {
		new LastingEffectClass(new DamageEffectInfo(defender, eventStats.burnDamage, DamageEffectType.BURNING, 5));

		var effectInfo = EffectManager.getEffectInfo(EffectType.BURN);
		if(effectInfo != null)
			effectInfo.play(attacker, defender);
	}

	//Poison
	if(eval(FormulaHelper.parseWithStats(FormulaHelper.PoisonChance, eventStats))) {
		new LastingEffectClass(new DamageEffectInfo(defender, eventStats.poisonDamage, DamageEffectType.POISON, 5));
		var effectInfo = EffectManager.getEffectInfo(EffectType.POISON);
		if(effectInfo != null)
			effectInfo.play(attacker, defender);
	}

	var vampAmount = eval(FormulaHelper.parseWithStats(FormulaHelper.VampirismAmount, eventStats));
	attacker.setHealth(Math.min(attacker.getMaxHealth(), attacker.getHealth() + vampAmount));
	combatEvent.setDamage(Math.max(1.0, eventStats.totalDamage));
}

RpgItemsOutputHandler.PrintInfo('Loaded base.js!');
// Create more functions to add your own custom logic into the formulas.yml!
// Example, we use rand(100) in formulas.yml